﻿/// <summary>
/// By SwanDEV 2017
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class MobileMediaTest : DImageDisplayHandler
{
	public CanvasScaler canvasScaler;

	public Image displayImage;
    public Toggle tog_Popup;

	public Text debugText;

	private string hints = "Mobile Media Plugin Demo\nTo test save/pick media to/from Native, please build Android, iOS app and test on device.\n\n";

	void Start()
	{
		debugText.text = hints;

		//Check screen orientation for setting canvas resolution
		if(Screen.width > Screen.height)
		{
			canvasScaler.referenceResolution = new Vector2(1920, 1080);
		}
		else
		{
			canvasScaler.referenceResolution = new Vector2(1080, 1920);
		}
	}

    /// <summary>
    /// Gets the photo thumbnail in the Album (iOS only). 
    /// </summary>
    public void GetPhotoThumbnail_iOS(DImageDisplayHandler imageHandler)
    {
        Image getImage = imageHandler.GetComponent<Image>();
        int fileType = 2; //Photo = 0, Video = 1, Photo & Video = 2 
        int fileIndex = 0;
        int targetSize = 0; //Use default size = 0

        MobileMedia.GetMediaPreviewPhoto_IOS((imagePath) =>
        {
            if (!string.IsNullOrEmpty(imagePath))
            {
                Debug.Log("Image Path: " + imagePath);
                debugText.text = hints + "Image Path: " + imagePath;

                Texture2D texture2D = new FilePathName().LoadImage(imagePath);
                if (texture2D)
                {
                    imageHandler.SetImage(getImage, texture2D); // Display image
                }
            }
            else
            {
                debugText.text = hints + "Path is empty or null.";
                Debug.Log("Path is empty or null.");
            }

        }, fileType, fileIndex, targetSize, "thumbnail_temp");
    }

	public void PickImage()
	{
		MobileMedia.PickImage((imagePath)=>{
			// Implement your code to load & use the image using the returned image path:
			if(!string.IsNullOrEmpty(imagePath))
			{
				//FileInfo fileInfo = new FileInfo(imagePath);
				byte[] imageBytes = File.ReadAllBytes(imagePath);
				Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, false); 
				texture2D.LoadImage(imageBytes);

				// Display image
				_ShowImage(_ToSprite(texture2D));

				Debug.Log("Image Path: " + imagePath);
				debugText.text = hints + "Image Path: " + imagePath;
			}
			else
			{
                debugText.text = hints + "Path is empty or null.";
				Debug.Log("Path is empty or null.");
			}
        }, "Pick an Image", "image/*", tog_Popup.isOn);
	}

	public void PickVideo()
	{
		MobileMedia.PickVideo((videoPath)=>{
			// Implement your code to load & play the video using the returned video path:
			if(!string.IsNullOrEmpty(videoPath))
			{
				Debug.Log("Video Path: " + videoPath);
				debugText.text = hints + "Video Path: " + videoPath;
			}
			else
			{
				Debug.Log("Path is empty or null.");
			}
        }, "Pick a Video", "video/*", tog_Popup.isOn);
	}

	public void SaveJPG()
	{
		TakeScreenshot((tex2D)=>{
			string savePath = MobileMedia.SaveImage(tex2D, "MobileMediaTest", new FilePathName().GetJpgFileName(), MobileMedia.ImageFormat.JPG);
			_ShowImage(_ToSprite(tex2D));

			debugText.text = hints + "Save Path: " + savePath;
			Debug.Log("Save Path: " + savePath);
		});
	}

	public void SavePNG()
	{
		TakeScreenshot((tex2D)=>{
			string savePath = MobileMedia.SaveImage(tex2D, "MobileMediaTest", new FilePathName().GetPngFileName(), MobileMedia.ImageFormat.PNG);
			_ShowImage(_ToSprite(tex2D));

			debugText.text = hints + "Save Path: " + savePath;
			Debug.Log("Save Path: " + savePath);
		});
	}

    /// <summary>
    /// To test save GIF file to Native. Put your GIF file in the Assets/StreamingAssets folder and rename it to SampleGIF.gif.
    /// </summary>
    public void SaveGIF()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, "SampleGIF.gif");

        if (!File.Exists(filePath))
        {
            Debug.LogWarning("To test save GIF file to Native. Put your GIF file in the Assets/StreamingAssets folder and rename it to SampleGIF.gif.");
            return;
        }

        string tmpText = hints + "(SaveGIF) Origin file path: " + filePath;
        debugText.text = tmpText;

        FilePathName fpn = new FilePathName();
        StartCoroutine(fpn.LoadFileUWR(filePath, (bytes) =>
        {
            string savePath = MobileMedia.SaveBytes(bytes, "MobileMediaTest", Path.GetFileNameWithoutExtension(filePath), Path.GetExtension(filePath).ToLower(), isImage: true);

            debugText.text += "\n\nSave path: " + savePath;
        }));

        //MobileMedia.CopyMedia(filePath, "MobileMediaTest", Path.GetFileNameWithoutExtension(filePath) + "_2", Path.GetExtension(filePath), isImage: true);
    }

    /// <summary>
    /// Convert texture(s) to GIF (Requires Pro GIF). 
    /// </summary>
    public void ConvertToGIF()
    {
        TakeScreenshot((tex2D) =>
        {
            string savePath = MobileMedia.SaveImage(tex2D, "MobileMediaTest", new FilePathName().GetGifFileName(), MobileMedia.ImageFormat.GIF);
            _ShowImage(_ToSprite(tex2D));

            debugText.text = hints + "Save Path: " + savePath;
            Debug.Log("Save Path: " + savePath);
        });
    }

    /// <summary>
    /// To test save MP4 file to Native. Put your MP4 file in the Assets/StreamingAssets folder and rename it to SampleMP4.mp4.
    /// </summary>
    public void SaveMP4()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, "SampleMP4.mp4");

        if(!File.Exists(filePath))
        {
            Debug.LogWarning("To test save MP4 file to Native. Put your MP4 file in the Assets/StreamingAssets folder and rename it to SampleMP4.mp4.");
            return;
        }

        string tmpText = hints + "(SaveMP4) Origin file path: " + filePath;
        debugText.text = tmpText;

        FilePathName fpn = new FilePathName();
        StartCoroutine(fpn.LoadFileUWR(filePath, (bytes) =>
        {
            string savePath = MobileMedia.SaveBytes(bytes, "MobileMediaTest", Path.GetFileNameWithoutExtension(filePath), Path.GetExtension(filePath).ToLower(), isImage: false);

            debugText.text += "\n\nSave path: " + savePath;
        }));


        //string directory = Application.streamingAssetsPath;
        //string getFilePath = "";
        //List<string> filePaths = new FilePathName().GetFilePaths(directory, new List<string> { ".mp4" });

        //// Debug text
        //string tmpText = hints + "MP4 Paths: ";
        //for (int i = 0; i < filePaths.Count; i++)
        //{
        //    tmpText = tmpText + "\n" + filePaths[i];
        //}
        //debugText.text = tmpText;
        //Debug.Log("MP4 Paths: " + tmpText);

        //if (filePaths != null && filePaths.Count > 0)
        //{
        //    getFilePath = filePaths[UnityEngine.Random.Range(0, filePaths.Count)];
        //    Debug.Log("Origin File Path: " + getFilePath + " | File Count: " + filePaths.Count);

        //    if (File.Exists(getFilePath))
        //    {
        //        //(1) Load as byte array to use in the scene and save to native gallery
        //        //byte[] bytes = new FilePathName().ReadFileToBytes(getFilePath);
        //        //string savePath2 = MobileMedia.SaveVideo(bytes, "MobileMediaTest", Path.GetFileNameWithoutExtension(getFilePath), Path.GetExtension(getFilePath).ToLower());

        //        //(2) Copy file to native gallery 
        //        string savePath = MobileMedia.CopyMedia(getFilePath, "MobileMediaTest", Path.GetFileNameWithoutExtension(getFilePath), Path.GetExtension(getFilePath), isImage: false);

        //        debugText.text += "\n\nSave Path: " + savePath;
        //        Debug.Log("Save Path: " + savePath);
        //    }
        //}
    }
        
	private void _ShowImage(Sprite sprite)
	{
		base.SetImage(displayImage, sprite);
	}


#region ----- Others -----
	public void MoreAssetsAndDocuments()
	{
		Application.OpenURL("https://www.swanob2.com/assets");
	}

	public void TakeScreenshot(Action<Texture2D> onComplete)
	{
		StartCoroutine(_TakeScreenshot(onComplete));
	}

	private IEnumerator _TakeScreenshot(Action<Texture2D> onComplete)
	{
		yield return new WaitForEndOfFrame();
		int width = Screen.width;
		int height = Screen.height;
		Texture2D readTex = new Texture2D(width, height, TextureFormat.RGB24, false);
		Rect rect = new Rect(0, 0, width, height);
		readTex.ReadPixels(rect, 0, 0);
		readTex.Apply();
		onComplete(readTex);
	}

	private Sprite _ToSprite(Texture2D texture)
	{
		if(texture == null) return null;

		Vector2 pivot = new Vector2(0.5f, 0.5f);
		float pixelPerUnit = 100;
		return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), pivot, pixelPerUnit);
	}
#endregion

}
