﻿/// <summary>
/// Created by SWAN DEV 2017
/// </summary>

using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pro GIF Texture Builder V1
/// </summary>
internal class ProGifTextureBuilderV1
{
    private bool _debugLog = false;

    public ProGifTextureBuilderV1(bool debugLog)
    {
        _debugLog = debugLog;
#if UNITY_EDITOR
        //Debug.Log("Pro GIF Texture Builder V1");
#endif
    }

    public Color32[] SetPixel(ProGifDecoder.GifData gifData, List<GifTexture> gifTexList, int decodeIndex, List<ushort> disposalMethodList,
     int imageIndex, byte[] decodedData, List<byte[]> colorTable, Color32? bgColor, int transparentIndex)
    {
        ProGifDecoder.ImageBlock imgBlock = gifData.m_imageBlockList[imageIndex];
        // Sort interlace GIF
        if (imgBlock.m_interlaceFlag)
        {
            decodedData = SortInterlace(decodedData, imgBlock.m_imageWidth);
        }

        int texWidth = gifData.m_logicalScreenWidth;
        int texHeight = gifData.m_logicalScreenHeight;

        bool filledPixels = false;
        bool useBeforeTex = false;

        int beforeIndex = -1;

        // Check dispose
        ushort lastDisposalMethod = decodeIndex > 0 ? disposalMethodList[decodeIndex - 1] : (ushort)2;
        if (decodeIndex > 0 && lastDisposalMethod == 0 || lastDisposalMethod == 1)
        {
            // before 1
            beforeIndex = decodeIndex - 1;
        }
        else if (lastDisposalMethod == 2)
        {
            filledPixels = true;
        }
        else if (decodeIndex > 1 && lastDisposalMethod == 3)
        {
            // Restore to the previous block
            for (int i = decodeIndex - 1; i >= 0; i--)
            {
                if (disposalMethodList[i] == 0 || disposalMethodList[i] == 1)
                {
                    beforeIndex = i;
                    break;
                }
            }
        }

        if (_debugLog)
        {
            ProGifDecoder.ImageBlock ib = gifData.m_imageBlockList[imageIndex];
            Debug.Log(" lastDisposalMethod: " + lastDisposalMethod + " W: " + ib.m_imageWidth + " H: " + ib.m_imageHeight
                + " T: " + ib.m_imageTopPosition + " L: " + ib.m_imageLeftPosition
                + " Logic W: " + gifData.m_logicalScreenWidth + " Logic H: " + gifData.m_logicalScreenHeight
                + " | DecoddeData: " + decodedData.Length);
        }

        Color32[] pix;
        if (beforeIndex >= 0)
        {
            // Do not dispose
            pix = (Color32[])gifTexList[beforeIndex].m_Colors.Clone();
            useBeforeTex = true;
            filledPixels = true;
        }
        else
        {
            pix = new Color32[texWidth * texHeight];
        }

        // Set pixel data: Reverse set pixels. because GIF data starts from the top left.
        int dataIndex = 0;
        for (int y = texHeight - 1; y >= 0; y--)
        {
            SetPixelRow(ref pix, texWidth, texHeight, y, gifData.m_imageBlockList[imageIndex], decodedData,
                ref dataIndex, colorTable, bgColor, transparentIndex, useBeforeTex, filledPixels);
        }

        return pix;
    }

    /// <summary>
    /// Set texture pixel row
    /// </summary>
    public void SetPixelRow(ref Color32[] pixels, int texWidth, int texHeight, int y, ProGifDecoder.ImageBlock imgBlock, byte[] decodedData,
        ref int dataIndex, List<byte[]> colorTable, Color32? bgColor, int transparentIndex, bool useBeforeTex, bool filledPixels)
    {
        // Row no (0~)
        int row = texHeight - 1 - y;
        int startX = imgBlock.m_imageLeftPosition;
        int startY = imgBlock.m_imageTopPosition;
        int endX = imgBlock.m_imageLeftPosition + imgBlock.m_imageWidth;
        int endY = imgBlock.m_imageTopPosition + imgBlock.m_imageHeight;

        Color32 c0 = new Color32(0, 0, 0, 0);

        for (int x = 0; x < texWidth; x++)
        {
            // Line no (0~)
            int line = x;

            // Out of image blocks
            if (row < startY || row >= endY || line < startX || line >= endX)
            {
                if (!useBeforeTex && !filledPixels)
                {
                    pixels[x + y * texWidth] = transparentIndex < 0 ? bgColor.Value : c0;
                }
                continue;
            }

            // Out of decoded data
            if (dataIndex >= decodedData.Length)
            {
                if (!filledPixels)
                {
                    pixels[x + y * texWidth] = Color.black;
                }
                continue;
            }

            // Get pixel color from color table
            byte colorIndex = decodedData[dataIndex];
            if (colorTable == null || colorTable.Count <= colorIndex)
            {
                if (!filledPixels)
                {
                    pixels[x + y * texWidth] = bgColor.Value;
                }
                continue;
            }

            // Set pixels
            byte[] rgb = colorTable[colorIndex];
            byte alpha = (byte)(transparentIndex >= 0 && transparentIndex == colorIndex ? 0 : 255);
            if (!filledPixels || alpha != 0 || !useBeforeTex)
            {
                pixels[x + y * texWidth] = new Color32(rgb[0], rgb[1], rgb[2], alpha);
            }

            dataIndex++;
        }
    }

    /// <summary>
    /// Sort interlace GIF data
    /// </summary>
    /// <param name="decodedData">Decoded GIF data</param>
    /// <param name="xNum">Pixel number of horizontal row</param>
    /// <returns>Sorted data</returns>
    public byte[] SortInterlace(byte[] decodedData, int xNum)
    {
        int rowNo = 0;
        int dataIndex = 0;
        byte[] newArr = new byte[decodedData.Length];
        // Every 8th. row, starting with row 0.
        for (int i = 0; i < newArr.Length; i++)
        {
            if (rowNo % 8 == 0)
            {
                newArr[i] = decodedData[dataIndex];
                dataIndex++;
            }
            if (i != 0 && i % xNum == 0)
            {
                rowNo++;
            }
        }
        rowNo = 0;
        // Every 8th. row, starting with row 4.
        for (int i = 0; i < newArr.Length; i++)
        {
            if (rowNo % 8 == 4)
            {
                newArr[i] = decodedData[dataIndex];
                dataIndex++;
            }
            if (i != 0 && i % xNum == 0)
            {
                rowNo++;
            }
        }
        rowNo = 0;
        // Every 4th. row, starting with row 2.
        for (int i = 0; i < newArr.Length; i++)
        {
            if (rowNo % 4 == 2)
            {
                newArr[i] = decodedData[dataIndex];
                dataIndex++;
            }
            if (i != 0 && i % xNum == 0)
            {
                rowNo++;
            }
        }
        rowNo = 0;
        // Every 2nd. row, starting with row 1.
        for (int i = 0; i < newArr.Length; i++)
        {
            if (rowNo % 8 != 0 && rowNo % 8 != 4 && rowNo % 4 != 2)
            {
                newArr[i] = decodedData[dataIndex];
                dataIndex++;
            }
            if (i != 0 && i % xNum == 0)
            {
                rowNo++;
            }
        }
        return newArr;
    }
}
