﻿/// <summary>
/// Created by SWAN DEV 2019
/// </summary>

using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pro GIF Texture Builder V2
/// </summary>
internal class ProGifTextureBuilderV2
{
    private bool _debugLog = false;

    public ProGifTextureBuilderV2(bool debugLog)
    {
        _debugLog = debugLog;
#if UNITY_EDITOR
        //Debug.Log("Pro GIF Texture Builder V2");
#endif
    }

    // last rect x, y, w, h
    private ushort lrx;
    private ushort lry;
    private ushort lrw;
    private ushort lrh;

    private Color32[] lastImageColors = null;

    /// <summary>
    /// Sets pixels to texture/colors.
    /// (*** Interlace Sorting Included)
    /// </summary>
    public Color32[] SetPixel(ProGifDecoder.GifData gifData, List<GifTexture> gifTexList, int decodeIndex, List<ushort> disposalMethodList,
        int imageIndex, byte[] decodedData, List<byte[]> colorTable, Color32? bgColor, int transparentIndex)
    {
        int width = gifData.m_logicalScreenWidth;
        int height = gifData.m_logicalScreenHeight;
        int pixelNum = width * height;

        // current rect x, y, w, h
        ushort ix = gifData.m_imageBlockList[imageIndex].m_imageLeftPosition;
        ushort iy = gifData.m_imageBlockList[imageIndex].m_imageTopPosition;
        ushort iw = gifData.m_imageBlockList[imageIndex].m_imageWidth;
        ushort ih = gifData.m_imageBlockList[imageIndex].m_imageHeight;

        Color32[] dest = new Color32[pixelNum];
        bool transparency = transparentIndex >= 0;
        bool interlace = gifData.m_imageBlockList[imageIndex].m_interlaceFlag;
        int frameCount = gifData.m_imageBlockList.Count;

        ushort lastDispose = decodeIndex > 0 ? disposalMethodList[decodeIndex - 1] : (ushort)2;
     
        try
        {
            // Fill in starting image contents base on last image's disposal method
            if (lastDispose >= 0)
            {
                // Restore to the previous block
                if (decodeIndex > 1 && lastDispose == 3)
                {
                    bool filled = false;
                    for (int i = decodeIndex - 1; i >= 0; i--)
                    {
                        if (disposalMethodList[i] == 0 || disposalMethodList[i] == 1)
                        {
                            lastImageColors = (Color32[])gifTexList[i].m_Colors.Clone();
                            filled = true;
                            break;
                        }
                    }
                    if (!filled) lastImageColors = null;
                }

                if (lastImageColors != null)
                {
                    Array.Copy(lastImageColors, dest, pixelNum);
                }

                // Fill last image rect with background color, or set as transparent
                if (lastDispose == 2)
                {
                    Color32 color = new Color32(0, 0, 0, 0);
                    if (!transparency)
                    {
                        color = bgColor.Value;
                    }
                    for (int i = 0; i < lrh; i++)
                    {
                        int startX = (height - 1 - lry - i) * width + lrx;
                        //int startX = (lry + i) * width + lrx; // flip horizontal
                        int endX = startX + lrw;
                        for (int k = startX; k < endX; k++)
                        {
                            if (k < dest.Length) dest[k] = color;
#if UNITY_EDITOR
                            //else Debug.LogWarning("Out of Range!!  k:" + k + " i: " + i + " dest.Len: " + dest.Length + " lrx: " + lrx + " lry: " + lry + " lrw: " + lrw + " lrh: " + lrh + " n1: " + startX + " n2: " + endX);
#endif
                        }
                    }
                }
            }

#if UNITY_EDITOR
            if (_debugLog) Debug.Log("lastDispose: " + lastDispose + " bgColor: " + bgColor.Value.ToString() + " width: " + width + " height: " + height + " ix: " + ix + " iy: " + iy + " iw: " + iw + " ih: " + ih);
#endif

            // Copy each source line to the appropriate place in the destination
            int pass = 1;
            int inc = 8;
            int iline = 0;
            int dataIndex = 0;
            for (int i = 0; i < ih; i++)
            {
                int line = i;
                if (interlace)
                {
                    if (iline >= ih)
                    {
                        pass++;
                        switch (pass)
                        {
                            case 2:
                                iline = 4;
                                break;
                            case 3:
                                iline = 2;
                                inc = 4;
                                break;
                            case 4:
                                iline = 1;
                                inc = 2;
                                break;
                            default:
                                break;
                        }
                    }
                    line = iline;
                    iline += inc;
                }

                line = height - 1 - iy - line;
                //line += iy;   // flip horizontal

                if (line < height)
                {
                    int k = line * width;
                    int dx = k + ix;                            // start of dest line
                    int dlim = dx + iw;                         // end of dest line
                    if ((k + width) < dlim) dlim = k + width;   // past dest edge

                    while (dx < dlim)
                    {
                        byte colorIndex = decodedData[dataIndex++];

                        // Get color from table and set to dest
                        byte[] rgb = colorTable[colorIndex];
                        if(colorIndex != transparentIndex) dest[dx] = new Color32(rgb[0], rgb[1], rgb[2], 255);

                        dx++;
                    }
                }
            }

            lry = iy;
            lrx = ix;
            lrw = iw;
            lrh = ih;
            lastImageColors = dest;
        }
        catch (Exception e)
        {
            Debug.LogWarning("SetPixel error: " + e.Message);
        }

        return dest;
    }
}
