﻿using UnityEngine;

namespace Masks.BUTTERFLY
{
    [RequireComponent(typeof(Animator))]
    public class ButterflyController : MonoBehaviour
    {
        #region > Editor Fields

        [SerializeField]
        private Transform moveToPoint;
        [SerializeField]
        private float speed = 10f;
        [SerializeField]
        private float speedAfterFinish = 10f;
        [SerializeField]
        private float rotationSpeed = 1f;

        #endregion > Editor Fields
        
        #region > Private Fields

        private bool isFinished = false;
        private float initialSpeed;
        
        private const float Epsilon = 0.013f;
        
        #endregion > Private Fields

        #region > Properties
        
        private Animator animator = null;
        private Animator Animator
        {
            get
            {
                if (animator == null)
                    animator = GetComponent<Animator>();

                return animator;
            }
        }
        
        private Rigidbody rigidbody = null;
        private Rigidbody Rigidbody
        {
            get
            {
                if (rigidbody == null)
                    rigidbody = GetComponent<Rigidbody>();

                return rigidbody;
            }
        }
        
        #endregion > Properties

        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            transform.localPosition = new Vector3(0f, Random.Range(-0.3f, 0.3f), 0f);
            initialSpeed = speed;
        }
        
        private void Start()
        {
            initialSpeed = speed;
            transform.rotation = moveToPoint.rotation;
        }
        
        #endregion > MonoBehaviour Callbacks
        
        private void Update()
        {
            Rigidbody.velocity = Vector3.zero;
            Rigidbody.angularVelocity = Vector3.zero;
            
            if (!isFinished)
            {
                var nor = (moveToPoint.position - transform.position).normalized;
                transform.localPosition = transform.localPosition + nor * speed * Time.deltaTime;
                transform.rotation = Quaternion.Lerp(transform.rotation, moveToPoint.rotation, rotationSpeed * Time.deltaTime);

                if ((transform.position - moveToPoint.position).magnitude < Epsilon)
                    SetIsFinished(true);
            }
            else
            {
                if ((transform.position - moveToPoint.position).magnitude < Epsilon)
                {
                    transform.position = moveToPoint.position;
                    transform.rotation = moveToPoint.rotation;
                }
                else
                    SetIsFinished(false);
            }
        }

        private void SetIsFinished(bool isFinished)
        {
            speed = speedAfterFinish;
            Animator.SetBool("Finished", isFinished);
            this.isFinished = isFinished;
        }
    }
}