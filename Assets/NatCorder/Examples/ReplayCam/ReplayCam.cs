/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

using FaceTracker;
using UnityEngine.Scripting;
using UnityEngine.UI;

namespace NatCorder.Examples {

    #if UNITY_EDITOR
	using UnityEditor;
	#endif
    using UnityEngine;
    using Clocks;
    using Inputs;
    

    public class ReplayCam : MonoBehaviour {

        /**
        * ReplayCam Example
        * -----------------
        * This example records the screen using a `CameraRecorder`.
        * When we want mic audio, we play the mic to an AudioSource and record the audio source using an `AudioRecorder`
        * -----------------
        * Note that UI canvases in Overlay mode cannot be recorded, so we use a different mode (this is a Unity issue)
        */
        [Header("Recording")]
        public int videoWidth = 512;
        public int videoHeight = 512;
        public Camera[] recordingCameras;

        [Header("Audio")] public AudioMode audioMode;
        public AudioSource audioSource;

        [Header("Use Only for masks recording")]
        public bool isTrackingRotationUsed = false;


        private MP4Recorder videoRecorder;
        private IClock recordingClock;
        private CameraInput cameraInput;
        private AudioInput audioInput;
        private VideoRecordingController videoRecordingController;
        public void StartRecording (VideoRecordingController videoRecordingController) {
            // Start recording
           // recordingCamera.rect = new Rect(0,0.3f,1,0.5f);
           // recordingCamera.fieldOfView = 26;
           this.videoRecordingController = videoRecordingController;
            recordingClock = new RealtimeClock();
            
            videoRecorder = new MP4Recorder(
                videoWidth,
                videoHeight,
                30,
                audioMode != AudioMode.None ? AudioSettings.outputSampleRate : 0,
                audioMode != AudioMode.None ? (int) AudioSettings.speakerMode : 0,

//                recordMicrophone ? AudioSettings.outputSampleRate : 0,
//                recordMicrophone ? (int)AudioSettings.speakerMode : 0,
                OnReplay
            );
            
            // Create recording inputs
            cameraInput = new CameraInput(videoRecorder, recordingClock,videoRecordingController.Offset,videoRecordingController.Scale, recordingCameras);
//            if (recordMicrophone) {
//                StartMicrophone();
//                audioInput = new AudioInput(videoRecorder, recordingClock, microphoneSource, true);
//            }
            switch (audioMode)
            {
                case AudioMode.Microphone:
                {
                    StartMicrophone();
                    audioInput = new AudioInput(videoRecorder, recordingClock, audioSource,true);
                    break;
                }
                case AudioMode.AudioFromApp:
                {
                    audioInput = new AudioInput(videoRecorder, recordingClock, audioSource);
                    break;
                }
            }

        }

        public void SetNewResolution(int width , int height)
        {
            videoRecorder = new MP4Recorder(
                width,
                height,
                30,
                audioMode != AudioMode.None ? AudioSettings.outputSampleRate : 0,
                audioMode != AudioMode.None ? (int) AudioSettings.speakerMode : 0,

//                recordMicrophone ? AudioSettings.outputSampleRate : 0,
//                recordMicrophone ? (int)AudioSettings.speakerMode : 0,
                OnReplay
            );
        }
        private void StartMicrophone()
        {
            if (audioMode != AudioMode.Microphone) return;
#if !UNITY_WEBGL || UNITY_EDITOR // No `Microphone` API on WebGL :(
            // Create a microphone clip
//            Microphone.devices[0].
//            audioSource.clip = Microphone.Start(null, true, 60, 48000);
            audioSource.clip = Microphone.Start(null, true, 30, 44100);
            while (Microphone.GetPosition(null) <= 0);
            // Play through audio source
            audioSource.timeSamples = Microphone.GetPosition(Microphone.devices[0]);
            audioSource.loop = true;
            audioSource.Play();
#endif
        }

//        private void StartMicrophone () {
//            #if !UNITY_WEBGL || UNITY_EDITOR // No `Microphone` API on WebGL :(
//            // Create a microphone clip
//            microphoneSource.clip = Microphone.Start(null, true, 60, 48000);
//            while (Microphone.GetPosition(null) <= 0) ;
//            // Play through audio source
//            microphoneSource.timeSamples = Microphone.GetPosition(null);
//            microphoneSource.loop = true;
//            microphoneSource.Play();
//            #endif
//        }

//        public void StopRecording () {
//            // Stop the recording inputs
//            if (recordMicrophone) {
//                StopMicrophone();
//                audioInput.Dispose();
//            }
//            cameraInput.Dispose();
//            // Stop recording
//            videoRecorder.Dispose();
//            
//        }
        public void StopRecording()
        {
            // Stop the recording inputs
            if (audioMode != AudioMode.None)
            {
                if (audioMode == AudioMode.Microphone)
                {
                    StopMicrophone();
                }

                audioInput.Dispose();
            }

            cameraInput.Dispose();
            // Stop recording
            videoRecorder.Dispose();
        }

        public void ClearRecorder()
        {
            videoRecorder = null;
            
        }

//        private void StopMicrophone () {
//            #if !UNITY_WEBGL || UNITY_EDITOR
//            Microphone.End(null);
//            microphoneSource.Stop();
//            #endif
//        }
        private void StopMicrophone()
        {
            if (audioMode == AudioMode.None) return;
#if !UNITY_WEBGL || UNITY_EDITOR
            if(audioMode == AudioMode.Microphone)
                Microphone.End(null);
            audioSource.Stop();
#endif
        }

        private void OnReplay (string path) {
            
            Debug.Log("Saved recording to: "+path);
            videoRecordingController.OnVideoWasExported?.Invoke(path);
//            // Playback the video
//            #if UNITY_EDITOR
//			EditorUtility.OpenWithDefaultApp(path);
//            #elif UNITY_IOS
//            Handheld.PlayFullScreenMovie("file://" + path);
//            #elif UNITY_ANDROID
//            Handheld.PlayFullScreenMovie(path);
//            #endif
        }
    }
    public enum AudioMode
    {
        None,
        Microphone,
        AudioFromApp
    }

}