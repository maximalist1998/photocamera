/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

namespace NatCorder.Clocks {

    using UnityEngine;
    using Internal;
    using Stopwatch = System.Diagnostics.Stopwatch;

    /// <summary>
    /// Realtime clock for generating timestamps
    /// </summary>
    [Doc(@"RealtimeClock")]
    public struct RealtimeClock : IClock {

        /// <summary>
        /// Current timestamp in nanoseconds.
        /// The very first value reported by this property will always be zero.
        /// </summary>
        [Doc(@"Timestamp")]
        public long Timestamp {
            get { return (stopwatch = stopwatch ?? Stopwatch.StartNew()).Elapsed.Ticks * 100L; }
        }

        /// <summary>
        /// Is the clock paused?
        /// </summary>
        [Doc(@"RealtimeClockPaused")]
        public bool Paused {
            get { return stopwatch != null && !stopwatch.IsRunning; }
            set { if (stopwatch != null) if (value) stopwatch.Stop(); else stopwatch.Start(); }
        }

        private Stopwatch stopwatch;
    }
}