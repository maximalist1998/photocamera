/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

namespace NatCorder.Internal {

    using AOT;
    using System;
    using System.Runtime.InteropServices;

    public sealed class MediaRecorderiOS : IMediaRecorder {
        
        #region --IMediaRecorder--

        public int pixelWidth { get; private set; }

        public int pixelHeight { get; private set; }

        public MediaRecorderiOS (IntPtr recorder, int width, int height, string recordingPath, Action<string> callback) {
            this.recorder = recorder;
            this.pixelWidth = width;
            this.pixelHeight = height;
            this.callback = callback;
            // Start recording
            recorder.StartRecording(recordingPath, OnRecording, (IntPtr)GCHandle.Alloc(this, GCHandleType.Normal));
        }

        public void Dispose () {
            recorder.StopRecording();
        }

        public void CommitFrame<T> (T[] pixelBuffer, long timestamp) where T : struct {
            var bufferHandle = GCHandle.Alloc(pixelBuffer, GCHandleType.Pinned);
            recorder.EncodeFrame(bufferHandle.AddrOfPinnedObject(), timestamp);
            bufferHandle.Free();
        }

        public void CommitSamples (float[] sampleBuffer, long timestamp) {
            recorder.EncodeSamples(sampleBuffer, sampleBuffer.Length, timestamp);
        }
        #endregion


        #region --Operations--

        private readonly IntPtr recorder;
        private readonly Action<string> callback;

        [MonoPInvokeCallback(typeof(Action<IntPtr, IntPtr>))]
        private static void OnRecording (IntPtr context, IntPtr path) {
            var pathStr = Marshal.PtrToStringAuto(path);
            var instanceHandle = (GCHandle)context;
            var instance = instanceHandle.Target as MediaRecorderiOS;
            instanceHandle.Free();
            Dispatcher.Dispatch(0, () => instance.callback(pathStr));
        }
        #endregion
    }
}