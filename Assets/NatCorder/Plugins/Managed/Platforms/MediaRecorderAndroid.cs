/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

namespace NatCorder.Internal {

    using UnityEngine;
    using UnityEngine.Scripting;
    using System;
    using System.Runtime.InteropServices;

    public sealed class MediaRecorderAndroid : AndroidJavaProxy, IMediaRecorder {
        
        #region --IMediaRecorder--

        public int pixelWidth { get; private set; }

        public int pixelHeight { get; private set; }

        public MediaRecorderAndroid (AndroidJavaObject recorder, int width, int height, string recordingPath, Action<string> callback) : base(@"com.yusufolokoba.natcorder.MediaRecorder$Callback") {
            this.recorder = recorder;
            this.pixelWidth = width;
            this.pixelHeight = height;
            this.callback = callback;
            using (var ByteBuffer = new AndroidJavaClass("java.nio.ByteBuffer"))
                using (var ByteOrder = new AndroidJavaClass("java.nio.ByteOrder"))
                    using (var nativeOrder = ByteOrder.CallStatic<AndroidJavaObject>("nativeOrder"))
                        using (var pixelBuffer = ByteBuffer.CallStatic<AndroidJavaObject>("allocateDirect", width * height * 4))
                            this.pixelBuffer = pixelBuffer.Call<AndroidJavaObject>("order", nativeOrder);
            // Start recording
            Dispatcher.Dispatch(
                Dispatcher.Target.RenderThread,
                () => {
                    AndroidJNI.AttachCurrentThread();
                    recorder.Call(@"startRecording", recordingPath, this);
                }
            );
        }

        public void Dispose () { // INCOMPLETE // CHECK // Start recording timing
            recorder.Call(@"stopRecording");
            recorder.Dispose();
            pixelBuffer.Dispose();
        }
        
        public void CommitFrame<T> (T[] pixelBuffer, long timestamp) where T : struct { // INCOMPLETE // CHECK // Start recording timing
            var bufferHandle = GCHandle.Alloc(pixelBuffer, GCHandleType.Pinned);
            var dstAddress = (IntPtr)this.pixelBuffer.Call<long>("address");
            AsyncGPUReadbackES.memcpy(dstAddress, bufferHandle.AddrOfPinnedObject(), (UIntPtr)(pixelWidth * pixelHeight * 4));
            using (var buffer = this.pixelBuffer.Call<AndroidJavaObject>("clear"))
                recorder.Call(@"encodeFrame", buffer, timestamp);
            bufferHandle.Free();
        }

        public void CommitSamples (float[] sampleBuffer, long timestamp) {
            AndroidJNI.AttachCurrentThread();
            recorder.Call(@"encodeSamples", sampleBuffer, timestamp);
        }
        #endregion


        #region --Operations--

        private readonly AndroidJavaObject recorder;
        private readonly Action<string> callback;
        private readonly AndroidJavaObject pixelBuffer;

        [Preserve]
        private void onRecording (string path) {
            callback(path);
        }
        #endregion
    }
}