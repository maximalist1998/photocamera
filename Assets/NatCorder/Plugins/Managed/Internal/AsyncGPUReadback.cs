/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

namespace NatCorder.Internal {

    using AOT;
    using UnityEngine;
    using UnityEngine.Rendering;
    using UnityEngine.Scripting;
    using System;
    using System.Runtime.InteropServices;

    public static class AsyncGPUReadbackES {

        public static void Request (Action<AsyncGPUReadbackRequestES> completionHandler) {
            IReadbackRequest request = new AsyncGPUReadbackRequestES(completionHandler);
            Dispatcher.Dispatch(Dispatcher.Target.RenderThread, request.Request);
            GL.InvalidateState();
        }

        public static void Request (Texture texture, Action<AsyncGPUReadbackRequestES> completionHandler) {
            IReadbackRequest request = new AsyncGPUReadbackRequestES(texture.GetNativeTexturePtr(), texture.width, texture.height, completionHandler);
            Dispatcher.Dispatch(Dispatcher.Target.RenderThread, request.Request);
            GL.InvalidateState();
        }

        public static void RequestAsyncReadback (this CommandBuffer commandBuffer, Texture texture, Action<AsyncGPUReadbackRequestES> completionHandler) {
            // Ensure RenderTexture has been created
            if (texture is RenderTexture) {
                var _ = (texture as RenderTexture).colorBuffer;
            }
            // Issue request
            var request = new AsyncGPUReadbackRequestES(texture.GetNativeTexturePtr(), texture.width, texture.height, completionHandler);
            commandBuffer.IssuePluginEventAndData(commitFrameAndroidHandle, 0, (IntPtr)GCHandle.Alloc(request, GCHandleType.Normal));
        }

        private interface IReadbackRequest {
            void Request ();
        }

        public class AsyncGPUReadbackRequestES : AndroidJavaProxy, IReadbackRequest {

            private static AndroidJavaClass AsyncGPUReadbackClass;
            private readonly int textureID, width, height;
            private readonly Action<AsyncGPUReadbackRequestES> handler;
            private AndroidJavaObject nativeBuffer;

            public AsyncGPUReadbackRequestES (Action<AsyncGPUReadbackRequestES> handler) : this(IntPtr.Zero, 0, 0, handler) {}

            public AsyncGPUReadbackRequestES (IntPtr texturePtr, int width, int height, Action<AsyncGPUReadbackRequestES> handler) : base(@"com.yusufolokoba.natrender.AsyncGPUReadback$Callback") {
                AsyncGPUReadbackClass = AsyncGPUReadbackClass ?? new AndroidJavaClass(@"com.yusufolokoba.natrender.AsyncGPUReadback");
                this.textureID = texturePtr.ToInt32();
                this.width = width;
                this.height = height;
                this.handler = handler;
            }

            public void CopyTo (byte[] pixelBuffer) {
                var srcAddress = (IntPtr)nativeBuffer.Call<long>("address");
                Marshal.Copy(srcAddress, pixelBuffer, 0, pixelBuffer.Length);
            }

            void IReadbackRequest.Request () {
                AsyncGPUReadbackClass.CallStatic(@"request", textureID, width, height, this);
            }

            [Preserve]
            private void onReadback (AndroidJavaObject nativeBuffer) {
                this.nativeBuffer = nativeBuffer;
                handler(this);
                this.nativeBuffer = null;
                nativeBuffer.Dispose();
            }
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void UnityRenderingEvent (int eventID, IntPtr context);
        private static readonly IntPtr commitFrameAndroidHandle;

        static AsyncGPUReadbackES () {
            commitFrameAndroidHandle = Marshal.GetFunctionPointerForDelegate((UnityRenderingEvent)CommitFrameAndroid);
        }

        [MonoPInvokeCallback(typeof(UnityRenderingEvent))]
        private static void CommitFrameAndroid (int eventID, IntPtr context) {
            var handle = (GCHandle)context;
            var readback = handle.Target as IReadbackRequest;
            readback.Request();
        }

        #if UNITY_ANDROID && !UNITY_EDITOR
        [DllImport(@"c")]
        public static extern IntPtr memcpy (IntPtr dst, IntPtr src, UIntPtr size);
        #else
        public static IntPtr memcpy (IntPtr dst, IntPtr src, UIntPtr size) { return IntPtr.Zero; }
        #endif
    }
}