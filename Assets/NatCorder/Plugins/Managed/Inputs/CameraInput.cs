/* 
/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

namespace NatCorder.Inputs
{
    using UnityEngine;
    using UnityEngine.Rendering;
    using System;
    using System.Collections;
    using Clocks;
    using Internal;

    /// <summary>
    /// Recorder input for recording one or more game cameras
    /// </summary>
    [Doc(@"CameraInput")]
    public sealed class CameraInput : IDisposable
    {
        #region --Client API--

        /// <summary>
        /// Control number of successive camera frames to skip while recording.
        /// This is very useful for GIF recording, which typically has a lower framerate appearance
        /// </summary>
        [Doc(@"CameraInputFrameSkip", @"CameraInputFrameSkipDiscussion")]
        public int frameSkip;

        /// <summary>
        /// Create a video recording input from a game camera
        /// </summary>
        /// <param name="mediaRecorder">Media recorder to receive committed frames</param>
        /// <param name="clock">Clock for generating timestamps</param>
        /// <param name="cameras">Game cameras to record</param>
        private Vector2 offset, scale;

        public CameraInput(IMediaRecorder mediaRecorder, IClock clock, Vector2 offset, Vector2 scale,
            params Camera[] cameras)
        {
            // Save state
            this.offset = offset;
            this.scale = scale;
            this.mediaRecorder = mediaRecorder;
            this.clock = clock;
            this.cameras = cameras;
            this.framebuffer = RenderTexture.GetTemporary(cameras[0].pixelWidth, cameras[0].pixelHeight, 24);
            this.framebufferDest = RenderTexture.GetTemporary(mediaRecorder.pixelWidth, mediaRecorder.pixelHeight, 24);
            this.frameHelper = cameras[0].gameObject.AddComponent<CameraInputAttachment>();
            this.pixelBuffer = new byte[mediaRecorder.pixelWidth * mediaRecorder.pixelHeight * 4];
            // Start recording
            frameHelper.StartCoroutine(OnFrame());
        }

        /// <summary>
        /// Stop recorder input and teardown resources
        /// </summary>
        [Doc(@"CameraInputDispose")]
        public void Dispose()
        {
            CameraInputAttachment.Destroy(frameHelper);
            RenderTexture.ReleaseTemporary(framebuffer);
            RenderTexture.ReleaseTemporary(framebufferDest);
            Texture2D.Destroy(readbackBuffer);
            mediaRecorder = null;
        }

        #endregion


        #region --Operations--

        private IMediaRecorder mediaRecorder;
        private readonly IClock clock;
        private readonly Camera[] cameras;
        private readonly RenderTexture framebuffer;
        private readonly RenderTexture framebufferDest;
        private readonly CameraInputAttachment frameHelper;
        private readonly byte[] pixelBuffer;
        private Texture2D readbackBuffer;
        private int frameCount;

        private IEnumerator OnFrame()
        {
            var yielder = new WaitForEndOfFrame();
            for (;;)
            {
                // Check frame index
                yield return yielder;
                var recordFrame = frameCount++ % (frameSkip + 1) == 0;
                if (recordFrame)
                {
                    // Render every camera
                    for (var i = 0; i < cameras.Length; i++)
                    {
                        var prevTarget = cameras[i].targetTexture;
                        cameras[i].targetTexture = framebuffer;
                        cameras[i].Render();
                        cameras[i].targetTexture = prevTarget;
                    }
                    Graphics.Blit(framebuffer, framebufferDest, offset:
                        offset,
                        scale: scale);

                    // Commit frame
                    CommitFrame(framebufferDest);
                }
            }
        }

        private void CommitFrame(RenderTexture framebuffer)
        {
            // Use async readback if available
            if (SystemInfo.supportsAsyncGPUReadback)
                AsyncGPUReadback.Request(framebuffer, 0, request =>
                {
                    if (mediaRecorder != null)
                    {
                        request.GetData<byte>().CopyTo(pixelBuffer);
                        mediaRecorder.CommitFrame(pixelBuffer, clock.Timestamp);
                    }
                });
            // Use our own async readback on Android
            else if (Application.platform == RuntimePlatform.Android)
                AsyncGPUReadbackES.Request(framebuffer, request =>
                {
                    if (mediaRecorder != null)
                    {
                        request.CopyTo(pixelBuffer);
                        mediaRecorder.CommitFrame(pixelBuffer, clock.Timestamp);
                    }
                });
            // Use stalling readback otherwise
            else
            {
                readbackBuffer = readbackBuffer ?? new Texture2D(mediaRecorder.pixelWidth, mediaRecorder.pixelHeight,
                                     TextureFormat.RGBA32, false, false);
                RenderTexture.active = framebuffer;
                readbackBuffer.ReadPixels(new Rect(0, 0, framebuffer.width, framebuffer.height), 0, 0, false);
                readbackBuffer.GetRawTextureData<byte>().CopyTo(pixelBuffer);
                mediaRecorder.CommitFrame(pixelBuffer, clock.Timestamp);
            }
        }

        private sealed class CameraInputAttachment : MonoBehaviour
        {
        }

        #endregion
    }
}