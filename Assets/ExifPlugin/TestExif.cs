﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using ExifLib;
using UnityEngine.UI;

[RequireComponent(typeof(GUIText))]
public class TestExif : MonoBehaviour
{

//	private string imagePath = "/Users/maximalist14/Desktop/PhotoCameraApp/Assets/Textures/facial-masks_1.jpg";//"https://lh3.googleusercontent.com/-1c82FJozx0s/U67jGIfUKXI/AAAAAAAAFyM/sdYWzujT9GA/s0-U-I/IMG_1939.JPG";
	//private string imagePath = "/Users/andy/Desktop/PhotoCameraApp/Assets/Textures/facial-masks_1.jpg";







	/// <summary>
	/// ExifLib - http://www.codeproject.com/Articles/47486/Understanding-and-Reading-Exif-Data
	/// </summary>
	public  static  Texture2D GetImage(string path)
	{
		Debug.Log("GEt file");
		if (!File.Exists(path)) return new Texture2D(0,0);
		Debug.Log("File Exist");
		byte[] fileData;
		fileData = File.ReadAllBytes(path);
		Debug.Log(fileData.Length);
		var tex = new Texture2D(2, 2);
		tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
		tex.Apply();
		
		var extension = Path.GetExtension(path).ToLower();
		Debug.Log("Before cheking format!!!!! " + path + "| extension - " + extension);
		if (extension != ".jpg" && extension != ".jpeg") return tex;
		Debug.Log("is jpg");
		var orientation = ExifLib.ExifReader.ReadJpeg(fileData, "Foo").Orientation;
		Debug.Log("orientation - " + orientation.ToString());
		if (orientation == ExifOrientation.Undefined || orientation == ExifOrientation.TopLeft|| orientation == 0) return tex;
		Debug.Log("Need rotation");
		Texture2D texRotated = null;
		Color32[] original = tex.GetPixels32();
		Color32[] rotated = new Color32[original.Length];
		switch (orientation)
		{
			case ExifOrientation.TopRight:
			{
				int w = tex.width;
				int h = tex.height;

				int iRotated, iOriginal;

				for (int j = 0; j < h; ++j)
				{
					for (int i = 0; i < w; ++i)
					{
						iRotated = (i + 1) * h - j - 1;
						iOriginal = original.Length - 1 - (j * w + i);
						rotated[iRotated] = original[iOriginal];
					}
				}

				texRotated = new Texture2D(h, w);
				texRotated.SetPixels32(rotated);
				texRotated.Apply();
				break;
			}
			case ExifOrientation.BottomLeft:
			{
				int w = tex.width;
				int h = tex.height;

				int iRotated, iOriginal;

				for (int j = 0; j < h; ++j)
				{
					for (int i = 0; i < w; ++i)
					{
						iRotated = (i + 1) * h - j - 1;
						iOriginal = j * w + i;
						rotated[iRotated] = original[iOriginal];
					}
				}

				texRotated = new Texture2D(h, w);
				texRotated.SetPixels32(rotated);
				texRotated.Apply();
				break;
			}
			case ExifOrientation.BottomRight:
			{
				int w = tex.width;
				int h = tex.height;
				
//				int count = w * h-1;
//				for (int i = 0; i <=count ; ++i)
//				{
//						rotated[i] = original[count-i];
//				}

				texRotated = new Texture2D(w, h);
				texRotated.SetPixels32(original.Reverse().ToArray());
				texRotated.Apply();
				break;
			}
			
		}
               
               
                DestroyImmediate(tex, true);
                return texRotated;
	}

	//private static ExifOrientation GetImageRotation(byte[] bytes)
//	{
		
	//	ExifLib.JpegInfo jpi = ExifLib.ExifReader.ReadJpeg(bytes, "Foo");
//		switch (jpi.Orientation)
//		{
//			case ExifOrientation.TopRight : return 90;
//			case ExifOrientation.BottomRight : return 180;
//			case ExifOrientation.BottomLeft : return 270;
//		}

	//	return 0;
//	}

}
