﻿using ActionUnits;
using UnityEditor;
using UnityEngine;

namespace EditorScripts
{
    [CustomPropertyDrawer(typeof(EffectActivator))]
    public class EffectActivatorEditor : PropertyDrawer
    {
        private SerializedProperty effectProp;
        private SerializedProperty typeProp;
        private SerializedProperty animationTriggerActivateProp;
        private SerializedProperty animationTriggerDeactivateProp;
        private SerializedProperty shouldWaitBeforeDeactivateProp;
        private SerializedProperty waitBeforeDeactivateDelayProp;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var yPosIncreas = 20;
            
            // Calculate rects
            var effectRect = new Rect(position.x, position.y + yPosIncreas, position.width, 15);
            yPosIncreas += 20;
            var typeRect = new Rect(position.x, position.y + yPosIncreas, position.width, 15);
            yPosIncreas += 20;

            effectProp = property.FindPropertyRelative("Effect");
            typeProp = property.FindPropertyRelative("Type");
            animationTriggerActivateProp = property.FindPropertyRelative("animationTriggerActivate");
            animationTriggerDeactivateProp = property.FindPropertyRelative("animationTriggerDeactivate");
            shouldWaitBeforeDeactivateProp = property.FindPropertyRelative("shouldWaitBeforeDeactivate");
            waitBeforeDeactivateDelayProp = property.FindPropertyRelative("waitBeforeDeactivateDelay");

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
//            EditorGUI.PrefixLabel(effectRect, GUIUtility.GetControlID(FocusType.Passive), new GUIContent("Test"));
            EditorGUI.PropertyField(effectRect, effectProp, new GUIContent("Effect"));
            EditorGUI.PropertyField(typeRect, typeProp, new GUIContent("Type"));

            if ((EffectType) typeProp.enumValueIndex == EffectType.AnimationTrigger)
            {
                var animationTriggerActivateRect = new Rect(position.x, position.y + yPosIncreas, position.width, 15);
                yPosIncreas += 20;
                EditorGUI.PropertyField(animationTriggerActivateRect, animationTriggerActivateProp, new GUIContent("Activate Trigger Name"));
                var animationTriggerDeactivateRect = new Rect(position.x, position.y + yPosIncreas, position.width, 15);
                yPosIncreas += 20;
                EditorGUI.PropertyField(animationTriggerDeactivateRect, animationTriggerDeactivateProp, new GUIContent("Deactivate Trigger Name"));
            }
            
            var shouldWaitBeforeDeactivateRect = new Rect(position.x, position.y + yPosIncreas, position.width, 15);
            yPosIncreas += 20;
            EditorGUI.PropertyField(shouldWaitBeforeDeactivateRect, shouldWaitBeforeDeactivateProp, new GUIContent("Should Wait", "Should Wait Before Deactivate"));

            if (shouldWaitBeforeDeactivateProp.boolValue)
            {
                var waitBeforeDeactivateDelayRect = new Rect(position.x, position.y + yPosIncreas, position.width, 15);
                yPosIncreas += 20;
                EditorGUI.PropertyField(waitBeforeDeactivateDelayRect, waitBeforeDeactivateDelayProp, new GUIContent("Wait Delay"));
            }

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
        
        public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
        {
            var height = 80;

            typeProp = property.FindPropertyRelative("Type");
            if ((EffectType) typeProp.enumValueIndex == EffectType.AnimationTrigger)
            {
                height += 40;
            }
            
            var shouldWaitBeforeDeactivateProp = property.FindPropertyRelative("shouldWaitBeforeDeactivate");
            if (shouldWaitBeforeDeactivateProp.boolValue)
            {
                height += 20;
            }

            return height;


//            SerializedObject childObj = new UnityEditor.SerializedObject(property.objectReferenceValue as EffectActivator);
//            SerializedProperty ite = childObj.GetIterator();
// 
//            float totalHeight = EditorGUI.GetPropertyHeight (property, label);
// 
//            while (ite.NextVisible(true))
//            {
//                totalHeight += EditorGUI.GetPropertyHeight(ite, label);
//            }
// 
// 
//            return totalHeight;
        }
        
//        public override VisualElement CreatePropertyGUI(SerializedProperty property)
//        {
//            // Create property container element.
//            var container = new VisualElement();
//
//            effectProp = property.FindPropertyRelative("Effect");
//            typeProp = property.FindPropertyRelative("Type");
//            shouldWaitBeforeDeactivateProp = property.FindPropertyRelative("shouldWaitBeforeDeactivate");
//            waitBeforeDeactivateDelayProp = property.FindPropertyRelative("waitBeforeDeactivateDelay");
//            
//            // Create property fields.
//            var effectField = new PropertyField(effectProp);
//            var typeField = new PropertyField(typeProp);
//            var shouldWaitBeforeDeactivateField = new PropertyField(shouldWaitBeforeDeactivateProp);
//            var waitBeforeDeactivateDelayField = new PropertyField(waitBeforeDeactivateDelayProp);
//
//            // Add fields to the container.
//            container.Add(effectField);
//            container.Add(typeField);
////            container.Add(nameField);
//
//            return container;
//        }
        
//        public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
//        {
//            effectProp = property.FindPropertyRelative("Effect");
//            typeProp = property.FindPropertyRelative("Type");
//            shouldWaitBeforeDeactivateProp = property.FindPropertyRelative("shouldWaitBeforeDeactivate");
//            waitBeforeDeactivateDelayProp = property.FindPropertyRelative("waitBeforeDeactivateDelay");
//         
////            EditorGUILayout.PropertyField (effectProp);
////            EditorGUILayout.PropertyField (typeProp);
////            EditorGUILayout.PropertyField (shouldWaitBeforeDeactivateProp);
////            if (shouldWaitBeforeDeactivateProp.boolValue)
////            {
////                EditorGUILayout.PropertyField (waitBeforeDeactivateDelayProp);
////            }
//            
//            EditorGUIUtility.wideMode = true;
//            EditorGUIUtility.labelWidth = 70;
//            rect.height /= 2;
////            positionProperty.vector3Value = EditorGUI.Vector3Field(rect, "Position", positionProperty.vector3Value);
//            EditorGUILayout.PropertyField (effectProp);
//            rect.y += rect.height;
////            normalProperty.vector3Value = EditorGUI.Vector3Field(rect, "Normal", normalProperty.vector3Value);
//            EditorGUILayout.PropertyField (typeProp);
//        }
        
//        private void OnEnable()
//        {
//            // Setup the SerializedProperties.
//        }
//
//        public override void OnInspectorGUI()
//        {
//            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
//            serializedObject.Update ();
//
//            EditorGUILayout.PropertyField (effectProp);
//            EditorGUILayout.PropertyField (typeProp);
//            EditorGUILayout.PropertyField (shouldWaitBeforeDeactivateProp);
//            if (shouldWaitBeforeDeactivateProp.boolValue)
//            {
//                EditorGUILayout.PropertyField (waitBeforeDeactivateDelayProp);
//            }
//
//            // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
//            serializedObject.ApplyModifiedProperties ();
//        }
    }
}