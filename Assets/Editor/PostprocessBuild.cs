﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;
using XcodeUnityCapability = UnityEditor.iOS.Xcode.ProjectCapabilityManager;

public class PostprocessBuild : MonoBehaviour
{
//    public static BuildType buildType {  get; private set; }

    [PostProcessBuild]
    private static void PostBuildActions(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
            XcodeUnityCapability projCapability =
                new XcodeUnityCapability(projPath, "Unity-iPhone/mmk.entitlements", "Unity-iPhone");
            string[] groups = { };
            string bundleID = "";
            var buildTypeScriptableObj = Resources.Load<BuildTypeScriptableObj>("BuildType");
            var buildType = buildTypeScriptableObj.buildType;
            switch (buildType)
            {
                case BuildTypeScriptableObj.BuildType.Release:
                {
                    groups = new[] {"group.com.pixchange.pixchange"};
                    bundleID = "com.pixchange.camera";
                    break;
                }
                case BuildTypeScriptableObj.BuildType.Develop:
                {
                    groups = new[] {"group.com.pixchange.pixchange.develop"};
                    bundleID = "com.pixchange.camera.develop";
                    break;
                }
                case BuildTypeScriptableObj.BuildType.Staging:
                {
                    groups = new[] {"group.com.pixchange.pixchange.staging"};
                    bundleID = "com.pixchange.camera.staging";
                    break;
                }
            }

            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromFile(plistPath);
            PlistElementDict rootDict = plist.root;
            rootDict.SetString("NSContactsUsageDescription", "for easy mobile plugin");
// Write plist
            File.WriteAllText(plistPath, plist.WriteToString());
            projCapability.AddAppGroups(groups);
            projCapability.WriteToFile();
            Debug.Log("buildType - " + buildType);
            var project = new PBXProject();
            project.ReadFromString(File.ReadAllText(projPath));
            Debug.Log("project - " + project);
            var target = project.TargetGuidByName("Unity-iPhone");
            project.AddBuildProperty(target, "PRODUCT_BUNDLE_IDENTIFIER", bundleID);
            Debug.Log("property added - " + project);
            project.WriteToFile(projPath);
        }

        Debug.Log("EditorUserBuildSettings.exportAsGoogleAndroidProject = " +
                  EditorUserBuildSettings.exportAsGoogleAndroidProject);
        if (buildTarget == BuildTarget.Android && EditorUserBuildSettings.exportAsGoogleAndroidProject)
        {
            //enable androidX
            path += "/" + Application.productName;
            string propertiesGradlePath = path + "/gradle.properties";
            string gradleProperties = File.ReadAllText(propertiesGradlePath);
            gradleProperties += ("\nandroid.useAndroidX=true\nandroid.enableJetifier=true");
            File.WriteAllText(propertiesGradlePath, gradleProperties);
            //add dependencies for ffmpeg
            string buildGradlePath = path + "/build.gradle";
            var buildGradle = File.ReadAllText(buildGradlePath);
            buildGradle = buildGradle.Replace("implementation fileTree(dir: 'libs', include: ['*.jar'])",
                "implementation fileTree(dir: 'libs', include: ['*.jar']) \nimplementation  ('com.writingminds:FFmpegAndroid:0.3.2')");
            File.WriteAllText(buildGradlePath, buildGradle);
            //create provider path xml
            string providerPath = path + "/src/main/res/xml/";
            if (!Directory.Exists(providerPath))
                Directory.CreateDirectory(providerPath);
            string providerXML = "<paths>" +
                                 "\n<external-path name=\"tempdata\" path=\".\"/>\n"
                                 + "</paths>";
            File.WriteAllText(providerPath + "provider_path.xml", providerXML);
            //copy FileUtils
            File.Copy(Application.dataPath + "/Extras/FileUtils.java",
                path + "/src/main/java/com/pixchange/camera/FileUtils.java"); //TODO: dynamic path from bundle id
            File.Copy(Application.dataPath + "/Extras/ffmpegBridge.java",
                path + "/src/main/java/com/pixchange/ffmpegBridge.java"); //TODO: dynamic path from bundle id
            string unityPlayerPath = path + "/src/main/java/com/pixchange/camera/UnityPlayerActivity.java";
          string unityPlayer =   File.ReadAllText(unityPlayerPath);
          string patch =   File.ReadAllText(Application.dataPath + "/Extras/unityPlayerPatch.java");
         unityPlayer =  unityPlayer.Replace("protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code",patch);
         unityPlayer = unityPlayer.Replace("public class UnityPlayerActivity extends Activity","public class UnityPlayerActivity extends com.visagetechnologies.visagetrackerunitydemo.CameraActivity");
         unityPlayer = unityPlayer.Replace("@Override protected void onDestroy","@Override public void onDestroy");
         unityPlayer = unityPlayer.Replace("@Override protected void onPause","@Override public void onPause");
         unityPlayer = unityPlayer.Replace("@Override protected void onResume","@Override public void onResume");
         unityPlayer = unityPlayer.Replace("@Override protected void onCreate","@Override public void onCreate");
         //add usings
         unityPlayer = unityPlayer.Replace("import com.unity3d.player.*;","import com.unity3d.player.*;\nimport android.content.pm.PackageManager;\nimport android.net.Uri;\nimport androidx.core.content.FileProvider;\nimport java.io.File;\nimport android.os.Build;\n");
         File.WriteAllText(unityPlayerPath,unityPlayer);
          //path + "/src/main/java/com/pixchange/ffmpegBridge.java"); //TODO: dynamic path from bundle id

        }
    }

//    [MenuItem("SetBuildType/Develop")]
//    private static void SetBuildDevelopType()
//    {
//        buildType = BuildType.Develop;
//    }
//
//    [MenuItem("SetBuildType/Staging")]
//    private static void SetBuildStagingType()
//    {
//        buildType = BuildType.Staging;
//    }
//
//    [MenuItem("SetBuildType/Release")]
//    private static void SetBuildReleaseType()
//    {
//        buildType = BuildType.Develop;
//    }
}