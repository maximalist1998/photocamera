//
//  NativeBridgeController.m
//  Unity-iPhone
//
//  Created by Andrii Sudoma on 6/12/19.
//

#import "NativeBridgeController.h"

void messageFromUnity(char * message) {
    NSString * messageFromUnity = [NSString stringWithUTF8String: message];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageFromUnity" object:messageFromUnity];
    NSLog(@ "[messageFromUnity]: %@", messageFromUnity);
}

@implementation NativeBridgeController
- (void) sendMessageToUnity:(NSString*)message {
    UnitySendMessage("NativeCommunicationBridge", "messageFromIOS", [message UTF8String] );
}

@end

