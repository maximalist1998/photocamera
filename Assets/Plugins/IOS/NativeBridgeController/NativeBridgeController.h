//
//  NativeBridgeController.h
//  Unity-iPhone
//
//  Created by Andrii Sudoma on 6/12/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NativeBridgeController : NSObject
- (void) sendMessageToUnity:(NSString*)message;
@end

NS_ASSUME_NONNULL_END
