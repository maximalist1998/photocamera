char* MakeNSStringCopy (NSString* ns)
{
    if (ns == nil)
        return NULL;
    
    const char* string = [ns UTF8String];
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

NSString* _GetSharedFolderPathInternal( NSString* groupIdentifier )
{
    NSURL *containerURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:groupIdentifier];
    if( containerURL != nil )
    {
        //NSLog( @"Path to share content is %@", [containerURL path] );
    }
    else
    {
        NSLog( @"Fail to call NSFileManager sharing" );
    }
    
    return [containerURL path];
}

// Unity script extern function shall call this function, interop NSString back to C-string,
// which then scripting engine will convert it to C# string to your script side.
const char* _GetSharedFolderPath( const char* groupIdentifier )
{
    NSString* baseurl = _GetSharedFolderPathInternal([[NSString alloc] initWithUTF8String:groupIdentifier] );
    return MakeNSStringCopy( baseurl );
}
