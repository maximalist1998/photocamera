protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
public boolean shareToPixchange(String path){
    PackageManager pm = this.getPackageManager();
    boolean isInstalled = isPackageInstalled("com.pixchange.pixchange", pm);
    if(isInstalled){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.getData();
        File file = new File(path);
        Uri uri = FileProvider.getUriForFile(this,this.getPackageName().concat(".provider"),file);
        intent.setPackage("com.pixchange.pixchange");
        intent.setDataAndType(uri, FileUtils.getMimeType(file));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivityForResult(Intent.createChooser(intent, "Edit image"), 202);
    }
    else
        {
            startActivity(
                   new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?com.pixchange.pixchange"))
           );
        }
return isInstalled;
    }
    public static boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            return packageManager.getApplicationInfo(packageName, 0).enabled;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
  @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SendRequestResultToUnity("OnAllow");
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if(shouldShowRequestPermissionRationale(permissions[0])){
                            SendRequestResultToUnity("OnDeny");
                        } else {
                            SendRequestResultToUnity("OnDenyAndNeverAskAgain");
                        }
                    }
                }
                break;
            }
        }
    }

    private void SendRequestResultToUnity(String result){
        UnityPlayer.UnitySendMessage("UniAndroidPermission", result, "");
    }

public void returnResult(String path)
{
    Intent data = new Intent();
    File file = new File(path);
    Uri uri = FileProvider.getUriForFile(this,this.getPackageName().concat(".provider"),file);
    data.setData(uri);
 data.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    setResult(RESULT_OK,data);
    finish();
}
    public void pickMedia(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("*/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, new String[] {"image/*", "video/*"});
        startActivityForResult(photoPickerIntent, 205);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK){
            if(requestCode == 205){
                String path =  FileUtils.getPath(this,data.getData());
                UnityPlayer.UnitySendMessage("PickingController", "StartPostprocess",path);}
        }

    }