package com.pixchange;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.unity3d.player.UnityPlayer;

public class ffmpegBridge {
static FFmpeg ffmpeg;
private static void loadFFMpeg() throws FFmpegNotSupportedException {
    if(ffmpeg ==null)
    {
        ffmpeg = FFmpeg.getInstance(UnityPlayer.currentActivity);
        ffmpeg.loadBinary(new LoadBinaryResponseHandler());
    }
}
private static void loadFFMpegWithCommand(String[] command) throws FFmpegNotSupportedException {
    if(ffmpeg ==null)
    {
        ffmpeg = FFmpeg.getInstance(UnityPlayer.currentActivity);
        ffmpeg.loadBinary(new LoadBinaryResponseHandler(){
            @Override
            public void onSuccess() {
                try {
                    executeCommand(command);
                } catch (FFmpegCommandAlreadyRunningException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
public static void sendFFMpegCommand(String[] command)  {
    for (int i =  0;i<command.length;i++  ) {

    System.out.println("comands is - " + command[i]);
 
    }

    if(ffmpeg == null)
    {
        try {
            loadFFMpegWithCommand(command);
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
    }else{
        try {
            executeCommand(command);
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

}

private static void executeCommand(String[] command) throws FFmpegCommandAlreadyRunningException {
    ffmpeg.execute(command, new ExecuteBinaryResponseHandler()
    {
        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            System.out.println(message);
            UnityPlayer.UnitySendMessage("FFmpeg", "OnFailure",message);
        }

        @Override
        public void onFinish() {
            super.onFinish();
            System.out.println("Finish");
            UnityPlayer.UnitySendMessage("FFmpeg", "OnSuccess","");

        }

        @Override
        public void onProgress(String message) {
            super.onProgress(message);
            System.out.println(message);
            UnityPlayer.UnitySendMessage("FFmpeg", "OnProgress",message);
        }
    });
}
}
