﻿using System;
using FaceTracker;
using UnityEngine;
using Utils;

public class PhotoTakingController : MonoBehaviour
{
    #region > Events

    public event Action OnPhotoTaken;
    
    #endregion > Events
    
    #region > Editor Fields

    [SerializeField]
    private Camera[] renderingCameras;
    [SerializeField]
    private RectTransform photoRect;
    [SerializeField]
    private RecordingButton recordingButton;
    [SerializeField]
    private float scaleMultiplier = 1.5f;

    private const int MaxImageSize = 4000;

    #endregion > Editor Fields
    
    #region > Private Fields

    private RenderTexture framebuffer;
    private RenderTexture framebufferDest;
    
    private Vector2 offset;
    private Vector2 scale;
    private Vector2Int resultTextureSize;
    
    #endregion > Private Fields
    
    #region > MonoBehaviour Callbacks
    
    private void OnEnable()
    {
        recordingButton.onPhotoButtonPressed += MakePhoto;
    }
    
    private void OnDisable()
    {
        recordingButton.onPhotoButtonPressed -= MakePhoto;
    }
    
    #endregion > MonoBehaviour Callbacks

    private void MakePhoto()
    {
        SetupCameraByRectTransform(photoRect);

        var multiplier = scaleMultiplier;
        if (renderingCameras[0].pixelWidth * multiplier > MaxImageSize ||
            renderingCameras[0].pixelHeight * multiplier > MaxImageSize)
            multiplier = 1f;
        
        if(framebuffer == null)
            framebuffer = RenderTexture.GetTemporary((int)(renderingCameras[0].pixelWidth * multiplier), (int)(renderingCameras[0].pixelHeight * multiplier), 24);
        if(framebufferDest == null)
            framebufferDest = RenderTexture.GetTemporary((int)(resultTextureSize.x * multiplier), (int)(resultTextureSize.y * multiplier), 24);

            for (var i = 0; i < renderingCameras.Length; i++)
        {
            var prevTarget = renderingCameras[i].targetTexture;
            renderingCameras[i].targetTexture = framebuffer;
            renderingCameras[i].Render();
            renderingCameras[i].targetTexture = prevTarget;
        }
        
        Graphics.Blit(framebuffer, framebufferDest, offset:offset, scale: scale);
        var resTexture = TextureUtils.ToTexture2D(framebufferDest);

        if (TrackerBasic.Instance.Orientation == 1)
        {
            var prevTexture = resTexture;
            resTexture = TextureUtils.RotateTexture(prevTexture, true);
            DestroyImmediate(prevTexture);
        }
        
        if (TrackerBasic.Instance.Orientation == 3)
        {
            var prevTexture = resTexture;
            resTexture = TextureUtils.RotateTexture(prevTexture, false);
            DestroyImmediate(prevTexture);
        }
        
        PostprocessController.Instance.StartPostprocess(resTexture, true);
        
        RecordingPanelActivator.Instance.DeactivateFaceMask();
        OnPhotoTaken?.Invoke();
    }
    
    private void SetupCameraByRectTransform(RectTransform rectTransform)
    {
        Vector3[] worldCorners = new Vector3[4];
        rectTransform.GetWorldCorners(worldCorners);
        var recordingCamera = renderingCameras[0];
        var minViewportPoint = recordingCamera.WorldToViewportPoint(worldCorners[0]);
        var maxViewportPoint = recordingCamera.WorldToViewportPoint(worldCorners[2]);
        resultTextureSize = new Vector2Int(
            (int)(Screen.width * (maxViewportPoint.x - minViewportPoint.x)),
            (int)(Screen.height * (maxViewportPoint.y - minViewportPoint.y))
            );
//        int rotation = Mathf.RoundToInt(rectTransform.transform.eulerAngles.z) % 360;
//
//        if (rotation == -90 || rotation == 270)
//        {
//            Debug.Log("-90 || 270");
//            var y = minViewportPoint.y;
//            minViewportPoint.y = maxViewportPoint.y;
//            maxViewportPoint.y = y;
//        }
//        else if (Mathf.Abs(rotation) == 180)
//        {
//            Debug.Log("180");
//            var viewport = maxViewportPoint;
//            maxViewportPoint = minViewportPoint;
//            minViewportPoint = viewport;
//        }
//        else if (rotation == 90 || rotation == -270)
//        {
//            Debug.Log("90 || -270");
//            var x = minViewportPoint.x;
//            minViewportPoint.x = maxViewportPoint.x;
//            maxViewportPoint.x = x;
//        }
        
        var newMin = new Vector3(
            Mathf.Min(minViewportPoint.x, maxViewportPoint.x),
            Mathf.Min(minViewportPoint.y, maxViewportPoint.y),
            0f
            );
        var newMax = new Vector3(
            Mathf.Max(minViewportPoint.x, maxViewportPoint.x),
            Mathf.Max(minViewportPoint.y, maxViewportPoint.y),
            0f
        );

        minViewportPoint = newMin;
        maxViewportPoint = newMax;

        offset = new Vector2(minViewportPoint.x, minViewportPoint.y);
        scale = new Vector2(
            maxViewportPoint.x - minViewportPoint.x,
            maxViewportPoint.y - minViewportPoint.y
        );
    }
}