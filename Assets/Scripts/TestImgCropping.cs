﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TestImgCropping : MonoBehaviour
{
	public static Action<Texture2D> OnLoadedImage;
	public static event Action<Texture2D> OnImageCropped;
	public static event Action OnCroppingCanceled;
	public static event Action<Texture2D> OnImageCroppedPreview;
	
	#region Variables

	public RawImage croppedImageHolder;
	public Text croppedImageSize;
	public float minAspectRatio, maxAspectRatio;
	
    #endregion

	#region Enable/Disable

	private void OnEnable()
	{
		ScreenshotCompanion.OnTakeScreenshoot +=  CropImage;
		
	}

	private void OnDisable()
	{
		ScreenshotCompanion.OnTakeScreenshoot -= CropImage;//CropImageForPreview;

	}

	#endregion

	#region Cropping
	
	public void Crop()
	{
		// If image cropper is already open, do nothing
		if (ImageCropper.Instance.IsOpen)
			return;

		//StartCoroutine( TakeScreenshotAndCrop() );
	}

	//with callback for marker 
	public void CropImage(Texture2D imageFromGallery)
	{
		StartCoroutine(TakeScreenshotAndCrop(imageFromGallery));
	}
	
	private IEnumerator TakeScreenshotAndCrop(Texture2D imageFromGallery)
	{

		if (imageFromGallery == null)
		{
			Debug.Log("image from screenshot is NULL");
		}
		else
		{
			Debug.Log("image from screenshot is not NULL");
		}
		
		
		yield return new WaitForEndOfFrame();

		bool ovalSelection = false;//ovalSelectionInput.isOn;
		bool autoZoom = true;//autoZoomInput.isOn;

		//string fileName = "/Users/andy/Documents/Projects/Upwork/photocamera/Screenshots/screenShotBeforCroping.jpg";
		//byte[] bytes = imageFromGallery.EncodeToJPG();
		//System.IO.File.WriteAllBytes(fileName, bytes);
		
		ImageCropper.Instance.Show(
			imageFromGallery, (bool result, Texture originalImage, Texture2D croppedImage) =>
			{
				// Destroy previously cropped texture (if any) to free memory
				// Destroy(croppedImageHolder.texture, 5f);

				// If screenshot was cropped successfully
				if (result)
				{
					// Assign cropped texture to the RawImage
					//croppedImageHolder.enabled = true;
					//croppedImageHolder.texture = originalImage;

					
					/*Vector2 size = croppedImageHolder.rectTransform.sizeDelta;
					if (croppedImage.height <= croppedImage.width)
						size = new Vector2(400f, 400f * (croppedImage.height / (float) croppedImage.width));
					else
						size = new Vector2(400f * (croppedImage.width / (float) croppedImage.height), 400f);
					croppedImageHolder.rectTransform.sizeDelta = size;*/					
					
					
					OnImageCropped?.Invoke(croppedImage);
				
					
					//croppedImageSize.enabled = true;
					//croppedImageSize.text = "Image size: " + croppedImage.width + ", " + croppedImage.height;
				}
				else
				{
					OnCroppingCanceled?.Invoke();
					//croppedImageHolder.enabled = false;
					//croppedImageSize.enabled = false;
				}

			},
			settings: new ImageCropper.Settings()
			{
				ovalSelection   = ovalSelection,
				autoZoomEnabled = autoZoom,
				imageBackground = Color.clear, // transparent background
				selectionMinAspectRatio = minAspectRatio,
				selectionMaxAspectRatio = maxAspectRatio,
				markTextureNonReadable = false
			},
			croppedImageResizePolicy: (ref int width, ref int height) =>
			{
				// uncomment lines below to save cropped image at half resolution
				//width /= 2;
				//height /= 2;
			});
	}
	
	//for image preview in admin menu
	private void CropImageForPreview(Texture2D imageFromGallery)
	{
		StartCoroutine(CropPreview(imageFromGallery));
	}
	
	private IEnumerator CropPreview(Texture2D imageFromGallery)
	{
		yield return new WaitForEndOfFrame();

		bool ovalSelection = false;//ovalSelectionInput.isOn;
		bool autoZoom = true;//autoZoomInput.isOn;

		string fileName = "/Users/andy/Documents/Projects/Upwork/photocamera/Screenshots/testImgGG.jpg";
		byte[] bytes = imageFromGallery.EncodeToJPG();
		System.IO.File.WriteAllBytes(fileName, bytes);

		ImageCropper.Instance.Show(
			imageFromGallery, (bool result, Texture originalImage, Texture2D croppedImage) =>
			{
				// If screenshot was cropped successfully
				if (result)
				{
					
					OnImageCroppedPreview?.Invoke(croppedImage);
				}

			},
			settings: new ImageCropper.Settings()
			{
				ovalSelection   = ovalSelection,
				autoZoomEnabled = autoZoom,
				imageBackground = Color.clear, // transparent background
				selectionMinAspectRatio = minAspectRatio,
				selectionMaxAspectRatio = maxAspectRatio,
				markTextureNonReadable = false
			},
			croppedImageResizePolicy: (ref int width, ref int height) =>
			{
				// uncomment lines below to save cropped image at half resolution
				//width /= 2;
				//height /= 2;
			});
	}
	
	#endregion

	#region Controll Methods

	public void PressSelectImg()
	{
		//CropImage(someGalleryimage.texture);
	}

	#endregion
}
