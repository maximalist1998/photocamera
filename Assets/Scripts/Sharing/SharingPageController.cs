﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;

public class SharingPageController : Singleton<SharingPageController>
{
    [SerializeField] private Button createAnotherGif;
    [SerializeField] private Button sharePixchange;
    [SerializeField] private GameObject panel;

    private string pathToResult;

    // Start is called before the first frame update
    void Start()
    {
        createAnotherGif.onClick.AddListener(() =>
        {
            HidePanel();

            PickingController.Instance.ShowPickingPanel();
        });
        sharePixchange.onClick.AddListener(() => { ShareToPixchange(pathToResult); });
    }

    public void ShareToPixchange(string path)
    {
#if UNITY_ANDROID

        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        if (currentActivity.Call<bool>("shareToPixchange", path))
        {
            panel.SetActive(false);
            PickingController.Instance.ShowPickingPanel();
        }
#elif UNITY_IOS
            string appDomain = "";
            var buildTypeScriptableObj = Resources.Load<BuildTypeScriptableObj>("BuildType");
            var buildType = buildTypeScriptableObj.buildType;
            switch (buildType)
            {
                case BuildTypeScriptableObj.BuildType.Release:
                {
                    appDomain = "pixchange://";
                    break;
                }
                case BuildTypeScriptableObj.BuildType.Develop:
                {
                    appDomain = "pixchanged://";
                    break;
                }
                case BuildTypeScriptableObj.BuildType.Staging:
                {
                    appDomain = "pixchanges://";
                    break;
                }
            }
            string sharedFolder = AppGroupPlugin.GetSharedFolderPathForDefaultAppGroup()+"/";
            var fileName = Path.GetFileName(path);
            Debug.Log("Shared path - " +sharedFolder );
            if (!Directory.Exists(sharedFolder ))
            {
                Directory.CreateDirectory(sharedFolder);
            }

            File.Copy(path,sharedFolder+fileName,true);

           // string url = appDomain + "create";
            string url = appDomain + "create?asset_filename=" + Path.GetFileName(fileName);
            Debug.Log(url);
            Application.OpenURL(url);
            Debug.Log("After URL opening");
         panel.SetActive(false);
        PickingController.Instance.ShowPickingPanel();
#endif
       
    }

    public void ShowPanel(string pathToResult, bool isRunnedFromExternalApp)
    {
        this.pathToResult = pathToResult;
        if (isRunnedFromExternalApp)
        {
#if UNITY_IOS
            ShareToPixchange(pathToResult);
#elif UNITY_ANDROID
            AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("returnResult", pathToResult);
            
            #endif
        }
        else
        {
            panel.SetActive(true);
        }
    }

    public void HidePanel()
    {
        panel.SetActive(false);
    }
#if UNITY_ANDROID
    
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape)&& panel.activeInHierarchy)
        {
            HidePanel();
            PickingController.Instance.ShowPickingPanel();
        }

       
    }
#endif
}