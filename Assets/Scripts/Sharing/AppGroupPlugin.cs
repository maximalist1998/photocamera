﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public static class AppGroupPlugin
{
#if UNITY_IOS
    /* Interface to native implementation */

    #region Native Link
#if UNITY_EDITOR

    private static string _GetSharedFolderPath( string groupIdentifier )
    {
        // Handle dummy folder in editor mode
        string path = System.IO.Path.Combine( System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), groupIdentifier );
        if( !System.IO.Directory.Exists( path ) )
            System.IO.Directory.CreateDirectory( path );

        return path;
    }

#elif UNITY_IOS

    [DllImport ("__Internal")]
    private static extern string _GetSharedFolderPath( string groupIdentifier );

#endif
    #endregion

    private static string GetSharedFolderPath( string groupIdentifier )
    {
        return _GetSharedFolderPath( groupIdentifier );
    }

    public static string GetSharedFolderPathForDefaultAppGroup()
    {
        var buildTypeScriptableObj = Resources.Load<BuildTypeScriptableObj>("BuildType");
        var buildType = buildTypeScriptableObj.buildType;
        string group = "";
            
        switch (buildType)
        {
            case BuildTypeScriptableObj.BuildType.Release:
            {
                group = "group.com.pixchange.pixchange";
               
                break;
            }
            case BuildTypeScriptableObj.BuildType.Develop:
            {
                group = "group.com.pixchange.pixchange.develop";
               
                break;
            } 
            case BuildTypeScriptableObj.BuildType.Staging:
            {
                group = "group.com.pixchange.pixchange.staging";
                break;
            }
        }

        return _GetSharedFolderPath(group);
    }
#endif
}