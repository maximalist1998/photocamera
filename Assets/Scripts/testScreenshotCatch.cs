﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testScreenshotCatch : MonoBehaviour
{
    public RawImage croppedImageHolder;
    public RawImage screenShotImage;
    
    private void OnEnable()
    {
        TestImgCropping.OnImageCropped += CropImage;
        ScreenshotCompanion.OnTakeScreenshoot += SetScreeToImage;
    }

    private void OnDisable()
    {
        TestImgCropping.OnImageCropped -= CropImage;
        ScreenshotCompanion.OnTakeScreenshoot -= SetScreeToImage;

    }

    private void CropImage(Texture2D croppedImage)
    {
        Debug.Log("catch cropped texture ... ");
       //string fileName = "/Users/andy/Documents/Projects/Upwork/photocamera/Screenshots/croppedScreenshot.jpg";
    //    byte[] bytes = croppedImage.EncodeToJPG();
       
      //  System.IO.File.WriteAllBytes(ScreenshotCompanion.PathToScreenShot, bytes);
		
        croppedImageHolder.enabled = true;
        croppedImageHolder.texture = croppedImage;
    }

    private void SetScreeToImage(Texture2D screenshot)
    {
        Debug.Log("catch set texture ... ");
        
        if (screenshot == null)
        {
            Debug.Log("image from screenshot is NULL");
        }
        else
        {
            Debug.Log("image from screenshot is not NULL");
        }
        
        screenShotImage.texture = screenshot;
        //string fileName = "/Users/andy/Documents/Projects/Upwork/photocamera/Screenshots/screenShotCatch.jpg";
        byte[] bytes = screenshot.EncodeToJPG();
        System.IO.File.WriteAllBytes(ScreenshotCompanion.PathToScreenShot, bytes);
    }
}
