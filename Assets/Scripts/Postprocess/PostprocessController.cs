﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PostprocessController : UniUtils.Singleton<PostprocessController>
{
    [Header("Process panels")] [SerializeField]
    private GameObject postprocessPanel;

    [SerializeField] private GameObject postprocessControllPanel;

    [SerializeField] private GameObject postprocessUI;
    //[SerializeField] private GameObject screenshotBackground;
    //[SerializeField] private GameObject whiteBackground;

    [SerializeField] private StickerPostprocess stickerPrefab;
    [SerializeField] private VideoStickerPostprocess videoStickerPrefab;
    [SerializeField] private TextPostprocess textPrefab;

    //just for test
    [SerializeField] private Texture2D testTexture;

    [SerializeField] private PostprocessCroppimgManager postprocessCroppimgManager;

    //   [SerializeField] private VideoClip testClip;
    [SerializeField] private Button addStickerButton;
    [SerializeField] private StickersPoolManager stickersPoolManager;
    [SerializeField] private StickersPoolManager stickersRecentManager;
    [SerializeField] private Button addVideoStickerButton;
    [SerializeField] private Button addTextButton;
    [SerializeField] private Button deleteActivePostProcElButton;
    [SerializeField] private Button deactivatePostProcElButton;
    [SerializeField] private Button saveButton;
    [SerializeField] private List<Texture2D> stickers;
    [SerializeField] private List<VideoClip> videoStickers;
    [SerializeField] private RectTransform parentForPostprocessElements;
    [SerializeField] private Image previewImage;
    [SerializeField] private Image gifImage;
    [SerializeField] private VideoPlayer previewPlayer;
    [SerializeField] private Camera recordingCamera;
    [SerializeField] private Canvas effectCanvas;
    [SerializeField] private TextMeshProUGUI progressText;
    [SerializeField] private GameObject waitingPanel;
    public bool startedFromExternalApp = false;
    public bool startedFromPixchange = true;
    private SavingOptions savingOptions = new SavingOptions(true, true);

    public bool IsCameFromRecording = false;

    [Header("Gif player")] [SerializeField]
    private GameObject gifPlayer;

    private ProcessedContentTypeTool.ContentType currentType;

//    [SerializeField] private GifController gifController;
    [SerializeField] private ProGifConroller proGifConroller;

    [Header("Borders")] [SerializeField] [UnityEngine.Range(0, 0.5f)]
    private float upperBorder = 0;

    [SerializeField] [UnityEngine.Range(0, 0.5f)]
    private float downBorder = 0;

    [SerializeField] [UnityEngine.Range(0, 0.5f)]
    private float rightBorder = 0;

    [SerializeField] [UnityEngine.Range(0, 0.5f)]
    private float leftBorder = 0;

    private ProGifPlayer proGifPLayer;

    private RectTransform _playerRect;
    private PostprocessElement _activePostprocElement;
    private List<PostprocessElement> postprocessElementsInScene;
    private LayerMask postProcLayerMask;

    //temporary album logic
    [SerializeField] private AlbumsPanelController albumsPanelController;
    [SerializeField] private List<Album> albums;
    [SerializeField] private VideoRecordingController videoRecordingController;
    [SerializeField] private List<GameObject> postprocessObjects;

    public PostprocessElement activePostprocElement
    {
        get { return _activePostprocElement; }
        set
        {
            if (_activePostprocElement != null)
                _activePostprocElement.Deselect();
            if (value != null)
            {
                value.Select();
                deleteActivePostProcElButton.gameObject.SetActive(true);
                deactivatePostProcElButton.gameObject.SetActive(true);
            }

            saveButton.gameObject.SetActive(value == null);

            _activePostprocElement = value;
        }
    }

    private RectTransform PlayerRect
    {
        get
        {
            if (_playerRect == null)
                _playerRect =
                    previewPlayer.gameObject
                        .GetComponent<RectTransform>(); //previewPlayer.GetComponent<RectTransform>();
            return _playerRect;
        }
    }

    private Coroutine recordingCoroutine;

    // private int nextStickerIndex = 0; //for test
    //private int nextVideoStickerIndex = 0; //for test
    //   public string json;

    private void Start()
    {
        //  SetPreviewRect(1280,720,PlayerRect,90);
        Debug.Log("camera rect = " + recordingCamera.rect);
        // GifController.OnVideoWasExported += SaveCurrentPostprocessStateToJson;
        activePostprocElement = null;

        //StartPostprocess("storage/emulated/0/Download/20190609_112312.mp4"); //just for test
        if (postprocessElementsInScene == null)
            postprocessElementsInScene = new List<PostprocessElement>();
        stickersPoolManager.ShowVideoStickers(videoStickers, (stickerId) =>
        {
            AddVideoSticker(videoStickers[stickerId], stickerId);
            StickersPanelController.Instance.HideStickersPanel();
        });
        stickersPoolManager.ShowStickers(stickers, (stickerId) =>
        {
            AddSticker(stickers[stickerId], stickerId);
            StickersPanelController.Instance.HideStickersPanel();
        });

        var recentStickers = new List<Texture2D>();
        for (int i = 0; i < 10; i++)
        {
            recentStickers.Add(stickers[i]);
        }

        stickersRecentManager.ShowStickers(recentStickers, (stickerId) =>
        {
            AddSticker(stickers[stickerId], stickerId);
            StickersPanelController.Instance.HideStickersPanel();
        });
        albumsPanelController.InitAlbums(albums,
            (album) => { StickersPanelController.Instance.ShowAlbumsInnerPanel(album.albumName); });
        addStickerButton.onClick.AddListener(StickersPanelController.Instance.ShowStickersPanel);
        // addVideoStickerButton.onClick.AddListener(AddVideoSticker);
        addTextButton.onClick.AddListener(AddText);
        deleteActivePostProcElButton.onClick.AddListener(DeleteActivePostProcEl);
        deactivatePostProcElButton.onClick.AddListener(() =>
        {
            deactivatePostProcElButton.gameObject.SetActive(false);
            activePostprocElement = null;
        });
        //   ResumePostprocessFromJSON(JsonUtility.FromJson<ResumingJSON>(json));
    }

    private void ActivateAllPostprocessObjects(bool isCameFromRecording = false)
    {
        IsCameFromRecording = isCameFromRecording;
        
        postprocessUI.SetActive(true);
        foreach (var go in postprocessObjects)
        {
            go.SetActive(true);
        }
    }

    public void DeactivatePostprocess()
    {
        postprocessUI.SetActive(false);
        foreach (var go in postprocessObjects)
        {
            go.SetActive(false);
        }
    }

    public void StartPostprocess(Texture2D textureForProcessing, bool isCameFromCamera = false)
    {
        ActivateAllPostprocessObjects(isCameFromCamera);
        OpenPanel(textureForProcessing);
        currentType = ProcessedContentTypeTool.ContentType.Image;
        //  MobileInputManager.Instance.OnTouch += CheckTouchOnPostprocess;
    }

    public void ResumePostprocessFromJSON(ResumingJSON resumingJson)
    {
        Debug.Log("ResumePostprocessFromJSON");
        foreach (var sticker in resumingJson.imageStickers)
        {
            //  Debug.Log("sticker - " + sticker.position);
            var transform1 = AddSticker(stickers[sticker.id], sticker.id).transform;
            // activePostprocElement.transform;
            //  Debug.Log("sticker - " + sticker.rotation);
            transform1.localPosition = sticker.position;
            //    Debug.Log("sticker - " + sticker.scale);
            transform1.localScale = sticker.scale;
            transform1.localRotation = sticker.rotation;
        }

        foreach (var sticker in resumingJson.videoStickers)
        {
            //   Debug.Log("sticker - " + sticker);
            var transform1 = AddVideoSticker(videoStickers[sticker.id], sticker.id).transform;
            //  activePostprocElement.transform;
            transform1.localPosition = sticker.position;
            transform1.localScale = sticker.scale;
            transform1.localRotation = sticker.rotation;
        }

        foreach (var text in resumingJson.postprocessTexts)
        {
            TextPostprocess ptext = AddText(true);
            var transform1 = ptext.transform;

            ptext.inputPostProc.text = text.text;
            ptext.inputPostProc.textComponent.font = TextPanel.Instance.GetFontByName(text.font);

            ptext.inputPostProc.textComponent.color = text.mainColor;
            var outline = ptext.inputPostProc.textComponent.GetComponent<Outline>();

            if (outline != null)
            {
                outline.enabled = true;
                outline.effectColor = text.outline;
            }


            transform1.localPosition = text.position;
            transform1.localScale = text.scale;
            transform1.localRotation = text.rotation;
        }

        Debug.Log("postprocess count - " + postprocessElementsInScene.Count);
        activePostprocElement = null;
    }

    public void StartPostprocess(VideoClip videoForProcessing)
    {
        ActivateAllPostprocessObjects();
        OpenPanel(videoForProcessing);
        currentType = ProcessedContentTypeTool.ContentType.Video;
        //    MobileInputManager.Instance.OnTouch += CheckTouchOnPostprocess;
    }

    public void StartPostprocess(string videoPath, float videoAngle = 0f, bool isCameFromRecording = false)
    {
        ActivateAllPostprocessObjects(isCameFromRecording);

        OpenPanel(videoPath, videoAngle);
        currentType = ProcessedContentTypeTool.ContentType.Video;
        //    MobileInputManager.Instance.OnTouch += CheckTouchOnPostprocess;
    }

    public void StartPostprocessGif(string gifPath)
    {
        ActivateAllPostprocessObjects();
        OpenPanelGIF(gifPath);
        currentType = ProcessedContentTypeTool.ContentType.GIF;
        //    MobileInputManager.Instance.OnTouch += CheckTouchOnPostprocess;
    }

    public void EndPostprocess()
    {
        //   MobileInputManager.Instance.OnTouch -= CheckTouchOnPostprocess;
    }


    public StickerPostprocess AddSticker(Texture2D stickerTexture, int id)
    {
        var sticker = Instantiate(stickerPrefab, parentForPostprocessElements);
        // Debug.Log("Sticker add " +sticker.name );
        var sprite = Sprite.Create(stickerTexture, new Rect(0, 0, stickerTexture.width, stickerTexture.height),
            Vector2.one * 0.5f);
        sticker.SetSprite(sprite, id);
        //  nextStickerIndex = nextStickerIndex < stickers.Count - 1 ? nextStickerIndex + 1 : 0;
        if (postprocessElementsInScene == null)
            postprocessElementsInScene = new List<PostprocessElement>();
        postprocessElementsInScene.Add(sticker);
        activePostprocElement = sticker;
        return sticker;
    }

    public VideoStickerPostprocess AddVideoSticker(VideoClip videoClip, int id)
    {
        var sticker = Instantiate(videoStickerPrefab, parentForPostprocessElements);
        sticker.SetVideoClip(videoClip, id);
        //     nextVideoStickerIndex = nextVideoStickerIndex < videoStickers.Count - 1 ? nextVideoStickerIndex + 1 : 0;
        activePostprocElement = sticker;
        if (postprocessElementsInScene == null)
            postprocessElementsInScene = new List<PostprocessElement>();
        postprocessElementsInScene.Add(sticker);
        return sticker;
    }


    public void AddText()
    {
        var text = Instantiate(textPrefab, parentForPostprocessElements);
        activePostprocElement = text;
        if (postprocessElementsInScene == null)
            postprocessElementsInScene = new List<PostprocessElement>();
        postprocessElementsInScene.Add(text);
    }

    public TextPostprocess AddText(bool ignoreActivation)
    {
        var text = Instantiate(textPrefab, parentForPostprocessElements);
        text.activateOnCreate = !ignoreActivation;

        if (postprocessElementsInScene == null)
            postprocessElementsInScene = new List<PostprocessElement>();
        postprocessElementsInScene.Add(text);
        return text;
    }

    private void SetPreviewRect(int contentWidth, int contentHeight, RectTransform rectTransform, int rotation = 0)
    {
        int screenWidth = Screen.width; // Mathf.RoundToInt(Screen.safeArea.width);
        int screenHeight = Screen.height; // Mathf.RoundToInt(Screen.safeArea.height);
        if (contentWidth == 0 || contentHeight == 0) return;
        rectTransform.localRotation = Quaternion.Euler(0, 0, -rotation);

        float maxPreviewHeight = screenHeight * (1 - Mathf.Clamp01(upperBorder + downBorder));
        float maxPreviewWidth = screenWidth * (1 - Mathf.Clamp01(rightBorder + leftBorder));
        float previewMaxAspect = maxPreviewWidth / maxPreviewHeight;

        if (rotation == 90 || rotation == 270 || rotation == -90)
        {
            var tempWidth = contentWidth;
            contentWidth = contentHeight;
            contentHeight = tempWidth;
        }

        float contentAspect = (float) contentWidth / contentHeight;
        Vector2 rectCenter = new Vector2((leftBorder * screenWidth + (1 - rightBorder) * screenWidth) / 2f,
            (downBorder * screenHeight + (1 - upperBorder) * screenHeight) / 2f);
        Vector2 size = previewMaxAspect < contentAspect
            ? new Vector2(maxPreviewWidth, maxPreviewWidth / contentAspect)
            : new Vector2(maxPreviewHeight * contentAspect, maxPreviewHeight);
        
        var ancMin = new Vector2((rectCenter.x - size.x / 2f) / screenWidth,
            (rectCenter.y - size.y / 2f) / screenHeight);
        var ancMax = new Vector2((rectCenter.x + size.x / 2f) / screenWidth,
            (rectCenter.y + size.y / 2f) / screenHeight);
        
        rectTransform.anchorMin = ancMin;
        rectTransform.anchorMax = ancMax;

        
        if (rotation == 90 || rotation == 270 || rotation == -90)
        {
            
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;

            var difference = (rectTransform.rect.width - rectTransform.rect.height) / 2f;
            //   var difference = (contentWidth - contentHeight) / 2f;

            rectTransform.offsetMin = new Vector2(difference, -difference);
            rectTransform.offsetMax = new Vector2(-difference, difference);
        }
        else
        {
            
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
        }

        
        OnSetRectToImage?.Invoke(rectTransform);
        // rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.y,rectTransform.sizeDelta.x);
        //return new Rect(position, size);
    }

    public static System.Action<RectTransform> OnSetRectToImage;

    public void OpenPanel(Texture2D processedTexture2D)
    {
        gifImage.gameObject.SetActive(false);
        postprocessCroppimgManager.Activate(); //temporary
        postprocessPanel.SetActive(true);
        previewImage.gameObject.SetActive(true);
        previewPlayer.gameObject.SetActive(false);
        postprocessControllPanel.SetActive(true);
//        var sprite = Sprite.Create(processedTexture2D,
//            new Rect(Vector2.zero, new Vector2(processedTexture2D.width, processedTexture2D.height)),
//            Vector2.one * 0.5f);
        SetPreviewRect(processedTexture2D.width, processedTexture2D.height, previewImage.rectTransform);
        if (previewImage.sprite != null)
            DestroyImmediate(previewImage.sprite);
        previewImage.sprite = Sprite.Create(processedTexture2D,
            new Rect(Vector2.zero, new Vector2(processedTexture2D.width, processedTexture2D.height)),
            Vector2.one * 0.5f);
        
        OnSetRectToImage?.Invoke(previewImage.GetComponent<RectTransform>());
    }

    public void OpenPanel(VideoClip processedVideoClip)
    {
        postprocessCroppimgManager.Deactivate(); //temporary
        postprocessPanel.SetActive(true);
        previewPlayer.gameObject.SetActive(true);
        previewImage.gameObject.SetActive(false);
        gifImage.gameObject.SetActive(false);
        postprocessControllPanel.SetActive(true);
        SetPreviewRect((int) processedVideoClip.width, (int) processedVideoClip.height,
            PlayerRect);
        previewPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        previewPlayer.clip = processedVideoClip;
        var text = new RenderTexture((int) processedVideoClip.width, (int) processedVideoClip.height, 0);

        previewPlayer.targetTexture = text;
        previewPlayer.GetComponent<RawImage>().texture = text;

        previewPlayer.Play();
    }

    public void OpenPanelGIF(string gifPath)
    {
        postprocessCroppimgManager.Deactivate(); //temporary
        postprocessPanel.SetActive(true);
        postprocessControllPanel.SetActive(true);
        previewImage.gameObject.SetActive(false);
        gifImage.gameObject.SetActive(true);
        previewPlayer.gameObject.SetActive(false);
        ProGifManager.Instance.PlayGif(gifPath, gifImage);
        proGifPLayer = ProGifManager.Instance.m_GifPlayer;
        proGifPLayer.SetOnFirstFrameCallback((frame) =>
        {
            SetPreviewRect((int) frame.width, (int) frame.height,
                gifImage.rectTransform);
        });
    }

    public void OpenPanel(string videoPath, float angle)
    {
        var properties = NativeGallery.GetVideoProperties(videoPath);
        var rotation = properties.rotation;

        Debug.Log($"video rotation - {rotation}");
        Debug.Log($"Additional video rotation - {angle}");
        postprocessCroppimgManager.Deactivate(); //temporary
        postprocessPanel.SetActive(true);
        postprocessControllPanel.SetActive(true);
        previewImage.gameObject.SetActive(false);
        gifImage.gameObject.SetActive(false);
        previewPlayer.gameObject.SetActive(true);
        previewPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        previewPlayer.source = VideoSource.Url;
#if UNITY_EDITOR
        previewPlayer.url = videoPath;
#elif UNITY_IOS
         previewPlayer.url = videoPath;
#else
         previewPlayer.url = "file://" + videoPath;
#endif
        previewPlayer.Prepare();
        previewPlayer.prepareCompleted += source =>
        {
            if (source == null)
            {
                return;
            }

            //   if (source.clip != null)
            //  {

            var texture = source.texture;
            
            // SetPreviewRect((int) texture.width, (int) texture.height,PlayerRect, Mathf.RoundToInt(rotation));
#if UNITY_EDITOR

            //SetPreviewRect(177, 100,PlayerRect, 90);
            SetPreviewRect(texture.width, texture.height, PlayerRect, Mathf.RoundToInt(rotation) + (int)angle);
#else
            SetPreviewRect(properties.width, properties.height,PlayerRect, Mathf.RoundToInt(rotation) + (int)angle);
#endif
            //  SetPreviewRect((int) texture.width, (int) texture.height,PlayerRect, 90);
            //     SetPreviewRect((int) texture.width, (int) texture.height,PlayerRect);
            var text = new RenderTexture((int) texture.width, (int) texture.height, 0);

            source.targetTexture = text;
            source.GetComponent<RawImage>().texture = text;

            source.Play();
            // }
            // else
            // {
            //    Debug.Log("clip is null");
            // }
        };
    }

//    private void Update()
    //  {
    //   Debug.Log("is playing - "+ previewPlayer.isPlaying);  
    //    Debug.Log("is paused - "+ previewPlayer.isPaused);  
//    }

//    public void OpenPanel() //for gif playing
//    {
//        Debug.Log("open post porcess");
//        postprocessControllPanel.SetActive(true);
//        postprocessPanel.SetActive(true);
//        gifPlayer.SetActive(true);
//        previewImage.gameObject.SetActive(false);
//        postprocessControllPanel.SetActive(true);
//        postprocessUI.SetActive(true);
//        var gifClip = proGifPLayer;
//
//        SetPreviewRect(gifClip.width, gifClip.height,
//            PlayerRect);
//
//        proGifPLayer.Resume();
//    }

    public void ClosePanel()
    {
        if (currentType == ProcessedContentTypeTool.ContentType.GIF)
        {
            if (proGifPLayer != null)
                if (proGifPLayer.State == ProGifPlayerComponent.PlayerState.Playing)
                    proGifPLayer.Stop();
        }

        StickersPanelController.Instance.HideImmediately();
        postprocessPanel.SetActive(false);
        previewPlayer.gameObject.SetActive(false);
        previewImage.gameObject.SetActive(false);
        gifImage.gameObject.SetActive(false);
        postprocessControllPanel.SetActive(false);
        gifPlayer.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        videoRecordingController.OnVideoWasExported += SaveCurrentPostprocessStateToJson;
    }

    public void SaveVideo(SavingOptions savingOptions)
    {
        this.savingOptions = savingOptions;
        foreach (var postprocessElement in postprocessElementsInScene)
        {
            if (postprocessElement is VideoStickerPostprocess)
            {
                var video = postprocessElement as VideoStickerPostprocess;
                video.PlayFromBegining();
            }
        }

        switch (currentType)
        {
            case ProcessedContentTypeTool.ContentType.Video:
            {
                CreateResumingJSON(currentType);

                var videoLength = previewPlayer.playbackSpeed * previewPlayer.frameCount / previewPlayer.frameRate;
                //  ImgaeGifCamera.Instance.SetSizeCamera(PlayerRect);

                //ImgaeGifCamera.Instance.SetRecorerSettings(videoLength);
//                proGifConroller.MaxSideResolution = Mathf.Max((int) previewPlayer.width, (int) previewPlayer.height);
//                proGifConroller.SetFPSbyLengs(videoLength);

                //gifController.StartRecording();
                postprocessUI.SetActive(false);
                waitingPanel.SetActive(true);
                activePostprocElement = null;
                if (recordingCoroutine != null)
                    StopCoroutine(recordingCoroutine);


//                previewPlayer.Stop();
//                if (!previewPlayer.isPlaying)
//                    previewPlayer.Play();
                previewPlayer.audioOutputMode = VideoAudioOutputMode.None;
                recordingCoroutine = StartCoroutine(StopRecordingAfter(Mathf.Clamp(videoLength, 1f, 15f)));
                break;
            }
            case ProcessedContentTypeTool.ContentType.Image:
            {
                foreach (var postprocessElement in postprocessElementsInScene)
                {
                    if (postprocessElement is VideoStickerPostprocess)
                    {
                        CreateResumingJSON(currentType);
                        //proGifConroller.SetupAspectRatio();
                        //  proGifConroller.MaxSideResolution = Mathf.Max(previewImage.mainTexture.width,
                        //     previewImage.mainTexture.height);
                        //    proGifConroller.SetFPSbyLengs(2.5f);
                        //    proGifConroller.StartGifRecording();
                        videoRecordingController.StartRecording(previewImage.rectTransform);
                        // gifController.StartRecording();
                        postprocessUI.SetActive(false);
                        waitingPanel.SetActive(true);
                        activePostprocElement = null;
                        if (recordingCoroutine != null)
                            StopCoroutine(recordingCoroutine);
                        recordingCoroutine = StartCoroutine(StopRecordingAfter(2.5f));
                        return;
                    }
                }

                StartCoroutine(SaveScreenshot());
                break;
            }
            case ProcessedContentTypeTool.ContentType.GIF:
            {
                CreateResumingJSON(currentType);
                //    ImgaeGifCamera.Instance.SetSizeCamera(previewImage.rectTransform);
                float gifTime =
                    Mathf.Clamp(proGifPLayer.playerComponent.totalFrame * proGifPLayer.playerComponent.interval, 1, 15);
                //ImgaeGifCamera.Instance.SetRecorerSettings(10);//do not forget change settings
                // Debug.Log($"total - {proGifPLayer.playerComponent.totalFrame}, interval {proGifPLayer.playerComponent.interval}");
                //proGifConroller.MaxSideResolution = Mathf.Max(proGifPLayer.width, proGifPLayer.height);
                //  proGifConroller.SetFPSbyLengs(gifTime);
                proGifPLayer.playerComponent.Stop(); // = 0;
                proGifPLayer.playerComponent.Resume();


                //gifController.StartRecording();
                waitingPanel.SetActive(true);
                postprocessUI.SetActive(false);
                activePostprocElement = null;
                if (recordingCoroutine != null)
                    StopCoroutine(recordingCoroutine);

                recordingCoroutine = StartCoroutine(StopRecordingAfter(gifTime));
                break;
            }
        }
    }

    private void CameraSize(RectTransform imageRect)
    {
        var imgMinAnchor = imageRect.anchorMin;
        var imgMaxAnchor = imageRect.anchorMax;

        var canvasRect = effectCanvas.GetComponent<RectTransform>();
        var canvasHeight = canvasRect.sizeDelta.y * canvasRect.lossyScale.y;

        var imageHeight = canvasHeight * (imgMaxAnchor.y - imgMinAnchor.y);
        recordingCamera.orthographicSize = imageHeight / 2f;

        var imageAnchorWidth = imgMaxAnchor.x - imgMinAnchor.x;
        var imageAnchorHeight = imgMaxAnchor.y - imgMinAnchor.y;
        recordingCamera.rect = new Rect(0f, 0f, imageAnchorWidth, imageAnchorHeight);
    }

    private IEnumerator SaveScreenshot()
    {
        yield return null;
        postprocessUI.SetActive(false);
        float defaultOrthoSize = recordingCamera.orthographicSize;
        //screenshotBackground.SetActive(false);
        //whiteBackground.SetActive(true);
        CameraSize(previewImage.rectTransform);

        int width = recordingCamera.pixelWidth;
        int height = recordingCamera.pixelHeight;
        Rect rect = new Rect(0, 0, Mathf.RoundToInt(width * recordingCamera.rect.height),
            Mathf.RoundToInt(height * recordingCamera.rect.width));
        float cropCoef = recordingCamera.rect.width * recordingCamera.rect.height;
        RenderTexture renderTexture = new RenderTexture(Mathf.RoundToInt(width * recordingCamera.rect.height),
            Mathf.RoundToInt(height * recordingCamera.rect.width), 24);
        Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
//     //   Texture2D temp = new Texture2D(clip.Width, clip.Height, TextureFormat.RGB24, false);
        screenShot.hideFlags = HideFlags.HideAndDontSave;
        screenShot.wrapMode = TextureWrapMode.Clamp;
        screenShot.filterMode = FilterMode.Bilinear;
        screenShot.anisoLevel = 0;
//
        var mCamera = recordingCamera;
        mCamera.targetTexture = renderTexture;
        RenderTexture.active = renderTexture;
        //renderTexture.Release();
        mCamera.Render();
        //   ImgaeGifCamera.Instance.SetSizeCamera(previewImage.rectTransform);
        // yield return new WaitForEndOfFrame();
//      ImgaeGifCamera.Instance.SetSizeCamera(previewImage.rectTransform);
        screenShot.ReadPixels(rect, 0, 0);
        screenShot.Apply();
        int newWidth = Mathf.RoundToInt(cropCoef * screenShot.width);
        int newHeight = Mathf.RoundToInt(cropCoef * screenShot.height);
        //  int diff = screenShot.height - newHeight;
        Texture2D croppeTexture2D = new Texture2D(newWidth, newHeight);
        //    Debug.Log($"{cropCoef}diff {diff}, width {newWidth}, height {newHeight}");
        for (int w = 0; w < newWidth; w++)
        {
            for (int h = 0; h < newHeight; h++)
            {
                croppeTexture2D.SetPixel(w, h, screenShot.GetPixel(w, h));
            }
        }

// screenShot.Apply();
        mCamera.targetTexture = null;
        RenderTexture.active = null;

        Destroy(renderTexture);
        Destroy(screenShot);
        croppeTexture2D.Apply();
        renderTexture = null;

        //screenshotBackground.SetActive(true);
        // whiteBackground.SetActive(false);

        SaveCurrentPostprocessStateToJson(croppeTexture2D);
        activePostprocElement = null;
        DeleteAllElements();
        recordingCamera.rect = new Rect(0, 0, 1, 1);
        recordingCamera.orthographicSize = defaultOrthoSize;
    }

    private IEnumerator StopRecordingAfter(float seconds)
    {
        if (currentType == ProcessedContentTypeTool.ContentType.GIF)
        {
            yield return new WaitForSeconds(0.6f);
//            proGifConroller.StartGifRecording();
            videoRecordingController.StartRecording(gifImage.rectTransform);
        }

        if (currentType == ProcessedContentTypeTool.ContentType.Video)
        {
//#if UNITY_ANDROID
            //        previewPlayer.waitForFirstFrame = true;
//#endif
            previewPlayer.Stop();
            while (previewPlayer.isPlaying)
            {
                yield return null;
            }


//            bool frameReady = false;
//            previewPlayer.frameReady += (frame, player) =>
//            {
//                Debug.Log("frameReady");
//                frameReady = true;
//            };
//            bool videoPlayed = false;
//
//            previewPlayer.started += (player) =>
//            {
//                Debug.Log("videoPlayed");
//                videoPlayed = true;
//            };
            previewPlayer.Prepare();
            while (!previewPlayer.isPrepared)
            {
                yield return null;
            }

            previewPlayer.Play();

//            while (!videoPlayed)
//            {
//                Debug.Log("previewPlayer.isPlaying - " + previewPlayer.isPlaying);
//                Debug.Log("stopped - ");
//                yield return new WaitForSeconds(0.2f);
//            }

            while (previewPlayer.frame < 0)
            {
                yield return null;
            }

            //     yield return new WaitForSeconds(0.2f);
            videoRecordingController.StartRecording(PlayerRect);
            //       proGifConroller.StartGifRecording();
        }

        yield return new WaitForSeconds(seconds);
        //gifController.StopRecording();
        DeleteAllElements();
        //  gifController.ExportGIF();
        //    proGifConroller.StopRecordingGif();
        videoRecordingController.StopRecording();
        switch (currentType)
        {
            case ProcessedContentTypeTool.ContentType.Image:
            {
                if (previewImage.sprite != null)
                    DestroyImmediate(previewImage.sprite);
                PostprocessCroppimgManager.Instance.ClearHoldedImage();
                break;
            }
            case ProcessedContentTypeTool.ContentType.Video:
            {
                if (previewPlayer.isPlaying)
                    previewPlayer.Stop();
                break;
            }
            case ProcessedContentTypeTool.ContentType.GIF:
            {
                proGifPLayer.Stop();
                break;
            }
        }
    }

    private void DeleteActivePostProcEl()
    {
        if (activePostprocElement != null)
        {
            activePostprocElement.Remove();
            postprocessElementsInScene.Remove(activePostprocElement);
        }

        saveButton.gameObject.SetActive(true);
        deleteActivePostProcElButton.gameObject.SetActive(false);
        deactivatePostProcElButton.gameObject.SetActive(false);
    }

    public void DeleteAllElements()
    {
        if (activePostprocElement != null)
            activePostprocElement.Remove();
        deleteActivePostProcElButton.gameObject.SetActive(false);
        deactivatePostProcElButton.gameObject.SetActive(false);
        if (postprocessElementsInScene == null) return;
        foreach (var postprocessElement in postprocessElementsInScene)
        {
            if (postprocessElement != null)
            {
                DestroyImmediate(postprocessElement.gameObject);
            }
        }

        postprocessElementsInScene = new List<PostprocessElement>();
    }

    private void SaveCurrentPostprocessStateToJson(Texture2D texture2D)
    {
        ResumingJSON resumingJson = new ResumingJSON();
        List<VideoSticker> videoStickers = new List<VideoSticker>();
        List<ImageSticker> imageStickers = new List<ImageSticker>();
        List<PostprocessText> postprocessTexts = new List<PostprocessText>();
        foreach (var element in postprocessElementsInScene)
        {
            if (element is VideoStickerPostprocess)
            {
                var sticker = element as VideoStickerPostprocess;
                videoStickers.Add(new VideoSticker(sticker.ID, sticker.transform.localRotation,
                    sticker.transform.localPosition, sticker.transform.localScale));
            }
            else if (element is StickerPostprocess)
            {
                var sticker = element as StickerPostprocess;
                imageStickers.Add(new ImageSticker(sticker.ID, sticker.transform.localRotation,
                    sticker.transform.localPosition, sticker.transform.localScale));
            }
            else if (element is TextPostprocess)
            {
                var text = element as TextPostprocess;
                var outlineComponent = text.inputPostProc.textComponent.GetComponent<Outline>();
                Color outlineColor = Color.clear;
                if (outlineComponent != null)
                    if (outlineComponent.enabled)
                        outlineColor = outlineComponent.effectColor;
                    else
                    {
                        outlineColor = Color.clear;
                    }

                postprocessTexts.Add(new PostprocessText(text.inputPostProc.text,
                    outlineColor,
                    text.inputPostProc.textComponent.color, text.inputPostProc.textComponent.font.name,
                    text.transform.localRotation, text.transform.localPosition, text.transform.localScale));
            }
        }

        resumingJson.videoStickers = videoStickers;
        resumingJson.imageStickers = imageStickers;
        resumingJson.postprocessTexts = postprocessTexts;
//#if UNITY_IOS && !UNITY_EDITOR
//        string sharedFolder = AppGroupPlugin.GetSharedFolderPathForDefaultAppGroup()+"/";
//Debug.Log("Shared path - " +sharedFolder );
//        if (!Directory.Exists(sharedFolder))
//        {
//            Directory.CreateDirectory(sharedFolder);
//        }

//        string croppedImageName = Guid.NewGuid() + ".png";
//
//        File.WriteAllBytes(sharedFolder+croppedImageName, previewImage.sprite.texture.EncodeToPNG());
//        string resultName = Guid.NewGuid() + ".png";
//        string result = sharedFolder + resultName;
//        File.WriteAllBytes(result, texture2D.EncodeToPNG());
//        resumingJson.croppedImagePath = croppedImageName;
//        resumingJson.mainImagePath = NativeBridgeController.pathToSource;
//#else

        if (!Directory.Exists(Application.temporaryCachePath + "/tempdata/"))
        {
            Directory.CreateDirectory(Application.temporaryCachePath + "/tempdata/");
        }

        string path = Application.temporaryCachePath + "/tempdata/" + Guid.NewGuid() + ".jpg";

        File.WriteAllBytes(path, previewImage.sprite.texture.EncodeToJPG());
        string result = Application.temporaryCachePath + "/tempdata/" + Guid.NewGuid() + ".jpg";

        File.WriteAllBytes(result, texture2D.EncodeToJPG());
        resumingJson.croppedImagePath = path;
        resumingJson.mainImagePath = NativeBridgeController.pathToSource;
//#endif
        MobileMedia.SaveImage(texture2D, "Pixchange", Guid.NewGuid() + ".jpg", MobileMedia.ImageFormat.JPG);
        SharingPageController.Instance.ShowPanel(result, startedFromExternalApp);
        startedFromExternalApp = false;
        //     Debug.Log(JsonUtility.ToJson(resumingJson));
        //  NativeBridgeController.SendJSONToNative(resumingJson, result);
    }

    private ResumingJSON resumingJson;

    private void CreateResumingJSON(ProcessedContentTypeTool.ContentType contentType)
    {
        resumingJson = new ResumingJSON();
        List<VideoSticker> videoStickers = new List<VideoSticker>();
        List<ImageSticker> imageStickers = new List<ImageSticker>();
        List<PostprocessText> postprocessTexts = new List<PostprocessText>();
        foreach (var element in postprocessElementsInScene)
        {
            if (element is VideoStickerPostprocess)
            {
                var sticker = element as VideoStickerPostprocess;
                videoStickers.Add(new VideoSticker(sticker.ID, sticker.transform.localRotation,
                    sticker.transform.localPosition, sticker.transform.localScale));
            }
            else if (element is StickerPostprocess)
            {
                var sticker = element as StickerPostprocess;
                imageStickers.Add(new ImageSticker(sticker.ID, sticker.transform.localRotation,
                    sticker.transform.localPosition, sticker.transform.localScale));
            }
            else if (element is TextPostprocess)
            {
                var text = element as TextPostprocess;
                var outlineComponent = text.inputPostProc.textComponent.GetComponent<Outline>();
                Color outlineColor;
                if (outlineComponent.enabled)
                    outlineColor = outlineComponent.effectColor;
                else
                {
                    outlineColor = Color.clear;
                }

                postprocessTexts.Add(new PostprocessText(text.inputPostProc.text,
                    outlineColor,
                    text.inputPostProc.textComponent.color, text.inputPostProc.textComponent.font.name,
                    text.transform.localRotation, text.transform.localPosition, text.transform.localScale));
            }
        }

        resumingJson.videoStickers = videoStickers;
        resumingJson.imageStickers = imageStickers;
        resumingJson.postprocessTexts = postprocessTexts;

        if (contentType == ProcessedContentTypeTool.ContentType.Image)
        {
#if UNITY_IOS && !UNITY_EDITOR
        string sharedFolder = AppGroupPlugin.GetSharedFolderPathForDefaultAppGroup()+"/";
Debug.Log("Shared path - " +sharedFolder );
            if (!Directory.Exists(sharedFolder ))
            {
                Directory.CreateDirectory(sharedFolder);
            }

            string path = Guid.NewGuid() + ".jpg";
            File.WriteAllBytes(sharedFolder+path, previewImage.sprite.texture.EncodeToJPG());
#else
            if (!Directory.Exists(Application.temporaryCachePath + "/tempdata/"))
            {
                Directory.CreateDirectory(Application.temporaryCachePath + "/tempdata/");
            }

            string path = Application.temporaryCachePath + "/tempdata/" + Guid.NewGuid() + ".jpg";
            File.WriteAllBytes(path, previewImage.sprite.texture.EncodeToJPG());
#endif
            resumingJson.croppedImagePath = path;
        }

        resumingJson.mainImagePath = NativeBridgeController.pathToSource;
    }

//
//    private void ClearRecorder()
//    {
//        proGifPLayer?.Clear();
//        proGifConroller?.ClearRecorder();
//        postprocessPanel?.SetActive(false);
//    }
    public int GetFPSbyLengs(long lengs)
    {
        int fps = 0;
        if (lengs < 5)
        {
            fps = 25;
        }
        else if (lengs > 5 && lengs < 8)
        {
            fps = 20;
        }
        else if (lengs > 8)
        {
            fps = 15;
        }

        return fps;
    }

    private void OnDisable()
    {
        videoRecordingController.OnVideoWasExported -= SaveCurrentPostprocessStateToJson;
    }

    private void ShareGeneratedVideo(string pathToVideo)
    {
        videoRecordingController.ClearRecorder();
        if (savingOptions.saveVideo)
        {
            MobileMedia.CopyMedia(pathToVideo, "Pixchange",
                System.IO.Path.GetFileNameWithoutExtension(pathToVideo), Path.GetExtension(pathToVideo),
                isImage: false);
        }

        var properties = NativeGallery.GetVideoProperties(pathToVideo);
        var fps = GetFPSbyLengs(properties.duration);
        if (savingOptions.saveGif)
        {
            progressText.gameObject.SetActive(true);
            progressText.text = "Generating GIF,  please wait";
            FFMpegBridge.Instance.ConvertToGif(pathToVideo, fps, (gifPath) =>
            {
                progressText.gameObject.SetActive(false);
                waitingPanel.SetActive(false);
                MobileMedia.CopyMedia(gifPath, "Pixchange", Path.GetFileNameWithoutExtension(gifPath),
                    Path.GetExtension(gifPath), true);
                if (!startedFromPixchange && savingOptions.saveGif && !savingOptions.saveVideo)
                    SharingPageController.Instance.ShowPanel(gifPath, startedFromExternalApp);
                else
                {
                    SharingPageController.Instance.ShowPanel(pathToVideo, startedFromExternalApp);
                }

                startedFromExternalApp = false;
            });
        }
        else
        {
            waitingPanel.SetActive(false);
            SharingPageController.Instance.ShowPanel(pathToVideo, startedFromExternalApp);
            startedFromExternalApp = false;
        }
    }


    private void SaveCurrentPostprocessStateToJson(string pathToVideo)
    {
        if (currentType == ProcessedContentTypeTool.ContentType.Video)
        {
            if (!Directory.Exists(Application.temporaryCachePath + "/tempdata/"))
            {
                Directory.CreateDirectory(Application.temporaryCachePath + "/tempdata/");
            }

            var recordingFilename =
                string.Format("recording_{0}.mp4", DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff"));
            var resultPath = Path.Combine(Application.temporaryCachePath, recordingFilename);
            FFMpegBridge.Instance.SetSoundFromAnotherVideo(previewPlayer.url, pathToVideo, resultPath,
                ShareGeneratedVideo);
        }
        else
        {
            ShareGeneratedVideo(pathToVideo);
        }
    }


//  NativeBridgeController.SendJSONToNative(resumingJson, pathTovideo);


    public bool IsStaticImageResult()
    {
        if (currentType == ProcessedContentTypeTool.ContentType.Image)
        {
            foreach (var element in postprocessElementsInScene)
            {
                if (element is VideoStickerPostprocess)
                    return false;
            }

            return true;
        }

        return false;
    }

    public struct SavingOptions
    {
        public bool saveGif;
        public bool saveVideo;

        public SavingOptions(bool gif, bool video)
        {
            saveGif = gif;
            saveVideo = video;
        }
    }
}