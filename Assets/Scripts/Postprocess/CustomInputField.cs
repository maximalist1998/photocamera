﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomInputField : InputFieldWithoutDeleteOnCancel
{
    public event Action onInputSelected;
    public event Action onKeyboardOpened;
    
    public event Action onInputDeselected;
    
    private const float maxClickTime = 0.5f;
    private Coroutine clickCoroutine;
    private bool isClick = false;

    protected override void Start()
    {
        base.Start();
        shouldHideMobileInput = true;
    }

    public override void OnSelect(BaseEventData eventData)
    {
        //base.OnSelect(eventData);
      //  Debug.Log("onSelect");
        onInputSelected?.Invoke();
    
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
     //   Debug.Log("OnPointerDown");
        base.OnPointerDown(eventData);
        if (clickCoroutine != null)
            StopCoroutine(clickCoroutine);
        clickCoroutine =  StartCoroutine(ClickTimeReseter());
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (isClick)
        {
          //  Debug.Log("OnPointerClick");
            base.OnPointerClick(eventData);
            onKeyboardOpened?.Invoke();
            isClick = false;
        }
        if (clickCoroutine != null)
            StopCoroutine(clickCoroutine);
    }

    public override void OnDeselect(BaseEventData eventData)
    {
      //  Debug.Log("OnDeselekt");
        base.OnDeselect(eventData);
        if (clickCoroutine != null)
            StopCoroutine(clickCoroutine);
        onInputDeselected?.Invoke();
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
       // Debug.Log("OnPointerUp");
        base.OnPointerUp(eventData);
        if (clickCoroutine != null)
            StopCoroutine(clickCoroutine);
    }

    private IEnumerator ClickTimeReseter()
    {
        isClick = true;
        yield return new WaitForSeconds(maxClickTime);
        isClick = false;
    }
}
