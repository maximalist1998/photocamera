﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;


public class TextPanel : Singleton<TextPanel>
{
    [SerializeField] private GameObject panel;
    [SerializeField] private GameObject colorsPanel;
    [SerializeField] private GameObject fontsPanel;
    [SerializeField] private GameObject colorFontsButtons;

    [SerializeField] private Toggle colorTogglePrefab;
    [SerializeField] private ToggleGroup colorToggleParent;
    [SerializeField] private Toggle fontTogglePrefab;
    [SerializeField] private ToggleGroup fontToggleParent;

    [SerializeField] private Button addTextButton;
    [SerializeField] private Button openTextColorButton;

    [SerializeField] private Image textColorButtonImage;
    [SerializeField] private Button openTextFontButton;
    [SerializeField] private Button outlineButton;
 //   [SerializeField] private Button setBlackOutlineButton;

//    [SerializeField] private Button nextColorButton;
    // [SerializeField] private Text fontsNameText;
    [SerializeField] private List<Color> colors;
    [SerializeField] private List<Font> fonts;
    private List<Toggle> colorToggles = new List<Toggle>();
    private List<Toggle> fontToggles = new List<Toggle>();
    private colorType currentCollorType;
    private Text currentText;
    private bool ignoreToggles = false;
    
    private Outline textOutline;
//    private int nextColorIndex = 0;
    //   private int nextFontIndex = 0;

    private void Start()
    {
        // nextFontButton.onClick.AddListener(SetNextFont);
//        nextColorButton.onClick.AddListener(SetNextColor);

        openTextColorButton.onClick.AddListener(ShowTextColorPanel);
        outlineButton.onClick.AddListener(ShowTextOutlinePanel);
        openTextFontButton.onClick.AddListener(ShowTextFontPanel);
//        setBlackOutlineButton.onClick.AddListener(SetBlackOutline);
   //     setWhiteOutlineButton.onClick.AddListener(SetWhiteOutline);
        foreach (var color in colors)
        {
            var toggle = Instantiate(colorTogglePrefab, colorToggleParent.transform);
            toggle.group = colorToggleParent;
            toggle.image.color = color;
            toggle.onValueChanged.AddListener(isActive =>
            {
                
                if(ignoreToggles) return;
              //      Debug.Log("value change - " + isActive);
                if (isActive)
                { 
                    SetColor(color);
                }
            });
            colorToggles.Add(toggle);
        }

        foreach (var font in fonts)
        {
            var toggle = Instantiate(fontTogglePrefab, fontToggleParent.transform);
            toggle.group = fontToggleParent;
            var toggleText = toggle.GetComponentInChildren<Text>();
            toggleText.font = font;
            toggleText.text = font.name.Length < 6 ? font.name : font.name.Substring(0, 6);
            toggle.onValueChanged.AddListener(isActive =>
            {
               // Debug.Log(" toggle.onValueChanged.AddListener");
                if(ignoreToggles) return;
                if (isActive)
                {
                    SetFont(font);
                }
            });
            fontToggles.Add(toggle);
        }
    }

    public void ShowPanel(Text text)
    {
        panel.SetActive(true);
        currentText = text;
        textOutline = text.GetComponent<Outline>();

     // colorToggles[0]?.Select();
      //  fontToggles[0]?.Select();
        colorFontsButtons.SetActive(true);
        addTextButton.gameObject.SetActive(false);
    //    outlineButton.gameObject.SetActive(true);
      ShowTextFontPanel();
      ignoreToggles = false;
      //  ShowTextColorPanel();
      //  nextFontIndex = 0;
      //  nextColorIndex = 0;
      //    fontsNameText.text = fonts[0].name;
    }

    public void HidePanel()
    {
        ignoreToggles = true;
        currentText = null;
        textOutline = null;
        panel.SetActive(false);
        colorFontsButtons.SetActive(false);
        addTextButton.gameObject.SetActive(true);
    //    outlineButton.gameObject.SetActive(false);
        ignoreToggles = false;
        //  nextFontIndex = 0;
        //  nextColorIndex = 0;
    }

    public Font GetFontByName(string name)
    {
        foreach (var font in fonts)
        {
            if (font.name == name) return font;
        }

        return fonts[0];
    }

    private void SetColor(Color color)
    {
        if (currentCollorType == colorType.main)
        {
            
        if (currentText != null)
            currentText.color = color;
       Debug.Log("set color - " + color);
        if (textColorButtonImage != null)
            textColorButtonImage.color = color;
        }
        else
        {
            if (textOutline == null) return;
            textOutline.enabled = true;
            textOutline.effectColor = color;
            outlineButton.image.color = color;
        }
    }

    private void SetFont(Font font)
    {
        if (currentText != null)
            currentText.font = font;
    }

    private void ShowTextColorPanel()
    {
        ignoreToggles = true;
        currentCollorType = colorType.main;
        if (colorsPanel.activeInHierarchy)
        {
//            if(textOutline!=null)
//            textOutline.enabled = false;
        ignoreToggles = false;
            return;
        }
        colorsPanel.SetActive(true);
        fontsPanel.SetActive(false);
        ignoreToggles = false;
        colorToggles[0]?.Select();
    }
    private void ShowTextOutlinePanel()
    {
        ignoreToggles = true;
        currentCollorType = colorType.outline;
        if (colorsPanel.activeInHierarchy)
        {
//            if(textOutline!=null)
//                textOutline.enabled = false;
        ignoreToggles = false;
            return;
        }
        colorsPanel.SetActive(true);
        fontsPanel.SetActive(false);
        ignoreToggles = false;
        colorToggles[0]?.Select();
        
    }

    private void ShowTextFontPanel()
    {
     //   Debug.Log("ShowTextFontPanel");
        ignoreToggles = true;
        if (fontsPanel.activeInHierarchy) return;
        colorsPanel.SetActive(false);
        fontsPanel.SetActive(true);
        ignoreToggles = false;
        fontToggles[0]?.Select();
    }

   

    enum colorType
    {
        main,
        outline
    }
}