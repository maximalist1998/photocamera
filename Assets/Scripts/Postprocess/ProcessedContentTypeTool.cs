﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ProcessedContentTypeTool
{
    private static string[] imageFormats = new[] {".jpg", ".png", ".jpeg",".heic"};
    private static string[] videoFormats = new[] {".mp4", ".webm", ".webp",".mov"};

    public static ContentType GetContentTypeByPath(string path)
    {
        if (string.IsNullOrEmpty(path)) return ContentType.Unknown;
        var extension = Path.GetExtension(path).ToLower();
        foreach (var format in imageFormats)
        {
            if (string.Equals(format, extension))
            {
                return ContentType.Image;
            }
        }
        foreach (var format in videoFormats)
        {
            if (string.Equals(format, extension))
            {
                return ContentType.Video;
            }
        }

        if (string.Equals(extension, ".gif"))
            return ContentType.GIF;
            return ContentType.Unknown;
    }

    public enum ContentType
    {
        Video,
        Image,
        GIF,
        Unknown
    }
}