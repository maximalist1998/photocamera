﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CancelButtonScript : MonoBehaviour
{
    [SerializeField] private SaveButton saveButton;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Cancel);
    }

    private void Cancel()
    {
        if (ImageCropper.Instance.gameObject.activeInHierarchy)
        {
            ImageCropper.Instance.Cancel();
        }
        else
        {
            saveButton.CloseSaveButtonMenu();
            PostprocessController.Instance.ClosePanel();
            PostprocessController.Instance.DeleteAllElements();

            if (PostprocessController.Instance.IsCameFromRecording)
            {
                RecordingPanelActivator.Instance.ActivateFaceMask();
            }
            else
            {
                PickingController.Instance.ShowPickingPanel();
            }
        }
#if UNITY_ANDROID
  //      AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
//        AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
      //  currentActivity.Call("finish");
   //     currentActivity.Call("closeUnity");
   //Application.Quit();
#elif UNITY_IOS
        
//NativeBridgeController.hideUnity();
#else 
  //  Application.Quit();
#endif
       // PostprocessController.Instance.DeleteAllElements();
        //PostprocessController.Instance.ClosePanel();
   //     RecordingPanelController.Instance.OpenRecordingPanel();
   
    }
#if UNITY_ANDROID
    
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Cancel();
    }
#endif
}
