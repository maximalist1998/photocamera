﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PostprocessElement:MonoBehaviour,IPointerDownHandler
{
    [SerializeField] private GameObject outlineActiveElement;

    public virtual void Select()
    {
     transform.SetSiblingIndex(transform.parent.childCount-1);
     outlineActiveElement?.SetActive(true);
     
    }

    public virtual void Deselect()
    {
        outlineActiveElement?.SetActive(false);  
    }
    public virtual void Remove()
    {
        Deselect();
        DestroyImmediate(gameObject,true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (PostprocessController.Instance.activePostprocElement!=this&&Input.touchCount == 1)
        {
        if(PostprocessController.Instance.activePostprocElement != this)
            PostprocessController.Instance.activePostprocElement = this;
        }
        
    }
}
