﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Toggle))]
public class AspectRatioToggle : MonoBehaviour
{
    
    [SerializeField] private Toggle toggle;
    [SerializeField] private float aspectRatioMin = 1;
    [SerializeField] private float aspectRatioMax = 1;
    [SerializeField] private Text text;
    [SerializeField] private Color activeColor;
    [SerializeField] private Color inActiveColor;
    
    [SerializeField] private bool defaultToggle = false;
    
    // Start is called before the first frame update
    
    void Start()
    {
                var sizeX = ImageCropper.Instance.Selection.sizeDelta.x/2;
        
        toggle.onValueChanged.AddListener(isActivated =>
        {
            Debug.Log("toggle - "+  gameObject.name + " value - " + isActivated );
            if (isActivated)
            {
                text.color = activeColor;
                ImageCropper.Instance.SetAspectRatio(aspectRatioMin,aspectRatioMax);
                
               ImageCropper.Instance.UpdateSelection(Vector2.zero,new Vector2(sizeX,sizeX*aspectRatioMax));
            ImageCropper.Instance.ResetView(true);
            }
            else
            {
                text.color = inActiveColor;
            }
        });
    }

    private void OnEnable()
    {
        if (defaultToggle)
        {
            Debug.Log("select");
         //   text.color = activeColor;
            toggle.Select();
            
        }
    }
}
