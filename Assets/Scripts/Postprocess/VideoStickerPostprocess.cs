﻿using System.Collections;
using System.Collections.Generic;
using System;
using GameInput;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent(typeof(RectTransform))]
public class VideoStickerPostprocess : PostprocessElement
{
    //  public float rot;
    //   [SerializeField] private Button activateButton;
    [SerializeField] private VideoPlayer stickerVideoPlayer;
    [SerializeField] private RawImage rawImage;
    public int ID;
    private RectTransform rectTransformSticker
    {
        get
        {
            if (_rectTransformSticker == null)
                _rectTransformSticker = GetComponent<RectTransform>();
            return _rectTransformSticker;
        }
    }

    private RectTransform _rectTransformSticker;

    public void PlayFromBegining()
    {
        stickerVideoPlayer.time = 0;
    }


//       activateButton.OnPointerDown.onClick.AddListener(() => { PostprocessController.Instance.activePostprocElement = this;});
       // SetVideoClip(stickerVideoPlayer.clip);
   // }

    public override void Select()
    {
        base.Select();
        //      activateButton.enabled = false;
        MobileInputManager.Instance.OnMove += Move;
        //MobileInputManager.Instance.OnRotate += Rotate;
        //MobileInputManager.Instance.OnPinch += Pinch;

        MobileInputManager.Pinching += Pinching;
        MobileInputManager.Rotating += Rotating;
    }

    public override void Deselect()
    {
        base.Deselect();
        //       activateButton.enabled = true;
        MobileInputManager.Instance.OnMove -= Move;
        //MobileInputManager.Instance.OnRotate -= Rotate;
        //MobileInputManager.Instance.OnPinch -= Pinch;       

        MobileInputManager.Pinching -= Pinching;
        MobileInputManager.Rotating -= Rotating;
    }

    
    public void SetVideoClip(VideoClip clip,int id)
    {
        ID = id;
        stickerVideoPlayer.clip = clip;
        // stickerVideoPlayer.targetTexture = new RenderTexture(Convert.ToInt32(clip.width),Convert.ToInt32(clip.height),1);
        // rawImage.texture =stickerVideoPlayer.texture;
        var defaultStickerSize = Screen.width / 5f; //calculate sticker size in pixels
        if (clip.width > clip.height)
        {
            rectTransformSticker.sizeDelta =
                new Vector2(defaultStickerSize, defaultStickerSize * clip.height / clip.width);
        }
        else
        {
            rectTransformSticker.sizeDelta =
                new Vector2(defaultStickerSize * clip.width / clip.height, defaultStickerSize);
        }


        //  stickerVideoPlayer.Play();
        stickerVideoPlayer.isLooping = true;
        stickerVideoPlayer.Prepare();
        stickerVideoPlayer.prepareCompleted += (VideoPlayer vp) =>
        {
            rawImage.texture = stickerVideoPlayer.texture;
            stickerVideoPlayer.Play();
        };
    }

    private void Move(Vector2 delta)
    {
       
        var localPosition = rectTransformSticker.localPosition;
        float x = Mathf.Clamp(localPosition.x + delta.x, -Screen.width / 2f, Screen.width / 2f);
        float y = Mathf.Clamp(localPosition.y + delta.y, -Screen.height / 2f, Screen.height / 2f);
        rectTransformSticker.localPosition = new Vector3(x, y);
        ;
    }

    private void Rotate(float angle)
    {
        rectTransformSticker.localRotation *= Quaternion.Euler(0f, 0f, angle * 2);
    }

    private void Pinch(float pinch)
    {
        Debug.Log("Pinch - " + pinch);
        float maxSize = Screen.width;
        float minSize = Screen.width / 8f;
        float width = Mathf.Clamp(rectTransformSticker.sizeDelta.x + pinch * 128, minSize, maxSize);
        var sizeDelta = rectTransformSticker.sizeDelta;
        float height = width * sizeDelta.y / sizeDelta.x;
        rectTransformSticker.sizeDelta = new Vector2(width, height);
    }

    //.......................................

    private float minScale = 0.1f;
    private float maxScale = 13f;
    private int scaleSpeed = 1;

    private void Pinching(float delta)
    {
        if (transform.localScale.x > minScale && transform.localScale.x < maxScale)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x <= minScale && delta > 0)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x >= maxScale && delta < 0)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x < minScale && delta < 0)
            transform.localScale = new Vector3(minScale, minScale, minScale);
        else if (transform.localScale.x > maxScale && delta > 0)
            transform.localScale = new Vector3(maxScale, maxScale, maxScale);
    }


    private int rotationSpeed = 1;

    private void Rotating(float angle)
    {
        gameObject.transform.rotation *= Quaternion.Euler(0, 0, (-angle * 360f / 60f) * rotationSpeed);
    }
}
