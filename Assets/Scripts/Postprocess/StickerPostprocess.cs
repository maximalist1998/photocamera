﻿using System.Collections;
using System.Collections.Generic;
using GameInput;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class StickerPostprocess : PostprocessElement
{
  //  public float rot;
    
    [SerializeField] private Image stickerImage;
    public int ID;
    private RectTransform rectTransformSticker {
        get
        {
            if (_rectTransformSticker == null)
                _rectTransformSticker = GetComponent<RectTransform>();
            return _rectTransformSticker;
        }
    }

    private RectTransform _rectTransformSticker ;
  

//    private  void Start()
//    {
//       activateButton.OnPointerDown.onClick.AddListener(() => { PostprocessController.Instance.activePostprocElement = this;});
//    }

    public override void Select()
    {
        base.Select();
        
  //      activateButton.enabled = false;
        MobileInputManager.Instance.OnMove += Move;
        //MobileInputManager.Instance.OnRotate += Rotate;
        //MobileInputManager.Instance.OnPinch += Pinch;

        MobileInputManager.Pinching += Pinching;
        MobileInputManager.Rotating += Rotating;
        

    }

    public override void Deselect()
    {
        base.Deselect();
 //       activateButton.enabled = true;
        MobileInputManager.Instance.OnMove -= Move;
        //MobileInputManager.Instance.OnRotate -= Rotate;
        //MobileInputManager.Instance.OnPinch -= Pinch;       
        
        MobileInputManager.Pinching -= Pinching;
        MobileInputManager.Rotating -= Rotating;
    }

    public void SetSprite(Sprite sprite,int id)
    {
        ID = id;
        stickerImage.sprite = sprite;
        var defaultStickerSize = Screen.width / 5f; //calculate sticker size in pixels
        if (sprite.rect.width > sprite.rect.height)
        {
            rectTransformSticker.sizeDelta = new Vector2(defaultStickerSize,defaultStickerSize * sprite.rect.height / sprite.rect.width);
        }
        else
        {
            rectTransformSticker.sizeDelta = new Vector2(defaultStickerSize * sprite.rect.width / sprite.rect.height,defaultStickerSize);
        }
    }

    private void Move(Vector2 delta)
    {
      
        var localPosition = rectTransformSticker.localPosition;
        float x = Mathf.Clamp(localPosition.x + delta.x,-Screen.width/2f,Screen.width/2f);
        float y = Mathf.Clamp(localPosition.y + delta.y,-Screen.height/2f,Screen.height/2f);
        rectTransformSticker.localPosition = new Vector3(x,y);;
    }

    private void Rotate(float angle)
    {
        rectTransformSticker.localRotation *= Quaternion.Euler(0f, 0f, angle*2);
    }
    private void Pinch(float pinch)
    {
        Debug.Log("Pinch - " + pinch);
        float maxSize = Screen.width;
        float minSize = Screen.width / 8f;
        float width = Mathf.Clamp(rectTransformSticker.sizeDelta.x + pinch*128, minSize, maxSize);
        var sizeDelta = rectTransformSticker.sizeDelta;
        float height = width*sizeDelta.y/sizeDelta.x;
        rectTransformSticker.sizeDelta = new Vector2(width,height);
    }

    //.......................................
    
    private float minScale = 0.1f;
    private float maxScale = 13f;
    private int scaleSpeed = 1;
    
    private void Pinching(float delta)
    {
        if (transform.localScale.x > minScale && transform.localScale.x < maxScale)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x <= minScale && delta > 0)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x >= maxScale && delta < 0)
            transform.localScale += Vector3.one * delta *scaleSpeed;
        else if (transform.localScale.x < minScale && delta < 0)
            transform.localScale = new Vector3(minScale, minScale, minScale);
        else if (transform.localScale.x > maxScale && delta > 0)
            transform.localScale = new Vector3(maxScale, maxScale, maxScale);
    }

    
    private int rotationSpeed = 1;
    private void Rotating(float angle)
    {
        gameObject.transform.rotation *= Quaternion.Euler(0, 0 ,(-angle * 360f / 60f) * rotationSpeed);
    }
}