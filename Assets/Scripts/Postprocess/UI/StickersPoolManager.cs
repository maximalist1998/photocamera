﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class StickersPoolManager : MonoBehaviour
{
    [SerializeField] private StickerUI stickerPrefab;
    [SerializeField] private VideoStickerUI videoStickerPrefab;
    [SerializeField] private Transform stickersParent;
    [SerializeField] private PaginationController paginationController;

    private List<StickerUI> stickersPool = new List<StickerUI>();
    private List<VideoStickerUI> videoStickersPool = new List<VideoStickerUI>();

   

    public void ShowStickers(List<Texture2D> stickersTexture2Ds, Action<int> onStickerSelected)
    {
        int stickersCount = stickersTexture2Ds.Count;
        int stickersDifference = stickersCount - stickersPool.Count;
        for (int i = 0; i < stickersDifference; i++)
        {
            var sticker = Instantiate(stickerPrefab, stickersParent);
            stickersPool.Add(sticker);
            if(sticker!= null&&paginationController!=null)
            paginationController.RegisterSticker(sticker.gameObject);
        }

        for (int i = 0; i < stickersPool.Count; i++)
        {
            if (i < stickersCount)
            {
                stickersPool[i].InitSticker(stickersTexture2Ds[i],i,onStickerSelected);
            }
            else
            {
                stickersPool[i].gameObject.SetActive(false);
            }
        }
        if(paginationController!=null)
        paginationController.ShowPage(1);
    }
    public void ShowVideoStickers(List<VideoClip> stickersClips, Action<int> onStickerSelected)
    {
        int stickersCount = stickersClips.Count;
        int stickersDifference = stickersCount - videoStickersPool.Count;
        for (int i = 0; i < stickersDifference; i++)
        {
            var sticker = Instantiate(videoStickerPrefab, stickersParent);
            videoStickersPool.Add(sticker);
            if(sticker!= null&&paginationController!=null)
            paginationController.RegisterSticker(sticker.gameObject);
        }

        for (int i = 0; i < videoStickersPool.Count; i++)
        {
            if (i < stickersCount)
            {
                videoStickersPool[i].InitSticker(stickersClips[i],i,onStickerSelected);
            }
            else
            {
                videoStickersPool[i].gameObject.SetActive(false);
            }
        }
    }
}