﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;

public class StickersPanelController : Singleton<StickersPanelController>
{
    [SerializeField] private Button shapeButton;
    [SerializeField] private CustomInputField searchField;
    [SerializeField] private Button emptySpaceButton;
    [SerializeField] private Text labelRecent;
    [SerializeField] private GameObject space1;
    [SerializeField] private GameObject recentStickers;
    [SerializeField] private Text categoryLabel;
    [SerializeField] private GameObject stickersAlbums;
    [SerializeField] private GameObject gifsStickersArtworks;
    [SerializeField] private GameObject space2;
    [SerializeField] private ScrollRect stickersScrollView;
    [SerializeField] private StickersPoolManager stickersPool;
    [SerializeField] private AlbumsPanelController albumsPanelController;
    [SerializeField] private Button backSearchButton;
    [SerializeField] private Button myAlbumsButton;
    [SerializeField] private Button myStickersButton;

    [SerializeField] private Animator panelAnimator;
    private bool isHalfOpened = false;
    private bool isFullOpened = false;
    private List<GameObject> panelElements = new List<GameObject>();

    private void Start()
    {
        myAlbumsButton.onClick.AddListener(ShowMyAlbumsPanel);
        myStickersButton.onClick.AddListener(ShowMyStickersPanel);
        emptySpaceButton.onClick.AddListener(HideStickersPanel);
        searchField.onInputSelected += ShowSearchPanel;
        shapeButton.onClick.AddListener(() =>
        {
            if (isHalfOpened && !isFullOpened)
                ShowMyStickersPanel();
            else if (isHalfOpened && isFullOpened)
            {
                ShowRecentStickersPanel();
            }
        });
        //  ShowRecentStickersPanel();
//ShowSearchPanel();
//ShowSearchPanel();
    }

    private void Awake()
    {
        panelElements.Add(albumsPanelController.gameObject);
        panelElements.Add(space1);
        panelElements.Add(space2);
        panelElements.Add(backSearchButton.gameObject);
        panelElements.Add(stickersScrollView.gameObject);
        panelElements.Add(categoryLabel.gameObject);
        panelElements.Add(recentStickers.gameObject);
        panelElements.Add(labelRecent.gameObject);
        panelElements.Add(gifsStickersArtworks.gameObject);
        panelElements.Add(stickersAlbums.gameObject);
    }


    private void ShowMyAlbumsPanel()
    {
        DisableAllPanelElementsExcept(stickersAlbums, albumsPanelController.gameObject, space1, space2);
        panelAnimator.SetBool("activateHalf", true);
        panelAnimator.SetBool("activateFull", true);
        isHalfOpened = true;
        isFullOpened = true;
    }

    public void ShowAlbumsInnerPanel(string albumName)
    {
        categoryLabel.text = albumName;
        DisableAllPanelElementsExcept(backSearchButton.gameObject, categoryLabel.gameObject,
            stickersScrollView.gameObject);
        panelAnimator.SetBool("activateHalf", true);
        panelAnimator.SetBool("activateFull", true);
        isHalfOpened = true;
        isFullOpened = true;
    }

    private void ShowSearchPanel()
    {
        DisableAllPanelElementsExcept(space1, space2, gifsStickersArtworks, stickersScrollView.gameObject);
        panelAnimator.SetBool("activateHalf", true);
        panelAnimator.SetBool("activateFull", true);
        isHalfOpened = true;
        isFullOpened = true;
    }

    private void ShowRecentStickersPanel()
    {
        DisableAllPanelElementsExcept( categoryLabel.gameObject,space2,
            stickersScrollView.gameObject);
        panelAnimator.SetBool("activateHalf", true);
        panelAnimator.SetBool("activateFull", false);
        isHalfOpened = true;
        isFullOpened = false;
    }

    private void ShowMyStickersPanel()
    {
        DisableAllPanelElementsExcept(categoryLabel.gameObject,space2,
            stickersScrollView.gameObject);
       // DisableAllPanelElementsExcept(stickersAlbums, stickersScrollView.gameObject, space1, space2);
        panelAnimator.SetBool("activateHalf", true);
        panelAnimator.SetBool("activateFull", true);
        isHalfOpened = true;
        isFullOpened = true;
    }

    private void DisableAllPanelElementsExcept(params GameObject[] activeElements)
    {
        foreach (var element in panelElements)
        {
            // Debug.Log("element - "+ element);
            bool active = false;
            foreach (var act in activeElements)
            {
                //  Debug.Log(act.name + " - name");
                if (act == element)
                {
                    //       Debug.Log("equal - " + act.name);
                    active = true;
                    break;
                }
            }

            if (element != null)
                element.SetActive(active);
            // Debug.Log("go - " + element.name + " active - " + active);
        }
    }

    public void ShowStickersPanel()
    {
        Debug.Log("ShowRecentStickersPanel");
        ShowRecentStickersPanel();
        emptySpaceButton.gameObject.SetActive(true);
    }

    public void HideImmediately()
    {
    //    panelAnimator.keepAnimatorControllerStateOnDisable = true;
        HideStickersPanel();
        //panelAnimator.gameObject.SetActive(false);
    }

    public void HideStickersPanel()
    {
        emptySpaceButton.gameObject.SetActive(false);
        panelAnimator.SetBool("activateHalf", false);
        panelAnimator.SetBool("activateFull", false);
        isHalfOpened = false;
        isFullOpened = false;
    }
}