﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class ControllPanelKeyboardPositioner : MonoBehaviour
{
    private RectTransform rectTransform;

    private Vector2 defaultAnchoredPosMin;

    private Vector2 defaultAnchoredPosMax;
    private Vector2 defaultSizeDelta;
    private Vector2 defaultPos;
    private float height = 0;
    // Start is called before the first frame update
#if UNITY_ANDROID || UNITY_IOS

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        defaultAnchoredPosMin = rectTransform.anchorMin;
        defaultAnchoredPosMax = rectTransform.anchorMax;
        defaultSizeDelta = rectTransform.sizeDelta;
        defaultPos = rectTransform.anchoredPosition;
    }

    // Update is called once per frame
    void Update()
    {
        var isKeyboardVisible = TouchScreenKeyboard.visible;
#if UNITY_IOS
         float keyboardHeight = TouchScreenKeyboard.area.y;
#else
        float keyboardHeight = GetKeyboardSize();
#endif
        if (Mathf.RoundToInt(keyboardHeight) == Mathf.RoundToInt(height)) return;
        height = keyboardHeight;
        Debug.Log("KEYBOARD active - " + isKeyboardVisible);
        if (isKeyboardVisible)
        {
            float yMin = defaultAnchoredPosMin.y + keyboardHeight / Screen.height;
            float yMax = defaultAnchoredPosMax.y + keyboardHeight / Screen.height;
            Debug.Log("Keyboard height - " + keyboardHeight);
            rectTransform.anchorMin = new Vector2(defaultAnchoredPosMin.x, yMin);
            rectTransform.anchorMax = new Vector2(defaultAnchoredPosMax.x, yMax);
        }
        else
        {
            rectTransform.anchorMax = defaultAnchoredPosMax;
            rectTransform.anchorMin = defaultAnchoredPosMin;
        }

        rectTransform.anchoredPosition = defaultPos;
        rectTransform.sizeDelta = defaultSizeDelta;

        Debug.Log("rectTransform  - " + rectTransform.anchorMax + ", " + rectTransform.anchorMin + ", rect - " +
                  rectTransform.rect);
    }

#if UNITY_ANDROID
    public int GetKeyboardSize()
    {
        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity")
                .Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");

            using (AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
            {
                View.Call("getWindowVisibleDisplayFrame", Rct);

                return Screen.height - Rct.Call<int>("height");
            }
        }
    }
#endif
#endif
}