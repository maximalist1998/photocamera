﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class VideoStickerUI : MonoBehaviour
{
    [SerializeField] private VideoPlayer player;
    [SerializeField] private Button button;
    [SerializeField] private RawImage rawImage;
     private RectTransform _rectTransform;
     private Coroutine checkingCor;
    private bool visible = false;
    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        
    }

//    private void OnEnable()
//    {
//        if(checkingCor!=null)
//            StopCoroutine(checkingCor);
//        checkingCor = StartCoroutine(CheckVisibility());
//    }
//
//    private void OnDisable()
//    {
//        if(checkingCor!=null)
//            StopCoroutine(checkingCor);
//        checkingCor = null;
//    }

    public void InitSticker(VideoClip sticker, int stickerId, Action<int> onStickerSelected)
    {
        player.clip = sticker;
        player.isLooping = true;
        player.Prepare();
        player.prepareCompleted += (VideoPlayer vp) =>
        {
            rawImage.texture = vp.texture;
           // vp.Play();
        };
       
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => { onStickerSelected?.Invoke(stickerId); });
        gameObject.SetActive(true);
    }

//    IEnumerator CheckVisibility()
//    {
//        while (true)
//        {
//            
//            yield return new WaitForSeconds(0.4f);
//            var positionY = _rectTransform.position.y;
//            bool currentVisible = positionY >= -_rectTransform.sizeDelta.y && positionY < Screen.height;
//            Debug.Log("currentVisible - " +currentVisible);
//            if(visible == currentVisible) yield return null;
//            visible = currentVisible;
//            if (visible)
//            {
//                if(player.isPlaying) yield return null;
//                Debug.Log("OnBecameVisible");
//                player.Play();
//            }
//            else
//            {
//                if(!player.isPlaying) yield return null;
//                Debug.Log("OnBecameINvisible");
//                player.Pause();
//            }
//        }
//    }

//    private void LateUpdate()
//    {
//        var positionY = _rectTransform.position.y;
//        bool currentVisible = positionY >= -_rectTransform.sizeDelta.y && positionY < Screen.height;
//        Debug.Log("currentVisible - " +currentVisible);
//        if(visible == currentVisible) return;
//        visible = currentVisible;
//        if (visible)
//        {
//            if(player.isPlaying) return;
//            Debug.Log("OnBecameVisible");
//            player.Play();
//        }
//        else
//        {
//            if(!player.isPlaying) return;
//            Debug.Log("OnBecameINvisible");
//            player.Stop();
//        }
//    }

//    private void OnBecameVisible()
//    {
//        Debug.Log("OnBecameVisible");
//        player.Play();
//    }
//
//    private void OnBecameInvisible()
//    {
//        Debug.Log("OnBecameINvisible");
//        player.Stop();
//    }
}
