﻿using System.Collections;
using System.Collections.Generic;
using GameInput;
using UnityEngine;
using UnityEngine.UI;

public class PaginationController : MonoBehaviour
{
    private List<GameObject> stickersUI;
    [SerializeField] private Button nextPageButton;
    [SerializeField] private Button prevPageButton;
    private int pageSize = 12;
    private int pages;
    private int currentPage;
    Vector2 startPosition;
    float startTime;
    private void Start()
    {
        nextPageButton.onClick.AddListener(ShowNextPage);
        prevPageButton.onClick.AddListener(ShowPrevPage);
        
    }

    private void OnEnable()
    {
        ShowPage(1);
        GlobalEvents.Instance.SwipeRight.AddListener( ShowPrevPage);
        GlobalEvents.Instance.SwipeLeft.AddListener( ShowNextPage);
    } 
    private void OnDisable()
    {
        GlobalEvents.Instance.SwipeRight.RemoveListener( ShowPrevPage);
        GlobalEvents.Instance.SwipeLeft.RemoveListener( ShowNextPage);
    }

    public void RegisterSticker(GameObject sticker)
    {
        if (stickersUI == null)
        {
            stickersUI = new List<GameObject>();
        }

        stickersUI.Add(sticker);
        pages = stickersUI.Count / pageSize;
        if (stickersUI.Count % pageSize != 0)
            pages++;
    }

    public void ShowNextPage()
    {
        ShowPage(currentPage+1);
    }
    public void ShowPrevPage()
    {
        ShowPage(currentPage-1);
    }

    public void ShowPage(int page)
    {
        
        var newPage = Mathf.Clamp(page, 1, pages);
        if (newPage == currentPage) return;
        
        currentPage = newPage;
        nextPageButton.gameObject.SetActive(currentPage<pages);
        prevPageButton.gameObject.SetActive(currentPage>1);
        for (int i = 0; i < stickersUI.Count; i++)
        {
            stickersUI[i].SetActive(i >= (currentPage - 1) * pageSize && i < currentPage * pageSize);
        }
    }
}