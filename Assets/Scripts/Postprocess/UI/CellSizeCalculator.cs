﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup))]
public class CellSizeCalculator : MonoBehaviour
{
    [SerializeField] private float aspect = 1;
    [SerializeField] private GridLayoutGroup additionaGridLayoutGroup; //TODO:hotfix need to be removed
    void Start()
    {
        var grid = GetComponent<GridLayoutGroup>();
        var rectTransform = GetComponent<RectTransform>();
        var count = grid.constraintCount;
        var spaceForColumns = rectTransform.rect.width - grid.spacing.x * (count - 1);
        float cellSize = spaceForColumns / count;
       Vector2 cellSizeGrid = new Vector2(cellSize, cellSize /aspect);
       grid.cellSize = cellSizeGrid;
       if (additionaGridLayoutGroup != null)
       {
           additionaGridLayoutGroup.cellSize = cellSizeGrid; //TODO:hotfix need to be removed
       }
    }
}