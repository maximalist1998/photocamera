﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsToggleGroup : MonoBehaviour
{
    [SerializeField] private List<ButtonWithText> buttons;
    [SerializeField] private Color activeButtonColor;
    [SerializeField] private Color inactiveButtonColor;
    [SerializeField] private Color activeButtonTextColor;
    [SerializeField] private Color inactiveButtonTextColor;

    // Start is called before the first frame update
    void Start()
    {
        foreach (var button in buttons)
        {
            button.button.onClick.AddListener(() => ActivateButton(button));
        }
    }

    private void ActivateButton(ButtonWithText button)
    {
        button.button.image.color = activeButtonColor;
        if (button.text != null)
        {
            button.text.color = activeButtonTextColor;
        }

        for (int i = 0; i < buttons.Count; i++)
        {
            if (buttons[i] == button) continue;
            buttons[i].button.image.color = inactiveButtonColor;
            if (buttons[i].text != null)
            {
                buttons[i].text.color = inactiveButtonTextColor;
            }
        }
    }
}

[Serializable]
public class ButtonWithText
{
    public Button button;
    public Text text;
}