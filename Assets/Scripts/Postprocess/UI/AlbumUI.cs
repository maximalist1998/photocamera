﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AlbumUI : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Button button;
    [SerializeField] private Text albumNameText;

    public void InitAlbum(Album album,Action<Album> onAlbumSelected)
    {
        if (image.sprite != null)
            DestroyImmediate(image.sprite);
        albumNameText.text = album.albumName;
        image.sprite = Sprite.Create(album.albumTexture,
            new Rect(0, 0, album.albumTexture.width, album.albumTexture.height), Vector2.one * 0.5f);
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => { onAlbumSelected?.Invoke(album); });
        gameObject.SetActive(true);
    }
}