﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlbumsPanelController : MonoBehaviour
{
    [SerializeField] private AlbumUI albumUIPrefab;
    [SerializeField] private Transform albumsParent;

    private List<AlbumUI> albumsPool = new List<AlbumUI>();

    public void InitAlbums(List<Album> albums, Action<Album> onAlbumSelected)
    {
        int albumsCount = albums.Count;
        int albumsDifference = albumsCount - albumsPool.Count;
        for (int i = 0; i < albumsDifference; i++)
        {
            var album = Instantiate(albumUIPrefab, albumsParent);
            albumsPool.Add(album);
        }

        for (int i = 0; i < albumsPool.Count; i++)
        {
            if (i < albumsCount)
            {
                albumsPool[i].InitAlbum(albums[i], onAlbumSelected);
            }
            else
            {
                albumsPool[i].gameObject.SetActive(false);
            }
        }
    }
}

[Serializable]

public class Album
{
    public string albumName;
    public Texture2D albumTexture;
}

