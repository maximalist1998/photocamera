﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StickerUI : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Button button;


    public void InitSticker(Texture2D stickerTexture2D, int stickerId, Action<int> onStickerSelected)
    {
        if (image.sprite != null)
            DestroyImmediate(image.sprite);
        image.sprite = Sprite.Create(stickerTexture2D,
            new Rect(0, 0, stickerTexture2D.width, stickerTexture2D.height), Vector2.one * 0.5f);
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => { onStickerSelected?.Invoke(stickerId); });
        gameObject.SetActive(true);
    }
}