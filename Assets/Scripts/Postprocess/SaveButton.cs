﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveButton : MonoBehaviour
{
    [SerializeField] private GameObject saveButtonPanel;
    [SerializeField] private Button saveButton;
    [SerializeField] private Button cancelButton;
    [SerializeField] private Button gifButton;
    [SerializeField] private Button videoButton;
    [SerializeField] private Image gifSeletor;
    [SerializeField] private Image videoSeletor;
    private bool isVideoMode = true;
    private bool isGifMode = false;

    private bool saveButtonOpened = false;

    // Start is called before the first frame update
    void Start()
    {
        saveButton.onClick.AddListener(OnClickSaveButton);
        cancelButton.onClick.AddListener(CloseSaveButtonMenu);
        gifButton.onClick.AddListener(SelectGif);
        videoButton.onClick.AddListener(SelectVideo);
    }

    private void OnClickSaveButton()
    {
        if (!saveButtonOpened)
        {
            if (PostprocessController.Instance.IsStaticImageResult())
            {
                PostprocessController.Instance.SaveVideo(new PostprocessController.SavingOptions(false, false));
            }
            else
            {
                OpenSaveButtonMenu();
            }

            return;
        }

        if (isGifMode || isVideoMode)
        {
            PostprocessController.Instance.SaveVideo(new PostprocessController.SavingOptions(isGifMode, isVideoMode));
            CloseSaveButtonMenu();
        }
    }

    public void CloseSaveButtonMenu()
    {
        saveButtonOpened = false;
        saveButtonPanel.SetActive(false);
        cancelButton.gameObject.SetActive(false);
    }

    private void OpenSaveButtonMenu()
    {
        saveButtonOpened = true;
        saveButtonPanel.SetActive(true);
        cancelButton.gameObject.SetActive(true);
    }

    private void SelectGif()
    {
        isGifMode = !isGifMode;
        gifSeletor.gameObject.SetActive(isGifMode);
    }

    private void SelectVideo()
    {
        isVideoMode = !isVideoMode;
        videoSeletor.gameObject.SetActive(isVideoMode);
    }
}