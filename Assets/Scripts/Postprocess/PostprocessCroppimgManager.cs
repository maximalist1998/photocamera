﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;

public class PostprocessCroppimgManager : Singleton<PostprocessCroppimgManager>
{
    private Texture2D holdedScreenshot;
    [SerializeField] private TestImgCropping imageCropper;
    [SerializeField] private Button croppingButton; 
    // Start is called before the first frame update
    private void OnEnable()
    {
        ScreenshotCompanion.OnTakeScreenshoot +=  HoldScreenshotLocal;
        croppingButton.onClick.AddListener(Crop);
    }

    private void OnDisable()
    {
        ScreenshotCompanion.OnTakeScreenshoot -= HoldScreenshotLocal;
        croppingButton.onClick.RemoveListener(Crop);

    }

//    private void CroppButtonListener()
//    {
//        Crop();
//        TestImgCropping.OnCroppingCanceled += OnCroppingCanceledListener;
//
//    }
//
//    private void OnCroppingCanceledListener()
//    {
//        TestImgCropping.OnCroppingCanceled -= OnCroppingCanceledListener;
//     //   PostprocessController.Instance.OpenPanel();
//    }

    public void HoldScreenshotLocal(Texture2D screenshot)
    {
        Debug.Log("HoldScreenshotLocal");
       // if(holdedScreenshot!= null)
      //  DestroyImmediate(holdedScreenshot,true);
      ClearHoldedImage();
        holdedScreenshot = screenshot;
    }

    public void ClearHoldedImage()
    {
        if(holdedScreenshot!= null)
            DestroyImmediate(holdedScreenshot);
    }

    private void Crop()
    {
        if (holdedScreenshot == null) return;
        PostprocessController.Instance.activePostprocElement = null;
        imageCropper.CropImage(holdedScreenshot);
        
        
    }

    public void Deactivate()
    {
        croppingButton.gameObject.SetActive(false);
       // gameObject.SetActive(false);
    }
    public void Activate()
    {
        croppingButton.gameObject.SetActive(true);
      //  gameObject.SetActive(true);
    }
}
