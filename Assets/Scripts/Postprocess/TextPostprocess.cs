﻿using System.Collections;
using System.Collections.Generic;
using GameInput;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class TextPostprocess : PostprocessElement
{
    //   public float rot;
    //   [SerializeField] private Button activateButton;
     public CustomInputField inputPostProc;

    private Vector3 currentPos;
    private Vector3 showedPosition;
    private bool ignoreMoving = false;
    public bool activateOnCreate = true;
    private RectTransform rectTransformText
    {
        get
        {
            if (_rectTransformText == null)
                _rectTransformText = GetComponent<RectTransform>();
            return _rectTransformText;
        }
    }

    private RectTransform _rectTransformText;


    private void Start()
    {
        showedPosition = new Vector3(0, Screen.height * 0.25f);
        UpdateTextSize();
        inputPostProc.onValueChanged.AddListener((eve) =>
        {
            UpdateTextSize();
        });
        if (activateOnCreate)
        {
        inputPostProc.ActivateInputField();
            
        transform.localPosition = showedPosition;
        }
        // activateButton.OnPointerDown.onClick.AddListener(() => { PostprocessController.Instance.activePostprocElement = this;});
    }

    private void UpdateTextSize()
    {
        int max = 6;
        if (inputPostProc.textComponent.cachedTextGenerator.lineCount > 1)
        {
            for (int i = 1; i < inputPostProc.textComponent.cachedTextGenerator.lineCount; i++)
            {
                int symbols = inputPostProc.textComponent.cachedTextGenerator.lines[i].startCharIdx -
                              inputPostProc.textComponent.cachedTextGenerator.lines[i - 1].startCharIdx;
                max = max < symbols ? symbols : max;
            }
        }
        else
        {
            max = max < inputPostProc.text.Length ? inputPostProc.text.Length : max;
        }

        rectTransformText.sizeDelta = new Vector2(Screen.width / 18f * max,
            (Screen.width / 7f) * Mathf.Clamp(inputPostProc.textComponent.cachedTextGenerator.lineCount, 1, 20));
    }

    private void OnEnable()
    {
        inputPostProc.onInputSelected += InputSelectingListener;
        //inputPostProc.onInputDeselected += InputDeselectingListener;
        inputPostProc.onKeyboardOpened += KeyboardOpenningListener;
        inputPostProc.onEndEdit.AddListener(InputDeselectingListener);
    }

    private void OnDisable()
    {
        inputPostProc.onInputSelected -= InputSelectingListener;
        inputPostProc.onEndEdit.RemoveListener(InputDeselectingListener);
        // inputPostProc.onInputDeselected -= InputDeselectingListener;
        inputPostProc.onKeyboardOpened -= KeyboardOpenningListener;
    }

    private void KeyboardOpenningListener()
    {
        currentPos = rectTransformText.localPosition;
        rectTransformText.localPosition = showedPosition;
        ignoreMoving = true;
    }

    private void InputSelectingListener()
    {
        if (PostprocessController.Instance.activePostprocElement != this)
            PostprocessController.Instance.activePostprocElement = this;
    }

    private void InputDeselectingListener(string s)
    {
        ignoreMoving = false;
        rectTransformText.localPosition = currentPos;
    }

    public override void Select()
    {
        base.Select();
        //      activateButton.enabled = false;
        MobileInputManager.Instance.OnMove += Move;
        //MobileInputManager.Instance.OnRotate += Rotate;
        //MobileInputManager.Instance.OnPinch += Pinch;
        MobileInputManager.Pinching += Pinching;
        MobileInputManager.Rotating += Rotating;
        TextPanel.Instance.ShowPanel(inputPostProc.textComponent);
    }

    public override void Deselect()
    {
        base.Deselect();
        //       activateButton.enabled = true;
        MobileInputManager.Instance.OnMove -= Move;
        //MobileInputManager.Instance.OnRotate -= Rotate;
        //MobileInputManager.Instance.OnPinch -= Pinch;
        MobileInputManager.Pinching -= Pinching;
        MobileInputManager.Rotating -= Rotating;
        TextPanel.Instance.HidePanel();
    }

//    public void SetTextSize()
//    {
//        stickerImage.sprite = sprite;
//        var defaultTextSize = Screen.width / 5f; //calculate text size in pixels
//        if (sprite.rect.width > sprite.rect.height)
//        {
//            rectTransformText.sizeDelta = new Vector2(defaultTextSize,defaultTextSize * sprite.rect.height / sprite.rect.width);
//        }
//        else
//        {
//            rectTransformText.sizeDelta = new Vector2(defaultTextSize * sprite.rect.width / sprite.rect.height,defaultTextSize);
//        }
//    }

    private void Move(Vector2 delta)
    {
        if(ignoreMoving) return;
        var localPosition = rectTransformText.localPosition;
        float x = Mathf.Clamp(localPosition.x + delta.x, -Screen.width / 2f, Screen.width / 2f);
        float y = Mathf.Clamp(localPosition.y + delta.y, -Screen.height / 2f, Screen.height / 2f);
        rectTransformText.localPosition = new Vector3(x, y);
    }

//    private void Rotate(float angle)
//    {
//        rectTransformText.localRotation *= Quaternion.Euler(0f, 0f, angle * 2);
//    }

//    private void Pinch(float pinch)
//    {
//        Debug.Log("Pinch - " + pinch);
//        float maxSize = Screen.width;
//        float minSize = Screen.width / 8f;
//        float width = Mathf.Clamp(rectTransformText.sizeDelta.x + pinch * 128, minSize, maxSize);
//        var sizeDelta = rectTransformText.sizeDelta;
//        float height = width * sizeDelta.y / sizeDelta.x;
//        rectTransformText.sizeDelta = new Vector2(width, height);
//    }


    //.......................................

    private float minScale = 0.1f;
    private float maxScale = 5f;
    private int scaleSpeed = 1;

    private void Pinching(float delta)
    {
        if (ignoreMoving)
        {
         return;
         
        }
        if (transform.localScale.x > minScale && transform.localScale.x < maxScale)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x <= minScale && delta > 0)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x >= maxScale && delta < 0)
            transform.localScale += Vector3.one * delta * scaleSpeed;
        else if (transform.localScale.x < minScale && delta < 0)
            transform.localScale = new Vector3(minScale, minScale, minScale);
        else if (transform.localScale.x > maxScale && delta > 0)
            transform.localScale = new Vector3(maxScale, maxScale, maxScale);
    }


    private int rotationSpeed = 1;

    private void Rotating(float angle)
    {
        if (ignoreMoving) return;
        gameObject.transform.rotation *= Quaternion.Euler(0, 0, (-angle * 360f / 60f) * rotationSpeed);
    }
}