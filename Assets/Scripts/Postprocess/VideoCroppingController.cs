﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoCroppingController : MonoBehaviour
{
  [SerializeField] private GameObject croppingPanel;

  public void StartCropping(VideoClip video)
  {
    croppingPanel.SetActive(true);
  }
}
