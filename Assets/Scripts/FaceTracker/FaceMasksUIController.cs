﻿using FaceTracker.DB;
using UnityEngine;
using UnityEngine.UI;

namespace FaceTracker
{
    public class FaceMasksUIController : MonoBehaviour
    {
        #region > Editor Fields

        [SerializeField]
        private Button deleteBtn;
        [SerializeField]
        private Button cancelBtn;

        #endregion > Editor Fields
        
        #region > Private Fields

        private MaskData currMask;
        
        #endregion > Private Fields
        
        #region > MonoBehaviour Callback

        private void Start()
        {
            deleteBtn.onClick.AddListener(DeleteBtnClickHandler);
        }
        
        private void OnEnable()
        {
            FaceMaskInstantiateController.OnMaskSelected += MaskSelectedHandler;
        }
        
        private void OnDisable()
        {
            FaceMaskInstantiateController.OnMaskSelected -= MaskSelectedHandler;
        }
        
        #endregion > MonoBehaviour Callback

        private void MaskSelectedHandler(MaskData newMask)
        {
            currMask = newMask;
            deleteBtn.gameObject.SetActive(currMask.IsLocalMask);
        }

        private void DeleteBtnClickHandler()
        {
            if (currMask.IsLocalMask)
            {
                FaceMasksData.Instance.DeleteLocalMaskByName(currMask.UniqueName);
                
            }
        }
    }
}