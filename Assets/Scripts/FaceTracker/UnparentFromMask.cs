﻿using FaceTracker;
using UnityEngine;

public class UnparentFromMask : MonoBehaviour
{
    #region > Editor Fields

    [SerializeField]
    private Vector3 position = Vector3.zero;
    [SerializeField]
    private Vector3 rotation = Vector3.zero;
    [SerializeField]
    private Vector3 scale = Vector3.one;
    
    #endregion > Editor Fields
    
    #region > Properties
    
    private FaceMaskController faceMaskController = null;
    public FaceMaskController FaceMaskController
    {
        get
        {
            if (faceMaskController == null)
                faceMaskController = GetComponentInParent<FaceMaskController>();

            return faceMaskController;
        }
    }
    
    #endregion > Properties

    #region > MonoBehaviour Callbacks
    
    private void Awake()
    {
        if (FaceMaskController == null)
            return;

        FaceMaskController.OnMaskActivated += ActivateChildren;
        FaceMaskController.OnMaskDeactivated += DeactivateChildren;
        FaceMaskController.OnMaskDestroyed += DestroyObj;
    }

    private void OnDestroy()
    {
        if (FaceMaskController != null)
        {
            FaceMaskController.OnMaskActivated -= ActivateChildren;
            FaceMaskController.OnMaskDeactivated -= DeactivateChildren;
            FaceMaskController.OnMaskDestroyed -= DestroyObj;
        }
    }

    private void Start()
    {
        if (FaceMaskController == null)
            return;

        Unparent();
    }
    
    #endregion > MonoBehaviour Callbacks

    private void Unparent()
    {
        transform.parent = null;
        transform.position = position;
        transform.rotation = Quaternion.Euler(rotation);
        transform.localScale = scale;
    }

    private void ActivateChildren()
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }
    
    private void DeactivateChildren()
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void DestroyObj()
    {
        DestroyImmediate(gameObject);
    }
}