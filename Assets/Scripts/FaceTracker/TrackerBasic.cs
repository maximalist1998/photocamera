﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using Executor;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;
using Utils;
#if PLATFORM_ANDROID && UNITY_2018_3_OR_NEWER
using UnityEngine.Android;

#endif

namespace FaceTracker
{
    public class TrackerBasic : Singleton<TrackerBasic>
    {
        #region > Events

        public static event Action<int, float[]> OnActionUnits;
        public static event Action<int> OnFaceUpdated;
        public static event Action<int> OnFaceStopTracking;

        #endregion > Events

        #region > Editor Fields

        #if UNITY_EDITOR
        public int deviceOrientation = 0;
        #endif

        public Camera Camera;

        [Header("Tracker configuration settings")]
        public string ConfigFileEditor;

        public string ConfigFileIOS;
        public string ConfigFileAndroid;

        [Header("Tracking settings")] public const int MAX_FACES = 4;

        [Header("Tracker output data info")] public int[] TrackerStatus = new int[MAX_FACES];

        [Header("Camera settings")] public Material CameraViewMaterial;
        public float CameraFocus;
        public int Orientation = 0;
        public int isMirrored = 1;
        public int camDeviceId = 0;
        public int defaultCameraWidth = -1;
        public int defaultCameraHeight = -1;


        [Header("Texture settings")] public int ImageWidth = 800;
        public int ImageHeight = 600;
        public int TexWidth = 512;
        public int TexHeight = 512;

        [Header("UI")] public Button ChangeCameraBtn;
        public GameObject waitingPanel;

        #endregion > Editor Fields

        #region > Private Fields

        private bool trackerInited = false;


        //Tracker output data info
        private bool isTracking = false;

        //Camera Settings
        private int currentOrientation = 0;
        private int currentMirrored = 1;
        private int AndroidCamDeviceId = 0;
        private int currentCamDeviceId = 0;
        private bool doSetupMainCamera = true;
        private bool camInited = false;

        //Texture settings
#if UNITY_ANDROID
        private TextureFormat TexFormat = TextureFormat.RGB24;
#else
        private TextureFormat TexFormat = TextureFormat.RGBA32;
#endif
        private Texture2D texture = null;
        private Color32[] texturePixels;
        private GCHandle texturePixelsHandle;

        [HideInInspector] public bool frameForAnalysis = false;
        public bool frameForRecog = false;


#if UNITY_ANDROID
        private AndroidJavaObject androidCameraActivity;
        private bool AppStarted = false;
        private AndroidJavaClass unity;
#endif

        private bool isAppPaused = false;

        #endregion > Private Fields

        #region > MonoBehaviour Callbacks

        protected virtual void Awake()
        {
//#if PLATFORM_ANDROID && UNITY_2018_3_OR_NEWER
//            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
//                Permission.RequestUserPermission(Permission.Camera);
//#endif


#if UNITY_ANDROID
            Unzip();
#endif

            string licenseFilePath = Application.streamingAssetsPath + "/" + "/Visage Tracker/";

            // Set license path depending on platform
            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
                    licenseFilePath = "Data/Raw/Visage Tracker/";
                    break;
                case RuntimePlatform.Android:
                    licenseFilePath = Application.persistentDataPath + "/";

                    break;
                case RuntimePlatform.OSXPlayer:
                    licenseFilePath = Application.dataPath + "/Resources/Data/StreamingAssets/Visage Tracker/";
                    break;
                case RuntimePlatform.OSXEditor:
                    licenseFilePath = Application.dataPath + "/StreamingAssets/Visage Tracker/";
                    break;
                case RuntimePlatform.WindowsEditor:
                    licenseFilePath = Application.streamingAssetsPath + "/Visage Tracker/";
                    break;
            }

            bool licFileExists = false;

            //checking if license key file exists in StreamingAssets/Visage Tracker
#if UNITY_IPHONE && !UNITY_EDITOR
	        var info = new FileInfo(Path.Combine(licenseFilePath, LicenseString.licenseString));
	        if (info.Exists == false)
				licFileExists = true;
#else
            if (File.Exists(Path.Combine(licenseFilePath, LicenseString.licenseString)))
                licFileExists = true;
#endif

            if (licFileExists)
            {
                VisageTrackerNative._initializeLicense(Path.Combine(licenseFilePath, LicenseString.licenseString));
            }
            else //case when license key is embedded
            {
                VisageTrackerNative._initializeLicense(LicenseString.licenseString);
            }

            ChangeCameraBtn.onClick.AddListener(ChangeCamera);
        }

        protected virtual void Start()
        {
            // Set configuration file path and name depending on a platform
            string configFilePath = Application.streamingAssetsPath + "/" + ConfigFileIOS;

            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
                    configFilePath = "Data/Raw/Visage Tracker/" + ConfigFileIOS;
                    break;
                case RuntimePlatform.Android:
                    configFilePath = Application.persistentDataPath + "/" + ConfigFileAndroid;
                    break;
                case RuntimePlatform.OSXPlayer:
                    configFilePath = Application.dataPath + "/Resources/Data/StreamingAssets/Visage Tracker/" +
                                     ConfigFileEditor;
                    break;
                case RuntimePlatform.OSXEditor:
                    configFilePath = Application.dataPath + "/StreamingAssets/Visage Tracker/" + ConfigFileEditor;
                    break;
                case RuntimePlatform.WindowsEditor:
                    configFilePath = Application.streamingAssetsPath + "/" + ConfigFileEditor;
                    break;
            }

            // Initialize tracker with configuration and MAX_FACES
            trackerInited = InitializeTracker(configFilePath);

            // Get current device orientation
            Orientation = GetDeviceOrientation();

            // Open camera in native code
            camInited = OpenCamera(Orientation, camDeviceId, defaultCameraWidth, defaultCameraHeight, isMirrored);

            if (SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLCore)
                Debug.Log("Notice: if graphics API is set to OpenGLCore, the texture might not get properly updated.");

            CoroutineExecutor.Instance.Execute(MicRequestCor());
        }

        private IEnumerator MicRequestCor()
        {
            if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
                yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);

            yield return null;
        }

        protected virtual void Update()
        {
            //signals analysis and recognition to stop if camera or tracker are not initialized and until new frame and tracking data are obtained
            frameForAnalysis = false;
            frameForRecog = false;

            if (!IsTrackerReady())
                return;

            if (isTracking)
            {
#if UNITY_ANDROID
                if (VisageTrackerNative._frameChanged())
                {
                    texture = null;
                    doSetupMainCamera = true;
                }
#endif
                Orientation = GetDeviceOrientation();
#if UNITY_EDITOR
                Orientation = deviceOrientation;
#endif

                // Check if orientation or camera device changed
                if (currentOrientation != Orientation || currentCamDeviceId != camDeviceId ||
                    currentMirrored != isMirrored)
                {
                    currentCamDeviceId = camDeviceId;
                    currentOrientation = Orientation;
                    currentMirrored = isMirrored;

                    // Reopen camera with new parameters 
                    OpenCamera(currentOrientation, currentCamDeviceId, defaultCameraWidth, defaultCameraHeight,
                        currentMirrored);
                    texture = null;
                    doSetupMainCamera = true;
                }

                // grab current frame and start face tracking
                VisageTrackerNative._grabFrame();

                VisageTrackerNative._track();
                VisageTrackerNative._getTrackerStatus(TrackerStatus);

                //After the track has been preformed on the new frame, the flags for the analysis and recognition are set to true
                frameForAnalysis = true;
                frameForRecog = true;

                // Set main camera field of view based on camera information
                if (doSetupMainCamera)
                {
                    // Get camera information from native
                    VisageTrackerNative._getCameraInfo(out CameraFocus, out ImageWidth, out ImageHeight);
                    var aspect = ImageWidth / (float) ImageHeight;
                    var yRange = (ImageWidth > ImageHeight) ? 1.0f : 1.0f / aspect;
#if !UNITY_ANDROID ||UNITY_EDITOR

                    //Ivan: Need recalculate according to rotation
                    if (Orientation == 1 || Orientation == 3)
                    {
                        aspect = ImageHeight / (float) ImageWidth;
                        yRange = (ImageHeight > ImageWidth) ? 1.0f : 1.0f / aspect;
                    }
#endif
                    Camera.fieldOfView = Mathf.Rad2Deg * 2.0f * Mathf.Atan(yRange / CameraFocus);
                    doSetupMainCamera = false;
                }
            }

            RefreshImage();

            for (int i = 0; i < MAX_FACES; ++i)
            {
                if (i < TrackerStatus.Length && TrackerStatus[i] == (int) TrackStatus.OK)
                {
                    OnFaceUpdated?.Invoke(i);

                    var actions = new float[23];
                    VisageTrackerNative._getActionUnitValues(actions, i);
                    OnActionUnits?.Invoke(i, actions);
                }
                else
                {
                    OnFaceStopTracking?.Invoke(i);
                }
            }
        }

        protected virtual void OnDestroy()
        {
#if UNITY_ANDROID
            this.androidCameraActivity.Call("closeCamera");
#else
            camInited = !(VisageTrackerNative._closeCamera());
#endif
        }
#if !UNITY_ANDROID
        
        void OnApplicationPause(bool pauseStatus)
        {
            isAppPaused = pauseStatus;

            if (isAppPaused)
            {
                //Activate loading panel
                waitingPanel.SetActive(true);

                //Stop camera and reset camera preview image
                TextureUtils.SetColorToTexture(texture, Color.black);
                #if UNITY_ANDROID
                this.androidCameraActivity.Call("closeCamera");
                #else
                VisageTrackerNative._closeCamera();
                #endif
            }
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus && isAppPaused)
            {
                isAppPaused = false;
                OpenCamera(currentOrientation, currentCamDeviceId, defaultCameraWidth, defaultCameraHeight,
                    currentMirrored);
                TextureUtils.SetColorToTexture(texture, Color.black);

                DestroyImmediate(texture);
                texture = null;

                //Deactivate waiting animation for 2.5 seconds
                CoroutineExecutor.Instance.Execute(DeactivateWaitingAfterDelay(2.5f));
            }
        }
#endif

        private IEnumerator DeactivateWaitingAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);

            waitingPanel.SetActive(false);
        }

        #endregion > MonoBehaviour Callbacks

        #region > Initialization

        private bool InitializeTracker(string config)
        {
            Debug.Log("Visage Tracker: Initializing tracker with config: '" + config + "'");

//			#if (UNITY_IPHONE || UNITY_ANDROID) && UNITY_EDITOR
//			    return false;
//			#endif

#if UNITY_ANDROID
            Shader shader = Shader.Find("Unlit/Texture");
            CameraViewMaterial.shader = shader;

            // initialize visage vision
            VisageTrackerNative._loadVisageVision();

            unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            this.androidCameraActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
#elif UNITY_WEBGL
			var shader = Shader.Find("Custom/RGBATex");
			CameraViewMaterial.shader = shader;
#else
//			var shader = Shader.Find("Custom/BGRATex");
            var shader = Shader.Find("Unlit/UnlitBGRA");
            CameraViewMaterial.shader = shader;
#endif

            VisageTrackerNative._initTracker(config, MAX_FACES);
            return true;
        }

        /// <summary>
        /// Get current device orientation.
        /// </summary>
        /// <returns>Returns an integer:
        /// <list type="bullet">
        /// <item><term>0 : DeviceOrientation.Portrait</term></item>
        /// <item><term>1 : DeviceOrientation.LandscapeRight</term></item>
        /// <item><term>2 : DeviceOrientation.PortraitUpsideDown</term></item>
        /// <item><term>3 : DeviceOrientation.LandscapeLeft</term></item>
        /// </list>
        /// </returns>
        private int GetDeviceOrientation()
        {
            int devOrientation;

//#if UNITY_ANDROID
//            //Device orientation is obtained in AndroidCameraPlugin so we only need information about whether orientation is changed
//            int oldWidth = ImageWidth;
//            int oldHeight = ImageHeight;
//
//            VisageTrackerNative._getCameraInfo(out CameraFocus, out ImageWidth, out ImageHeight);
//
//            if ((oldWidth != ImageWidth || oldHeight != ImageHeight) && ImageWidth != 0 && ImageHeight != 0 &&
//                oldWidth != 0 && oldHeight != 0)
//                devOrientation = (Orientation == 1) ? 0 : 1;
//            else
//                devOrientation = Orientation;
//#else
            if (Input.deviceOrientation == DeviceOrientation.Portrait)
                devOrientation = 0;
            else if (Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
                devOrientation = 2;
            else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
                devOrientation = 3;
            else if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
                devOrientation = 1;
            else if (Input.deviceOrientation == DeviceOrientation.FaceUp)
                devOrientation = Orientation;
            else if (Input.deviceOrientation == DeviceOrientation.Unknown)
                devOrientation = Orientation;
            else
                devOrientation = 0;
//#endif

            return devOrientation;
        }

        /// <summary> 
        /// Open camera from native code. 
        /// </summary>
        /// <param name="orientation">Current device orientation:
        /// <list type="bullet">
        /// <item><term>0 : DeviceOrientation.Portrait</term></item>
        /// <item><term>1 : DeviceOrientation.LandscapeRight</term></item>
        /// <item><term>2 : DeviceOrientation.PortraitUpsideDown</term></item>
        /// <item><term>3 : DeviceOrientation.LandscapeLeft</term></item>
        /// </list>
        /// </param>
        /// <param name="camDeviceId">ID of the camera device.</param>
        /// <param name="width">Desired width in pixels (pass -1 for default 800).</param>
        /// <param name="height">Desired width in pixels (pass -1 for default 600).</param>
        /// <param name="isMirrored">true if frame is to be mirrored, false otherwise.</param>
        private bool OpenCamera(int orientation, int cameraDeviceId, int width, int height, int isMirrored)
        {
            #if UNITY_ANDROID
            if (cameraDeviceId == AndroidCamDeviceId && AppStarted)
                return false;

            AndroidCamDeviceId = cameraDeviceId;
            //camera needs to be opened on main thread
            this.androidCameraActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                this.androidCameraActivity.Call("closeCamera");
                this.androidCameraActivity.Call("GrabFromCamera", width, height, camDeviceId);
            }));
            AppStarted = true;
            return true;
            #else
            VisageTrackerNative._openCamera(orientation, cameraDeviceId, width, height, isMirrored);
            return true;
            #endif
        }

        #endregion > Initialization

        public void ChangeCamera()
        {
            camDeviceId = (camDeviceId == 1) ? 0 : 1;
        }

        #region > Helpers

        /// <summary>
        /// Update Unity texture with frame data from native camera.
        /// </summary>
        private void RefreshImage()
        {
            // Initialize texture
            if (texture == null && isTracking && ImageWidth > 0)
            {
                TexWidth = Convert.ToInt32(Math.Pow(2.0, Math.Ceiling(Math.Log(ImageWidth) / Math.Log(2.0))));
                TexHeight = Convert.ToInt32(Math.Pow(2.0, Math.Ceiling(Math.Log(ImageHeight) / Math.Log(2.0))));
                texture = new Texture2D(TexWidth, TexHeight, TexFormat, false);

                var cols = texture.GetPixels32();
                for (var i = 0; i < cols.Length; i++)
                    cols[i] = Color.black;

                texture.SetPixels32(cols);
                texture.Apply(false);

                CameraViewMaterial.SetTexture("_MainTex", texture);

#if UNITY_STANDALONE_WIN
				// "pin" the pixel array in memory, so we can pass direct pointer to it's data to the plugin,
				// without costly marshaling of array of structures.
				texturePixels = ((Texture2D)texture).GetPixels32(0);
				texturePixelsHandle = GCHandle.Alloc(texturePixels, GCHandleType.Pinned);
#endif
            }

            if (texture != null && isTracking && TrackerStatus[0] != (int) TrackStatus.OFF)
            {
#if UNITY_STANDALONE_WIN
				// send memory address of textures' pixel data to VisageTrackerUnityPlugin
				VisageTrackerNative._setFrameData(texturePixelsHandle.AddrOfPinnedObject());
				((Texture2D)texture).SetPixels32(texturePixels, 0);
				((Texture2D)texture).Apply();
#elif UNITY_IPHONE || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_ANDROID
                if (SystemInfo.graphicsDeviceVersion.StartsWith("Metal"))
                    VisageTrackerNative._bindTextureMetal(texture.GetNativeTexturePtr());
                else
                    VisageTrackerNative._bindTexture((int) texture.GetNativeTexturePtr());
#elif UNITY_WEBGL
				VisageTrackerNative._bindTexture(texture.GetNativeTexturePtr());
#endif
            }
        }

        private bool IsTrackerReady()
        {
            isTracking = camInited && trackerInited;
            return isTracking;
        }

#if UNITY_ANDROID
        private void Unzip()
        {
            string[] pathsNeeded =
            {
                "candide3.fdp",
                "candide3.wfm",
                "331-948-342-847-229-159-118-987-828-386-854.vlc",
                "jk_300.fdp",
                "jk_300.wfm",
                "Head Tracker.cfg",
                "Facial Features Tracker - High.cfg",
                "Face Detector.cfg",
                "visage_powered.png",
                "warning.png",
                "bdtsdata/FF/ff.dat",
                "bdtsdata/LBF/lv",
                "bdtsdata/LBF/vfadata/ad/ad0.lbf",
                "bdtsdata/LBF/vfadata/ad/ad1.lbf",
                "bdtsdata/LBF/vfadata/ad/ad2.lbf",
                "bdtsdata/LBF/vfadata/ad/ad3.lbf",
                "bdtsdata/LBF/vfadata/ad/ad4.lbf",
                "bdtsdata/LBF/vfadata/ad/ae.bin",
                "bdtsdata/LBF/vfadata/ad/regressor.lbf",
                "bdtsdata/LBF/vfadata/ed/ed0.lbf",
                "bdtsdata/LBF/vfadata/ed/ed1.lbf",
                "bdtsdata/LBF/vfadata/ed/ed2.lbf",
                "bdtsdata/LBF/vfadata/ed/ed3.lbf",
                "bdtsdata/LBF/vfadata/ed/ed4.lbf",
                "bdtsdata/LBF/vfadata/ed/ed5.lbf",
                "bdtsdata/LBF/vfadata/ed/ed6.lbf",
                "bdtsdata/LBF/vfadata/gd/gd.lbf",
                "bdtsdata/NN/fa.lbf",
                "bdtsdata/NN/fc.lbf",
                "bdtsdata/NN/fr.bin",
                "bdtsdata/NN/pr.bin",
                "bdtsdata/NN/is.bin",
                "bdtsdata/NN/model.bin",
                "bdtsdata/LBF/ye/lp11.bdf",
                "bdtsdata/LBF/ye/W",
                "bdtsdata/LBF/ye/landmarks.txt", "dev.vlc"
            };
            string outputDir;
            string localDataFolder = "Visage Tracker";

            outputDir = Application.persistentDataPath;

            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }

            foreach (string filename in pathsNeeded)
            {
                WWW unpacker = new WWW("jar:file://" + Application.dataPath + "!/assets/" + localDataFolder + "/" +
                                       filename);

                while (!unpacker.isDone)
                {
                }

                if (!string.IsNullOrEmpty(unpacker.error))
                {
                    continue;
                }

                if (filename.Contains("/"))
                {
                    string[] split = filename.Split('/');
                    string name = "";
                    string folder = "";
                    string curDir = outputDir;

                    for (int i = 0; i < split.Length; i++)
                    {
                        if (i == split.Length - 1)
                        {
                            name = split[i];
                        }
                        else
                        {
                            folder = split[i];
                            curDir = curDir + "/" + folder;
                        }
                    }

                    if (!Directory.Exists(curDir))
                    {
                        Directory.CreateDirectory(curDir);
                    }

                    File.WriteAllBytes("/" + curDir + "/" + name, unpacker.bytes);
                }
                else
                {
                    File.WriteAllBytes("/" + outputDir + "/" + filename, unpacker.bytes);
                }
            }
        }
#endif

        #endregion > Helpers
    }
}