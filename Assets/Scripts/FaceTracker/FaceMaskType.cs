﻿namespace FaceTracker
{
    public enum FaceMaskType
    {
        None = 0,
        Mask = 1,
        Object = 2
    }
}