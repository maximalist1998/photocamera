﻿using System;
using System.IO;
using ActionUnits;
using UnityEngine;
using Object = System.Object;

namespace FaceTracker
{
	[RequireComponent(typeof(FaceActionUnitsController))]
    public class FaceMaskController : MonoBehaviour
    {
	    #region > Public Fields

	    public FaceMaskType MaskType = FaceMaskType.None;
	    public Vector3 Translation = new Vector3(); 
	    public Vector3 Rotation = new Vector3();
	    [SerializeField]
	    private int faceMaskIndex = 0;
	    public int FaceMaskIndex
	    {
		    get { return faceMaskIndex; }
		    set
		    {
			    faceMaskIndex = value;
			    OnFaceChanged?.Invoke(faceMaskIndex);
		    }
	    }

	    #endregion > Public Fields
	    
	    #region > Private Fields
	    
	    // Mesh information
	    private const int MaxVertices = 1024;
	    private const int MaxTriangles = 2048;

	    private int VertexNumber = 0;
	    private int TriangleNumber = 0;
	    private Vector2[] TexCoords = { };
	    private Vector3[] Vertices;
	    private int[] Triangles = { };
	    private float[] vertices = new float[MaxVertices * 3];
	    private int[] triangles = new int[MaxTriangles * 3];
	    private float[] texCoords = new float[MaxVertices * 2];
	    private MeshFilter meshFilter;
	    private MeshCollider meshCollider;
	    private Vector2[] modelTexCoords;
	    private bool texCoordsStaticLoaded = false;
	    
	    //Tracker output data info
	    private float[] translation = new float[3];
	    private float[] rotation = new float[3];
	    
	    private Vector3 startingPosition;
	    private Vector3 startingRotation;

	    private GameObject defaultMaskPrefab = null;
	    private GameObject DefaultMaskPrefab
	    {
		    get
		    {
			    if (defaultMaskPrefab == null)
				    defaultMaskPrefab = Resources.Load<GameObject>("VisageDefaultMask/default");

			    return defaultMaskPrefab;
		    }
	    }
	    
	    private MeshFilter defaultMaskMeshFilter = null;
	    private MeshFilter DefaultMaskMeshFilter
	    {
		    get
		    {
			    if (defaultMaskMeshFilter == null)
				    defaultMaskMeshFilter = DefaultMaskPrefab.GetComponent<MeshFilter>();

			    return defaultMaskMeshFilter;
		    }
	    }
	    
	    #endregion > Private Fields
	    
	    #region > Evenst

	    public event Action<int> OnFaceChanged;
	    public event Action OnMaskDestroyed;
	    public event Action OnMaskActivated;
	    public event Action OnMaskDeactivated;
	    
	    #endregion > Evenst
	    
	    #region > MonoBehaviour Callbacks
	    
	    private void OnEnable()
	    {
		    TrackerBasic.OnFaceUpdated += FaceUpdatedHandler;
		    TrackerBasic.OnFaceStopTracking += FaceStopTrackingHandler;
	    }
	    
	    private void OnDisable()
	    {
		    TrackerBasic.OnFaceUpdated -= FaceUpdatedHandler;
		    TrackerBasic.OnFaceStopTracking -= FaceStopTrackingHandler;
	    }

	    private void Awake()
	    {
		    startingPosition = transform.position;
		    startingRotation = transform.rotation.eulerAngles;

		    // Create an empty mesh and load tiger texture coordinates
		    meshFilter = GetComponent<MeshFilter>();
		    meshCollider = GetComponent<MeshCollider>();
		    if (meshFilter != null && MaskType == FaceMaskType.Mask)
		    {
			    meshFilter.mesh = new Mesh();
			    if (meshCollider != null)
				    meshCollider.sharedMesh = meshFilter.mesh;
		    }
		    
		    HideControllableObject();
	    }

	    private void OnDestroy()
	    {
		    OnMaskDestroyed?.Invoke();
	    }

	    #endregion > MonoBehaviour Callbacks

	    private void FaceUpdatedHandler(int maskNum)
	    {
		    if(FaceMaskIndex == maskNum)
			    UpdateControllableObject();
	    }
	    
	    private void FaceStopTrackingHandler(int maskNum)
	    {
		    if(FaceMaskIndex == maskNum)
			    HideControllableObject();
	    }
	    
        private void UpdateControllableObject()
	    { 
		    if(!texCoordsStaticLoaded)
		    {
			    texCoordsStaticLoaded = GetTextureCoordinates(out modelTexCoords);
		    }
		    
	        TriangleNumber = VisageTrackerNative._getFaceModelTriangleCount();
	        VertexNumber = VisageTrackerNative._getFaceModelVertexCount();
	        if(meshFilter != null)
				meshFilter.mesh.Clear();

	        transform.position -= new Vector3(0, 0, 10000);

			// update translation and rotation
			VisageTrackerNative._getHeadTranslation(translation, FaceMaskIndex);
	        VisageTrackerNative._getHeadRotation(rotation, FaceMaskIndex);
	        
	        Translation = new Vector3(translation[0], translation[1], translation[2]);
	        Rotation = new Vector3(rotation[0], rotation[1], rotation[2]);
	        var rotMultiplier = Quaternion.identity;
	        
	        Transform3DData();
	        
	        //Recalculate mask mesh
	        if (MaskType == FaceMaskType.Mask)
	        {
		        VisageTrackerNative._getFaceModelVertices(vertices, FaceMaskIndex);
		        VisageTrackerNative._getFaceModelTriangles(triangles, FaceMaskIndex);
	                
		        // Get mesh vertices
		        if (Vertices == null || Vertices.Length != VertexNumber)
			        Vertices = new Vector3[VertexNumber];

		        for (int j = 0; j < VertexNumber; j++)
		        {
			        Vertices[j] = new Vector3(vertices[j * 3 + 0], vertices[j * 3 + 1], vertices[j * 3 + 2]);
		        }

		        // Get mesh triangles
		        if (Triangles.Length != TriangleNumber)
			        Triangles = new int[TriangleNumber * 3];

		        for (int j = 0; j < TriangleNumber * 3; j++)
		        {
			        Triangles[j] = triangles[j];
		        }

		        // Get mesh texture coordinates
		        if (TexCoords.Length != VertexNumber)
			        TexCoords = new Vector2[VertexNumber];

		        for (int j = 0; j < VertexNumber; j++)
		        {
			        TexCoords[j] = new Vector2(modelTexCoords[j].x, modelTexCoords[j].y);
		        }

//		        var mesh = meshFilter.mesh;
//		        mesh.vertices = Vertices; //needs to be obtained for each face
//		        mesh.triangles = Triangles; //not changing 
//		        mesh.uv = TexCoords; // tiger texture coordinates
//		        mesh.uv2 = TexCoords; // tiger texture coordinates
//		        mesh.RecalculateNormals();
//		        mesh.RecalculateBounds();

		        var mesh = meshFilter.mesh;
//		        mesh.vertices = TestMaskObjectCreating.Instance.Vertices.ToArray(); //needs to be obtained for each face
		        mesh.vertices = Vertices; //needs to be obtained for each face
		        mesh.triangles = Triangles; //not changing 
		        mesh.uv = TexCoords; // tiger texture coordinates
		        mesh.uv2 = TexCoords; // tiger texture coordinates
		        mesh.RecalculateNormals();
		        mesh.RecalculateBounds();
//		        var savePath = Path.Combine(Application.dataPath, "FaceObject.obj");
//		        ObjExporter.MeshToFile(meshFilter, savePath);
	        }
	        
	        // Update position
	        transform.position = startingPosition + Translation;
	        transform.rotation = Quaternion.Euler(startingRotation + Rotation) * rotMultiplier;
	        
	        OnMaskActivated?.Invoke();
	    }

        private void HideControllableObject()
        {
	        Translation = new Vector3(0, 0, -10000);
	        Rotation = new Vector3(0, 0, 0);
	        transform.position = startingPosition + Translation;
	        transform.rotation = Quaternion.Euler(startingRotation + Rotation);
	        
	        OnMaskDeactivated?.Invoke();
        }
        
        #region > Helpers
        
        /// <summary>
        /// Helper function for transforming data obtained from tracker
        /// </summary>
        public void Transform3DData()
        {
	        Translation.x *= (-1);
	        Rotation.x = 180.0f * Rotation.x / 3.14f;
	        Rotation.y += 3.14f;
	        Rotation.y = 180.0f * (-Rotation.y) / 3.14f;
	        Rotation.z = 180.0f * (-Rotation.z) / 3.14f;
        }
        
        /// <summary>
        /// Loads static texture coordinates from the plugin.
        /// </summary>
        /// <returns>Returns true on successful load, false otherwise.</returns>
        private bool GetTextureCoordinates(out Vector2[] texCoords)
        {      
	        int texCoordsNumber;
	        float[] buffer = new float[1024];
	        VisageTrackerNative._getTexCoordsStatic(buffer, out texCoordsNumber);

	        texCoords = new Vector2[texCoordsNumber / 2];
	        for (int i = 0; i < texCoordsNumber / 2; i++)
	        {
		        texCoords[i] = new Vector2(buffer[i * 2], buffer[i * 2 + 1]);
	        }

	        return texCoordsNumber > 0;
        }
        
        #endregion > Helpers
    }
}