﻿using UnityEngine;

namespace ActionUnits
{
    public class FaceEffectController : MonoBehaviour
    {
        public FaceAnimationAction[] FaceActions;
        public EffectActivator Effect;

        private bool isActivated = false;
        
        private FaceActionUnitsController faceActionUnitsController = null;
        private FaceActionUnitsController FaceActionUnitsController
        {
            get
            {
                if (faceActionUnitsController == null)
                    faceActionUnitsController = GetComponent<FaceActionUnitsController>();

                return faceActionUnitsController;
            }
        }

        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            FaceActionUnitsController.OnActionUnitsChanged += ActionUnitsUpdatedHandler;
        }
        
        private void OnDisable()
        {
            FaceActionUnitsController.OnActionUnitsChanged -= ActionUnitsUpdatedHandler;
        }

        #endregion > MonoBehaviour Callbacks

        private void ActionUnitsUpdatedHandler()
        {
            if (IsActionsWork() && !isActivated)
            {
                Effect.Activate();
                isActivated = true;
            }
            else if (!IsActionsWork() && isActivated)
            {
                Effect.Deactivate();
                isActivated = false;
            }
        }

        private bool IsActionsWork()
        {
            foreach (var faceAction in FaceActions)
            {
                if (!faceAction.IsActive(FaceActionUnitsController.GetActionUnitValue(faceAction.Type)))
                {
                    return false;                    
                }
            }

            return true;
        }
    }
}
