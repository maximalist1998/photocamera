﻿using System;

namespace ActionUnits
{
    [Serializable]
    public enum ActionUnitType
    {
        None = 0,
        NoseWrinkles = 1,
        //Not Active
//    JawZPush = 2,
        JawXPush = 3,
        JawDrop = 4,
        LowerLipDrop = 5,
        UpperLipDrop = 6,
        LipStretcherLeft = 7,
        LipCornerDepressor = 8,
        //Not Active
//    LipPresser = 9,
        LeftOuterBrowRaiser = 10,
        LeftInnerBrowsRaiser = 11,
        LeftBrowLowerer = 12,
        LeftEyeClosed = 13,
        //Not Active
//    LidTightener = 14,
        //Not Active
//    UpperLidRaiser = 15,
        RotateEyesLeft = 16,
        RotateEyesDown = 17,
        LowerLipXPush = 18,
        LipStretcherRight = 19,
        RightOuterBrowRaiser = 20,
        RightInnerBrowRaiser = 21,
        RightBrowLowerer = 22,
        RightEyeClosed = 23,
        RotationX = 24,
        RotationY = 25,
        RotationZ = 26
    }
}