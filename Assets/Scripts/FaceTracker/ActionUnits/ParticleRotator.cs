﻿using UnityEngine;

namespace FaceTracker.ActionUnits
{
    public class ParticleRotator : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem effectParticle;
        
        [SerializeField]
        private float landscapeLeftAngle = 90f;
        [SerializeField]
        private float landscapeRightAngle = -90f;

        private int currentOrientation = 0;
        private Vector3 startRotation;

        private void Awake()
        {
            var main = effectParticle.main;
            main.startRotation3D = true;
            startRotation = new Vector3(effectParticle.main.startRotationX.constant, effectParticle.main.startRotationY.constant, effectParticle.main.startRotationZ.constant);
        }

        private void Update()
        {
            if (currentOrientation != TrackerBasic.Instance.Orientation)
            {
                currentOrientation = TrackerBasic.Instance.Orientation;
                UpdateParticleOrientation();
            }
        }

        private void UpdateParticleOrientation()
        {
            if (currentOrientation == 0)
            {
                var main = effectParticle.main;
                main.startRotationX = startRotation.x;
                main.startRotationY = startRotation.y;
                main.startRotationZ = startRotation.z;
            }
            
            if (currentOrientation == 3)
            {
                var main = effectParticle.main;
                main.startRotationX = startRotation.x;
                main.startRotationY = startRotation.y;
                main.startRotationZ = new ParticleSystem.MinMaxCurve(startRotation.z + landscapeLeftAngle * Mathf.Deg2Rad);
            }
            
            if (currentOrientation == 1)
            {
                var main = effectParticle.main;
                main.startRotationX = startRotation.x;
                main.startRotationY = startRotation.y;
                main.startRotationZ = new ParticleSystem.MinMaxCurve(startRotation.z + landscapeRightAngle * Mathf.Deg2Rad);
            }
        }
    }
}