﻿using System;
using FaceTracker;
using UnityEngine;

namespace ActionUnits
{
    public class FaceActionUnitsController : MonoBehaviour
    {
        #region > Editor Fields

        [Header("Face Data")] public int FaceIndex = 0;

        [Header("Face Action Units Values")] public float NoseWrinkles = 0f;

//        public float JawZPush = 0f;
        public float JawXPush = 0f;
        public float JawDrop = 0f;
        public float LowerLipDrop = 0f;
        public float UpperLipDrop = 0f;
        public float LipStretcherLeft = 0f;

        public float LipCornerDepressor = 0f;

//        public float LipPresser = 0f;
        public float LeftOuterBrowRaiser = 0f;
        public float LeftInnerBrowsRaiser = 0f;
        public float LeftBrowLowerer = 0f;

        public float LeftEyeClosed = 0f;

//        public float LidTightener = 0f;
//        public float UpperLidRaiser = 0f;
        public float RotateEyesLeft = 0f;
        public float RotateEyesDown = 0f;
        public float LowerLipXPush = 0f;
        public float LipStretcherRight = 0f;
        public float RightOuterBrowRaiser = 0f;
        public float RightInnerBrowRaiser = 0f;
        public float RightBrowLowerer = 0f;
        public float RightEyeClosed = 0f;
        public float RotationX = 0f;
        public float RotationY = 0f;
        public float RotationZ = 0f;

        #endregion > Editor Fields

        #region > Events

        public event Action OnActionUnitsChanged;

        #endregion > Events

        #region > Private Fields

        private float[] currActions;

        #endregion > Private Fields

        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            TrackerBasic.OnActionUnits += OnActionUnitsUpdated;
        }

        private void OnDisable()
        {
            TrackerBasic.OnActionUnits -= OnActionUnitsUpdated;
        }

        #endregion > MonoBehaviour Callbacks

        public void SetFaceIndex(int faceIndex)
        {
            FaceIndex = faceIndex;
        }

        public float GetActionUnitValue(ActionUnitType type)
        {
            if (currActions != null)
            {
                if ((int) type > currActions.Length)
                {
                    if (type == ActionUnitType.RotationX)
                        return ConvertToUnsignedAngle(transform.rotation.eulerAngles.x);
                    if(type == ActionUnitType.RotationY)
                        return ConvertToUnsignedAngle(transform.rotation.eulerAngles.y);
                    if(type == ActionUnitType.RotationZ)
                        return ConvertToUnsignedAngle(transform.rotation.eulerAngles.z);
                }
                else
                {
                    return currActions[((int) type) - 1];
                }
            }

            return 0f;
        }

        private void OnActionUnitsUpdated(int faceIndex, float[] actions)
        {
            if (faceIndex != FaceIndex)
                return;

            currActions = actions;
            UpdateEditorFields();
            OnActionUnitsChanged?.Invoke();
        }

        private void UpdateEditorFields()
        {
            NoseWrinkles = currActions[0];
//                JawZPush = currActions[1];
            JawXPush = currActions[2];
            JawDrop = currActions[3];
            LowerLipDrop = currActions[4];
            UpperLipDrop = currActions[5];
            LipStretcherLeft = currActions[6];
            LipCornerDepressor = currActions[7];
//                LipPresser = currActions[8];
            LeftOuterBrowRaiser = currActions[9];
            LeftInnerBrowsRaiser = currActions[10];
            LeftBrowLowerer = currActions[11];
            LeftEyeClosed = currActions[12];
//                LidTightener = currActions[13];
//                UpperLidRaiser = currActions[14];
            RotateEyesLeft = currActions[15];
            RotateEyesDown = currActions[16];
            LowerLipXPush = currActions[17];
            LipStretcherRight = currActions[18];
            RightOuterBrowRaiser = currActions[19];
            RightInnerBrowRaiser = currActions[20];
            RightBrowLowerer = currActions[21];
            RightEyeClosed = currActions[22];
            RotationX = ConvertToUnsignedAngle(transform.rotation.eulerAngles.x);
            RotationY = ConvertToUnsignedAngle(transform.rotation.eulerAngles.y);
            RotationZ = ConvertToUnsignedAngle(transform.rotation.eulerAngles.z);
        }
        
        private static float ConvertToUnsignedAngle(float angle)
        {
            angle %= 360;
            if (angle < 0)
                angle += 360f;

            return angle;
        }
    }
}