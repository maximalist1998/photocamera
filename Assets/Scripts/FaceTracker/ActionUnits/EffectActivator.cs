﻿using System;
using System.Collections;
using Executor;
using UnityEngine;

namespace ActionUnits
{
    [Serializable]
    public class EffectActivator
    {
        #region > Editor Fields
        
        public GameObject Effect;
        public EffectType Type;
//        public bool shouldSpawnPrefab;
        public string animationTriggerActivate;
        public string animationTriggerDeactivate;
        public bool shouldWaitBeforeDeactivate;
        public float waitBeforeDeactivateDelay;
        
        #endregion > Editor Fields
        
        #region > Private Fields

        private bool needDeactivate = false;
        private bool isInWaitingCoroutine = false;
        private Coroutine activateCoroutine;
        
        #endregion > Private Fields

        public void Activate()
        {
            needDeactivate = false;

            if (isInWaitingCoroutine)
            {
                CoroutineExecutor.Instance.Stop(activateCoroutine);
                activateCoroutine = CoroutineExecutor.Instance.StartCoroutine(ActivateCoroutine());
                return;
            }
            
            switch (Type)
            {
                case EffectType.None:
                    break;
                case EffectType.ActivateObject:
                    if(Effect != null)
                        Effect.SetActive(true);
                    break;
                case EffectType.StartParticle:
                    var particle = Effect.GetComponent<ParticleSystem>();
                    if (particle != null)
                        particle.Play();
                    else
                        Debug.LogError("Can't find Particle component!");
                    break;
                case EffectType.StartAudio:
                    var source = Effect.GetComponent<AudioSource>();
                    if(source != null)
                        source.Play();
                    else
                        Debug.LogError("Can't find AudioSource component!");
                    break;
                case EffectType.AnimationTrigger:
                    var animator = Effect.GetComponent<Animator>();
                    if(animator != null)
                        animator.SetTrigger(animationTriggerActivate);
                    else
                        Debug.LogError("Can't find Animator component!");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (shouldWaitBeforeDeactivate)
            {
                activateCoroutine = CoroutineExecutor.Instance.StartCoroutine(ActivateCoroutine());
            }
        }

        public void Deactivate()
        {   
            if (isInWaitingCoroutine)
            {
                needDeactivate = true;
                return;
            }
            
            switch (Type)
            {
                case EffectType.None:
                    break;
                case EffectType.ActivateObject:
                    if(Effect != null)
                        Effect.SetActive(false);
                    break;
                case EffectType.StartParticle:
                    var particle = Effect.GetComponent<ParticleSystem>();
                    if (particle != null)
                        particle.Stop();
                    else
                        Debug.LogError("Can't find Particle component!");
                    break;
                case EffectType.StartAudio:

                    break;
                case EffectType.AnimationTrigger:
                    var animator = Effect.GetComponent<Animator>();
                    if(animator != null)
                        animator.SetTrigger(animationTriggerDeactivate);
                    else
                        Debug.LogError("Can't find Animator component!");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private IEnumerator ActivateCoroutine()
        {
            isInWaitingCoroutine = true;
            
            yield return new WaitForSeconds(waitBeforeDeactivateDelay);

            isInWaitingCoroutine = false;
            
            if (needDeactivate)
            {
                activateCoroutine = null;
                Deactivate();
            }
        }
    }

    public enum EffectType
    {
        None = 0,
        ActivateObject = 1,
        StartParticle = 2,
        StartAudio = 3,
        AnimationTrigger = 4,
    }
}