﻿namespace ActionUnits
{
    public enum ConditionType
    {
        None = 0,
        BiggerThan = 1,
        SmallerThan = 2
    }
}