﻿using System;
using UnityEngine;

namespace ActionUnits
{
    [Serializable]
    public struct FaceAnimationAction
    {
        public ActionUnitType Type;
        public ConditionType Condition;
        public float Value;

        public bool IsActive(float value)
        {
            switch (Condition)
            {
                case ConditionType.None:
                    break;
                case ConditionType.BiggerThan:
                    return value > Value;
                case ConditionType.SmallerThan:
                    return value < Value;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return false;
        }
    }
}