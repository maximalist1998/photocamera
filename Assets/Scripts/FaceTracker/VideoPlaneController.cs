using FaceTracker;
using UnityEngine;

public class VideoPlaneController : MonoBehaviour
{
	public TrackerBasic Tracker;
	public Transform CamerasParent;
	public Camera VideoCamera;
    
	private float aspect;
    private float texCoordX;
    private float texCoordY;
	
	private void Update ()
	{
		if (Tracker == null)
			return;
		
		var imageWidth = Tracker.ImageWidth;
		var imageHeight = Tracker.ImageHeight;
		var texWidth = Tracker.TexWidth;
		var texHeight = Tracker.TexHeight;
		gameObject.transform.localRotation = Quaternion.Euler(-90f, 0f, 0f);
		CamerasParent.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
		#if !UNITY_ANDROID ||UNITY_EDITOR
			
		//LandscapeLeft
		if (Tracker.Orientation == 3)
		{
			CamerasParent.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
		}
		
		//LandscapeRight
		if (Tracker.Orientation == 1)
		{
			CamerasParent.transform.rotation = Quaternion.Euler(0f, 0f, -90f);
		}
		#endif
		
		
        if (imageWidth != 0 && imageHeight != 0)
        {
	        // Get mesh filter
            MeshFilter meshFilter = this.GetComponent<MeshFilter>();
            Vector2[] uv = meshFilter.mesh.uv;
            Vector2[] uv2 = meshFilter.mesh.uv2;

            // Calculate texture coordinates for the 
            // video frame part of the texture (texCoordX, texCoordY)
            //
            //   <------<TexWidth>------>
            // <ImageWidth>
            // uv[0]---uv[3]---------------
            // |          |   ^           |     ^
            // |          |   |           |     |
            // |          | <ImageHeight> |     |
            // |          |   |           |     |
            // |          |   v           |     |
            // uv[1]---uv[2]              |<TexHeight>
            // |           <texCoordX,    |     |
            // |           texCoordY,>    |     |
            // |                          |     |   
            // |                          |     v
            // ----------------------------

            // Calculate uv[2] texture coordinates
            texCoordX = imageWidth / (float)texWidth;
            texCoordY = imageHeight / (float)texHeight;

            // Apply new coordinates
            uv[1].y = texCoordY;
            uv[2].x = texCoordX;
            uv[2].y = texCoordY;
            uv[3].x = texCoordX;

            meshFilter.mesh.uv = uv;
            meshFilter.mesh.uv2 = uv2;

            // Adjust texture scale so the frame fits
            // Fixate so the the frame always fits by height
            aspect = imageWidth / (float)imageHeight;
            float yScale = 100.0f;
            float xScale = yScale * aspect;
            
            // NOTE: due to rotation on the VideoPlane yScale is applied on z coordinate
            gameObject.transform.localScale = new Vector3(xScale, 1.0f, yScale);
#if !UNITY_ANDROID ||UNITY_EDITOR

            if (Tracker.Orientation == 3 || Tracker.Orientation == 1)
            {
	            VideoCamera.orthographicSize = xScale / 2f;
            }
            else
            {
	            VideoCamera.orthographicSize = yScale / 2f;
            }
            #endif
        }
	}
}
