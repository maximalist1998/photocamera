﻿using System;
using UnityEngine;

namespace FaceTracker
{
    public class RecordingController : MonoBehaviour
    {
        #region > Editor Fields

        [SerializeField] private RecordingButton recordingButton;
        [SerializeField] private RectTransform recordingRect;

        [SerializeField] private VideoRecordingController recordingController;
        public event Action OnStopRecording;
        public event Action OnStartPostprocess;
        #endregion > Editor Fields

        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            if (recordingButton == null)
                return;

            recordingController.OnVideoWasExported += OnVideoExported;
            recordingButton.onVideoButtonPressed += StartRecording;
            recordingButton.onVideoButtonReleased += StopRecording;
        }

        private void OnDisable()
        {
            if (recordingButton == null)
                return;

            recordingController.OnVideoWasExported -= OnVideoExported;
            recordingButton.onVideoButtonPressed -= StartRecording;
            recordingButton.onVideoButtonReleased -= StopRecording;
        }

        #endregion > MonoBehaviour Callbacks

        private void StartRecording()
        {
            recordingController.StartRecording(recordingRect);
        }

        private void StopRecording()
        {
            recordingController.StopRecording();
            OnStopRecording?.Invoke();
        }

        private void OnVideoExported(string path)
        {
            var videoRotation = GetTrackerRotation();
            RecordingPanelActivator.Instance.DeactivateFaceMask();
            OnStartPostprocess?.Invoke();
            recordingController.ClearRecorder();
            PostprocessController.Instance.StartPostprocess(path, videoRotation, true);
        }
        
        private float GetTrackerRotation()
        {
            if (TrackerBasic.Instance.Orientation == 1)
                return 90f;
            if (TrackerBasic.Instance.Orientation == 3)
                return -90f;

            return 0;
        }
    }
}