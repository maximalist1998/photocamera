﻿using System;
using System.Collections;
using Executor;
using FaceTracker.DB;
using TMPro;
using UnityEngine;

namespace FaceTracker
{
    public class FaceMaskInstantiateController : MonoBehaviour
    {
        #region > Events

        public static event Action<MaskData> OnMaskSelected;
        
        #endregion > Events
        
        #region > Edito Fields

        [SerializeField]
        private TextMeshProUGUI callToActionText;
        [SerializeField]
        private RecordingController recordingController;
        [SerializeField]
        private PhotoTakingController PhotoTakingController;
        
        #endregion > Edito Fields
        
        #region > Private Fields

        private GameObject[] currentMasks = new GameObject[TrackerBasic.MAX_FACES];
        private Coroutine callToActionCor;
        
        #endregion > Private Fields
        
        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            RecordingContentPanelController.onContentSelected += ContentSelectedHandler;
            FaceMasksData.OnFaceMasksUpdated += FaceMasksUpdatedHandler;
            recordingController.OnStopRecording += StopRecordingHandler;
            PhotoTakingController.OnPhotoTaken += StopRecordingHandler;
        }
        
        private void OnDisable()
        {
            RecordingContentPanelController.onContentSelected -= ContentSelectedHandler;
            FaceMasksData.OnFaceMasksUpdated -= FaceMasksUpdatedHandler;
            recordingController.OnStopRecording -= StopRecordingHandler;
        }
        
        #endregion > MonoBehaviour Callbacks

        private void StopRecordingHandler()
        {
            DestroyCurrentMasks();
        }

        private void FaceMasksUpdatedHandler()
        {
            DestroyCurrentMasks();
        }

        private void ContentSelectedHandler(RecordingContent content)
        {
            if (content.contentType != RecordingContentType.Mask)
                return;

            var mask = FaceMasksData.Instance.GetMaskByName(content.id);
            if (mask.MaskPrefab == null)
                return;
            
            DestroyCurrentMasks();
            InstantiateNewMasks(mask.MaskPrefab);
            SetCallToAction(mask);
            OnMaskSelected?.Invoke(mask);
        }
        
        private void InstantiateNewMasks(GameObject maskPrefab)
        {
            for (var i = 0; i < currentMasks.Length; i++)
            {
                var newMask = Instantiate(maskPrefab);
                currentMasks[i] = newMask;

                //Set face index
                var controller = newMask.GetComponent<FaceMaskController>();
                controller.FaceMaskIndex = i;
            }
        }

        private void DestroyCurrentMasks()
        {
            foreach (var mask in currentMasks)
            {
                if(mask != null)
                    DestroyImmediate(mask);
            }
        }
        
        #region > Call To Action

        private void SetCallToAction(MaskData mask)
        {
            if(callToActionCor != null)
                CoroutineExecutor.Instance.Stop(callToActionCor);
            
            if (!string.IsNullOrEmpty(mask.CallToActionText))
            {
                callToActionText.text = mask.CallToActionText;
                callToActionCor = CoroutineExecutor.Instance.Execute(CallToActionShowCor());
            }
            else
            {
                callToActionText.text = "";
            }
        }

        private IEnumerator CallToActionShowCor()
        {
            callToActionText.color = Color.white;
            
            var showTime = 1f;
            var startTime = Time.time;
            var percent = (Time.time - startTime) / showTime;
            
            while (percent < 1f)
            {
                percent = (Time.time - startTime) / showTime;
                callToActionText.color = new Color(1f, 1f, 1f, percent);
                yield return null;
            }
            callToActionText.color = new Color(1f, 1f, 1f, 1f);
            
            yield return new WaitForSeconds(4f);
            
            var hideTime = 1f;
            startTime = Time.time;
            percent = (Time.time - startTime) / hideTime;
            
            while (percent < 1f)
            {
                percent = (Time.time - startTime) / hideTime;
                callToActionText.color = new Color(Color.white.r, Color.white.g, Color.white.b, 1f - percent);
                yield return null;
            }
            callToActionText.color = new Color(Color.white.r, Color.white.g, Color.white.b, 0f);
        }
        
        #endregion > Call To Action
    }
}