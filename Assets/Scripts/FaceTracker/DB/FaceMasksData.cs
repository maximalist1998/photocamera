﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace FaceTracker.DB
{
    public class FaceMasksData : ScriptableObject
    {
        public static event Action OnFaceMasksUpdated;
        
        #region > Editor Fields
        
        [SerializeField]
        private List<MaskData> masks;
        private List<MaskData> Masks
        {
            get
            {
                if (!isLocalMasksLoaded)
                {
                    UpdateLocalMasks();
                    isLocalMasksLoaded = true;
                }

                return masks;
            }
        }
        
        [NonSerialized]
        private bool isLocalMasksLoaded = false;
        
        #endregion > Editor Fields
        
        #region > Singleton

        private static FaceMasksData instance = null;
        public static FaceMasksData Instance
        {
            get
            {
                if (instance == null)
                    instance = Resources.Load<FaceMasksData>("FaceMasksData");

                if (instance == null)
                    instance = ScriptableObject.CreateInstance<FaceMasksData>();

                return instance;
            }
        }

        #endregion > Singleton

        public List<MaskData> GetMasks()
        {
            return Masks;
        }

        public MaskData GetMaskByName(string name)
        {
            return Masks.FirstOrDefault(m => m.UniqueName == name);
        }

        public void DeleteLocalMaskByName(string name)
        {
            var mask = GetMaskByName(name);
            if (!mask.IsLocalMask)
                return;

            Masks.Remove(mask);
            var maskFolderPath = MaskData.GetMaskDirectoryPathById(mask.UniqueName);
            if(Directory.Exists(maskFolderPath))
                Directory.Delete(maskFolderPath, true);
            
            OnFaceMasksUpdated?.Invoke();
        }

        public void UpdateLocalMasks()
        {
            var directories = Directory.GetDirectories(MaskData.GetLocalMasksFolderPath());
            var wasAdded = false;
            foreach (var dir in directories)
            {
                var id = Path.GetFileNameWithoutExtension(dir);
                if (masks.All(m => m.UniqueName != id))
                {
                    masks.Add(new MaskData(){UniqueName = id});
                    wasAdded = true;
                }
            }
            
            if(wasAdded)
                OnFaceMasksUpdated?.Invoke();
        }
    }
}