﻿using System;
using System.IO;
using UnityEngine;

namespace FaceTracker.DB
{
    [Serializable]
    public struct MaskData
    {
        public string UniqueName;
        public string CallToActionText;
        [SerializeField]
        private GameObject maskPrefab;
        [SerializeField]
        private Sprite preview;

        #region > Properties
        
        public GameObject MaskPrefab
        {
            get
            {
                if (maskPrefab == null && MaskTexture != null)
                {
                    var prefab = Resources.Load<GameObject>("MaskBuilder/BlankMaskPrefab");
                    var renderer = prefab.GetComponent<Renderer>();
                    if (renderer != null)
                        renderer.sharedMaterial.mainTexture = MaskTexture;

                    return prefab;
                }
                
                return maskPrefab;
            }
        }

        public Sprite Preview
        {
            get
            {
                if (preview == null && PreviewTexture != null)
                {
                    preview = Sprite.Create(PreviewTexture, new Rect(0, 0, PreviewTexture.width, PreviewTexture.height), new Vector2(0.5f, 0.5f));
                }

                return preview;
            }
        }

        private Texture2D maskTexture;
        public Texture2D MaskTexture
        {
            get
            {
                if (maskTexture == null && File.Exists(GetMaskPathById(UniqueName)))
                {
                    maskTexture = new Texture2D(2, 2);
                    maskTexture.LoadImage(File.ReadAllBytes(GetMaskPathById(UniqueName)));
                }

                return maskTexture;
            }
        }
        
        private Texture2D previewTexture;
        private Texture2D PreviewTexture
        {
            get
            {
                if (previewTexture == null && File.Exists(GetPreviewPathById(UniqueName)))
                {
                    previewTexture = new Texture2D(2, 2);
                    previewTexture.LoadImage(File.ReadAllBytes(GetPreviewPathById(UniqueName)));
                }

                return previewTexture;
            }
        }

        public bool IsLocalMask
        {
            get
            {
                if (File.Exists(GetMaskPathById(UniqueName)))
                    return true;

                return false;
            }
        }
        
        #endregion > Properties
        
        #region > Pathes

        public static string GetLocalMasksFolderPath()
        {
            var dirPath = Path.Combine(Application.persistentDataPath, "LocalMasks");
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            return dirPath;
        }
        
        public static string GetMaskDirectoryPathById(string id)
        {
            return Path.Combine(GetLocalMasksFolderPath(), id);
        } 
        
        public static string GetMaskPathById(string id)
        {
            return Path.Combine(GetMaskDirectoryPathById(id), "mask.png");
        }
        
        public static string GetPreviewPathById(string id)
        {
            return Path.Combine(GetMaskDirectoryPathById(id), "preview.png");
        }
        
        #endregion > Pathes
        
        #region > Saving

        public static string SaveLocalMask(Texture2D mask, Texture2D preview)
        {
            var id = Guid.NewGuid().ToString();
            var dir = GetMaskDirectoryPathById(id);
            Directory.CreateDirectory(dir);
            File.WriteAllBytes(GetMaskPathById(id), mask.EncodeToPNG());
            File.WriteAllBytes(GetPreviewPathById(id), preview.EncodeToPNG());

            return id;
        }
        
        #endregion > Saving
    }
}