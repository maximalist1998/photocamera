﻿using UnityEngine;

namespace Utils
{
    public static class TextureUtils
    {
        public static Texture2D RotateTexture(Texture2D originalTexture, bool clockwise)
        {
            Color32[] original = originalTexture.GetPixels32();
            Color32[] rotated = new Color32[original.Length];
            int w = originalTexture.width;
            int h = originalTexture.height;
 
            int iRotated, iOriginal;
 
            for (int j = 0; j < h; ++j)
            {
                for (int i = 0; i < w; ++i)
                {
                    iRotated = (i + 1) * h - j - 1;
                    iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                    rotated[iRotated] = original[iOriginal];
                }
            }
 
            Texture2D rotatedTexture = new Texture2D(h, w);
            rotatedTexture.SetPixels32(rotated);
            rotatedTexture.Apply();
            return rotatedTexture;
        }
        
        public static void SetColorToTexture(Texture2D texture, Color color)
        {
            var texturePixels = texture.GetPixels();
            for(var i = 0; i < texturePixels.Length; i++)
            {
                texturePixels[i] = color;
            }
            texture.SetPixels(texturePixels);
            texture.Apply();
        }
        
        public static Texture2D ToTexture2D(RenderTexture rt)
        {
            Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.RGBA32, false);
            var prevActive = RenderTexture.active;
            RenderTexture.active = rt;
            tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            tex.Apply();
            RenderTexture.active = prevActive;
            
            return tex;
        }
        
        public static Texture2D ToMiddleSquareTexture2D(RenderTexture rt)
        {
            var size = Mathf.Min(rt.width, rt.height);
            
            Texture2D tex = new Texture2D(size, size, TextureFormat.RGBA32, false);
            var prevActive = RenderTexture.active;
            RenderTexture.active = rt;
            if (rt.width < rt.height)
            {
                tex.ReadPixels(new Rect(0, (int)((rt.height - tex.height)/2f), tex.width, tex.height), 0, 0);
            }
            else
            {
                tex.ReadPixels(new Rect((int)((rt.width - tex.width)/2f), 0, tex.width, tex.height), 0, 0);
            }
            tex.Apply();
            RenderTexture.active = prevActive;
            
            return tex;
        }
        
        public static Texture2D ToMiddleSquareTexture2D(RenderTexture rt, float yCenter)
        {
            var size = Mathf.Min(rt.width, rt.height);
            
            var tex = new Texture2D(size, size, TextureFormat.RGBA32, false);
            var prevActive = RenderTexture.active;
            RenderTexture.active = rt;
            if (rt.width < rt.height)
            {
                var yStartPixel = (int)(rt.height * yCenter);
                var yStart = yStartPixel - (int)(tex.height / 2f);
                tex.ReadPixels(new Rect(0, yStart, tex.width, tex.height), 0, 0);
            }
            else
            {
                tex.ReadPixels(new Rect((int)((rt.width - tex.width)/2f), 0, tex.width, tex.height), 0, 0);
            }
            tex.Apply();
            RenderTexture.active = prevActive;
            
            return tex;
        }
        
        public static Texture2D ApplyTextureAlpha(Texture2D tex, float newAlpha)
        {
            var pixels = tex.GetPixels();
            var newPixels = new Color[pixels.Length];

            for (var i = 0; i < newPixels.Length; i++)
            {
                if(pixels[i].a > newAlpha)
                    newPixels[i] = new Color(pixels[i].r, pixels[i].g, pixels[i].b, newAlpha);
                else
                    newPixels[i] = pixels[i];
            }
            
            var newTexture = new Texture2D(tex.width, tex.height, TextureFormat.RGBA32, false);
            newTexture.SetPixels(newPixels);
            newTexture.Apply();

            return newTexture;
        }
        
        public static Texture2D ApplyTextureAlpha(Texture2D tex, Texture2D alphaMask)
        {
            if (tex.width != alphaMask.width || tex.height != alphaMask.height)
                return null;
            
            var pixels = tex.GetPixels();
            var alphaPixels = alphaMask.GetPixels();
            var newPixels = new Color[pixels.Length];

            for (var i = 0; i < newPixels.Length; i++)
            {
                if(pixels[i].a > alphaPixels[i].a)
                    newPixels[i] = new Color(pixels[i].r, pixels[i].g, pixels[i].b, alphaPixels[i].a);
                else
                    newPixels[i] = pixels[i];
            }
            
            var newTexture = new Texture2D(tex.width, tex.height, TextureFormat.RGBA32, false);
            newTexture.SetPixels(newPixels);
            newTexture.Apply();

            return newTexture;
        }
    }
}