﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class BuildTypeScriptableObj : ScriptableObject
{
    [SerializeField] public BuildType buildType;
[Serializable]
    public enum BuildType
    {
        Release,
        Staging,
        Develop
    }
}
