﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniUtils;
public class GlobalEvents :Singleton<GlobalEvents> {
	public float Delay;
	public float DelayBetweenTaps;
	bool isOnCooldown;

	#region Events
	public UnityEvent Tap;
	public UnityEvent DoubleTap;
	public UnityEvent TripleTap;
	public UnityEventVector MoveFinger;
	//public UnityEvent Swipe;
	public UnityEventVector Swipe;
	public UnityEvent SwipeUp;
	public UnityEvent SwipeDown;
	public UnityEvent SwipeLeft;
	public UnityEvent SwipeRight;
	public UnityEvent OnStartPinch;
	public UnityEventFloat Pinching;
	public UnityEvent OnEndPinch;
	public UnityEvent OnStartRotating;
	public UnityEventFloat Rotating;
	public UnityEvent OnEndRotating;
	public UnityEventVector TwoFingersMove;
	#endregion

	bool isSwipe;
	Vector2 startPos;
	Vector2 previousPos;
	float startTime;
	float nonMovingTime;

	float pinchStartDistance;
	Vector2 startRotateDirection;
	int topFingerId,bottomFingedId;
	bool twoFingersGesture;

	Coroutine tapCor;

	void Start ()
	{
		topFingerId = -1;
		bottomFingedId = -1;
	}

	void Update ()
	{
		if(Input.touchCount == 1 && !isOnCooldown && !twoFingersGesture)
		{
			Touch currentTouch = Input.touches[0];			 
			//Debug.Log(startPos);
			switch(currentTouch.phase)
			{
			case TouchPhase.Began:
				{
					//debugger.text = "Touch Began";
					isSwipe = true;
					startPos = currentTouch.position/Screen.dpi;
					previousPos = startPos;
					startTime = Time.time;
					nonMovingTime = 0;
				}
				break;
			case TouchPhase.Moved:
				{
					Vector2 currPos = currentTouch.position/Screen.dpi;
					if(MoveFinger != null)
						MoveFinger.Invoke(currPos-previousPos);
					previousPos = currPos;
				}
				break;
			case TouchPhase.Stationary:
				{
					nonMovingTime += Time.deltaTime;
					if(nonMovingTime > .1f)
						isSwipe = false;
				}
				break;
			case TouchPhase.Ended:
				{
					//Debug.Log("Here");
					Vector2 endPos = currentTouch.position/Screen.dpi; //Camera.main.ScreenToWorldPoint(currentTouch.position);
					float totalTime = Time.time - startTime;
					if(Vector2.Distance(endPos,startPos) < .5f && totalTime >= 0)
					{
						if(tapCor != null)
						{
							StopCoroutine(tapCor);
							tapCor = null;
							tapCor = StartCoroutine(TapCoroutine(currentTouch));
						} else
						{
							tapCor = StartCoroutine(TapCoroutine(currentTouch));
						}
					}
					if(Vector3.Distance(endPos,startPos) > .75f && totalTime >= 0 && isSwipe && totalTime < .4f)
					{
						Vector2 dir = endPos - startPos;
						Swipe.Invoke(dir);
						//Vector2 dir = endPos - startPos;
						if(Mathf.Abs(dir.x) >= Mathf.Abs(dir.y))
						{
							if(dir.x > 0)
								SwipeRight.Invoke();
							else
								SwipeLeft.Invoke();
						}	else
						{
							if(dir.y > 0)
								SwipeUp.Invoke();
							else
								SwipeDown.Invoke();
						}

						StartCoroutine(Cooldown());
						isOnCooldown = true;
					}
					//debugger.text = "Touch Ended with Time - " + totalTime.ToString() + " " +
					//"and distance " + Vector3.Distance(endPos,startPos).ToString();

					isSwipe = false;
					startPos = new Vector3();
					startTime = Time.time;
				}
				break;
			}
		}
		if(Input.touchCount == 2 && !isOnCooldown)
		{
			//debugger.text = "2 touches";
			Touch firstTouch = Input.touches[0];
			Touch secondTouch = Input.touches[1];
			#region Pinch
			if(firstTouch.phase == TouchPhase.Began || secondTouch.phase == TouchPhase.Began)
			{
				twoFingersGesture = true;
				if(OnStartPinch != null)
					OnStartPinch.Invoke();
				pinchStartDistance = (secondTouch.position - firstTouch.position).magnitude/Screen.dpi;
			}
			if(firstTouch.phase == TouchPhase.Moved || secondTouch.phase == TouchPhase.Moved)
			{
				float currentDistance = (secondTouch.position - firstTouch.position).magnitude/Screen.dpi;
				float distanceDifference = currentDistance - pinchStartDistance;
				if(Pinching != null)
					Pinching.Invoke(distanceDifference);
				pinchStartDistance = currentDistance;
				//debugger.text = "Pinch distance " + distanceDifference.ToString();
			}
			if(firstTouch.phase == TouchPhase.Ended || secondTouch.phase == TouchPhase.Ended)
			{
				if(OnEndPinch != null)
					OnEndPinch.Invoke();

				//StartCoroutine(Cooldown());
			//	twoFingersGesture = false;
			}
			#endregion

			#region TwoFingersRotate 
			Touch topTouch = firstTouch;
			Touch bottomTouch = secondTouch;
			if(topFingerId != -1)
			{
				if(firstTouch.fingerId == topFingerId)
					topTouch = firstTouch;
				else if(secondTouch.fingerId == topFingerId)
					topTouch = secondTouch;
			} else
			{
				if(firstTouch.position.y > secondTouch.position.y)
				{
					topTouch = firstTouch;
					topFingerId = firstTouch.fingerId;
				}
				else
				{
					topTouch = bottomTouch;
					topFingerId = bottomTouch.fingerId;
				}
			}
			if(bottomFingedId != -1)
			{
				if(firstTouch.fingerId == bottomFingedId)
					bottomTouch = firstTouch;
				else if(secondTouch.fingerId == bottomFingedId)
					bottomTouch = secondTouch;
			} else
			{
				if(firstTouch.position.y < secondTouch.position.y)
				{
					bottomTouch = firstTouch;
					bottomFingedId = firstTouch.fingerId;
				}
				else
				{
					bottomTouch = secondTouch;
					bottomFingedId = secondTouch.fingerId;
				}
			}
								
			if(topTouch.phase == TouchPhase.Began || bottomTouch.phase == TouchPhase.Began)
			{
				twoFingersGesture = true;
				startRotateDirection = topTouch.position - bottomTouch.position;
				if(OnStartRotating != null)
					OnStartRotating.Invoke();
				
				previousPos = firstTouch.position/Screen.dpi;
			}
			if(topTouch.phase == TouchPhase.Moved || bottomTouch.phase == TouchPhase.Moved)
			{
				
				//debugger.text = angle.ToString() + " Angle";

				float distanceBetweanTouches = Vector3.Distance(topTouch.position,bottomTouch.position) / Screen.dpi;

				//Debug.Log("* distance / dpi " + distanceBetweanTouches / Screen.dpi);

				if (distanceBetweanTouches < 0.7f)
				{
					Vector2 currPos = firstTouch.position/Screen.dpi;
					if(TwoFingersMove != null)
						TwoFingersMove.Invoke(currPos-previousPos);
					previousPos = currPos;
					//Debug.Log("two fingers move");
				}
				else
				{
					Vector2 currentDirection = topTouch.position - bottomTouch.position;
					float angle = Vector2.SignedAngle(currentDirection, startRotateDirection);
					if (Rotating != null)
						Rotating.Invoke(angle);
					startRotateDirection = currentDirection;
					//Debug.Log("two fingers rotate ");
				}

			}
			if(topTouch.phase == TouchPhase.Ended || bottomTouch.phase == TouchPhase.Ended)
			{
				topFingerId = -1;
				bottomFingedId = -1;
				if(OnEndRotating != null)
					OnEndRotating.Invoke();
				//debugger.text = "Finger up " + twoFingersGesture.ToString();
			}
			#endregion

		}
		if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended && twoFingersGesture)
			twoFingersGesture = false;
	}

	IEnumerator Cooldown ()
	{
		isOnCooldown = true;
		yield return new WaitForSeconds(Delay);
		isOnCooldown = false;
	}

	IEnumerator TapCoroutine(Touch currentTouch)
	{
		yield return new WaitForSeconds(DelayBetweenTaps);
		if(currentTouch.tapCount == 1 && Tap != null)
			Tap.Invoke();
		if(currentTouch.tapCount == 2 && DoubleTap != null)
			DoubleTap.Invoke();
		if(currentTouch.tapCount == 3 && TripleTap != null)
			TripleTap.Invoke();
		//debugger.text = currentTouch.tapCount + " amount of taps";

		tapCor = null;
	}
}


[System.Serializable]
public class UnityEventVector : UnityEvent<Vector2>
{
}
[System.Serializable]
public class UnityEventFloat : UnityEvent<float>
{
	
}
