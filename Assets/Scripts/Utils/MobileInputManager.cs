using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace GameInput
{
	public class MobileInputManager : MonoBehaviour
	{
		#region > Events
	
		public event Action<Vector3> OnTouch;
		public event Action<Vector2> OnMove;
		//public event Action<Vector2> OnMoveUI;
		public event Action OnPinchStarted;
		public event Action<float> OnPinch;
	//	public event Action OnPinchStartedUI;
	//	public event Action<float> OnPinchUI;
		public event Action<float> OnRotate;
		public event Action<float> OnRotateStarted;
		
		
		//....................................
		public static Action OnStartPinch;
		public static Action<float> Pinching;
		public static Action OnEndPinch;
		
		public static Action OnStartRotating;
		public static Action<float> Rotating;
		public static Action OnEndRotating;
		public static Action<Vector2> TwoFingersMove;
    
		#endregion > Events
    
		#region > Private fields

		private Vector2 startTouchPos;
		private bool isTouchMoved = false;
		private bool isTouchIgnored = false;
		private bool isPinchStarted = false;
		private bool isRotateStarted = false;
		
		private float touchesStartDistance;
		private float previousTouchesDistance;
		private float touchesStartPositionX;
		//.....................................
		private float pinchStartDistance;
		int topFingerId,bottomFingedId;
		Vector2 startRotateDirection;
		Vector2 previousPos;
		private bool twoFingersGesture;
		private int layerDontCapturing;
		
		private GraphicRaycasterLayer graphicRaycasterLayer;
		#endregion > Private fields
	
		#region > Constants

		private const float MinTouchMoveDifference = 0.05f;
		private const float MinPinchDifference = 0.1f;
	
		#endregion > Constants

		#region > Singletone


		public static MobileInputManager  Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MobileInputManager>();
				if (instance == null)
					instance = new GameObject("MobileInputManager").AddComponent<MobileInputManager>();
				
				return instance;
			}

		}
		private static MobileInputManager instance;
		#endregion > Singletone

		private void Start()
		{
			layerDontCapturing = LayerMask.NameToLayer("UIdontCapturing");
			graphicRaycasterLayer = FindObjectOfType<GraphicRaycasterLayer>();
		}

		private void Update()
		{
			CheckMobileInputs();
		}

		private void CheckMobileInputs()
		{
			if (Input.touchCount == 1)
			{

				var touch = Input.GetTouch(0);
				//Ignore UI Clicks
				//	bool touchOnUI = EventSystem.current.IsPointerOverGameObject(touch.fingerId);
				/*	if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
					{
						isTouchIgnored = true;
						return;
					}*/
if (graphicRaycasterLayer.RaycastLayer(layerDontCapturing))
	return;
				if (touch.phase == TouchPhase.Began)
				{
					startTouchPos = touch.position;

					isTouchIgnored = false;
				}
				else if (touch.phase == TouchPhase.Moved)
				{
					if (isTouchMoved)
					{
//						if (touchOnUI)
//						{
//							OnMoveUI?.Invoke(touch.deltaPosition);
//						}
//						else
//						{
						OnMove?.Invoke(touch.deltaPosition);
						//	}
					}
					else if ((startTouchPos - touch.position).magnitude / Screen.dpi > MinTouchMoveDifference)
					{
						isTouchMoved = true;
//						if (touchOnUI)
//						{
//							Debug.Log("OnMoveUI");
//							OnMoveUI?.Invoke(touch.deltaPosition);
//						}
//						else
//						{
						Debug.Log("OnMove");
						OnMove?.Invoke(touch.deltaPosition);
						//	}
					}
				}

				if (touch.phase == TouchPhase.Ended)
				{
					if (!isTouchMoved && !isTouchIgnored)
					{
						OnTouch?.Invoke(touch.position);
					}

					isTouchMoved = false;
				}
			}

			/*
			if (Input.touchCount == 2)
			{
				//bool firstTouchOnUI = EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
				//	bool secondTouchOnUI = EventSystem.current.IsPointerOverGameObject(Input.GetTouch(1).fingerId);
				//	if (firstTouchOnUI ^ secondTouchOnUI)
				//	{
				//	isTouchIgnored = true;
				//		return;
				//}


				Touch topTouch, bottomTouch;
				if (Input.touches[0].position.y < Input.touches[1].position.y)
				{
					bottomTouch = Input.touches[0];
					topTouch = Input.touches[1];
				}
				else
				{
					bottomTouch = Input.touches[1];
					topTouch = Input.touches[0];
				}


				if (topTouch.phase == TouchPhase.Began || bottomTouch.phase == TouchPhase.Began)
				{
					touchesStartDistance = (topTouch.position - bottomTouch.position).magnitude / Screen.dpi;
					previousTouchesDistance = touchesStartDistance;
					if (touchesStartDistance < 1f)
						touchesStartPositionX = topTouch.position.x;

					isTouchIgnored = false;
				}

				if (bottomTouch.phase == TouchPhase.Moved || topTouch.phase == TouchPhase.Moved)
				{
					var currDist = (topTouch.position - bottomTouch.position).magnitude / Screen.dpi;
					if (Mathf.Abs(currDist - previousTouchesDistance) > MinPinchDifference)
					{

						if (!isPinchStarted)
						{
							//if(firstTouchOnUI && secondTouchOnUI)
							//OnPinchStartedUI?.Invoke();
							//else
							OnPinchStarted?.Invoke();
							isPinchStarted = true;
						}
						else
						{
							//	if (firstTouchOnUI && secondTouchOnUI)
							//	OnPinchUI?.Invoke(currDist - previousTouchesDistance);
							//	else
							//	{
							OnPinch?.Invoke(currDist - previousTouchesDistance);
							//	}
						}

						previousTouchesDistance = currDist;
					}


					//if (currDist < 1f)
					//{
					if (!isRotateStarted)
					{

						OnRotateStarted?.Invoke((topTouch.position.x - touchesStartPositionX) / Screen.dpi);
						isRotateStarted = true;
					}
					else
					{
						OnRotate?.Invoke((topTouch.position.x - touchesStartPositionX) / Screen.dpi);
					}

					//}
				}

				if (bottomTouch.phase == TouchPhase.Ended || topTouch.phase == TouchPhase.Ended)
				{
					isPinchStarted = false;
					isRotateStarted = false;
				}

			}
*/
			if (Input.touchCount == 2)
			{
				//debugger.text = "2 touches";
				Touch firstTouch = Input.touches[0];
				Touch secondTouch = Input.touches[1];

				#region Pinch

				if (firstTouch.phase == TouchPhase.Began || secondTouch.phase == TouchPhase.Began)
				{
					twoFingersGesture = true;
					if (OnStartPinch != null)
						OnStartPinch.Invoke();
					pinchStartDistance = (secondTouch.position - firstTouch.position).magnitude / Screen.dpi;
				}

				if (firstTouch.phase == TouchPhase.Moved || secondTouch.phase == TouchPhase.Moved)
				{
					float currentDistance = (secondTouch.position - firstTouch.position).magnitude / Screen.dpi;
					float distanceDifference = currentDistance - pinchStartDistance;
					if (Pinching != null)
						Pinching.Invoke(distanceDifference);
					pinchStartDistance = currentDistance;
					//debugger.text = "Pinch distance " + distanceDifference.ToString();
				}

				if (firstTouch.phase == TouchPhase.Ended || secondTouch.phase == TouchPhase.Ended)
				{
					if (OnEndPinch != null)
						OnEndPinch.Invoke();

				}

				#endregion

				#region TwoFingersRotate 

				Touch topTouch = firstTouch;
				Touch bottomTouch = secondTouch;
				if (topFingerId != -1)
				{
					if (firstTouch.fingerId == topFingerId)
						topTouch = firstTouch;
					else if (secondTouch.fingerId == topFingerId)
						topTouch = secondTouch;
				}
				else
				{
					if (firstTouch.position.y > secondTouch.position.y)
					{
						topTouch = firstTouch;
						topFingerId = firstTouch.fingerId;
					}
					else
					{
						topTouch = bottomTouch;
						topFingerId = bottomTouch.fingerId;
					}
				}

				if (bottomFingedId != -1)
				{
					if (firstTouch.fingerId == bottomFingedId)
						bottomTouch = firstTouch;
					else if (secondTouch.fingerId == bottomFingedId)
						bottomTouch = secondTouch;
				}
				else
				{
					if (firstTouch.position.y < secondTouch.position.y)
					{
						bottomTouch = firstTouch;
						bottomFingedId = firstTouch.fingerId;
					}
					else
					{
						bottomTouch = secondTouch;
						bottomFingedId = secondTouch.fingerId;
					}
				}

				if (topTouch.phase == TouchPhase.Began || bottomTouch.phase == TouchPhase.Began)
				{
					twoFingersGesture = true;
					startRotateDirection = topTouch.position - bottomTouch.position;
					if (OnStartRotating != null)
						OnStartRotating.Invoke();

					previousPos = firstTouch.position / Screen.dpi;
				}

				if (topTouch.phase == TouchPhase.Moved || bottomTouch.phase == TouchPhase.Moved)
				{

					float distanceBetweanTouches =
						Vector3.Distance(topTouch.position, bottomTouch.position) / Screen.dpi;

					if (distanceBetweanTouches < 0.7f)
					{
						Vector2 currPos = firstTouch.position / Screen.dpi;
						if (TwoFingersMove != null)
							TwoFingersMove.Invoke(currPos - previousPos);
						previousPos = currPos;
						//Debug.Log("two fingers move");
					}
					else
					{
						Vector2 currentDirection = topTouch.position - bottomTouch.position;
						float angle = Vector2.SignedAngle(currentDirection, startRotateDirection);
						if (Rotating != null)
							Rotating.Invoke(angle);
						startRotateDirection = currentDirection;
						//Debug.Log("two fingers rotate ");
					}

				}

				if (topTouch.phase == TouchPhase.Ended || bottomTouch.phase == TouchPhase.Ended)
				{
					topFingerId = -1;
					bottomFingedId = -1;
					if (OnEndRotating != null)
						OnEndRotating.Invoke();
					//debugger.text = "Finger up " + twoFingersGesture.ToString();
				}

				#endregion

			}
			if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended && twoFingersGesture)
				twoFingersGesture = false;
		}
	}
}