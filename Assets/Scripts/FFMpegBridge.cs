﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_IOS
using FFmpeg;
#endif
using UnityEngine;
using UniUtils;

public class FFMpegBridge : Singleton<FFMpegBridge>
#if UNITY_IOS
    , IFFmpegHandler
#endif
{
    private string resultPath;
    private Action<string> onSuccess, onProgress, onFailure;
#if UNITY_ANDROID
    private AndroidJavaClass ffmpeg;
#endif


    private void Awake()
    {
#if UNITY_IOS
        FFmpegParser.Handler = this;
#endif
    }

    public void SetSoundFromAnotherVideo(string soundVideoPath, string editableVideoPath, string resultPath,
        Action<string> onSoundAdded, Action<string> onProgress = null, Action<string> onFailure = null)
    {
        this.resultPath = resultPath; 
        string[] command;
        command = new[]
        {
            "-i", editableVideoPath, "-i", soundVideoPath, "-c", "copy", "-map", "0:v:0", "-map", "1:a:0", "-shortest",
            resultPath
        };
#if UNITY_IOS

        FFmpegCommands.SendCustomCommand(command);
        this.onSuccess = onSoundAdded;
        this.onProgress = onProgress;
        this.onFailure = onFailure;
#elif UNITY_ANDROID
        SendAndroidFFMpegCommand(command, (msg) => { onSoundAdded.Invoke(resultPath); }, onProgress,onFailure);
        #endif
    }

    public void GetSoundFromVideo(string soundVideoPath, Action<string> onSoundExtarcted, float start = 0f,
        float finish =
            0f)
    {
    }

    public void AddSoundToVideo(string soundPath, string videoPath, Action<string> onSoundAdded)
    {
    }


    public void ConvertToGif(string path, int fps = 0, Action<string> onSuccess = null,
        Action<string> onProgress = null, Action<string> onFailure = null)
    {
        string gifPath = Path.ChangeExtension(path, ".gif");
        resultPath = gifPath;
#if UNITY_IOS
        var config = new ConvertData();
        config.inputPath = path;
        config.fps = fps;
        config.outputPath = gifPath;
        FFmpegCommands.Convert(config);
        this.onSuccess = onSuccess;
        this.onProgress = onProgress;
        this.onFailure = onFailure;
#elif UNITY_ANDROID
        string[] command;
        if (fps == 0)
            command = new[] {"-y", "-i", path, gifPath};
        else
        {
            command = new[] {"-y", "-i", path, "-r", fps.ToString(), gifPath};
        }

        SendAndroidFFMpegCommand(command, onSuccess, onProgress, onFailure);
#endif
    }
#if UNITY_ANDROID
    public void SendAndroidFFMpegCommand(string [] command, Action<string> onSuccess = null,
        Action<string> onProgress = null, Action<string> onFailure = null)
    {
        this.onSuccess = onSuccess;
        this.onProgress = onProgress;
        this.onFailure = onFailure;
       
        if (ffmpeg  == null)
        ffmpeg = new AndroidJavaClass("com.pixchange.ffmpegBridge");
        ffmpeg.CallStatic("sendFFMpegCommand", command);
       
    }
#endif
    public void OnStart()
    {
//throw new System.NotImplementedException();
    }

    public void OnProgress(string msg)
    {
        onProgress?.Invoke(msg);
//  throw new System.NotImplementedException();
    }

    public void OnFailure(string msg)
    {
//    throw new System.NotImplementedException();
    }

    public void OnSuccess(string msg)
    {
        onSuccess?.Invoke(resultPath);
//    throw new System.NotImplementedException();
    }

    public void OnFinish()
    {
//    throw new System.NotImplementedException();
    }
}