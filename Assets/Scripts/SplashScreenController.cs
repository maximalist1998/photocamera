﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SplashScreenController : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;
    private AsyncOperation loading;
    // Start is called before the first frame update
    void Awake()
    {
  
 
   videoPlayer.Play();
        videoPlayer.started += (vp)=>
        {
            StartCoroutine(CheckVideoEnding());
        };
    }

    private void Start()
    {
        loading =    SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        loading.allowSceneActivation = false;
            Debug.Log("Start pl");
            
    }

   

    IEnumerator CheckVideoEnding()
    {
        while (true)
        {
            long playerCurrentFrame = videoPlayer.frame;
            long playerFrameCount = Convert.ToInt64(videoPlayer.frameCount);
            Debug.Log(playerCurrentFrame + " - frame");

            if (playerCurrentFrame >= playerFrameCount-1)
            {

                if (loading.isDone)
                {
                    Debug.Log("doneeee");
              loading.allowSceneActivation = true;
                SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
                SceneManager.UnloadSceneAsync(0);
                }

                else
                {
                        Debug.Log("scene loading");
              loading.allowSceneActivation = true;
                    loading.completed += operation =>
                    {
                        Debug.Log("scene loading completeds");
               SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
                        
                        SceneManager.UnloadSceneAsync(0);
                    };
                }
               break; 
            }

            yield return null;
        }
    
}
}
