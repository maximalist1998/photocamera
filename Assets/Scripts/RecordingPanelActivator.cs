﻿using System.Collections.Generic;
using UnityEngine;
using UniUtils;

public class RecordingPanelActivator : Singleton<RecordingPanelActivator>
{
    [SerializeField] private List<GameObject> faceMasksObjects;
    [SerializeField] private PostprocessController postprocessController;

    public void ActivateFaceMask()
    {
        postprocessController.DeactivatePostprocess();
        foreach (var go in faceMasksObjects)
        {
            go.SetActive(true);
        }
    }

    public void DeactivateFaceMask()
    {
        foreach (var go in faceMasksObjects)
        {
            go.SetActive(false);
        }
    }
}