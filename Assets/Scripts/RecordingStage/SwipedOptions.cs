﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipedOptions : MonoBehaviour
{
    public event Action<float> onOptionSelected;
    [SerializeField] private int currentOption;
    [SerializeField] private List<AspectRatioOption> aspectRatioOptions = new List<AspectRatioOption>();

    [SerializeField] private Image hintPanel;
    [SerializeField] private float hintShowingTime = 2f;
    [SerializeField] private float hintDisappearanceTime = 1f;
    [SerializeField] private RectTransform swipebleArea;
    [SerializeField] private Camera mainCamera;
    private Coroutine hintCoroutine;

    private void Start()
    {
        
        foreach (var aspectRatioOption in aspectRatioOptions)
        {
            aspectRatioOption.Init(Select);
        }

        Select(currentOption);
      
    }

    private IEnumerator ShowHint()
    {
        hintPanel.color = Color.white;
        hintPanel.gameObject.SetActive(true);
        foreach (var aspectRatioOption in aspectRatioOptions)
        {
            aspectRatioOption.SetTextColor(Color.white);
            aspectRatioOption.Enable();
        }

        yield return new WaitForSeconds(hintShowingTime);

        float timer = hintDisappearanceTime;
        while (true)
        {
            yield return null;

            timer -= Time.deltaTime;
            float alpha = Mathf.Lerp(0, 1, Mathf.Clamp01(timer / hintDisappearanceTime));
            var color = new Color(1, 1, 1, alpha);
            hintPanel.color = color;
            foreach (var aspectRatioOption in aspectRatioOptions)
            {
                aspectRatioOption.SetTextColor(color);
            }

            if (alpha <= 0)
            {
                hintPanel.gameObject.SetActive(false);
                foreach (var aspectRatioOption in aspectRatioOptions)
                {
                    aspectRatioOption.Disable();
                }

                break;
            }
        }
    }

    private void OnEnable()
    {
        GlobalEvents.Instance.SwipeLeft.AddListener(SelectNext);
        GlobalEvents.Instance.SwipeRight.AddListener(SelectPrevious);
        if (hintCoroutine != null)
            StopCoroutine(hintCoroutine);
        hintCoroutine = StartCoroutine(ShowHint());
    }

    private void OnDisable()
    {
        if (GlobalEvents.Instance == null)
            return;
        
        GlobalEvents.Instance.SwipeLeft.RemoveListener(SelectNext);
        GlobalEvents.Instance.SwipeRight.RemoveListener(SelectPrevious);
    }

    private void SelectNext()
    {
        Debug.Log("Swipe left");
        if(IgnoreSwipe()) return;
        Select(currentOption + 1);
    }

    private void SelectPrevious()
    {
        Debug.Log("Swipe right");
        if(IgnoreSwipe()) return;
        Select(currentOption - 1);
    }

    private void Select(AspectRatioOption aspectRatioOption)
    {
        for (int i = 0; i < aspectRatioOptions.Count; i++)
        {
            if (aspectRatioOption == aspectRatioOptions[i])
            {
                Select(i);
                return;
            }
        }

        aspectRatioOptions.Add(aspectRatioOption);
        Select(aspectRatioOptions.Count - 1);
        
    }

    private bool IgnoreSwipe()
    {
        Touch currentTouch = Input.touches[0];
        bool ignore = currentTouch.position.y < Screen.height*0.33f;
//        bool ignore = !swipebleArea.rect.Contains(mainCamera.ScreenToViewportPoint(currentTouch.position));
//        Debug.Log("currentTouch.position - "+currentTouch.position);
//        Debug.Log("mainCamera.ScreenToWorldPoint(currentTouch.position) - "+ mainCamera.ScreenToWorldPoint(currentTouch.position));
//        Debug.Log("swipebleArea.rect = " +swipebleArea.rect);
//        Debug.Log("ignore swipe - " + !swipebleArea.rect.Contains(mainCamera.ScreenToViewportPoint(currentTouch.position)));
//        Debug.Log("ignore swipe - " + ignore);
        return ignore;
    }

    private void Select(int index)
    {
        int countOfOptions = aspectRatioOptions.Count-1;
        if (countOfOptions == 0) return;

        aspectRatioOptions[currentOption].Deselect();
        currentOption = Mathf.Clamp(index, 0, countOfOptions);

        aspectRatioOptions[currentOption].Select();
        onOptionSelected?.Invoke(aspectRatioOptions[currentOption].aspectRatio);
        if (hintCoroutine != null)
            StopCoroutine(hintCoroutine);
        hintCoroutine = StartCoroutine(ShowHint());
    }
}

[Serializable]
public class AspectRatioOption
{
    [SerializeField] private Button optionButton;
    [SerializeField] private Text text;
    public float aspectRatio;

    public void Init(Action<AspectRatioOption> onButtonPressed)
    {
        optionButton.onClick.RemoveAllListeners();
        optionButton.onClick.AddListener(() => { onButtonPressed?.Invoke(this); });
    }

    public void Disable()
    {
        optionButton.gameObject.SetActive(false);
    }

    public void Enable()
    {
        optionButton.gameObject.SetActive(true);
    }

    public void SetTextColor(Color color)
    {
        text.color = color;
    }

    public void Select()
    {
        text.fontStyle = FontStyle.Bold;
    }

    public void Deselect()
    {
        text.fontStyle = FontStyle.Normal;
    }
}