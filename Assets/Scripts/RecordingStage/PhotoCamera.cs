﻿using System;
using System.Collections;
using System.Collections.Generic;
using Executor;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace PhotoCapture
{
    public class PhotoCamera
    {
        private RawImage rawimage;
        private WebCamTexture webcamTexture;
        private bool isPlaying = false;
        private int canvasHeight;
        private int bottomLineHeight = 0;
        private Coroutine cameraWaiterCoroutine;
        public WebCamTexture WebcamTexture => webcamTexture;

        public PhotoCamera(RawImage rawimage, int canvasHeight)
        {
            this.rawimage = rawimage;
            this.canvasHeight = canvasHeight;
        }

        public void RunCamera()
        {
            if (isPlaying) return;
            if (webcamTexture == null)
                webcamTexture = new WebCamTexture(4096, 4096);
            //SetImageSizeByWebcamRatio(rawimage, webcamTexture.height / webcamTexture.width);
            if (cameraWaiterCoroutine != null)
                CoroutineExecutor.Instance.StopCoroutine(cameraWaiterCoroutine);
            cameraWaiterCoroutine = CoroutineExecutor.Instance.StartCoroutine(CameraRunWaiter());
            //isPlaying = true;
        }

        private IEnumerator CameraRunWaiter()
        {
            webcamTexture.Play();
#if UNITY_IOS
            while (webcamTexture.width == webcamTexture.height)
            {
                yield return null;
            }

#else
            while (!webcamTexture.isPlaying)
            {
                yield return null;
            }
#endif
            SetImageSize(rawimage, webcamTexture);
            rawimage.texture = webcamTexture;
            rawimage.material.mainTexture = webcamTexture;
        }

        public void Pause()
        {
            if (cameraWaiterCoroutine != null)
                CoroutineExecutor.Instance.StopCoroutine(cameraWaiterCoroutine);
            if (!isPlaying || webcamTexture == null) return;
            webcamTexture.Pause();
            isPlaying = false;
        }

        public void StopCamera()
        {
            if (cameraWaiterCoroutine != null)
                CoroutineExecutor.Instance.StopCoroutine(cameraWaiterCoroutine);
            if (webcamTexture != null)
            {
                webcamTexture.Stop();
                Object.Destroy(webcamTexture);
            }

            isPlaying = false;
        }


        public Texture2D TakePhoto()
        {
            if (!isPlaying) RunCamera();
            if (webcamTexture == null)
                return new Texture2D(0, 0);
            Texture2D snap = new Texture2D(webcamTexture.width, webcamTexture.height);
            snap.SetPixels(webcamTexture.GetPixels());
            snap.Apply();

            // rotate texture
            var original = snap.GetPixels32();
            var length = original.Length;
            var rotated = new Color32[original.Length];
            var w = snap.width;
            var h = snap.height;

            for (var j = 0; j < h; ++j)
            {
                for (var i = 0; i < w; ++i)
                {
                    var iRotated = (i + 1) * h - j - 1;
                    var iOriginal = length - 1 - (j * w + i);
                    rotated[iRotated] = original[iOriginal];
                }
            }

            var rotatedTexture = new Texture2D(h, w);
            rotatedTexture.SetPixels32(rotated);
            rotatedTexture.Apply();

            return rotatedTexture;
        }

        private void SetImageSizeByWebcamRatio(RawImage image, float aspectRatio)
        {
            int height = Screen.height;
            int width = Mathf.RoundToInt(aspectRatio * height);
            image.rectTransform.sizeDelta = new Vector2(height, width);
        }

        // .... new ......

        private void SetImageSize(RawImage rawImage, WebCamTexture webcamTexture)
        {
            var webCamWidth = webcamTexture.width;
            var webCamHeight = webcamTexture.height;

            // to get the correct resolution
            if (webCamWidth == webCamHeight)
            {
                webcamTexture.Play();
                isPlaying = true;
                webCamWidth = webcamTexture.width;
                webCamHeight = webcamTexture.height;
            }

            var aspectRatio = (float) webCamWidth / webCamHeight;

            var imageY = canvasHeight - (bottomLineHeight * 2);
            var imageX = imageY / aspectRatio;

            // reversed because the plane is flipped
//#if UNITY_IOS
//            rawImage.uvRect = new Rect(Vector2.right, new Vector2(-1, 1));
//            rawImage.rectTransform.localRotation = Quaternion.Euler(0,0,-90);
//#endif
#if UNITY_IOS
            rawImage.uvRect = new Rect(Vector2.right, new Vector2(-1, 1));
            rawImage.rectTransform.localRotation = Quaternion.Euler(0, 0, 90);
#endif
            var rectTransform = rawImage.rectTransform;
            rectTransform.sizeDelta = new Vector2(imageY, imageX);
            rectTransform.localScale = Vector3.one;
            Debug.LogFormat("set ing size - {0}, {1}", rectTransform.localScale, rectTransform.rect);
            if (isPlaying) return;
            webcamTexture.Play();
            isPlaying = true;
        }
    }
}