﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;

public class ImgaeGifCamera : MonoBehaviour
{
    [SerializeField] private Canvas effectCanvas;
    [SerializeField] private Canvas mainCanvas;
    [SerializeField] private Camera imageGifCam;

    private void Start()
    {
      SetCanvasSize();
    }

    #region Enable/Disable

    private void OnEnable()
    {
        PostprocessController.OnSetRectToImage += SetSizeCamera;  
    }

    private void OnDisable()
    {
        PostprocessController.OnSetRectToImage -= SetSizeCamera;
    }

    #endregion

    #region Size camera
    
    public void SetSizeCamera(RectTransform imageRect)
    {
        CameraSize(imageRect);
        
        //var recorder = imageGifCam.GetComponent<Recorder>();
        //recorder?.Setup(true, recorder.Width, recorder.Height, recorder.FramePerSecond, recorder.Length);
        
        
    }
//    private void CameraSize(RectTransform imageRect)
//    {
//        var imgMinAnchor = imageRect.anchorMin;
//        var imgMaxAnchor = imageRect.anchorMax;
//
//        var canvasRect = effectCanvas.GetComponent<RectTransform>();
//        var canvasHeight = canvasRect.sizeDelta.y * canvasRect.lossyScale.y;
//        Debug.Log($" canvas rect {canvasRect.sizeDelta}, canvas height - {canvasHeight}");
//        var imageHeight = canvasHeight * (imgMaxAnchor.y - imgMinAnchor.y);
//        Debug.Log($"image name - {imageRect.gameObject.name} image heught - {imageHeight},img min {imageRect.anchorMin}, img max {imageRect.anchorMax}");
//        imageGifCam.orthographicSize = imageHeight / 2f;
//Debug.Log("imageGifCam.orthographicSize = imageHeight / 2f; - " + imageGifCam.orthographicSize);
//        var imageAnchorWidth = imgMaxAnchor.x - imgMinAnchor.x;
//        var imageAnchorHeight = imgMaxAnchor.y - imgMinAnchor.y;
//     //   imageGifCam.rect = new Rect(0f, 0f, imageAnchorWidth, imageAnchorHeight);
//    }
    private void CameraSize(RectTransform imageRect)
    {
        var imgMinAnchor = imageRect.anchorMin;
        var imgMaxAnchor = imageRect.anchorMax;

        var canvasRect = effectCanvas.GetComponent<RectTransform>();
        var canvasHeight = canvasRect.sizeDelta.y * canvasRect.lossyScale.y;
        var canvasWidth = canvasRect.sizeDelta.x * canvasRect.lossyScale.x;
        var imageHeight = canvasHeight * (imgMaxAnchor.y - imgMinAnchor.y);
        var imageWidth = canvasWidth * (imgMaxAnchor.x - imgMinAnchor.x);
        if(Screen.width / (float)Screen.height > imageWidth/imageHeight)
        {
            imageGifCam.orthographicSize = imageHeight / 2f;
        }
        else
        {
            var newImageHeight = Screen.height * imageWidth / (float)Screen.width;
            imageGifCam.orthographicSize = newImageHeight / 2f;
        }
    }
    #endregion


    private void SetCanvasSize()
    {
        //var effectRect = effectCanvas.gameObject.GetComponent<RectTransform>();
        //var mainRect = mainCanvas.gameObject.GetComponent<RectTransform>();

        //effectRect.sizeDelta = mainRect.sizeDelta;

        effectCanvas.renderMode = RenderMode.WorldSpace;
    }

    public void SetRecorerSettings(float lengs)
    {
     //   var recorder = imageGifCam.GetComponent<Recorder>();
        //recorder?.Setup(true, recorder.Width, recorder.Height, recorder.FramePerSecond, recorder.Length);
        // recorder?.SetFPSbyLengs(lengs);
    }

}

