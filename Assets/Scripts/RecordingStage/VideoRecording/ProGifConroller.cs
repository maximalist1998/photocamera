﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProGifConroller : MonoBehaviour
{
    private System.Action _OnStartRecord;
    public static System.Action<string> OnVideoWasExported;

    public static string PP_LastGifPathKey = "ProGIF_LastGifPathKey";
    [HideInInspector]
    public ProGifRecorder m_GifRecorder = null;
    [HideInInspector]
    public  string m_CurrentGifPath = "";
    
    #region ----- GIF Recorder Settings -----

    [Header("Gif recording settings")] 
    public Camera recordingCamera;
    private int m_MaxFps = 60;

    public Vector2 m_AspectRatio = new Vector2(0, 0);
    public bool m_AutoAspect = false;
    public int m_Width = 360;
    public int m_Height = 360;
    public float m_Duration = 3f;
    [Range(1, 60)] public int m_Fps = 15;
    public int m_Loop = 0;								//-1: no repeat, 0: infinite, >0: repeat count
    [Range(1, 100)] public int m_Quality = 20;			//(1 - 100), 1: best(larger storage size), 100: faster(smaller storage size)

    public ImageRotator.Rotation m_Rotation = ImageRotator.Rotation.None;
    public Color32 m_TransparentColor = new Color32(0, 0, 0, 0);
    public bool m_AutoTransparent = false;
    public bool m_OptimizeMemoryUsage = true;

    public int MaxSideResolution
    {
        get { return maxSideResolution; }
        set
        {
            maxSideResolution = Mathf.Clamp(value,128,512);
        }
    }

    private int maxSideResolution = 512;
    [SerializeField] private TextMeshProUGUI progressText;
    [SerializeField] private Image uiActionsBlockerImage;
    #endregion
    
    #region ---- Callbacs ----
    private System.Action<int, string> _OnFileSavedAction = null;
    private System.Action<int, float> _OnFileSaveProgressAction = null;
    private System.Action _OnRecorderPreProcessingDoneAction = null;
    private System.Action<float> _OnRecordProgressAction = null;
    private System.Action _OnRecordDurationMaxAction = null;
    #endregion

    #region Playing controll
    
    public void StartGifRecording()
    {
        //Debug.Log("dropdown_Rotation.value: " + dropdown_Rotation.value + " - " + _GetRotation(dropdown_Rotation.value));

        //SetupAspectRatio();
        _InitRecorder(recordingCamera, true);

        //Start the recording with a callback that will be called when max. frames are stored in recorder
        m_GifRecorder.Record(OnRecordDurationMax);

        //Set the callback to update the record progress during recording.
        m_GifRecorder.SetOnRecordAction(OnRecordProgress);
        _OnStartRecord?.Invoke(); 
        uiActionsBlockerImage.gameObject.SetActive(true);
        progressText.gameObject.SetActive(true);
        progressText.text = "Recording, please wait";
    }
    
    public void SetFPSbyLengs(float lengs)
    {

        if (lengs < 5)
        {
            m_Fps = 25;
        }else if (lengs >5 && lengs < 8)
        {
            m_Fps = 20;
        }
        else if (lengs > 8)
        {
            m_Fps = 15;
        }
        
        SetupAspectRatio();
               
        
       // Setup(false,m_Width,_height,_framePerSecond,lengs);
        SetRecordSettings(false,m_Width,m_Height,lengs,m_Fps,0,10);
    }

    public void SetupAspectRatio()
    {

        if (recordingCamera.pixelWidth > recordingCamera.pixelHeight)
        {
            m_Width = 512;
            float deltaRotio = (float)recordingCamera.pixelWidth / (float)recordingCamera.pixelHeight;
            m_Height = (int)(m_Width / deltaRotio);
            
            Debug.Log("width - " + m_Width  + "  height - " + m_Height );
        }
        else
        {
            m_Height = 512;
            float deltaRotio = (float)recordingCamera.pixelHeight / (float)recordingCamera.pixelWidth;
            m_Width = (int)(m_Height / deltaRotio);
            
            Debug.Log("width - " + m_Width  + "  height - " + m_Height );
        }
        
        m_AspectRatio = new Vector2(recordingCamera.pixelWidth,recordingCamera.pixelHeight);;
    }

    #region Recording Callbacks

    private void OnRecordDurationMax()
    {
        Debug.Log("max frame was recorded ");
    }

    private void OnRecordProgress(float progress)
    {
       // progressText.text = string.Format(" GENERATING ... {0:P0}", progress);
    }

    #endregion
    

    public void StopRecordingGif()
    {
        m_GifRecorder.Stop();
        
        SaveRecord(
            () => { Debug.Log("On recorder pre-processing done."); },

            (id, progress) =>
            {
                progressText.gameObject.SetActive(true);
                uiActionsBlockerImage.gameObject.SetActive(true);
                progressText.text = string.Format(" GENERATING ... {0:P0}", progress);
            },

            (id, path) =>
            {
                //bool loadFile = false; // ProGifManager.Instance.LoadFile;
                Debug.Log("OnFileSave: " + path);

                //ShowGifPreviewAndSharePanel(path, loadFile:false);
                progressText.gameObject.SetActive(false);
                uiActionsBlockerImage.gameObject.SetActive(false);
                // Save to Native Gallery (Native Save Path available on Android only)
#if UNITY_ANDROID
                
                
                string androidNativeSavePath = MobileMedia.CopyMedia(path, "Pixchange",
                    System.IO.Path.GetFileNameWithoutExtension(path), ".gif", isImage: true);
                Debug.Log("Mobile Media Save Path: " + androidNativeSavePath);
#endif
               OnVideoWasExported?.Invoke(path);
            }
        );
    }
    public void SaveRecord(System.Action onRecorderPreProcessingDone = null, System.Action<int, float> onFileSaveProgress = null, System.Action<int, string> onFileSaved = null, string fileNameWithoutExtension = "")
    {
        //Set callbacks
        _OnRecorderPreProcessingDoneAction = onRecorderPreProcessingDone;
        _OnFileSaveProgressAction = onFileSaveProgress;
        _OnFileSavedAction = onFileSaved;

        //Saves the stored frames to a gif file. 
        m_GifRecorder.Save(fileNameWithoutExtension);
    }
    
    public void PauseGifRecord()
    {
        Debug.Log("Pause Recording");
        ProGifManager.Instance.PauseRecord();
    }

    public void ResumeGifRecord()
    {
        Debug.Log("Resume Recording");
        ProGifManager.Instance.ResumeRecord();
    }
    #endregion

    #region >Settings

    public void SetRecordSettings(bool autoAspect, int width, int height, float duration, int fps, int loop, int quality)
    {
        m_AutoAspect = autoAspect;
        m_Width = width;
        m_Height = height;
        m_Fps = fps;
        m_Duration = duration;
        m_Loop = loop;
        m_Quality = quality;

        m_AspectRatio = new Vector2(recordingCamera.pixelWidth,recordingCamera.pixelHeight); //Use auto aspect
    }
    

    private void _InitRecorder(Camera camera, bool autoClear)
    {
        Clear();

        if(m_Fps > m_MaxFps) m_Fps = m_MaxFps;

        m_GifRecorder = new ProGifRecorder(camera);
        if(m_AspectRatio.x > 0 && m_AspectRatio.y > 0)
        {
            m_GifRecorder.Setup(
                m_AspectRatio, 	//a specify aspect ratio for cropping gif
                m_Width,  		//width
                m_Height,  		//height
                m_Fps,   		//fps
                m_Duration, 	//recorder time
                m_Loop,    		//repeat, -1: no repeat, 0: infinite, >0: repeat count
                m_Quality);  	//quality (1 - 100), 1: best, 100: faster
        }
        else
        {
            m_GifRecorder.Setup(
                m_AutoAspect, 	//autoAspect
                m_Width,  		//width
                m_Height,  		//height
                m_Fps,   		//fps
                m_Duration, 	//recorder time
                m_Loop,    		//repeat, -1: no repeat, 0: infinite, >0: repeat count
                m_Quality);  	//quality (1 - 100), 1: best, 100: faster
        }

        //Set the gif transparent color
        m_GifRecorder.SetTransparent(m_TransparentColor);

        //Enable/Disable auto detect the image transparent setting for enabling transparent
        m_GifRecorder.SetTransparent(m_AutoTransparent);

        //Set the gif rotation
        m_GifRecorder.SetGifRotation(m_Rotation);

        //Set the callback to be called when pre-processing complete
        m_GifRecorder.OnPreProcessingDone += _OnRecorderPreProcessingDone;

        //Set the callback to update the gif save progress
        m_GifRecorder.OnFileSaveProgress += _OnRecorderFileSaveProgress;

        //Set the callback to be called when gif file saved
        m_GifRecorder.OnFileSaved += _OnRecorderFileSaved;

        //Set the callback to clear the recorder after gif saved
        if(autoClear)
        {
            System.Action<int, string> clearRecorder = (id, path) => {
                ClearRecorder();
            };
            m_GifRecorder.OnFileSaved += clearRecorder;
        }
    }
    
    /// Set Enable or Disable for optimizing memory usage for gif players.
    /// (Call this method before playing GIF)
    public void SetPlayerOptimization(bool enable)
    {
        m_OptimizeMemoryUsage = enable;
    }

    #region >>Callbacs

    /// <summary>
    /// To be called during save frames to gif file in worker thread
    /// </summary>
    /// <param name="id">Identifier.</param>
    /// <param name="progress">Progress.</param>
    private void _OnRecorderFileSaveProgress(int id, float progress)
    {
        if(_OnFileSaveProgressAction != null) _OnFileSaveProgressAction(id, progress);
    }

    /// <summary>
    /// To be called when PreProcessing complete
    /// (PreProcessing: The process to extract, crop frame data and send everything to a separate worker thread)
    /// </summary>
    private void _OnRecorderPreProcessingDone()
    {
        if(_OnRecorderPreProcessingDoneAction != null) _OnRecorderPreProcessingDoneAction();
    }
    
    //Unsubscribe and memory flushing in OnFileSaved callback
    private void _OnRecorderFileSaved(int id, string path)
    {
        #if UNITY_EDITOR
        Debug.Log("Current saved gif path: " + path);
        #endif
        m_CurrentGifPath = path;
        PlayerPrefs.SetString(PP_LastGifPathKey, path);

        if(_OnFileSavedAction != null) _OnFileSavedAction(id, path);
    }

    #endregion
    
    #region >> Clear

    public void Clear()
    {
       ClearRecorder();
    }
    
    public void ClearRecorder()
    {
        if(m_GifRecorder != null)
        {
            m_GifRecorder.OnPreProcessingDone -= _OnRecorderPreProcessingDone;
            m_GifRecorder.OnFileSaveProgress -= _OnRecorderFileSaveProgress;
            m_GifRecorder.OnFileSaved -= _OnRecorderFileSaved;

            m_GifRecorder.Clear();
            m_GifRecorder = null;
        }
    }
    
    #endregion
    
    #endregion
}
