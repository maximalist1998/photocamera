﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using NatCorder.Examples;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UniUtils;

public class VideoRecordingController : MonoBehaviour
{
    public System.Action<string> OnVideoWasExported;

    [SerializeField] private Image uiActionsBlockerImage;
    [SerializeField] private ReplayCam replayCam;

    [SerializeField] private TextMeshProUGUI progressText;
    private const int maxSideResolution = 1024;
    public Vector2 Offset { get; private set; }
    public Vector2 Scale { get; private set; }

    public bool isFaceMaskRecording = false;


    public void StartRecording(RectTransform recordingRect)
    {
        Debug.LogError($"Start Recording! {recordingRect.gameObject.name}");
        if (progressText != null)
        {
            progressText.gameObject.SetActive(true);
            progressText.text = "Recording, please wait";
        }

        if (uiActionsBlockerImage != null)
            uiActionsBlockerImage.gameObject.SetActive(true);
        SetupCameraByRectTransform(recordingRect);
        int videoWidth;
        int videoHeight;
        var rect = recordingRect.rect;
        int rotation = Mathf.RoundToInt(recordingRect.transform.eulerAngles.z) % 360;
        if (!isFaceMaskRecording && (rotation == 90 || rotation == -90 || rotation == 270 || rotation == -270))
        {
            rect = new Rect(rect.x, rect.y, rect.height, rect.width);
        }
        
        if (rect.width > rect.height)
        {
            videoWidth = maxSideResolution;
            videoHeight = Mathf.RoundToInt(maxSideResolution * rect.height / rect.width);
        }
        else
        {
            videoWidth = Mathf.RoundToInt(maxSideResolution * rect.width / rect.height);
            videoHeight = maxSideResolution;
        }
        
//        int rotation = Mathf.RoundToInt(recordingRect.transform.eulerAngles.z) % 360;
//        var absRotarion = Mathf.Abs(rotation);
//        if (absRotarion == 0 || absRotarion == 180)
//        {
            replayCam.videoWidth = videoWidth;
            replayCam.videoHeight = videoHeight;
//        }
//        else
//        {
//            replayCam.videoWidth = videoHeight;
//            replayCam.videoHeight = videoWidth;
//        }

        replayCam.StartRecording(this);
    }

    private void SetupCameraByRectTransform(RectTransform rectTransform)
    {
        Vector3[] worldCorners = new Vector3[4];
        rectTransform.GetWorldCorners(worldCorners);
        var recordingCamera = replayCam.recordingCameras[0];
        var minViewportPoint = recordingCamera.WorldToViewportPoint(worldCorners[0]);
        var maxViewportPoint = recordingCamera.WorldToViewportPoint(worldCorners[2]);
//        int rotation = Mathf.RoundToInt(rectTransform.transform.eulerAngles.z) % 360;
//
//        if (rotation == -90 || rotation == 270)
//        {
//            var y = minViewportPoint.y;
//            minViewportPoint.y = maxViewportPoint.y;
//            maxViewportPoint.y = y;
//        }
//        else if (Mathf.Abs(rotation) == 180)
//        {
//            var viewport = maxViewportPoint;
//            maxViewportPoint = minViewportPoint;
//            minViewportPoint = viewport;
//        }
//        else if (rotation == 90 || rotation == -270)
//        {
//            var x = minViewportPoint.x;
//            minViewportPoint.x = maxViewportPoint.x;
//            maxViewportPoint.x = x;
//        }

        var newMin = new Vector3(
            Mathf.Min(minViewportPoint.x, maxViewportPoint.x),
            Mathf.Min(minViewportPoint.y, maxViewportPoint.y),
            0f
        );
        var newMax = new Vector3(
            Mathf.Max(minViewportPoint.x, maxViewportPoint.x),
            Mathf.Max(minViewportPoint.y, maxViewportPoint.y),
            0f
        );

        minViewportPoint = newMin;
        maxViewportPoint = newMax;

        Offset = new Vector2(minViewportPoint.x, minViewportPoint.y);
        Scale = new Vector2(
            maxViewportPoint.x - minViewportPoint.x,
            maxViewportPoint.y - minViewportPoint.y
        );
//        Debug.Log($"offset - {Offset}, scale - {Scale}");
//        foreach (var corner in worldCorners)
//        {
//            Debug.Log($"world corner - {corner}");
//        }
//
//        Debug.Log(
//            $"min viewport - {minViewportPoint}, max viewport - {maxViewportPoint} offset - {Offset}, Scale - {Scale}");
    }

    public void StopRecording()
    {
        if (progressText != null)
            progressText.gameObject.SetActive(false);
        if (uiActionsBlockerImage != null)
            uiActionsBlockerImage.gameObject.SetActive(false);
        replayCam.StopRecording();
    }

    public void ClearRecorder()
    {
        replayCam.ClearRecorder();
    }
}