﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class RecordingContent
{
    public string id;
    public RecordingContentType contentType;
    public Sprite previewImage;
}
public enum RecordingContentType
{
    Mask,
    Filter,
    Object
}