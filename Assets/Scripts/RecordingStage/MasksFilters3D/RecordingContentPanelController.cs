﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FaceTracker.DB;
using MaskBuilder;
using UnityEngine;
using UnityEngine.UI;

public class RecordingContentPanelController : MonoBehaviour
{
    [SerializeField] private List<RecordingContent> masks = new List<RecordingContent>();
    [SerializeField] private List<RecordingContent> filters = new List<RecordingContent>();
    [SerializeField] private List<RecordingContent> objects = new List<RecordingContent>();
    [SerializeField] private RectTransform mainButtons;
    [SerializeField] private Button masksButton;
    [SerializeField] private Button filtersButton;
    [SerializeField] private Button objectsButton;

    [SerializeField] private Button modesSwitchObjectsButton;
    [SerializeField] private Button modesSwitchMasksButton;
    [SerializeField] private Button modesSwitchBackButton;

    [SerializeField] private RecordingContentScrollView recordingContentScrollView;

    [SerializeField] private float panelMovingTime = 1f;

    public static event Action<RecordingContent> onContentSelected;

    private Coroutine movingCoroutine;
    private float hiddenPosition;
    private float defaultPosition;
    [SerializeField] private RecordingButton recordingButton;

    private void OnEnable()
    {
        recordingButton.onVideoButtonPressed += Disable;
        recordingButton.onVideoButtonReleased += Show;

        FaceMasksData.OnFaceMasksUpdated += OnMasksUpdatedHandler;

        //Ivan: Added for MVP version
        mainButtons.anchoredPosition = new Vector2(defaultPosition, mainButtons.anchoredPosition.y);
        modesSwitchBackButton.gameObject.SetActive(false);
        mainButtons.gameObject.SetActive(true);
        recordingContentScrollView.HideInFrame();
    }
    private void OnDisable()
    {
        recordingButton.onVideoButtonPressed -= Disable;
        recordingButton.onVideoButtonReleased -= Show;
        
        FaceMasksData.OnFaceMasksUpdated -= OnMasksUpdatedHandler;
    }

    private void Start()
    {
        //Initialize masks list from DB
        UpdateMasksList();
        
        defaultPosition = mainButtons.anchoredPosition.x;
        hiddenPosition = defaultPosition + Screen.width;
        masksButton.onClick.AddListener(() =>
        {
            if (movingCoroutine != null)
                StopCoroutine(movingCoroutine);
            movingCoroutine =
                StartCoroutine(Hide(() =>
                {
                    //Ivan: Disable For MVP
                    modesSwitchBackButton.gameObject.SetActive(true);
//                    modesSwitchObjectsButton.gameObject.SetActive(true);
                    modesSwitchMasksButton.gameObject.SetActive(false);
                    recordingContentScrollView.Show(masks, onContentSelected);
                }));
        });
        
        modesSwitchBackButton.onClick.AddListener(() =>
        {
            recordingContentScrollView.Hide(() =>
            {
                Show();
            });
        });
        
        //Ivan: Disable For MVP
//        filtersButton.onClick.AddListener(() =>
//        {
//            if (movingCoroutine != null)
//                StopCoroutine(movingCoroutine);
//            movingCoroutine =
//                StartCoroutine(Hide(() =>
//                {
//                    modesSwitchObjectsButton.gameObject.SetActive(false);
//                    modesSwitchMasksButton.gameObject.SetActive(false);
//                    recordingContentScrollView.Show(filters, onContentSelected);
//                }));
//        });
//        objectsButton.onClick.AddListener(() =>
//        {
//            if (movingCoroutine != null)
//                StopCoroutine(movingCoroutine);
//            movingCoroutine =
//                StartCoroutine(Hide(() =>
//                {
//                    modesSwitchObjectsButton.gameObject.SetActive(false);
//                    modesSwitchMasksButton.gameObject.SetActive(true);
//                    recordingContentScrollView.Show(objects, onContentSelected);
//                }));
//        });
//        modesSwitchObjectsButton.onClick.AddListener(() =>
//        {
//            recordingContentScrollView.HideAndOpenAnother(objects, onContentSelected);
//            modesSwitchObjectsButton.gameObject.SetActive(false);
//            modesSwitchMasksButton.gameObject.SetActive(true);
//        });
//        modesSwitchMasksButton.onClick.AddListener(() =>
//        {
//            recordingContentScrollView.HideAndOpenAnother(masks, onContentSelected);
//            modesSwitchObjectsButton.gameObject.SetActive(true);
//            modesSwitchMasksButton.gameObject.SetActive(false);
//        });
    }

    private void OnMasksUpdatedHandler()
    {
        UpdateMasksList();
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);
        movingCoroutine = StartCoroutine(Hide(() =>
            {
                //Ivan: Disable For MVP
                modesSwitchBackButton.gameObject.SetActive(true);
//                modesSwitchObjectsButton.gameObject.SetActive(true);
                modesSwitchMasksButton.gameObject.SetActive(false);
                recordingContentScrollView.Show(masks, onContentSelected);
            }));
    }

    private void UpdateMasksList()
    {
        masks.Clear();

        foreach (var mask in FaceMasksData.Instance.GetMasks())
        {
            var newMask = new RecordingContent
            {
                id = mask.UniqueName, 
                contentType = RecordingContentType.Mask, 
                previewImage = mask.Preview
            };
            masks.Add(newMask);
        }
    }

    private IEnumerator Hide(Action onFinish)
    {
        float timer = 0;
        float startPosition = mainButtons.anchoredPosition.x;
        while (mainButtons.anchoredPosition.x != hiddenPosition)
        {
            yield return null;

            timer += Time.deltaTime;
            mainButtons.anchoredPosition =
                new Vector2(Mathf.Lerp(startPosition, hiddenPosition, Mathf.Clamp01(timer / panelMovingTime)),
                    mainButtons.anchoredPosition.y);
            if (timer >= panelMovingTime)
            {
                break;
            }
        }

        mainButtons.gameObject.SetActive(false);
        onFinish?.Invoke();
    }

    private IEnumerator Show(Action onFinish)
    {
        float timer = 0;
        float startPosition = mainButtons.anchoredPosition.x;
        modesSwitchObjectsButton.gameObject.SetActive(false);
        modesSwitchMasksButton.gameObject.SetActive(false);
        modesSwitchBackButton.gameObject.SetActive(false);
        mainButtons.gameObject.SetActive(true);
        while (mainButtons.anchoredPosition.x != defaultPosition)
        {
            yield return null;

            timer += Time.deltaTime;
            mainButtons.anchoredPosition =
                new Vector2(Mathf.Lerp(startPosition, defaultPosition, Mathf.Clamp01(timer / panelMovingTime)),
                    mainButtons.anchoredPosition.y);
            if (timer >= panelMovingTime)
            {
                break;
            }
        }

        recordingContentScrollView.Disable();
        onFinish?.Invoke();
    }

    public void Show()
    {
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);
        movingCoroutine =
            StartCoroutine(Show(() => { }));
    }

    public void Disable()
    {
        mainButtons.gameObject.SetActive(false);
        recordingContentScrollView.Disable();
    }
    
}