﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RecordingContentUI:MonoBehaviour
{
    
    [SerializeField] private Button contentButton;
    [SerializeField] private Image contentImage;
    [SerializeField] private Image borderImage;

    private RecordingContent currContent;

    public void Init(RecordingContent recordingContent,Action<RecordingContent> onSelect)
    {
        currContent = recordingContent;
        
        gameObject.SetActive(true);
        contentImage.sprite = recordingContent.previewImage;// Sprite.Create(previewTexture2D,new Rect(0,0,previewTexture2D.width,previewTexture2D.height),new Vector2(0.5f,0.5f) );
        contentButton.onClick.RemoveAllListeners();
        contentButton.onClick.AddListener(() => { onSelect?.Invoke(recordingContent);});
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }

    public RecordingContent GetContent()
    {
        return currContent;
    }

    public void ActivateBorder()
    {
        borderImage.color = Color.green;
    }
    
    public void DeactivateBorder()
    {
        borderImage.color = Color.white;
    }
}
