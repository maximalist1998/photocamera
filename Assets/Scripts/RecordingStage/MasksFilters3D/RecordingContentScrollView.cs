﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class RecordingContentScrollView : MonoBehaviour
{
    [SerializeField] private RectTransform scrollViewPanel;
    

    private List<RecordingContentUI> recordingContentUis = new List<RecordingContentUI>();
    [SerializeField] private RecordingContentUI recordingContentUiPrefab;
    [SerializeField] private RecordingContentPanelController recordingContentPanelController;
    [SerializeField] private Transform contentParent;

    [SerializeField] private GameObject masksLabel;
    [SerializeField] private GameObject filtersLabel;
    [SerializeField] private GameObject objectsLabel;
    
    
    
    
    private float hiddenPosition = Screen.width;
    private float defaultPosition = 0;
    [SerializeField] private float panelMovingTime = 1f;
    private Coroutine movingCoroutine;
    
    private RecordingContent currentSelected = null;

    

    public void Show(List<RecordingContent> recordingContents, Action<RecordingContent> onSelect)
    {
        if (recordingContents == null) return;
        if (recordingContents.Count == 0) return;


        switch (recordingContents[0].contentType)
        {
            case RecordingContentType.Mask:
            {
                masksLabel.SetActive(true);
                filtersLabel.SetActive(false);
                objectsLabel.SetActive(false);
                break;
            }
            case RecordingContentType.Object:
            {
                masksLabel.SetActive(false);
                filtersLabel.SetActive(false);
                objectsLabel.SetActive(true);
                break;
            }
            case RecordingContentType.Filter:
            {
                masksLabel.SetActive(false);
                filtersLabel.SetActive(true);
                objectsLabel.SetActive(false);
                break;
            }
        }

        int countOfObjects = recordingContents.Count;
        int countOfNeededNew = Mathf.Clamp(countOfObjects - recordingContentUis.Count, 0, countOfObjects);
        for (int i = 0; i < countOfNeededNew; i++)
        {
            recordingContentUis.Add(Instantiate(recordingContentUiPrefab, contentParent));
        }

        for (int i = countOfObjects; i < recordingContentUis.Count; i++)
        {
            recordingContentUis[i].Disable();
        }

        for (int i = 0; i < countOfObjects; i++)
        {
            recordingContentUis[i].Init(recordingContents[i],(data)=>
            {
//                Hide();
                currentSelected = data;
                DeactivateAll();
                ActivateForContent(currentSelected);

                onSelect?.Invoke(data);
            });
        }
       
        scrollViewPanel.gameObject.SetActive(true);
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);
        movingCoroutine = StartCoroutine(Show(() => { }));

        currentSelected = null;
        DeactivateAll();
    }

    private IEnumerator Show(Action onFinish)
    {
        float timer = 0;
        float startPosition = hiddenPosition;
        scrollViewPanel.gameObject.SetActive(true);
        contentParent.localPosition = new Vector3(0,contentParent.localPosition.y,contentParent.localPosition.z);
        
        scrollViewPanel.anchoredPosition =
            new Vector2(hiddenPosition,
                scrollViewPanel.anchoredPosition.y);
        while (true)
        {

            yield return null;
            timer += Time.deltaTime;
            scrollViewPanel.anchoredPosition =
                new Vector2(Mathf.Lerp(startPosition, defaultPosition, Mathf.Clamp01(timer / panelMovingTime)),
                    scrollViewPanel.anchoredPosition.y);
            if (timer >= panelMovingTime)
            {
                break;
            }
        }

        onFinish?.Invoke();
    }

    private void DeactivateAll()
    {
        foreach (var content in recordingContentUis)
        {
            content.DeactivateBorder();
        }
    }

    private void ActivateForContent(RecordingContent content)
    {
        foreach (var uiContent in recordingContentUis)
        {
            if (uiContent.GetContent() == content)
            {
                uiContent.ActivateBorder();
                return;
            }
        }
    }
    
    private IEnumerator HideCor(Action onFinish)
    {
        float timer = 0;
        float startPosition =scrollViewPanel.anchoredPosition.x;
        float contentStartPosition =contentParent.localPosition.x;
        
        while (true)
        {
            yield return null;

            timer += Time.deltaTime;
            float percentage = Mathf.Clamp01(timer / panelMovingTime);
            scrollViewPanel.anchoredPosition =
                new Vector2(Mathf.Lerp(startPosition, hiddenPosition, percentage),
                    scrollViewPanel.anchoredPosition.y);
            contentParent.localPosition = new Vector3(Mathf.Lerp(contentStartPosition,0,percentage),contentParent.localPosition.y,contentParent.localPosition.z);
            if (timer >= panelMovingTime)
            {
                break;
            }
        }
        scrollViewPanel.gameObject.SetActive(false);

        onFinish?.Invoke();
    }
    public void Hide(Action onFinished)
    {
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);
        movingCoroutine = StartCoroutine(HideCor(() =>
        {
            foreach (var recordingContent in recordingContentUis)
            {
                recordingContent.Disable();
            } 
            recordingContentPanelController.Show();
            onFinished?.Invoke();
        }));
    }
    
    public void HideInFrame()
    {
        scrollViewPanel.anchoredPosition = new Vector2(hiddenPosition, scrollViewPanel.anchoredPosition.y);
        foreach (var recordingContent in recordingContentUis)
        {
            recordingContent.Disable();
        }
    }

    public void HideAndOpenAnother(List<RecordingContent> recordingContents, Action<RecordingContent> onSelect)
    {
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);
        movingCoroutine = StartCoroutine(HideCor(() => { Show(recordingContents, onSelect); }));
       
    }
    public void Disable()
    {
        scrollViewPanel.gameObject.SetActive(false);
    }
}