﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LayoutElement))]
public class PreferedWidthSync : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        var layoutElement = GetComponent<LayoutElement>();
        layoutElement.preferredWidth = layoutElement.preferredHeight;
    }

  
}
