﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using UniUtils;

namespace PhotoCapture
{
    public class PhotoCameraController : Singleton<PhotoCameraController>
    {
       
        [SerializeField] private RawImage cameraViewImage;
    

        private Action<Texture2D> OnImagePicked;
        private Action OnCancel;

        private PhotoCamera photoCamera;
        private Texture2D textureFromCamera;

        private void Start()
        {
            StartPhotoCamera();
        }

        public void StartPhotoCamera()
        {
            
            photoCamera = new PhotoCamera(cameraViewImage,Screen.height);
photoCamera.RunCamera();
            SetNormaImageSize();
        }

//      

      

     

     


     
       

     

        private void SetNormaImageSize()
        {
#if UNITY_IOS
            cameraViewImage.uvRect = new Rect(Vector2.right, new Vector2(-1, 1));
            cameraViewImage.rectTransform.localRotation = Quaternion.Euler(0,0,90);
#endif
//#if UNITY_IOS
//            cameraViewImage.uvRect = new Rect(Vector2.right, new Vector2(-1, 1));
//            cameraViewImage.rectTransform.localRotation = Quaternion.Euler(0,180,-90);
//#endif
            cameraViewImage.rectTransform.localScale = Vector3.one;
            Debug.LogFormat("SetNormaImageSize- {0}, {1}", cameraViewImage.rectTransform.localScale,cameraViewImage.rectTransform.rect);
        }
    }
}
