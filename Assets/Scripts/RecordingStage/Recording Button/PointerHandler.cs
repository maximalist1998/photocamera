﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerHandler : MonoBehaviour,IPointerDownHandler, IPointerUpHandler
{
    public event Action<PointerEventData> onPointerDown;
    public event Action<PointerEventData> onPointerUp;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        onPointerDown?.Invoke(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onPointerUp?.Invoke(eventData);
    }
}
