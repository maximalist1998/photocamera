﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Executor;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RecordingButton : MonoBehaviour
{
    public event Action onPhotoButtonPressed;
    public event Action onVideoButtonPressed;
    public event Action onVideoButtonReleased;

    public float delayForVideoModeStarting = 1f;
    [Range(0.1f, 1)] [SerializeField] private float lockActivation = 1f;
    [SerializeField] private float maxVideoTime = 15f;

    [SerializeField] private PointerHandler recordingButton;
    [SerializeField] private RectTransform recordingButtonParentRectTransform;
    [SerializeField] private Image recordingButtonOutline;
    [SerializeField] private Image lockImage;
    [SerializeField] private Image lockArrow;
    [SerializeField] private Image videoRecordingButtonLocked;
    [SerializeField] private Image videoRecordingButtonUnlocked;

    private RectTransform recordingButtonRectTransform;
    private float maxButtonMovingHeight;
    private bool isLocked = false;
    private bool isRecording = false;
    private Coroutine clickCoroutine;
    private Coroutine movingCoroutine;
#if UNITY_IOS

    protected virtual void Awake()
    {
        MMVibrationManager.iOSInitializeHaptics();
    }
#endif
    private void Start()
    {
        recordingButtonRectTransform = recordingButton.GetComponent<RectTransform>();
        var rect = recordingButtonParentRectTransform.rect;
        maxButtonMovingHeight =
            Mathf.Clamp(rect.height - recordingButtonRectTransform.rect.height, 0,
                rect.height); //according to size of button if button height 48% of panel size we can use 52%
    }

    private void OnEnable()
    {
        recordingButton.onPointerDown += PointerDownHandler;
        recordingButton.onPointerUp += PointerUpHandler;
    }

    private void PointerDownHandler(PointerEventData eventData)
    {
        if (!isRecording)
        {
            if (clickCoroutine != null)
                StopCoroutine(clickCoroutine);
            clickCoroutine = StartCoroutine(ClickCoroutine(eventData));
        }
        else if (isLocked)
        {
            ActivateDefaultState();
            onVideoButtonReleased?.Invoke();
            MMVibrationManager.Vibrate();
            if (movingCoroutine != null)
            {
                StopCoroutine(movingCoroutine);
            }

            recordingButtonRectTransform.anchoredPosition = Vector2.zero;
        }
    }

    private void PointerUpHandler(PointerEventData eventData)
    {
        if (isRecording && !isLocked)
        {
            ActivateDefaultState();
            onVideoButtonReleased?.Invoke();
            Debug.Log("!!!onVideoButtonReleased 2");

            if (clickCoroutine != null)
                StopCoroutine(clickCoroutine);
            recordingButtonRectTransform.anchoredPosition = Vector2.zero;
        }
        else if (!isRecording)
        {
            ActivateDefaultState();
            onPhotoButtonPressed?.Invoke();
            MMVibrationManager.Vibrate();
            Debug.Log("!!!onPhotoButtonPressed 3");

            if (clickCoroutine != null)
                StopCoroutine(clickCoroutine);
            recordingButtonRectTransform.anchoredPosition = Vector2.zero;
        }
    }

    private IEnumerator MovingCoroutine(PointerEventData eventData)
    {
        var pointerId = eventData.pointerId;
        float previousTouchY = eventData.position.y;
        float positionY = 0;
        while (true)
        {
            yield return null;
            var anchoredPosition = recordingButtonRectTransform.anchoredPosition;
            float touchY =
#if UNITY_EDITOR
                Input.mousePosition.y;
#else
                Input.GetTouch(pointerId).position.y;
#endif
            positionY = Mathf.Clamp(anchoredPosition.y + (touchY - previousTouchY), 0, maxButtonMovingHeight);
            recordingButtonRectTransform.anchoredPosition = new Vector2(anchoredPosition.x, positionY);


            if (positionY / maxButtonMovingHeight >= lockActivation)
                break;

            previousTouchY = touchY;
        }

        recordingButtonRectTransform.anchoredPosition = Vector2.zero;
        LockVideo();
    }

    private IEnumerator ClickCoroutine(PointerEventData eventData)
    {
        yield return new WaitForSeconds(delayForVideoModeStarting-0.3f);
     
        
        MMVibrationManager.Vibrate();
        yield return new WaitForSeconds(0.3f);
        onVideoButtonPressed?.Invoke();
            
      
        Debug.Log("!!!onVideoButtonPressed 4");
        UnLockVideo();

        if (movingCoroutine != null)
        {
            StopCoroutine(movingCoroutine);
            recordingButtonRectTransform.anchoredPosition = Vector2.zero;
        }

        movingCoroutine = StartCoroutine(MovingCoroutine(eventData));

        yield return new WaitForSeconds(maxVideoTime);
        ActivateDefaultState();
        onVideoButtonReleased?.Invoke();
        Debug.Log("!!!onVideoButtonReleased 5");
    }

    private void LockVideo()
    {
        Debug.Log("!!!LockVideo");
        isRecording = true;
        isLocked = true;
        videoRecordingButtonUnlocked.gameObject.SetActive(false);
        lockArrow.gameObject.SetActive(false);
        lockImage.gameObject.SetActive(true);
        lockImage.color = Color.white;
        recordingButtonOutline.gameObject.SetActive(true);
        videoRecordingButtonLocked.gameObject.SetActive(true);
    }

    private void UnLockVideo()
    {
        Debug.Log("!!!UnLockVideo");
        isRecording = true;
        isLocked = false;
        videoRecordingButtonUnlocked.gameObject.SetActive(true);
        lockArrow.gameObject.SetActive(true);
        lockImage.gameObject.SetActive(true);
        lockImage.color = new Color(1, 1, 1, 0.5f);
        recordingButtonOutline.gameObject.SetActive(false);
        videoRecordingButtonLocked.gameObject.SetActive(false);
    }

    private void ActivateDefaultState()
    {
        Debug.Log("!!!ActivateDefaultState");
        isRecording = false;
        isLocked = false;
        if (clickCoroutine != null)
            StopCoroutine(clickCoroutine);
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);

        recordingButton.onPointerDown -= PointerDownHandler;
        recordingButton.onPointerUp -= PointerUpHandler;

        recordingButtonRectTransform.anchoredPosition = Vector2.zero;
        videoRecordingButtonUnlocked.gameObject.SetActive(false);
        videoRecordingButtonLocked.gameObject.SetActive(false);
        lockArrow.gameObject.SetActive(false);
        lockImage.gameObject.SetActive(false);
        lockImage.color = new Color(1, 1, 1, 0.5f);

        recordingButtonOutline.gameObject.SetActive(true);
//        CoroutineExecutor.Instance.Execute(ActivateRecordBtnAfterDelay(0.5f));
    }

//    private IEnumerator ActivateRecordBtnAfterDelay(float delay)
//    {
//        yield return new WaitForSeconds(delay);
//        
//        Debug.Log("!!!Activate recordingButtonOutline");
//        recordingButtonOutline.gameObject.SetActive(true);
//    }
}