﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.Video;
using UniUtils;

public class NativeBridgeController : Singleton<NativeBridgeController>
{
    public static string pathToSource;
   // public Texture2D debugTexture;
    public string debugVideoPath;
#if UNITY_IOS
    /*
   * Provide function decalaration of the functions defined in iOS
   * and need to be called here.
   */
 //   [System.Runtime.InteropServices.DllImport("__Internal")]
 //   extern static public void messageFromUnity(string message);
 //   [System.Runtime.InteropServices.DllImport("__Internal")]
   // extern static public void hideUnity();

    //Sends message to iOS
    static void SendImageToIOS(string imagePath)
    {
    //    messageFromUnity(imagePath);
    }
    
public     void messageFromIOS(string message)
     {
       
         string path = AppGroupPlugin.GetSharedFolderPathForDefaultAppGroup()+"/"+ Path.GetFileName(message);
       var jsonIOS = JsonUtility.FromJson<IOSJson>(path);
       pathToSource = jsonIOS.source;
           PostprocessController.Instance.startedFromExternalApp = true;
       if (string.IsNullOrEmpty(jsonIOS.json))
       {
           var contentType = ProcessedContentTypeTool.GetContentTypeByPath(pathToSource);
           switch (contentType)
           {
               case ProcessedContentTypeTool.ContentType.Image:
               {
                   SetTextureFromNative(pathToSource);
                   break;
               }
                    
               case ProcessedContentTypeTool.ContentType.Video :
               {
                   Debug.Log("ProcessedContentTypeTool.ContentType.Video - "+ pathToSource);
                   PostprocessController.Instance.StartPostprocess(pathToSource);
                   break;
               }
               case ProcessedContentTypeTool.ContentType.GIF:
               {
                   PostprocessController.Instance.StartPostprocessGif(pathToSource);
                   break;
               }
           }
        
       }
       else
       {
           StartWithResumingJSON(jsonIOS.json);
       }
    }
#endif
//    public static void SendTextureToNative(Texture2D texture2D)
//    {
//        string path = Application.temporaryCachePath+"/" +  Guid.NewGuid() + ".png";
//        Debug.Log(path);
//        File.WriteAllBytes(path, texture2D.EncodeToPNG());
//#if UNITY_IOS && !UNITY_EDITOR
//        SendImageToIOS(path);
//#elif UNITY_ANDROID && !UNITY_EDITOR
//        SendResourceToAndroid(path);
//        Application.Quit();
//#endif
//    }
    public static void SendJSONToNative(ResumingJSON resumingJson, string pathToResult)
    {
//        Debug.Log(path);

#if UNITY_IOS && !UNITY_EDITOR
        string json = JsonUtility.ToJson(resumingJson);
        
var jsonresult = new IOSJson(json,pathToResult);

        SendImageToIOS(JsonUtility.ToJson(jsonresult));
#elif UNITY_ANDROID && !UNITY_EDITOR
        SendResourceToAndroid(pathToResult,JsonUtility.ToJson(resumingJson));
      //  Application.Quit();
#endif
    }

    private static void SetTextureFromNative(string path)
    {
        var tex = TestExif.GetImage(path);
        PostprocessController.Instance.StartPostprocess(tex);
        PostprocessCroppimgManager.Instance.HoldScreenshotLocal(tex);
        

    }

    private void Start()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        PickingController.Instance.ShowPickingPanel();
        #endif
#if UNITY_EDITOR

    //    Debug.Log("my path - " + Application.dataPath);
      //  string path = Application.dataPath + "/Textures/facial-masks_1.jpg";
        //string path = "/Users/maximalist14/Desktop/PhotoCameraApp/Assets/Textures/facial-masks_1.jpg"
     //   SetTextureFromNative(path);
        
     //   PostprocessController.Instance.StartPostprocess(debugVideoPath);
//PostprocessController.Instance.StartPostprocessGif("/Users/maximalist14/Desktop/0a8f61e7-65aa-4ae8-984c-842ebc515db2.gif");
   //   PostprocessController.Instance.StartPostprocess(debugTexture);
      //  PostprocessCroppimgManager.Instance.HoldScreenshotLocal(debugTexture);
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
GetResourceFromAndroid();
       // PostprocessController.Instance.StartPostprocess(debugTexture);
 //      PostprocessCroppimgManager.Instance.HoldScreenshotLocal(debugTexture);
       

#endif
    }
    private void StartWithResumingJSON(string json)
    {
        var resumingJson = JsonUtility.FromJson<ResumingJSON>(json);
        pathToSource = resumingJson.mainImagePath;
                
        Debug.Log("pathToSource2 - " + pathToSource);
        var contentType = ProcessedContentTypeTool.GetContentTypeByPath(pathToSource);
        // Texture2D texRotated = null;
        switch (contentType)
        {
            case ProcessedContentTypeTool.ContentType.Image:
            {
   
                SetTextureFromNative(resumingJson.croppedImagePath);
                PostprocessCroppimgManager.Instance.HoldScreenshotLocal(TestExif.GetImage(pathToSource));
                        

                break;
            }
            case ProcessedContentTypeTool.ContentType.Video :
            {
                Debug.Log("ProcessedContentTypeTool.ContentType.Video - " + pathToSource);
                PostprocessController.Instance.StartPostprocess(pathToSource);
                break;
            }
            case ProcessedContentTypeTool.ContentType.GIF:
            {
                PostprocessController.Instance.StartPostprocessGif(pathToSource);
                break;
            }
        }


        PostprocessController.Instance.ResumePostprocessFromJSON(resumingJson);
    }
#if UNITY_ANDROID //&& !UNITY_EDITOR

    private void GetResourceFromAndroid()
    {
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");

            var uri = intent.Call<AndroidJavaObject>("getData");
            Debug.Log("uri " + uri);
        bool hasExtra = intent.Call<bool>("hasExtra", "returnMimeType");
        PostprocessController.Instance.startedFromPixchange = hasExtra;
        if (uri!=null)
        {
            string path =
                new AndroidJavaClass("com.pixchange.camera.FileUtils").CallStatic<string>("getPath", currentActivity,
                    uri);
            Debug.Log("Path - " +  path);
          //  string path = extras.Call<string>("getString", "file_url");
         //   string json = extras.Call<string>("getString", "json_uri");
//            Debug.Log(json);
           pathToSource = path;
            Debug.Log("pathToSource - " + pathToSource);
          //  if (!string.IsNullOrEmpty(json))
          //  {
           //   StartWithResumingJSON(json);
          //  }
           // else
          //  {
          PostprocessController.Instance.startedFromExternalApp = true;
                var contentType = ProcessedContentTypeTool.GetContentTypeByPath(pathToSource);
                switch (contentType)
                {
                    case ProcessedContentTypeTool.ContentType.Image:
                    {
                        SetTextureFromNative(path);
                        break;
                    }
                    
                    case ProcessedContentTypeTool.ContentType.Video :
                    {
                        Debug.Log("ProcessedContentTypeTool.ContentType.Video - "+ path);
                        PostprocessController.Instance.StartPostprocess(path);
                        break;
                    }
                    case ProcessedContentTypeTool.ContentType.GIF:
                    {
                        PostprocessController.Instance.StartPostprocessGif(path);
                        break;
                    }
               // }
            }
               
        }
        else
        {
            PickingController.Instance.ShowPickingPanel();
        }
    }

   


    private static void SendResourceToAndroid(string path, string json)
    {
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        intentObject.Call<AndroidJavaObject>("putExtra", "file_url", path);
        intentObject.Call<AndroidJavaObject>("putExtra", "json_uri", json);
        currentActivity.Call("setResult", -1, intentObject);
       // Application.Quit();
      //  currentActivity.Call("finish");
    }

#endif
}

[Serializable]
class IOSJson
{
    public string json;
    public string result;
    public string source;

    public IOSJson(string json, string result)
    {
        this.json = json;
        this.result = result;
    }
}