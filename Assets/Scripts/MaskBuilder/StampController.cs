﻿using PaintIn3D;
using UnityEngine;

namespace MaskBuilder
{
    [RequireComponent(typeof(P3dPaintFill))]
    public class StampController : MonoBehaviour
    {
        #region > Properties

        private P3dPaintFill paintFill = null;
        private P3dPaintFill PaintFill
        {
            get
            {
                if (paintFill == null)
                    paintFill = GetComponent<P3dPaintFill>();

                return paintFill;
            }
        }

        #endregion > Properties

        public void SetStamp(Texture2D texture, Color color, Collider collider)
        {
            PaintFill.Color = color;
            PaintFill.Opacity = color.a;
            PaintFill.Texture = texture;
         
            P3dStateManager.StoreAllStates();
            PaintFill.HandleHitPoint(null, null, false, collider, Vector3.zero, Quaternion.identity, 1f);
        }
    }
}