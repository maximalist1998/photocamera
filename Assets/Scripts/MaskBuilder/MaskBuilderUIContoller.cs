﻿using UnityEngine;
using UnityEngine.UI;

namespace MaskBuilder
{
    public class MaskBuilderUIContoller : MonoBehaviour
    {
        #region > Editor Fields

        [SerializeField]
        private GameObject maskBuilderPanel;
        [SerializeField]
        private Button saveMaskBtn;
        [SerializeField]
        private Button closeBtn;

        #endregion > Editor Fields
    
        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            MaskBuilderController.OnOpenMaskBuilder += OpenMaskBuilderHandler;
            MaskBuilderController.OnCloseMaskBuilder += CloseMaskBuilderHandler;

            ViewModeSelector.OnViewModeSelected += SetViewMode;
        }
    
        private void OnDisable()
        {
            MaskBuilderController.OnOpenMaskBuilder -= OpenMaskBuilderHandler;
            MaskBuilderController.OnCloseMaskBuilder -= CloseMaskBuilderHandler;
            
            ViewModeSelector.OnViewModeSelected -= SetViewMode;
        }

        private void Start()
        {
            saveMaskBtn.onClick.AddListener(SaveMaskHandler);
            closeBtn.onClick.AddListener(CloseBuilderHandler);
        }
    
        #endregion > MonoBehaviour Callbacks

        private void OpenMaskBuilderHandler(Texture2D texture)
        {
            maskBuilderPanel.SetActive(true);
        }
        
        private void CloseMaskBuilderHandler(string maskId)
        {
            maskBuilderPanel.SetActive(false);
        }

        private void SetViewMode(ViewMode viewMode)
        {
            MaskBuilderController.Instance.SetCameraViewMode(viewMode);
        }

        private void SaveMaskHandler()
        {
            MaskBuilderController.Instance.SaveMaskAndClose();
        }

        private void CloseBuilderHandler()
        {
            MaskBuilderController.Instance.CloseWithoutSaving();
        }
    }
}