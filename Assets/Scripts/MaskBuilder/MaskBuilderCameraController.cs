﻿using System.Collections;
using Executor;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace MaskBuilder
{
    public class MaskBuilderCameraController : MonoBehaviour
    {
        #region > Editor Fields

        [SerializeField]
        private RawImage cameraImage;
        [SerializeField]
        private Camera camera;
        [SerializeField]
        private float pinchSpeed = 1f;
        [SerializeField]
        private float moveSpeed = 1f;

        [Header("Camera Positions")]
        [SerializeField]
        private Vector3 minPosition = new Vector3(-0.3f, -0.3f, -1.5f);
        [SerializeField]
        private Vector3 maxPosition = new Vector3(0.3f, 0.3f, 0.35f);
        
        [SerializeField]
        private CameraTransformData frontPosition = new CameraTransformData(new Vector3(0f, 0f, -1f), Vector3.zero);
        [SerializeField]
        private CameraTransformData rightPosition = new CameraTransformData(new Vector3(1f, 0f, 0f), new Vector3(0f, -90f, 0f));
        [SerializeField]
        private CameraTransformData leftPosition = new CameraTransformData(new Vector3(-1f, 0f, 0f), new Vector3(0f, 90f, 0f));
        [SerializeField]
        private float positionChangeTime = 0.5f;

        #endregion > Editor Fields
        
        #region > Private Fields

        private Coroutine cameraRepositionCoroutine = null;

        #endregion > Private Fields
        
        #region > Properties

        private RenderTexture cameraRT = null;
        private RenderTexture CameraRT
        {
            get
            {
                if(cameraRT == null)
                    cameraRT = new RenderTexture(Screen.width, Screen.height, 24);

                return cameraRT;
            }
        }

        #endregion > Properties
        
        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            MaskBuilderController.OnOpenMaskBuilder += OpenMaskBuilderHandler;
            MaskBuilderController.OnCloseMaskBuilder += MaskBuilderCloseHandler;
        }
        
        private void OnDisable()
        {
            MaskBuilderController.OnOpenMaskBuilder -= OpenMaskBuilderHandler;
            MaskBuilderController.OnCloseMaskBuilder -= MaskBuilderCloseHandler;
        }
        
        #endregion > MonoBehaviour Callbacks

        public void PauseTouchesHandlers()
        {
            GlobalEvents.Instance.Pinching.RemoveListener(Pinching);
            GlobalEvents.Instance.TwoFingersMove.RemoveListener(Move);
        }
        
        public void RestartTouchesHandlers()
        {
            GlobalEvents.Instance.Pinching.AddListener(Pinching);
            GlobalEvents.Instance.TwoFingersMove.AddListener(Move);
        }

        private void OpenMaskBuilderHandler(Texture2D texture)
        {
            //Activate and setup camera
            CamerasController.Instance.StartMaskBuilderCameras();
            camera.targetTexture = CameraRT;

            //Setup UI view
            cameraImage.texture = CameraRT;

            GlobalEvents.Instance.Pinching.AddListener(Pinching);
            GlobalEvents.Instance.TwoFingersMove.AddListener(Move);
        }

        private void MaskBuilderCloseHandler(string maskId)
        {
            CamerasController.Instance.StartFaceMasksCameras();
            
            GlobalEvents.Instance.Pinching.RemoveListener(Pinching);
            GlobalEvents.Instance.TwoFingersMove.RemoveListener(Move);
        }
        
        #region > Repositioning

        public void SetFrontPosition(bool inTime = false)
        {
            MoveCameraToTransform(frontPosition, inTime);
        }
        
        public void SetRightPosition(bool inTime = false)
        {
            MoveCameraToTransform(rightPosition, inTime);
        }
        
        public void SetLeftPosition(bool inTime = false)
        {
            MoveCameraToTransform(leftPosition, inTime);
        }

        private void MoveCameraToTransform(CameraTransformData moveTo, bool inTime)
        {
            if(cameraRepositionCoroutine != null)
                CoroutineExecutor.Instance.Stop(cameraRepositionCoroutine);

            if (inTime)
            {
                camera.transform.position = moveTo.Position;
                camera.transform.rotation = Quaternion.Euler(moveTo.Rotation);
            }
            else
            {
                cameraRepositionCoroutine = CoroutineExecutor.Instance.Execute(PositionResetCoroutine(moveTo, positionChangeTime));
            }
        }
        
        private IEnumerator PositionResetCoroutine(CameraTransformData moveTo, float time)
        {
            var startPos = camera.transform.position;
            var startRot = camera.transform.rotation;
            var len = Vector3.Distance(startPos, moveTo.Position);
            var moveTime = time * len;
            var startTime = Time.time;
            var percent = (Time.time - startTime) / moveTime;

            while (percent < 1)
            {
                camera.transform.position = Vector3.Lerp(startPos, moveTo.Position, percent);
                camera.transform.rotation = Quaternion.Lerp(startRot, Quaternion.Euler(moveTo.Rotation), percent);

                yield return null;
                
                percent = (Time.time - startTime) / moveTime;
            }
            
            camera.transform.position = moveTo.Position;
            camera.transform.rotation = Quaternion.Euler(moveTo.Rotation);
        }

        #endregion > Repositioning

        public Texture2D CreatePreviewTexture()
        {
            var prevCamTransform = new CameraTransformData(camera.transform);

//            camera.backgroundColor = new Color(camera.backgroundColor.r, camera.backgroundColor.g, camera.backgroundColor.b, 1f/256f);
            camera.Render();
            var previewTex = TextureUtils.ToMiddleSquareTexture2D(camera.targetTexture, 0.53f);
            Debug.Log("Preview Texture - " + previewTex.width + "x" + previewTex.height);
            
            MoveCameraToTransform(prevCamTransform, true);
//            camera.backgroundColor = new Color(camera.backgroundColor.r, camera.backgroundColor.g, camera.backgroundColor.b, 1f);
            
            return previewTex;
        }
        
        #region > Gestures

        private void Pinching(float val)
        {
            Debug.Log("");
            
            var newPos = camera.transform.position + camera.transform.forward * val * pinchSpeed;
            if (newPos.x < minPosition.x || newPos.x > maxPosition.x ||
                newPos.y < minPosition.y || newPos.y > maxPosition.y ||
                newPos.z < minPosition.z || newPos.z > maxPosition.z)
                return;
            
            camera.transform.position = newPos;
        }
        
        private void Move(Vector2 val)
        {
            var newPos = camera.transform.position + camera.transform.right * -val.x * moveSpeed +
                         camera.transform.up * -val.y * moveSpeed;
            if (newPos.x < minPosition.x || newPos.x > maxPosition.x ||
                newPos.y < minPosition.y || newPos.y > maxPosition.y ||
                newPos.z < minPosition.z || newPos.z > maxPosition.z)
                return;

            camera.transform.position = newPos;
        }
        
        #endregion > Gestures
    }
}