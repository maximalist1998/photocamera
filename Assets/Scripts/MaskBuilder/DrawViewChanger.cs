﻿using UnityEngine;

namespace MaskBuilder
{
    public class DrawViewChanger : MonoBehaviour
    {
        #region > Editor Fields
        
        [Header("Drawing View")] 
        [SerializeField]
        private Mesh faceMesh;
        [SerializeField]
        private Mesh quadMesh;
        [SerializeField]
        private MeshFilter drawingFilter;
        [SerializeField]
        private MeshCollider drawingMesh;
        [SerializeField]
        private Texture mode2DTexture;
        [SerializeField]
        private Texture mode3DTexture;
        
        #endregion > Editor Fields
        
        #region > Consts
        
        private readonly Vector3 facePosition = new Vector3(0f, 0f, 0f);
        private readonly Vector3 faceRotation = new Vector3(0f, 180f, 0f);
        private readonly Vector3 faceScale = new Vector3(1f, 1f, 1f);
        private readonly Vector3 quadPosition = new Vector3(0f, 0f, 0f);
        private readonly Vector3 quadRotation = new Vector3(0f, 0f, 0f);
        private readonly Vector3 quadScale = new Vector3(-0.2f, 0.2f, 0.2f);
        
        #endregion > Consts
        
        #region > Properties

        private Renderer renderer = null;
        private Renderer Renderer
        {
            get
            {
                if (renderer == null)
                    renderer = drawingMesh.GetComponent<Renderer>();

                return renderer;
            }
        }
        
        #endregion > Properties

        private void OnEnable()
        {
            SetFaceView();
        }
        
        public void SetFaceView()
        {
            drawingFilter.mesh = faceMesh;
            drawingMesh.sharedMesh = faceMesh;
            
            drawingFilter.transform.position = facePosition;
            drawingFilter.transform.rotation = Quaternion.Euler(faceRotation);
            drawingFilter.transform.localScale = faceScale;
            Renderer.sharedMaterial.SetTexture("_TopTex", mode3DTexture);
        }
        
        public void SetTextureView()
        {
            drawingFilter.mesh = quadMesh;
            drawingMesh.sharedMesh = quadMesh;
            
            drawingFilter.transform.position = quadPosition;
            drawingFilter.transform.rotation = Quaternion.Euler(quadRotation);
            drawingFilter.transform.localScale = quadScale;
            Renderer.sharedMaterial.SetTexture("_TopTex", mode2DTexture);
        }
    }
}