using System;
using MasksBuilder;
using PaintIn3D;
using UnityEngine;
using UnityEngine.UI;

namespace MaskBuilder
{
    public class DrawingController : MonoBehaviour
    {
        #region > Editor Fields
        
        [Header("Drawing Setup")]
        [SerializeField]
        private GameObject drawingMaskPrefab;
        [SerializeField]
        private Transform drawingMaskParent;

        [Header("Drawing Tools")]
        [SerializeField]
        private P3dPaintSphere paintLine;
        [SerializeField]
        private P3dPaintFill paintFill;
        [SerializeField]
        private P3dPaintSphere paintClear;
        [SerializeField]
        private P3dPaintSphere paintTexture;
        [SerializeField]
        private P3dPaintSphereBlur paintSoften;
        [SerializeField]
        private P3dPaintDecal paintDecal;
        [SerializeField]
        private StampController paintStamp;

        [Header("UI")] 
        [SerializeField]
        private GameObject decalPlacingPanel;
        [SerializeField]
        private GameObject otherButtonsPanel;
        [SerializeField]
        private Button placeDecalBtn;
        [SerializeField]
        private Button cancelDecalBtn;

        [Header("Mask Builder Properties")] 
        [SerializeField]
        private MaskBuilderCameraController maskBuilderCameraController;

        #endregion > Editor Fields
        
        #region > Private Fields

        private DrawingTool currTool;
        
        private GameObject currDrawingMask;
        
        #endregion > Private Fields
        
        #region > Properties

        private DecalHitScreen decalHitScreen;
        private DecalHitScreen DecalHitScreen
        {
            get
            {
                if (decalHitScreen == null && paintDecal != null)
                    decalHitScreen = paintDecal.gameObject.GetComponent<DecalHitScreen>();

                return decalHitScreen;
            }
        }
        
        private P3dPaintable currPaintable;
        public P3dPaintable CurrPaintable
        {
            get
            {
                if (currPaintable == null)
                    currPaintable = currDrawingMask.GetComponentInChildren<P3dPaintable>();

                return currPaintable;
            }
        }
        
        private DrawViewChanger currDrawViewChanger = null;
        public DrawViewChanger CurrDrawViewChanger
        {
            get
            {
                if (currDrawViewChanger == null)
                    currDrawViewChanger = currDrawingMask.GetComponentInChildren<DrawViewChanger>();

                return currDrawViewChanger;
            }
        }
        
        private Collider currDrawCollider = null;
        public Collider CurrDrawCollider
        {
            get
            {
                if (currDrawCollider == null)
                    currDrawCollider = currDrawingMask.GetComponentInChildren<Collider>();

                return currDrawCollider;
            }
        }
        
        #endregion > Properties
        
        #region > MonoBehaviour Callbacks

        private void OnEnable()
        {
            MaskBuilderController.OnOpenMaskBuilder += OpenBuilderHandler;
            DrawingPanel.OnDrawingToolChanged += OnDrawingToolChangedHandler;
            MasksBuilder.StickersPanelController.OnTextureSelected += OnStickerSelectedHandler;
            StampPanelController.OnStampSelected += StampSelectedHandler;
        }
        
        private void OnDisable()
        {
            MaskBuilderController.OnOpenMaskBuilder -= OpenBuilderHandler;
            DrawingPanel.OnDrawingToolChanged -= OnDrawingToolChangedHandler;
            MasksBuilder.StickersPanelController.OnTextureSelected -= OnStickerSelectedHandler;
            StampPanelController.OnStampSelected -= StampSelectedHandler;
        }
        
        private void Start()
        {
            placeDecalBtn.onClick.AddListener(PlaceDecal);
            cancelDecalBtn.onClick.AddListener(CancelDecalPlacing);
            
            currTool = new DrawingTool(ToolType.Draw, Color.white, 0.1f, null);
        }
        
        #endregion > MonoBehaviour Callbacks

        private void OpenBuilderHandler(Texture2D texture)
        {
            InstantiateDrawingMask(MaskBuilderController.Instance.CurrentBuildMask);
        }
        
        private void InstantiateDrawingMask(Texture2D startWithTexture)
        {
            DestroyDrawingMask();

            currDrawingMask = Instantiate(drawingMaskPrefab, drawingMaskParent);

            var paintableTexture = CurrPaintable.GetComponent<P3dPaintableTexture>();
            paintableTexture.Texture = startWithTexture;
            paintableTexture.Activate();
        }
        
        private void DestroyDrawingMask()
        {
            if(currDrawingMask != null)
                DestroyImmediate(currDrawingMask);
        }

        #region > Drawing Tool

        private void OnDrawingToolChangedHandler(DrawingTool tool)
        {
            currTool = tool;
            
            if (tool.texture2D != null && tool.toolType != ToolType.Fill)
            {
                SetupForTexture(tool);
                return;
            }
            
            switch (tool.toolType)
            {
                case ToolType.Draw:
                    SetupForDraw(tool);
                    break;
                case ToolType.Erase:
                    SetupForErase(tool);
                    break;
                case ToolType.Fill:
                    SetupForFill(tool);
                    break;
                case ToolType.Soften:
                    SetupForSoften(tool);
                    break;
                case ToolType.Blend:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetupForDraw(DrawingTool tool)
        {
            DeactivateAllPainters();
            paintLine.gameObject.SetActive(true);

            paintLine.Color = tool.color;
            paintLine.Opacity = tool.color.a;
            paintLine.Radius = tool.lineThickness;
        }
        
        private void SetupForErase(DrawingTool tool)
        {
            DeactivateAllPainters();
            paintClear.gameObject.SetActive(true);
            
            paintClear.Radius = tool.lineThickness;
            paintClear.Opacity = tool.color.a;
        }
        
        private void SetupForFill(DrawingTool tool)
        {
            DeactivateAllPainters();
            paintFill.gameObject.SetActive(true);

            paintFill.Color = tool.color;
            paintFill.Opacity = tool.color.a;
            paintFill.Texture = tool.texture2D;
        }
        
        private void SetupForTexture(DrawingTool tool)
        {
            DeactivateAllPainters();
            paintTexture.gameObject.SetActive(true);

            paintTexture.Color = tool.color;
            paintTexture.Opacity = tool.color.a;
            paintTexture.Radius = tool.lineThickness;
            paintTexture.SetBlendModeTexture(tool.texture2D);
            paintTexture.SetBlendModeColor(tool.color);
        }
        
        private void SetupForSoften(DrawingTool tool)
        {
            DeactivateAllPainters();
            paintSoften.gameObject.SetActive(true);

            paintSoften.Opacity = tool.color.a;
            paintSoften.Radius = tool.lineThickness;
        }
        
        #endregion > Drawing Tool
        
        #region > Stickers

        private const float MinDecalScale = 0.2f;
        private const float MaxDecalScale = 3f;
        private const float DecalPinchSpeed = 1f;
        
        private void OnStickerSelectedHandler(Texture2D sticker)
        {
            DeactivateAllPainters();
            paintDecal.gameObject.SetActive(true);

            paintDecal.Texture = sticker;
            paintDecal.Scale = Vector2.one;
            paintDecal.Angle = 0f;
            paintDecal.Radius = 0.04f;
            paintDecal.Scale = Vector2.one;
            
            maskBuilderCameraController.PauseTouchesHandlers();
            maskBuilderCameraController.SetFrontPosition(true);
            GlobalEvents.Instance.Rotating.AddListener(ChangeDecalRotation);
            GlobalEvents.Instance.Pinching.AddListener(ChangeDecalScale);
            
            decalPlacingPanel.SetActive(true);
            otherButtonsPanel.SetActive(false);
        }

        private void ChangeDecalRotation(float val)
        {
            paintDecal.Angle -= val;
        }
        
        private void ChangeDecalScale(float val)
        {
            Debug.Log($"ChangeDecalScale {val}");
            paintDecal.Scale = new Vector2(paintDecal.Scale.x + val * DecalPinchSpeed, paintDecal.Scale.y + val * DecalPinchSpeed);
            Debug.Log($"paintDecal.Scale {paintDecal.Scale}");
            
            if(paintDecal.Scale.x < MinDecalScale)
                paintDecal.Scale = new Vector2(MinDecalScale, MinDecalScale);
            if(paintDecal.Scale.x > MaxDecalScale)
                paintDecal.Scale = new Vector2(MaxDecalScale, MaxDecalScale);
        }

        private void PlaceDecal()
        {
            DecalHitScreen.PainCurrent();
            CancelDecalPlacing();
        }
        
        private void CancelDecalPlacing()
        {
            decalPlacingPanel.SetActive(false);
            otherButtonsPanel.SetActive(true);
            
            GlobalEvents.Instance.Rotating.RemoveListener(ChangeDecalRotation);
            GlobalEvents.Instance.Pinching.RemoveListener(ChangeDecalScale);
            maskBuilderCameraController.RestartTouchesHandlers();
            
            OnDrawingToolChangedHandler(currTool);
        }
        
        #endregion > Stickers
        
        #region > Stamps

        private void StampSelectedHandler(Texture2D texture, Color color, StampType stampType)
        {
            DeactivateAllPainters();
            paintStamp.gameObject.SetActive(true);
            
            paintStamp.SetStamp(texture, color, CurrDrawCollider);

            OnDrawingToolChangedHandler(currTool);
        }
        
        #endregion > Stamps

        private void DeactivateAllPainters()
        {
            paintLine.gameObject.SetActive(false);
            paintFill.gameObject.SetActive(false);
            paintClear.gameObject.SetActive(false);
            paintTexture.gameObject.SetActive(false);
            paintSoften.gameObject.SetActive(false);
            paintDecal.gameObject.SetActive(false);
            paintStamp.gameObject.SetActive(false);
        }
    }
}