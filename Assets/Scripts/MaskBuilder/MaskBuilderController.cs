﻿using System;
using FaceTracker.DB;
using PaintIn3D;
using UnityEngine;
using UniUtils;
using Utils;

namespace MaskBuilder
{
    public class MaskBuilderController : Singleton<MaskBuilderController>
    {
        #region > Events

        public static event Action<Texture2D> OnOpenMaskBuilder;
        public static event Action<string> OnCloseMaskBuilder;

        #endregion > Events
        
        #region > Editor Fields

        [SerializeField]
        private DrawingController drawingController;
        
        #endregion > Editor Fields
        
        #region > Properties

        private Texture2D maskTemplateBackground = null;
        public Texture2D MaskTemplateBackground
        {
            get
            {
                if (maskTemplateBackground == null)
                    maskTemplateBackground = Resources.Load<Texture2D>("MaskBuilder/MaskTemplateBackground");

                return maskTemplateBackground;
            }
        }
        
        private Texture2D maskTemplateEmpty = null;
        public Texture2D MaskTemplateEmpty
        {
            get
            {
                if (maskTemplateEmpty == null)
                    maskTemplateEmpty = Resources.Load<Texture2D>("MaskBuilder/MaskTemplateEmpty");

                return maskTemplateEmpty;
            }
        }

        private Texture2D currentBuildMask = null;
        public Texture2D CurrentBuildMask
        {
            get { return currentBuildMask; }
        }

        private MaskBuilderCameraController maskBuilderCameraController = null;
        public MaskBuilderCameraController MaskBuilderCameraController
        {
            get
            {
                if (maskBuilderCameraController == null)
                    maskBuilderCameraController = FindObjectOfType<MaskBuilderCameraController>();

                return maskBuilderCameraController;
            }
        }

        private Texture2D maskAlphaMask = null;
        private Texture2D MaskAlphaMask
        {
            get
            {
                if (maskAlphaMask == null)
                    maskAlphaMask = Resources.Load<Texture2D>("MaskAlphaMask");

                return maskAlphaMask;
            }
        }
        
        #endregion > Properties

        public void OpenMaskBuilder()
        {
            //Destroy old mask if exists
            if(currentBuildMask != null)
                DestroyImmediate(currentBuildMask);
            
            //Create new mask texture
            currentBuildMask = CopyTexture(MaskTemplateEmpty);
            
            OnOpenMaskBuilder?.Invoke(currentBuildMask);
        }

        public void CloseWithoutSaving()
        {
            OnCloseMaskBuilder?.Invoke(null);
        }
        
        public void SaveMaskAndClose()
        {
            drawingController.CurrDrawViewChanger.SetFaceView();
            MaskBuilderCameraController.SetFrontPosition(true);
            
            var rt = drawingController.CurrPaintable.GetComponent<MeshRenderer>().material.GetTexture("_SecondaryTex") as RenderTexture;
            var tempTexture = TextureUtils.ToTexture2D(rt);
//            var finaleMask = TextureUtils.ApplyTextureAlpha(tempTexture, 0.4f);
            var finaleMask = TextureUtils.ApplyTextureAlpha(tempTexture, MaskAlphaMask);
            DestroyImmediate(tempTexture);

            if (finaleMask == null)
            {
                Debug.LogError("Error while creating mask texture!");
                return;
            }

            var preview = MaskBuilderCameraController.CreatePreviewTexture();

            var maskId = MaskData.SaveLocalMask(finaleMask, preview);
            FaceMasksData.Instance.UpdateLocalMasks();
            
            DestroyImmediate(finaleMask);
            DestroyImmediate(preview);
            
            OnCloseMaskBuilder?.Invoke(maskId);
        }

        public void SetCameraViewMode(ViewMode viewMode)
        {
            switch (viewMode)
            {
                case ViewMode.Left:
                    drawingController.CurrDrawViewChanger.SetFaceView();
                    MaskBuilderCameraController.SetLeftPosition();
                    break;
                case ViewMode.Front:
                    drawingController.CurrDrawViewChanger.SetFaceView();
                    MaskBuilderCameraController.SetFrontPosition();
                    break;
                case ViewMode.Right:
                    drawingController.CurrDrawViewChanger.SetFaceView();
                    MaskBuilderCameraController.SetRightPosition();
                    break;
                case ViewMode.TwoD:
                    drawingController.CurrDrawViewChanger.SetTextureView();
                    MaskBuilderCameraController.SetFrontPosition(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(viewMode), viewMode, null);
            }
        }

        private static Texture2D CopyTexture(Texture2D texture)
        {
            var newTex = new Texture2D(texture.width, texture.height, TextureFormat.ARGB32, false);
            newTex.SetPixels(texture.GetPixels());
            newTex.Apply();

            return newTex;
        }
    }
}