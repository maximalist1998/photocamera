﻿using System;
using UnityEngine;

namespace MaskBuilder
{
    [Serializable]
    public struct CameraTransformData
    {
        public Vector3 Position;
        public Vector3 Rotation;

        public CameraTransformData(Vector3 pos, Vector3 rot)
        {
            Position = pos;
            Rotation = rot;
        }
        
        public CameraTransformData(Transform cam)
        {
            Position = cam.position;
            Rotation = cam.rotation.eulerAngles;
        }
    }
}