﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MasksBuilder
{
    

public class UploadTexturePanel : MonoBehaviour
{
    [SerializeField] private RectTransform panels;
    [SerializeField] private float animationTime = 0.3f;
    private Coroutine panelsMoving;
    public void ShowPanel()
    {
            
        panels.gameObject.SetActive(true);
        gameObject.SetActive(true);
       
        if(panelsMoving!= null) StopCoroutine(panelsMoving);
        panelsMoving = StartCoroutine(ActivatePanels());
    }

    private IEnumerator ActivatePanels()
    {
        panels.gameObject.SetActive(true);
        var startPosition = new Vector2(panels.anchorMin.y, panels.anchorMax.y);
        var finishPosition = new Vector2(0.08f, 0.39f);
            
        float currentTime = 0;
        while (currentTime<=animationTime)
        {
            yield return null;
            currentTime += Time.deltaTime;
            float percentage = Mathf.Clamp01(currentTime / animationTime);
            var currentPosition = Vector2.Lerp(startPosition, finishPosition, percentage);
            panels.anchorMin = new Vector2(panels.anchorMin.x,currentPosition.x);
            panels.anchorMax = new Vector2(panels.anchorMax.x,currentPosition.y);
        }
    }
}
}
