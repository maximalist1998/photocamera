﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MasksBuilder.Backend
{
    

public class SearchRequestController : MonoBehaviour
{


    private Coroutine searchCoroutine;
    private int currentPage;
    private string currentSearchRequest;
    public void SearchStaticStickers(string searchRequest,Action<List<string>> onLoaded)
    {
        currentSearchRequest = searchRequest;
        
        if (string.IsNullOrEmpty(searchRequest))
        {
            onLoaded.Invoke(null);
            return;
        }
        
   SearchStickersAtPage(searchRequest,1, (json) =>
   {
       Debug.Log("loaded json - " + json);
       var stickersJson =  JsonUtility.FromJson<StickersJSON>(json);

       if (stickersJson != null)
       {
              
           var staticStickers = GetStaticLinksFromStickersJSON(stickersJson);
           onLoaded?.Invoke(staticStickers);
       }
   });
    }

    public void LoadNextPage(Action<List<string>> onLoaded)
    {
        currentPage++;
        Debug.Log("load next page - "+ currentPage);
        SearchStickersAtPage(currentSearchRequest,currentPage, (json) =>
        {
            var stickersJson =  JsonUtility.FromJson<StickersJSON>(json);
          currentPage = Mathf.Clamp(stickersJson.current_page,1,stickersJson.total_pages);
          onLoaded?.Invoke(  GetStaticLinksFromStickersJSON(stickersJson));
        });
    }

    private void SearchStickersAtPage(string searchRequest,int page,Action<string> onLoaded)
    {
        currentSearchRequest = searchRequest;
        currentPage = page;
//        string url = String.Format("https://pixchange-staging.mlsdev.com/api/images?page={0}&per_page=2&q={1}&type=sticker&provider=pixchange",page,searchRequest);
        string url = String.Format("https://pixchangeapp.com/api/images?page={0}&per_page=2&q={1}&type=sticker&provider=pixchange", page, searchRequest);
        if(searchCoroutine!= null) StopCoroutine(searchCoroutine);
        searchCoroutine= StartCoroutine(LoadStickersJson(url, onLoaded));

    }

    private IEnumerator LoadStickersJson(string url,Action<string> OnLoaded, Action<string> onError = null)
    {
        var www =  new WWW(url);
        yield return www;
        if(string.IsNullOrEmpty(www.error))
            OnLoaded?.Invoke(www.text);
        else 
        onError?.Invoke(www.error);
    }

    private List<string> GetStaticLinksFromStickersJSON(StickersJSON stickersJson)
    {
        if(stickersJson == null) return null;
        if(stickersJson.collection == null) return null;
        List<string> staticStickers = new List<string>();
//        Debug.Log($"stickersJson.total_pages - {stickersJson.total_pages}, stickersJson.current_page - {stickersJson.current_page}");
        foreach (var collection in stickersJson.collection)
        {
            if (collection?.image?.url == null) continue;
           
            if (collection.image.versions.fixed_width_small.url.Contains(".png"))
            {
                staticStickers.Add(collection.image.versions.fixed_width_small.url);   
                
            }

        }

        return staticStickers;
    }

    public void LoadTextures(List<string> urls, Action<List<Texture2D>> onLoaded)
    {
        StartCoroutine(LoadTexturesFromList(urls, onLoaded));
    }
  
    
    private IEnumerator LoadTexturesFromList(List<string> texturesURLs, Action<List<Texture2D>> onLoaded)
    {
        List<Texture2D> texture2Ds = new List<Texture2D>();
        foreach (var url in texturesURLs)
        {
            var www = new WWW(url);
            yield return www;
            Debug.Log("load");
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("error - "+ www.error);
                continue;
            }
            var texture = www.texture;
            if(texture!=null) texture2Ds.Add(texture);
            Debug.Log("addedTexture");
        }
        
        onLoaded?.Invoke(texture2Ds);
    }
}
}
