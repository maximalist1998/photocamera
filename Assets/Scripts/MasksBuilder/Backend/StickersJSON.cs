﻿using System;
using System.Collections.Generic;

namespace MasksBuilder.Backend
{
    [Serializable]
    public class FixedLarge
    {
        public string url;
        public object still;
        public string webp;
        public object mp4;
        public int width;
        public int height;
    }

    [Serializable]
    public class FixedWidthSmall
    {
        public string url;
        public object still;
        public string webp;
        public int width;
        public int height;
    }

    [Serializable]
    public class FixedHeightSmall
    {
        public string url;
        public object still;
        public string webp;
        public int width;
        public int height;
    }

    [Serializable]
    public class Versions
    {
        public FixedLarge fixed_large;
        public FixedWidthSmall fixed_width_small;
        public FixedHeightSmall fixed_height_small;
    }

    [Serializable]
    public class Image
    {
        public string url;
        public string webp;
        public int width;
        public int height;
        public string display_mode;
        public bool animated;
        public string status;
        public Versions versions;
    }

    [Serializable]
    public class Collection
    {
        public string id;
        public string title;
        public List<string> tags;
        public int download_counter;
        public string type;
        public Image image;
        public string web_page_url;
        public string dynamic_link;
        public bool can_edit;
        public bool is_top;
        public string status;
        public bool is_favorite;
        public bool is_for_sale;
        public bool image_bought;
        public object subcategory_id;
        public object audio;
    }

    [Serializable]
    public class StickersJSON
    {
        public List<Collection> collection;
        public int total_pages;
        public int current_page;
    }
}