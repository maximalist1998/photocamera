﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MasksBuilder
{
    public class TextureButtonsPanel : MonoBehaviour
    {
        public event Action<Texture2D> OnTexturePicked;
        public GameObject texturePrefab;
        public Transform textureParent;
        public List<Texture2D> textures;
        
        private List<TextureButton> textureButtons = new List<TextureButton>();

        private bool isTexturesInitialized = false;
        
        #region > Properties
        
        public Texture2D ActiveTexture
        {
            get { return ActiveButton.texture2D; }
        }

        public TextureButton ActiveButton
        {
            set
            {
                if (value == null) return;
                if (activeButton == value)
                {
                    
                    return;
                }

                activeButton = value;
                foreach (var textureButton in textureButtons)
                {
                    if (textureButton == value) textureButton.Select();
                    else
                    {
                        textureButton.Deselect();
                    }
                }
            }
            get
            {
                if (activeButton == null)
                    ActiveButton = textureButtons[0]; //default button
                return activeButton;
            }
        }
        
        private TextureButton activeButton;
        
        #endregion > Properties

        #region > MonoBehaviour Callbacks
        
        private void Awake()
        {
            InitializeTextures();
        }

        private void OnEnable()
        {  
            foreach (var textureButton in textureButtons)
            {
                textureButton.OnButtonPressed += SelectTextureButton;
            }
        }   
        private void OnDisable()
        {
            foreach (var textureButton in textureButtons)
            {
                textureButton.OnButtonPressed -= SelectTextureButton;
            }
        }
        
        #endregion > MonoBehaviour Callbacks

        public void DeselectTexture()
        {
            SelectTextureButton(textureButtons[0]);
        }

        private void InitializeTextures()
        {
            if (isTexturesInitialized)
                return;
            
            AddTexture(null);
            
            foreach (var texture in textures)
            {
                AddTexture(texture);
            }

            isTexturesInitialized = true;
        }

        private void AddTexture(Texture2D tex)
        {
            var obj = Instantiate(texturePrefab, textureParent);
            var texButton = obj.GetComponent<TextureButton>();
            if (texButton != null)
            {
                texButton.Initialize(tex);
                textureButtons.Add(texButton);
            }
        }

        private void SelectTextureButton(TextureButton textureButton)
        {
            ActiveButton = textureButton;
            OnTexturePicked?.Invoke(ActiveTexture);
        }        
    }
}