﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MasksBuilder
{
    public class TabBar : MonoBehaviour
    {
        public event Action<BuilderMode> onTabBarButtonPressed;

        [SerializeField] private List<ModeButton> modeButtons;

        private void Start()
        {
            foreach (var modeButton in modeButtons)
            {
                modeButton.button.onClick.AddListener(() => {SetMode(modeButton.mode); });
            }
        }

        private void SetMode(BuilderMode mode)
        {
            foreach (var modeButton in modeButtons)
            {
                modeButton.SetActive(modeButton.mode == mode);
            }
            onTabBarButtonPressed?.Invoke(mode);
        }
    }
}