﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class StickerUI : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image image;
        [SerializeField] private Image loader;
        private Action<Texture2D> onSelected;
        private Texture2D texture2d;
        private Coroutine textureLoading;
        private void OnEnable()
        {
            button.onClick.AddListener(SelectSticker);
        }
        private void OnDisable()
        {
            button.onClick.RemoveListener(SelectSticker);
        }

        public void ShowSticker(string url, Action<Texture2D> onSelected)
        {
            ClearSticker();
            this.onSelected = onSelected;
            gameObject.SetActive(true);
           if(textureLoading!=null) StopCoroutine(textureLoading);
           textureLoading = StartCoroutine(LoadTexture(url));
        }

        public void ShowSticker(Texture2D texture, Action<Texture2D> onSelected)
        {
            ClearSticker();
            gameObject.SetActive(true);
            this.onSelected = onSelected;
            ShowSticker(texture);
        }
        private void ShowSticker(Texture2D texture)
        {
            if (texture == null) return;
            image.sprite = Sprite.Create(texture,new Rect(0,0,texture.width,texture.height),Vector2.one*0.5f );
            texture2d = texture;
            image.preserveAspect = true;
        }

        public void ClearSticker()
        {
            if(textureLoading!=null) StopCoroutine(textureLoading);
            onSelected = null;
            DestroyImmediate(texture2d);
            
        }

        public void SelectSticker()
        {
            if (texture2d == null) return;
            onSelected?.Invoke(texture2d);
        }
        private IEnumerator LoadTexture(string url)
        {
     loader.gameObject.SetActive(true);
            var www = new WWW(url);
            //yield return www;
            while (!www.isDone)
            {
            //    Debug.Log("loading - " + www.progress);
            loader.fillAmount = www.progress;
                yield return null;
            }
     loader.gameObject.SetActive(false);
     //       Debug.Log("load");
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("error - "+ www.error);
             
            }
            else
            {
                
                var texture = www.texture;
                if(texture!=null) ShowSticker(texture);
            }
           
       
        }
    }
}