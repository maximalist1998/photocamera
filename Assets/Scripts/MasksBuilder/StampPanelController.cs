﻿using System;
using System.Collections;
using System.Collections.Generic;
using MasksBuilder.Backend;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

namespace MasksBuilder
{
    public class StampPanelController : MonoBehaviour
    {
        [SerializeField] private RectTransform panels;


        public ScrollRect scrollView;
        [SerializeField] private float animationTime = 0.3f;
        [SerializeField] private Transform stickersParent;
        [SerializeField] private StampUIPrefab stampUIPrefab;
        [SerializeField] private List<Texture2D> tatoos;
        [SerializeField] private Texture2D stampsLip;
        [SerializeField] private Texture2D stampsEyelid;
        [SerializeField] private Texture2D stampsEyelash;
        [SerializeField] private Button emptySpaceButton;
        
        public static event Action<Texture2D, Color, StampType> OnStampSelected;
        private List<StampUIPrefab> stampsUI;
        private Coroutine panelsMoving;
       // private StampModeButton currentActiveButton;
        [SerializeField] private List<StampModeButton> stampModeButtons;

        private void Start()
        {
            stampsUI = new List<StampUIPrefab>();
          //  currentActiveButton = stampModeButtons[0];
         //   stampModeButtons[0].Select();
            ShowStamps(tatoos);
        }

        private void OnEnable()
        {
            foreach (var modeButton in stampModeButtons)
            {
                modeButton.OnStampModeButtonSelected += OnModeButtonSelectedListener;
            }
            
            emptySpaceButton.onClick.AddListener(EmptySpaceButtonClickHandler);
        }

        private void OnDisable()
        {
            foreach (var modeButton in stampModeButtons)
            {
                modeButton.OnStampModeButtonSelected -= OnModeButtonSelectedListener;
            }
            
            emptySpaceButton.onClick.RemoveListener(EmptySpaceButtonClickHandler);
        }
        
        private void EmptySpaceButtonClickHandler()
        {
            HidePanel();
        }

//        private void UpdateColorInList(Color32 color)
//        {
//            foreach (var stamp in stampsUI)
//            {
//                stamp.SetColor(color);
//            }
//        }

        private void OnModeButtonSelectedListener(StampModeButton modeButton)
        {
         

          
            //  currentActiveButton.OnColorChanged += UpdateColorInList;

            //     UpdateColorInList(currentActiveButton.Color);


            switch (modeButton.StampType)
            {
                case StampType.Lip:
                {
                    OnStampSelected?.Invoke(stampsLip, modeButton.Color, modeButton.StampType);
                   
                    break;
                }
                case StampType.Eyelash:
                {
                    OnStampSelected?.Invoke(stampsEyelash, modeButton.Color, modeButton.StampType);
                
                    break;
                }
                case StampType.Eyelid:
                {
                    OnStampSelected?.Invoke(stampsEyelid, modeButton.Color, modeButton.StampType);
                   
                    break;
                }
            }
            HidePanel();
        }

        private void ShowStamps(List<Texture2D> stamps)
        {
            int stampsCount = stamps.Count;
            int newStickersCount = stampsCount - stampsUI.Count;
            for (int i = 0; i < newStickersCount; i++)
            {
                stampsUI.Add(Instantiate(stampUIPrefab, stickersParent));
            }

            for (int i = 0; i < stampsUI.Count; i++)
            {
                if (i < stampsCount)
                {
                    stampsUI[i].ShowStamp(stamps[i], OnStampSelectedListener);
                }
                else
                {
                    stampsUI[i].gameObject.SetActive(false);
                }
            }
            scrollView.normalizedPosition = Vector2.up;
        }

        private void OnStampSelectedListener(Texture2D texture2D)
        {
            OnStampSelected?.Invoke(texture2D, Color.white, StampType.Tatoo);
            HidePanel();
        }

        public void HidePanel()
        {
            if (panelsMoving != null) StopCoroutine(panelsMoving);
            panelsMoving = StartCoroutine(MovePanel(new Vector2(-0.605f, 0.08f)));
            
            emptySpaceButton.gameObject.SetActive(false);
        }

        public void ShowPanel()
        {
            panels.gameObject.SetActive(true);
            gameObject.SetActive(true);

            if (panelsMoving != null) StopCoroutine(panelsMoving);
            panelsMoving = StartCoroutine(MovePanel(new Vector2(0.08f, 0.685f)));
            
            emptySpaceButton.gameObject.SetActive(true);
        }

        private IEnumerator MovePanel(Vector2 yAnchors)
        {
            panels.gameObject.SetActive(true);
            var startPosition = new Vector2(panels.anchorMin.y, panels.anchorMax.y);


            float currentTime = 0;
            while (currentTime <= animationTime)
            {
                yield return null;
                currentTime += Time.deltaTime;
                float percentage = Mathf.Clamp01(currentTime / animationTime);
                var currentPosition = Vector2.Lerp(startPosition, yAnchors, percentage);
                panels.anchorMin = new Vector2(panels.anchorMin.x, currentPosition.x);
                panels.anchorMax = new Vector2(panels.anchorMax.x, currentPosition.y);
            }
        }
    }

    public enum StampType
    {
        Lip,
        Eyelid,
        Eyelash,
        Tatoo
    }
}