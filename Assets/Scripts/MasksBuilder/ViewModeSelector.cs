﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewModeSelector : MonoBehaviour
{
    public static event Action<ViewMode> OnViewModeSelected;
    [SerializeField] private Button leftButton;
    [SerializeField] private Button rightButton;
    [SerializeField] private Button frontButton;
    [SerializeField] private Button twoDButton;

    private void OnEnable()
    {
       leftButton.onClick.AddListener(ActivateLeftMode);
       rightButton.onClick.AddListener(ActivateRightMode);
       frontButton.onClick.AddListener(ActivateFrontMode);
       twoDButton.onClick.AddListener(Activate2DMode);
    }
    
    private void OnDisable()
    {
        leftButton.onClick.RemoveListener(ActivateLeftMode);
        rightButton.onClick.RemoveListener(ActivateRightMode);
        frontButton.onClick.RemoveListener(ActivateFrontMode);
        twoDButton.onClick.RemoveListener(Activate2DMode);
    }

    private void ActivateRightMode()
    {
        ActivateMode(ViewMode.Right);
    }
    private void ActivateLeftMode()
    {
        ActivateMode(ViewMode.Left);
    }
    private void ActivateFrontMode()
    {
        ActivateMode(ViewMode.Front);
    }
    private void Activate2DMode()
    {
        ActivateMode(ViewMode.TwoD);
    }

    private void ActivateMode(ViewMode viewMode)
    {
        OnViewModeSelected?.Invoke(viewMode);
    }
}

public enum ViewMode
{
    Left,
    Front,
    Right,
    TwoD

}
