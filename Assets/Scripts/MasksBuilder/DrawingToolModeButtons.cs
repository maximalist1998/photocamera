﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class DrawingToolModeButtons : MonoBehaviour
    {
        public event Action<ToolType> OnToolSelected;

        [SerializeField] private Button drawButton;
        [SerializeField] private Image drawButtonImage;
        [SerializeField] private Button eraseButton;
        [SerializeField] private Image eraseButtonImage;
        [SerializeField] private Button fillButton;
        [SerializeField] private Image fillButtonImage;
        [SerializeField] private Button softenButton;
        [SerializeField] private Image softenButtonImage;
        [SerializeField] private Button blendButton;
        [SerializeField] private Image blendButtonImage;

        [SerializeField] private Color activeButtonColor;
        [SerializeField] private Color inActiveButtonColor;
        [SerializeField] private Color activeImageColor;
        [SerializeField] private Color inActiveImageColor;
        
        public ToolType CurrentType
        {
            get { return currentType; }
            set
            {
                if (currentType == value) return;

                SelectNewTool(value);
                currentType = value;
            }
        }

        private ToolType currentType = ToolType.Draw;

        private void Start()
        {
            drawButton.onClick.AddListener(()=>
            {
                CurrentType = ToolType.Draw;
            });
            eraseButton.onClick.AddListener(()=>
            {
                CurrentType =ToolType.Erase ;
            });
            fillButton.onClick.AddListener(()=>
            {
                CurrentType = ToolType.Fill;
            });
            softenButton.onClick.AddListener(()=>
            {
                CurrentType =ToolType.Soften ;
            });
            blendButton.onClick.AddListener(()=>
            {
                CurrentType = ToolType.Blend;
            });
        }

        private void SelectNewTool(ToolType newTool)
        {
            if (currentType == newTool) return;
            
            drawButton.image.color = inActiveButtonColor;
            drawButtonImage.color = inActiveImageColor;
            eraseButton.image.color = inActiveButtonColor;
            eraseButtonImage.color = inActiveImageColor;
            fillButton.image.color = inActiveButtonColor;
            fillButtonImage.color = inActiveImageColor;
            softenButton.image.color = inActiveButtonColor;
            softenButtonImage.color = inActiveImageColor;
            blendButton.image.color = inActiveButtonColor;
            blendButtonImage.color = inActiveImageColor;
            switch (newTool)
            {
                case ToolType.Draw:
                {
                    drawButton.image.color = activeButtonColor;
                    drawButtonImage.color = activeImageColor;  
                    break;
                }
                case ToolType.Erase:
                {
                    eraseButton.image.color = activeButtonColor;
                    eraseButtonImage.color = activeImageColor;  
                    break;
                }
                case ToolType.Fill:
                {
                    fillButton.image.color = activeButtonColor;
                    fillButtonImage.color = activeImageColor;  
                    break;
                }
                case ToolType.Blend:
                {
                    blendButton.image.color = activeButtonColor;
                    blendButtonImage.color = activeImageColor;  
                    break;
                }
                case ToolType.Soften:
                {
                    softenButton.image.color = activeButtonColor;
                    softenButtonImage.color = activeImageColor;  
                    break;
                }
                
            }

            OnToolSelected?.Invoke(newTool);
        }
    }
}