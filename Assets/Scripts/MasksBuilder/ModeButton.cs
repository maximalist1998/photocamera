﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class ModeButton : MonoBehaviour
    {
        public BuilderMode mode;
        public Button button;
        [SerializeField] private Image image;
        [SerializeField] private Sprite activeSprite;
        [SerializeField] private Sprite inactiveSprite;

        public void SetActive(bool active)
        {
            image.sprite = active ? activeSprite : inactiveSprite;
        }
    }
}