﻿using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class PanelsActivatorController : MonoBehaviour
    {
        [SerializeField] private BuilderModeController builderModeController;
        [SerializeField] private Button emptySpaceButton;
        [SerializeField] private DrawingPanel drawPanel;
        [SerializeField] private UploadTexturePanel uploadTexturePanel;
        [SerializeField] private StickersPanelController stickersPanel;
        [SerializeField] private StampPanelController stampsPanel;

        private void OnEnable()
        {
            builderModeController.OnModeSelected += OpenPanel;
            OpenPanel(builderModeController.CurrentMode);
        }

        private void OnDisable()
        {
            builderModeController.OnModeSelected -= OpenPanel;
        }

        private void OpenPanel(BuilderMode builderMode)
        {
            switch (builderMode)
            {
                case BuilderMode.Drawing:
                {
                    stickersPanel.gameObject.SetActive(false);
                    uploadTexturePanel.gameObject.SetActive(false);
                    stampsPanel.gameObject.SetActive(false);
                    emptySpaceButton.gameObject.SetActive(true);
                    drawPanel.ShowPanel(false);
                    break;
                }
                case BuilderMode.UploadTexture:
                {
                    stampsPanel.gameObject.SetActive(false);
                    stickersPanel.gameObject.SetActive(false);
                    drawPanel.HidePanel();
                    emptySpaceButton.gameObject.SetActive(false);
                    uploadTexturePanel.ShowPanel();
                    break;
                }
                case BuilderMode.Stickers:
                {
                    stampsPanel.gameObject.SetActive(false);
                    uploadTexturePanel.gameObject.SetActive(false);
                    emptySpaceButton.gameObject.SetActive(false);
                    drawPanel.HidePanel();
                    stickersPanel.ShowPanel();
                    break;
                }
                case BuilderMode.Stamp:{
                    stickersPanel.gameObject.SetActive(false);
                    uploadTexturePanel.gameObject.SetActive(false);
                    emptySpaceButton.gameObject.SetActive(false);
                    drawPanel.HidePanel();
                    stampsPanel.ShowPanel();
                    break;
                }
                default:
                {
                    stampsPanel.gameObject.SetActive(false);
                    emptySpaceButton.gameObject.SetActive(false);
                    drawPanel.HidePanel();
                    uploadTexturePanel.gameObject.SetActive(false);
                    break;
                }
            }
        }
    }
}