﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class TextureButton : MonoBehaviour
    {
        public Button button;
        public GameObject selector;
        public Texture2D texture2D;
        public event Action<TextureButton> OnButtonPressed;
        [SerializeField] private Image texturePreview;

        public void Initialize(Texture2D tex)
        {
            texture2D = tex;
            SetTexture(texture2D);
        }

        private void OnEnable()
        {
            button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            OnButtonPressed?.Invoke(this);
        }

        public void Select()
        {
            selector.SetActive(true);
        }

        public void SetTexture(Texture2D texture2D)
        {
            if (texture2D == null)
            {
                texturePreview.sprite = null;
                return;
            }
            this.texture2D = texture2D;
            texturePreview.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height),
                Vector2.one * 0.5f);
        }

        public void Deselect()
        {
            selector.SetActive(false);
        }
    }
}