﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    

public class ColorPicker : MonoBehaviour
{
   [SerializeField] private Button cancelButton;
   [SerializeField] private Button acceptButton;
   [SerializeField] private FlexibleColorPicker flexibleColorPicker;
   
   
   private Color currentColor;
   private Action<Color> onPicked;
   private Action onCancel;

   private void OnEnable()
   {
      acceptButton.onClick.AddListener(AcceptColor);
      cancelButton.onClick.AddListener(DeclineColor);
   }
   private void OnDisable()
   {
      acceptButton.onClick.RemoveListener(AcceptColor);
      cancelButton.onClick.RemoveListener(DeclineColor);
   }

   private void AcceptColor()
   {
      onPicked?.Invoke(flexibleColorPicker.color);
      gameObject.SetActive(false);
   }
 private void DeclineColor()
   {
      onCancel?.Invoke();
      gameObject.SetActive(false);
   }

   public void ShowColorPicker(Color showedColor,Action<Color> onPicked,Action onCancel = null)
   {
      this.onPicked = onPicked;
      this.onCancel = onCancel;
      currentColor = showedColor;
      flexibleColorPicker.startingColor = currentColor;
      gameObject.SetActive(true);
     
   }
}
}
