﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    
    public class BuilderModeController : MonoBehaviour
    {
        public event Action<BuilderMode> OnModeSelected;
        [SerializeField] private TabBar tabBar;

         
        public BuilderMode CurrentMode {
            get { return currentMode; }
        }
        private BuilderMode currentMode = BuilderMode.Inactive;

        public void SetMode(BuilderMode mode)
        {
           // if(currentMode == mode) return;

            currentMode = mode;
            OnModeSelected?.Invoke(mode);
            
        }

        private void OnEnable()
        {
            tabBar.onTabBarButtonPressed += SetMode;
            
        }
        private void OnDisable()
        {
            tabBar.onTabBarButtonPressed -= SetMode;
        }
    }

public enum BuilderMode
    {
        Stickers,
        Drawing,
        Saving,
        UploadTexture,
        Stamp,
        Inactive
    }
}