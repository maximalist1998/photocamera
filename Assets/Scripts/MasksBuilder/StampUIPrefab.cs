﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class StampUIPrefab : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image image;
        [SerializeField] private Image loader;
        private Action<Texture2D> onSelected;
        private Texture2D texture2d;
        private Coroutine textureLoading;
        private void OnEnable()
        {
            button.onClick.AddListener(SelectStamp);
        }
        private void OnDisable()
        {
            button.onClick.RemoveListener(SelectStamp);
        }

        public void ShowStamp(string url, Action<Texture2D> onSelected)
        {
            ClearStamp();
            //this.stampType = stampType;
            this.onSelected = onSelected;
            gameObject.SetActive(true);
           if(textureLoading!=null) StopCoroutine(textureLoading);
           textureLoading = StartCoroutine(LoadTexture(url));
        }

        public void ShowStamp(Texture2D texture, Action<Texture2D> onSelected)
        {
            ClearStamp();
          //  this.stampType = stampType;
            gameObject.SetActive(true);
            this.onSelected = onSelected;
            ShowStamp(texture);
        }
        private void ShowStamp(Texture2D texture)
        {
            if (texture == null) return;
            image.preserveAspect = true;
            image.sprite = Sprite.Create(texture,new Rect(0,0,texture.width,texture.height),Vector2.one*0.5f );
            image.preserveAspect = true;
            texture2d = texture;
        }

//        public void SetColor(Color32 color)
//        {
//            Debug.Log("Set color - " + color);
//            image.color = color;
//        }

        public void ClearStamp()
        {
            if(textureLoading!=null) StopCoroutine(textureLoading);
            onSelected = null;

           if(image.sprite!=null)
            Destroy(image.sprite);
            
        }

        public void SelectStamp()
        {
            if (texture2d == null) return;
            onSelected?.Invoke(texture2d);
        }
        private IEnumerator LoadTexture(string url)
        {
     loader.gameObject.SetActive(true);
            var www = new WWW(url);
            //yield return www;
            while (!www.isDone)
            {
            //    Debug.Log("loading - " + www.progress);
            loader.fillAmount = www.progress;
                yield return null;
            }
     loader.gameObject.SetActive(false);
     //       Debug.Log("load");
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("error - "+ www.error);
             
            }
            else
            {
                
                var texture = www.texture;
                if(texture!=null) ShowStamp(texture);
            }
           
       
        }
    }
}