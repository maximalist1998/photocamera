﻿using System;
using System.Collections;
using System.Collections.Generic;
using MasksBuilder.Backend;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace MasksBuilder
{
    public class StickersPanelController : MonoBehaviour
    {
        [SerializeField] private RectTransform panels;
        
        
         public ScrollRect scrollView;
        [SerializeField] private float animationTime = 0.3f;
        [SerializeField] private Transform stickersParent;
        [SerializeField] private StickerUI stickerUIPrefab;
        [SerializeField] private CustomInputField searchInputField;
        [SerializeField] private SearchRequestController searchRequestController;
        [SerializeField] private string defaultSearchStr = "happy";
        public static event Action<Texture2D> OnTextureSelected;
        private List<StickerUI> stickersUI;
        private Coroutine panelsMoving;
       // public bool isShowed { get; private set; }
       private int currentActiveStickersCount = 0;
        private void OnEnable()
        {
            searchInputField.onValueChanged.AddListener(SearchStickers);
            scrollView.onValueChanged.AddListener(ScrollViewPositionListener);
      //      isShowed = true;
        }

        private void OnDisable()
        {
            searchInputField.onValueChanged.RemoveListener(SearchStickers);
            scrollView.onValueChanged.RemoveListener(ScrollViewPositionListener);
        }

        private void Start()
        {
            stickersUI = new List<StickerUI>();
            SearchStickers(defaultSearchStr);
           
        }

        private bool ignoreScrollViewPostionChange = true;

        private void ScrollViewPositionListener(Vector2 viewPosition)
        { 
//            Debug.Log("scrollView.verticalScroller.value - " + viewPosition);
//            Debug.Log("ignoreScrollViewPostionChange - "+ ignoreScrollViewPostionChange);
            if (viewPosition.y<=0&&!ignoreScrollViewPostionChange)
            {
                ignoreScrollViewPostionChange = true;
                searchRequestController.LoadNextPage((urls) =>
                {
                    ignoreScrollViewPostionChange = false;
                    AddStickers(urls);
                });
            }
        }

        private void SearchStickers(string searchRequest)
        {
            ignoreScrollViewPostionChange = true;
//            Debug.Log("search stickers - " + searchRequest);
            scrollView.normalizedPosition = new Vector2(0,1);
            searchRequestController.SearchStaticStickers(searchRequest, (stickers)=>
            {
                ShowStickers(stickers);
                ignoreScrollViewPostionChange = false;
            });
        }

        private void AddStickers(List<string> urls)
        {
//            Debug.Log("additional stickers count - "+ urls.Count);
            int newStickersCount = currentActiveStickersCount+urls.Count - stickersUI.Count;
            for (int i = 0; i < newStickersCount; i++)
            {
                stickersUI.Add(Instantiate(stickerUIPrefab, stickersParent));
            }

            for (int i = currentActiveStickersCount; i < stickersUI.Count; i++)
            {
                if (i < urls.Count+currentActiveStickersCount)
                {
                    stickersUI[i].ShowSticker(urls[i-currentActiveStickersCount], OnStickerSelected);
                }
                else
                {
                    stickersUI[i].gameObject.SetActive(false);
                }
            }
            currentActiveStickersCount += urls.Count;
        }

        public void ShowStickers(List<string> urls)
        {
            
            int urlsCount = urls.Count;
            currentActiveStickersCount = urlsCount;
            int newStickersCount = urlsCount - stickersUI.Count;
            for (int i = 0; i < newStickersCount; i++)
            {
                stickersUI.Add(Instantiate(stickerUIPrefab, stickersParent));
            }

            for (int i = 0; i < stickersUI.Count; i++)
            {
                if (i < urlsCount)
                {
                    stickersUI[i].ShowSticker(urls[i], OnStickerSelected);
                }
                else
                {
                    stickersUI[i].gameObject.SetActive(false);
                }
            }
        }

        private void OnStickerSelected(Texture2D texture2D)
        {
            OnTextureSelected?.Invoke(texture2D);
            HidePanel();
        }

        public void HidePanel()
        {
     //       if (!isShowed) return;
            if (panelsMoving != null) StopCoroutine(panelsMoving);
            panelsMoving = StartCoroutine(MovePanel(new Vector2(-0.9f, 0.08f)));
    //        isShowed = false;
        }

        public void ShowPanel()
        {
          //  if (isShowed) return;
            panels.gameObject.SetActive(true);
            gameObject.SetActive(true);

        //    isShowed = true;
            if (panelsMoving != null) StopCoroutine(panelsMoving);
            panelsMoving = StartCoroutine(MovePanel(new Vector2(0.08f, 0.98f)));
        }

        private IEnumerator MovePanel(Vector2 yAnchors)
        {
            panels.gameObject.SetActive(true);
            var startPosition = new Vector2(panels.anchorMin.y, panels.anchorMax.y);
            

            float currentTime = 0;
            while (currentTime <= animationTime)
            {
                yield return null;
                currentTime += Time.deltaTime;
                float percentage = Mathf.Clamp01(currentTime / animationTime);
                var currentPosition = Vector2.Lerp(startPosition, yAnchors, percentage);
                panels.anchorMin = new Vector2(panels.anchorMin.x, currentPosition.x);
                panels.anchorMax = new Vector2(panels.anchorMax.x, currentPosition.y);
            }
        }
    }
}