﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class ColorPanel : MonoBehaviour
    {
        public event Action<Color> OnColorPicked;
        [SerializeField] private ColorPicker colorPicker;
        [SerializeField] private List<ColorButton> colorButtons;

        public Color ActiveColor
        {
            get
            {
                return ActiveButton.button.image.color;
            }
        }

        private ColorButton activeButton;

        private void OnEnable()
        {
            var activeCol = ActiveColor;
            
            foreach (var colorButton in colorButtons)
            {
                colorButton.OnButtonPressed += SelectColorButton;
            }
        }   
        private void OnDisable()
        {
            foreach (var colorButton in colorButtons)
            {
                colorButton.OnButtonPressed -= SelectColorButton;
            }
        }

        public void SelectDefaultColor(bool change = true)
        {
            if (change)
            {
                SelectColorButton(colorButtons[0]);
            }
            else
            {
                activeButton = colorButtons[0];
                ResetSelectedColorInUI();
                OnSelectColor(ActiveButton.button.image.color);
            }
        }

        private void SelectColorButton(ColorButton colorButton)
        {
            ActiveButton = colorButton;
            //Ivan made color select invoke OnDrawingToolChanged event from DrawingPanel
            OnSelectColor(ActiveButton.button.image.color);
        }

        public ColorButton ActiveButton
        {
            set
            {
                if (value == null) return;
                if (DrawingPanel.IsFullOpened || (activeButton == value && activeButton.canChangeColor))
                {
                    colorPicker.ShowColorPicker(ActiveColor, OnSelectColor);
                }

                activeButton = value;
                ResetSelectedColorInUI();
            }
            get
            {
                if (activeButton == null)
                    ActiveButton = colorButtons[0]; //default button
                return activeButton;
            }
        }

        private void ResetSelectedColorInUI()
        {
            foreach (var colorButton in colorButtons)
            {
                if (colorButton == ActiveButton) colorButton.Select();
                else
                {
                    colorButton.Deselect();
                }
            }
        }

        private void OnSelectColor(Color color)
        {
            ActiveButton.button.image.color = color;
            OnColorPicked?.Invoke(color);
        }
    }
}