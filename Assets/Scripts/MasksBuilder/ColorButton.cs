﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class ColorButton : MonoBehaviour
    {
        public Button button;
        public GameObject selector;
        public bool canChangeColor = true;
        
        public event Action<ColorButton> OnButtonPressed;

        private void OnEnable()
        {
            button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            OnButtonPressed?.Invoke(this);
        }

        public void Select()
        {
            selector.SetActive(true);
        }

        public void Deselect()
        {
            selector.SetActive(false);
        }
    }
}