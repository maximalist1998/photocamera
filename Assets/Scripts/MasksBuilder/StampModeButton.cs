﻿using System;
using System.Collections;
using System.Collections.Generic;
using MasksBuilder;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StampModeButton : MonoBehaviour
{
    [SerializeField] private Color32 inactiveColor;
    [SerializeField] private Color32 activeColor;
    [SerializeField] private EventTrigger modeButton;
    [SerializeField] private GameObject modeButtonSelector;
    [SerializeField] private Image modeButtonImage;
    [SerializeField] private StampType stampType;
    [SerializeField] private Button colorButton;
    [SerializeField] private ColorPicker colorPicker;
    public Action<StampModeButton> OnStampModeButtonSelected;
    
    public Color32 Color
    {
        get { return colorButton.image.color; }
    }

    public StampType StampType
    {
        get { return stampType; }
    }

    private bool isSelected;
    // Start is called before the first frame update
    private void OnEnable()
    {
        colorButton.onClick.AddListener(ShowColorPicker);
      //  modeButton.onClick.AddListener(OnClickModeButton);
        modeButton.triggers[0].callback.AddListener(Select); ;
        modeButton.triggers[1].callback.AddListener(Deselect); ;
    }

    private void OnDisable()
    {
        colorButton.onClick.RemoveListener(ShowColorPicker);
   //     modeButton.onClick.RemoveListener(OnClickModeButton);
        
    }

    private void ShowColorPicker()
    {
        colorPicker.ShowColorPicker(colorButton.image.color, (color) =>
        {
            colorButton.image.color = color;
          //  OnStampModeButtonSelected?.Invoke(this);
        });
    }

    private void OnClickModeButton()
    {
        OnStampModeButtonSelected?.Invoke(this);
    }

    private void Select(BaseEventData pointerEventData)
    {
        if(isSelected) return;
        isSelected = true;
        modeButtonSelector.SetActive(true);
        modeButtonImage.color = activeColor;
    }

    private void Deselect(BaseEventData pointerEventData)
    {
        if(!isSelected) return;
        isSelected = false;
        modeButtonSelector.SetActive(false);
        modeButtonImage.color = inactiveColor;
        OnClickModeButton();
    }
    
}