﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasksBuilder
{
    public class DrawingPanel : MonoBehaviour
    {
        public static event Action<DrawingTool> OnDrawingToolChanged;
        
        [SerializeField] private DrawingToolModeButtons drawingToolModeButtons;
        [SerializeField] private Slider lineThicknessSlider;
        [SerializeField] private ColorPanel colorPanel;
        [SerializeField] private TextureButtonsPanel textureButtons;
        [SerializeField] private Button topButton;
        [SerializeField] private Button emptySpaceButton;
        [SerializeField] private RectTransform panels;
        [SerializeField] private GameObject drawingPanel;
        [SerializeField] private RectTransform bottomButtons;
        private Coroutine panelsMoving;
        [SerializeField] private float animationTime = 0.3f;

        private readonly Vector2 BottomPanelHalfModeAnchorMin = new Vector2(0f, 0.2f);
        private readonly Vector2 BottomPanelHalfModeAnchorMax = new Vector2(1f, 0.245f);
        private readonly Vector2 BottomPanelNotHalfModeAnchorMin = new Vector2(0f, 0.1f);
        private readonly Vector2 BottomPanelNotHalfModeAnchorMax = new Vector2(1f, 0.145f);

        private static bool isFullOpened = false;
        public static bool IsFullOpened
        {
            get { return isFullOpened; }
        }

        public DrawingTool CurrentDrawingTool
        {
            get { return currentDrawingTool; }
            private set
            {
                if (currentDrawingTool.Equals(value)) return;   
              
                currentDrawingTool = value;
                OnDrawingToolChanged?.Invoke(currentDrawingTool);
            }
        }

        public void ShowPanel(bool halfMode)
        {
            emptySpaceButton.onClick.AddListener(EmptySpaceButtonOnClick);
            
            isFullOpened = !halfMode;
            drawingPanel.SetActive(true);
            emptySpaceButton.gameObject.SetActive(!halfMode);
            if(panelsMoving!= null) StopCoroutine(panelsMoving);
            panelsMoving = StartCoroutine(ActivatePanels(halfMode));
            
            //Set Current tool when open drawing panel
            OnDrawingToolChanged?.Invoke(currentDrawingTool);
        }
        
        public void HidePanel()
        {
            emptySpaceButton.onClick.RemoveListener(EmptySpaceButtonOnClick);
            
            drawingPanel.SetActive(false);
            bottomButtons.anchorMin = BottomPanelNotHalfModeAnchorMin;
            bottomButtons.anchorMax = BottomPanelNotHalfModeAnchorMax;
            
            if(panelsMoving!= null) StopCoroutine(panelsMoving);
        }

        private IEnumerator ActivatePanels(bool halfMode)
        {
            var startPosition = new Vector2(panels.anchorMin.y, panels.anchorMax.y);
            var finishPosition = halfMode?new Vector2(-0.393f,0.2f): new Vector2(0.08f, 0.58f);
            var bottomButtonsStartPosition = new Vector2(bottomButtons.anchorMin.y, bottomButtons.anchorMax.y);
            var bottomButtonsFinishPosition = halfMode
                ? new Vector2(BottomPanelHalfModeAnchorMin.y, BottomPanelHalfModeAnchorMax.y)
                : new Vector2(BottomPanelNotHalfModeAnchorMin.y, BottomPanelNotHalfModeAnchorMax.y);
            
            float currentTime = 0;
            while (currentTime<=animationTime)
            {
                yield return null;
                currentTime += Time.deltaTime;
                float percentage = Mathf.Clamp01(currentTime / animationTime);
                var currentPosition = Vector2.Lerp(startPosition, finishPosition, percentage);
                panels.anchorMin = new Vector2(panels.anchorMin.x,currentPosition.x);
                panels.anchorMax = new Vector2(panels.anchorMax.x,currentPosition.y);

                var bottomButtonsCurr =
                    Vector2.Lerp(bottomButtonsStartPosition, bottomButtonsFinishPosition, percentage);
                bottomButtons.anchorMin = new Vector2(bottomButtons.anchorMin.x, bottomButtonsCurr.x);
                bottomButtons.anchorMax = new Vector2(bottomButtons.anchorMax.x, bottomButtonsCurr.y);
            }
            
            isFullOpened = !halfMode;
        }

        private DrawingTool currentDrawingTool;

        private void Start()
        {
            currentDrawingTool = new DrawingTool(drawingToolModeButtons.CurrentType, colorPanel.ActiveColor, lineThicknessSlider.value, textureButtons.ActiveTexture);
        }

        private void OnEnable()
        {
            topButton.onClick.AddListener(TopButtonOnClick);
            drawingToolModeButtons.OnToolSelected += OnToolSelected;
            lineThicknessSlider.onValueChanged.AddListener(OnLineThicknessSelected);
            colorPanel.OnColorPicked += OnColorSelected;
            textureButtons.OnTexturePicked += OnTextureSelected;
        }
        private void OnDisable()
        {
            topButton.onClick.RemoveListener(TopButtonOnClick);
            drawingToolModeButtons.OnToolSelected -= OnToolSelected;
            colorPanel.OnColorPicked -= OnColorSelected;
            textureButtons.OnTexturePicked -= OnTextureSelected;
            lineThicknessSlider.onValueChanged.RemoveListener(OnLineThicknessSelected);
        }

        private void EmptySpaceButtonOnClick()
        {
            ShowPanel(true);
        }
        private void TopButtonOnClick()
        {
            if(isFullOpened)
                ShowPanel(true);
            else
                ShowPanel(false);
        }

        private void OnToolSelected(ToolType toolType)
        {
            if((CurrentDrawingTool.toolType == ToolType.Fill && toolType == ToolType.Draw) || toolType == ToolType.Erase || toolType == ToolType.Soften)
                textureButtons.DeselectTexture();
            
            CurrentDrawingTool = new DrawingTool(toolType,currentDrawingTool.color,currentDrawingTool.lineThickness,currentDrawingTool.texture2D);
        }
        private void OnColorSelected(Color color)
        {
            CurrentDrawingTool = new DrawingTool(currentDrawingTool.toolType,color,currentDrawingTool.lineThickness,currentDrawingTool.texture2D);
        }
        private void OnLineThicknessSelected(float lineThickness)
        {
            CurrentDrawingTool = new DrawingTool(currentDrawingTool.toolType,currentDrawingTool.color,lineThickness,currentDrawingTool.texture2D);
        }
        private void OnTextureSelected(Texture2D texture2D)
        {
            if (texture2D != null)
            {
                colorPanel.SelectDefaultColor(false);
            }

            if (CurrentDrawingTool.toolType == ToolType.Erase || CurrentDrawingTool.toolType == ToolType.Soften)
                drawingToolModeButtons.CurrentType = ToolType.Draw;
            
            CurrentDrawingTool = new DrawingTool(currentDrawingTool.toolType,currentDrawingTool.color,currentDrawingTool.lineThickness,texture2D);
        }
        
    }

    public struct DrawingTool
    {
        public Color color;
        public Texture2D texture2D;
        public ToolType toolType;
        public float lineThickness;

       

        public DrawingTool(ToolType toolType, Color color,float lineThickness = 1f, Texture2D texture2D = null)
        {
            this.color = color;
            this.toolType = toolType;
            this.lineThickness = lineThickness;
            this.texture2D = texture2D;
        }
    }

    public enum ToolType
    {
        Draw,
        Erase,
        Fill,
        Soften,
        Blend
    }
}
