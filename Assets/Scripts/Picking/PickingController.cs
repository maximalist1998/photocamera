﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;
using UniUtils;
using CameraType = EasyMobile.CameraType;

public class PickingController : Singleton<PickingController>
{
    [SerializeField] private Button getFromGalleryVideoButton;
    [SerializeField] private Button getFromGalleryPhotoButton;

    [SerializeField] private Button getFromCameraVideoButton;
    // [SerializeField] private Button getFromCameraPhotoButton;

    [SerializeField] private GameObject panelPicking;
    [SerializeField] private Texture2D debugTexture;
    [SerializeField] private string debugVideoPath;

    private Coroutine permissionsCoroutine;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        getFromGalleryPhotoButton.gameObject.SetActive(false);
        getFromGalleryVideoButton.transform.GetComponentInChildren<Text>().text = "Upload from Gallery.";
        getFromGalleryVideoButton.onClick.AddListener(PickFromGallery);
        UniAndroidPermission.RequestPermission(AndroidPermission.RECORD_AUDIO, () => { });
#elif UNITY_IOS
        getFromGalleryPhotoButton.onClick.AddListener(PickPhotoFromIOSGallery);
        getFromGalleryVideoButton.onClick.AddListener(PickVideoFromIOSGallery);
#endif
        //  getFromCameraPhotoButton.onClick.AddListener(PickPhotoFromCamera);
        getFromCameraVideoButton.onClick.AddListener(OpenMasks);

        if (!EasyMobile.RuntimeManager.IsInitialized())
            EasyMobile.RuntimeManager.Init();
    }
#if UNITY_IOS
    private void OnEnable()
    {
        PickerEventListener.onImageSelect += OnImageSelect;
    }
    private void OnDisable()
    {
        PickerEventListener.onImageSelect -= OnImageSelect;
    }

    private void OnImageSelect(string path, ImageAndVideoPicker.ImageOrientation rotation)
    {
        StartPostprocess(path);
    }
#endif
    public void ShowPickingPanel()
    {
        panelPicking.SetActive(true);
    }

    private void PickPhotoFromCamera()
    {
#if UNITY_EDITOR
        panelPicking.SetActive(false);
        PostprocessController.Instance.StartPostprocess(debugTexture);
        PostprocessCroppimgManager.Instance.HoldScreenshotLocal(debugTexture);
        //  return;
#endif

#if UNITY_ANDROID
        if (permissionsCoroutine != null)
            StopCoroutine(permissionsCoroutine);
        permissionsCoroutine = StartCoroutine(RequestAndroidPermissions(() =>
#endif
            {
                if (EasyMobile.Media.Camera.IsCameraAvailable(CameraType.Rear))
                    EasyMobile.Media.Camera.TakePicture(CameraType.Rear, (msg, result) =>
                    {
                        Debug.Log($"msg - '{msg}', result - '{result}'");
                        if (result != null)
                        {
                            if (result.Uri != null)
                            {
                                // panelPicking.SetActive(false);
                                StartPostprocess(result.absoluteUri);
                            }
                        }
                    });
                else if (EasyMobile.Media.Camera.IsCameraAvailable(CameraType.Front))
                {
                    EasyMobile.Media.Camera.TakePicture(CameraType.Front, (msg, result) =>
                    {
                        Debug.Log($"msg - '{msg}', result - '{result}'");
                        if (result != null)
                        {
                            if (result.Uri != null)
                            {
                                //  panelPicking.SetActive(false);
                                StartPostprocess(result.absoluteUri);
                            }
                        }
                    });
                }
                else
                {
                    Debug.Log("Camera unavailable!");
                }
            }
#if UNITY_ANDROID
            , Permission.Camera, Permission.ExternalStorageRead));
#endif
    }
#if UNITY_IOS
    private void PickPhotoFromIOSGallery()
    {
//        NativeGallery.GetImageFromGallery(path =>
//        {
//            if (!string.IsNullOrEmpty(path))
//            {
//                StartPostprocess(path);
//            }
//        });
        ImageAndVideoPicker.IOSPicker.BrowseImage(false);
    }
    private void PickVideoFromIOSGallery()
    {
        NativeGallery.GetVideoFromGallery(path =>
        {
            if (!string.IsNullOrEmpty(path))
            {
                StartPostprocess(path);
            }
        });
    }
#endif
    private void OpenMasks()
    {
#if UNITY_ANDROID

        var isCam = UniAndroidPermission.IsPermitted(AndroidPermission.CAMERA);
        var isMic = UniAndroidPermission.IsPermitted(AndroidPermission.RECORD_AUDIO);

        if (!isCam)
        {
            UniAndroidPermission.RequestPermission(AndroidPermission.CAMERA,
                () =>
                {
//                    if (!isMic)
//                    {
//                        UniAndroidPermission.RequestPermission(AndroidPermission.RECORD_AUDIO, () =>
//                        {
                    Debug.Log("On granted permission!!!!!");
                    panelPicking.SetActive(false);
                    RecordingPanelActivator.Instance.ActivateFaceMask();
                    //  });
                    //  }
                });
        }
        else if (!isMic)
        {
            UniAndroidPermission.RequestPermission(AndroidPermission.RECORD_AUDIO, () =>
            {
                Debug.Log("On granted permission!!!!!");
                panelPicking.SetActive(false);
                RecordingPanelActivator.Instance.ActivateFaceMask();
            });
        }
        else
        {
            Debug.Log("On granted permission!!!!!");
            panelPicking.SetActive(false);
            RecordingPanelActivator.Instance.ActivateFaceMask();
        }

#else
Debug.Log("On granted permission!!!!!");
panelPicking.SetActive(false);
RecordingPanelActivator.Instance.ActivateFaceMask();
#endif
    }

    public void CloseMasks()
    {
        RecordingPanelActivator.Instance.DeactivateFaceMask();
        panelPicking.SetActive(true);
    }

    private void PickVideoFromCamera()
    {
#if UNITY_EDITOR
        panelPicking.SetActive(false);
        PostprocessController.Instance.StartPostprocess(debugVideoPath);
        //   return;
#endif
#if UNITY_ANDROID
        if (permissionsCoroutine != null)
            StopCoroutine(permissionsCoroutine);
        permissionsCoroutine = StartCoroutine(RequestAndroidPermissions(() =>
#endif
            {
                if (EasyMobile.Media.Camera.IsCameraAvailable(CameraType.Rear))
                    EasyMobile.Media.Camera.RecordVideo(CameraType.Rear, (msg, result) =>
                    {
                        if (result != null)
                        {
                            if (result.Uri != null)
                            {
                                // panelPicking.SetActive(false);
                                StartPostprocess(result.absoluteUri);
                            }
                        }
                    });
                else if (EasyMobile.Media.Camera.IsCameraAvailable(CameraType.Front))
                {
                    EasyMobile.Media.Camera.RecordVideo(CameraType.Front, (msg, result) =>
                    {
                        if (result != null)
                        {
                            if (result.Uri != null)
                            {
                                //    panelPicking.SetActive(false);
                                StartPostprocess(result.absoluteUri);
                            }
                        }
                    });
                }
                else
                {
                    Debug.Log("Camera unavailable!");
                }
            }
#if UNITY_ANDROID
            , Permission.Camera, Permission.ExternalStorageRead));
#endif
    }

    private void PickFromGallery()
    {
#if UNITY_ANDROID

        if (permissionsCoroutine != null)
            StopCoroutine(permissionsCoroutine);
        permissionsCoroutine = StartCoroutine(RequestAndroidPermissions(() =>
        {
            AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("pickMedia");
        }, Permission.ExternalStorageRead));

#endif

#if UNITY_IOS
        EasyMobile.Media.Gallery.Pick((msg, result) =>
        {
            if (result != null)
            {
                

                StartPostprocess(result[0].Uri);
            }
        });
#endif
    }
#if UNITY_ANDROID

    private IEnumerator RequestAndroidPermissions(Action onGranted, params string[] permissions)
    {
        bool allPermissionsGranted = false;

        //TODO REMOVE AFTER TESTING
        getFromCameraVideoButton.gameObject.SetActive(false);

        while (!allPermissionsGranted)
        {
            Debug.Log("Wait all permissions");
            allPermissionsGranted = true;
            foreach (var permission in permissions)
            {
                Debug.Log($"permission {permission} granted - {Permission.HasUserAuthorizedPermission(permission)}");
            }

            foreach (var permission in permissions)
            {
                if (!Permission.HasUserAuthorizedPermission(permission))
                {
                    Debug.Log($"permission not granted - {permission}");
                    try
                    {
                        Permission.RequestUserPermission(permission);
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning(e);
                    }

                    Debug.Log("Permission.RequestUserPermission(permission);");
                    allPermissionsGranted = false;
                    Debug.Log("allPermissionsGranted = false;");
                    yield return new WaitForSeconds(1f);
                    Debug.Log("yield return new WaitForSeconds(1f);");
                    break;
                }
            }

            yield return null;
        }

        //TODO REMOVE AFTER TESTING
        getFromCameraVideoButton.gameObject.SetActive(true);

        Debug.Log("Before  onGranted?.Invoke();");
        onGranted?.Invoke();
    }
#endif
    public void StartPostprocess(string path)
    {
        if (string.IsNullOrEmpty(path)) return;
#if UNITY_IOS
        path = path.Replace("file://", "");
#endif

        panelPicking.SetActive(false);
        Debug.Log("postprocess path - " + path);
        var contentType = ProcessedContentTypeTool.GetContentTypeByPath(path);
        Debug.Log(contentType.ToString());
        switch (contentType)
        {
            case ProcessedContentTypeTool.ContentType.Image:
            {
                SetTextureFromNative(path);
                break;
            }

            case ProcessedContentTypeTool.ContentType.Video:
            {
                Debug.Log("ProcessedContentTypeTool.ContentType.Video - " + path);
                PostprocessController.Instance.StartPostprocess(path);
                break;
            }
            case ProcessedContentTypeTool.ContentType.GIF:
            {
                PostprocessController.Instance.StartPostprocessGif(path);
                break;
            }
        }
    }
#if UNITY_ANDROID

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && panelPicking.activeInHierarchy)
            Application.Quit();
    }
#endif
    private static void SetTextureFromNative(string path)
    {
        var tex = TestExif.GetImage(path);
        PostprocessController.Instance.StartPostprocess(tex);
        PostprocessCroppimgManager.Instance.HoldScreenshotLocal(tex);
    }
}