﻿using System.Collections;
using Executor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ComingSoonUiController : MonoBehaviour
{
    #region > Editor Fields

    [SerializeField]
    private Image backgroundImg;
    [SerializeField]
    private TextMeshProUGUI text;
    [SerializeField]
    private float showTime = 3f;
    [SerializeField]
    private float fadeInTime = 1f;

    #endregion > Editor Fields
    
    #region > Private Fields

    private Coroutine showCoroutine;
    
    #endregion > Private Fields

    public void ShowComingSoon()
    {
        if(showCoroutine != null)
            CoroutineExecutor.Instance.Stop(showCoroutine);

        showCoroutine = CoroutineExecutor.Instance.Execute(ShowCoroutine());
    }

    private IEnumerator ShowCoroutine()
    {
        backgroundImg.gameObject.SetActive(true);
        yield return FadeEffect(true);
        
        yield return new WaitForSeconds(showTime);
        
        yield return FadeEffect(false);
        backgroundImg.gameObject.SetActive(false);
    }

    private IEnumerator FadeEffect(bool isFadeIn)
    {
        var startTime = Time.time;
        var percent = (Time.time - startTime) / fadeInTime;

        while (percent < 1)
        {
            percent = (Time.time - startTime) / fadeInTime;
            var alpha = isFadeIn? percent : 1f - percent;
            
            backgroundImg.color = new Color(backgroundImg.color.r, backgroundImg.color.g, backgroundImg.color.b, alpha);
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);

            yield return null;
        }

        var finaleAlpha = isFadeIn ? 1f : 0f;
        backgroundImg.color = new Color(backgroundImg.color.r, backgroundImg.color.g, backgroundImg.color.b, finaleAlpha);
        text.color = new Color(text.color.r, text.color.g, text.color.b, finaleAlpha);
        
    }
}