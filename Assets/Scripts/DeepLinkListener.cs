﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;

public class DeepLinkListener : MonoBehaviour
{
#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void DeepLinkReceiverIsAlive();


    public bool dontDestroyOnLoad = true;

    void Start()
    {
        if (dontDestroyOnLoad)
            DontDestroyOnLoad(this.gameObject);
        DeepLinkReceiverIsAlive(); // Let the App Controller know it's ok to call URLOpened now.
    }

    public void URLOpened(string url)
    {
        Debug.Log("recieved deep link - " + url);
        NativeBridgeController.Instance.messageFromIOS(url);
    }
#endif
}