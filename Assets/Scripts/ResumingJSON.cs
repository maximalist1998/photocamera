﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class ResumingJSON
{
    public string mainImagePath;
    public string croppedImagePath;
    public List<VideoSticker> videoStickers;
    public List<ImageSticker> imageStickers;
    public List<PostprocessText> postprocessTexts;
}

[Serializable]
public class VideoSticker
{
    public int id;
    public Quaternion rotation;
    public Vector3 position;
    public Vector3 scale;

    public VideoSticker(int id, Quaternion rotation, Vector3 position, Vector3 scale)
    {
        this.id = id;
        this.rotation = rotation;
        this.position = position;
        this.scale = scale;
    }
}
[Serializable]
public class ImageSticker
{
    public int id;
    public Quaternion rotation;
    public Vector3 position;
    public Vector3 scale;

    public ImageSticker(int id, Quaternion rotation, Vector3 position, Vector3 scale)
    {
        this.id = id;
        this.rotation = rotation;
        this.position = position;
        this.scale = scale;
    }
}
[Serializable]
public class PostprocessText
{
    public string text;
    public Color outline;
    public Color mainColor;
    public string font;
    public Quaternion rotation;
    public Vector3 position;
    public Vector3 scale;

    public PostprocessText(string text, Color outline, Color mainColor, string font, Quaternion rotation, Vector3 position, Vector3 scale)
    {
        this.text = text;
        this.outline = outline;
        this.mainColor = mainColor;
        this.font = font;
        this.rotation = rotation;
        this.position = position;
        this.scale = scale;
    }

    [Serializable]
    public class ResultJSON
    {
        public string pathToJSON;
        public string pathToResultImage;
    }
}