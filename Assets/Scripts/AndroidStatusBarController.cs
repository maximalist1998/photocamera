﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidStatusBarController : MonoBehaviour
{
    // Start is called before the first frame update
   #if UNITY_ANDROID &&!UNITY_EDITOR
    void Start()
    {
        ApplicationChrome.statusBarState = ApplicationChrome.States.TranslucentOverContent;
        ApplicationChrome.navigationBarColor = 0x00000000;
    }
#endif

}
