﻿using UnityEngine;
using UniUtils;

public class CamerasController : Singleton<CamerasController>
{
    #region > Editor Fields

    [Header("Postprocessing Cameras")]
    [SerializeField]
    private Camera imageGifCamera;
    [SerializeField]
    private Camera mainCamera;
    
    [Header("FaceMasks Cameras")]
    [SerializeField]
    private Camera videoCamera;
    [SerializeField]
    private Camera objectsCamera;
    
    [Header("Masks Builder Cameras")]
    [SerializeField]
    private Camera maskBuilderCam;

    #endregion > Editor Fields
    
    #region > MonoBehaviour Callbacks

    private void Start()
    {
        //TODO change in future
        StartFaceMasksCameras();
    }
    
    #endregion > MonoBehaviour Callbacks

    public void StartPostProcessingCameras()
    {
        StopAllCameras();
        
        imageGifCamera.gameObject.SetActive(true);
        mainCamera.gameObject.SetActive(true);
    }
    
    public void StartFaceMasksCameras()
    {
        StopAllCameras();
        
        videoCamera.gameObject.SetActive(true);
        objectsCamera.gameObject.SetActive(true);
    }
    
    public void StartMaskBuilderCameras()
    {
        StopAllCameras();
        
        maskBuilderCam.gameObject.SetActive(true);
        objectsCamera.gameObject.SetActive(true);
    }

    private void StopAllCameras()
    {
        imageGifCamera.gameObject.SetActive(false);
        mainCamera.gameObject.SetActive(false);
        
        videoCamera.gameObject.SetActive(false);
        objectsCamera.gameObject.SetActive(false);
        
        maskBuilderCam.gameObject.SetActive(false);
    }
}