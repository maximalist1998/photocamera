﻿using System;
using System.Collections;
using UnityEngine;
using UniUtils;

namespace Executor
{
    public class CoroutineExecutor : Singleton<CoroutineExecutor>, IExecutor
    {
        public Coroutine Execute(IEnumerator routine)
        {
            return StartCoroutine(routine);
        }
        
        public void Stop(Coroutine coroutine)
        {
            StopCoroutine(coroutine);
        }

        public Coroutine Execute(IEnumerator routine, Action<object> onComplete, object obj)
        {
            return StartCoroutine(ExecuteImpl(routine, onComplete, obj));
        }

        private IEnumerator ExecuteImpl(IEnumerator routine, Action<object> onComplete, object obj)
        {
            yield return StartCoroutine(routine);
            
            if(onComplete != null)
                onComplete.Invoke(obj);
        }
    }
}