﻿using System;
using System.Collections;
using UnityEngine;

namespace Executor
{
    public interface IExecutor
    {
        Coroutine Execute(IEnumerator routine);
        Coroutine Execute(IEnumerator routine, Action<object> onComplete, object obj);
    }
}